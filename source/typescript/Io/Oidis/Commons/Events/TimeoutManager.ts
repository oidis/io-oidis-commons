/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "../Enums/Events/EventType.js";
import { ExceptionsManager } from "../Exceptions/ExceptionsManager.js";
import { IEventsManager } from "../Interfaces/IEventsManager.js";
import { ITimeoutHandler } from "../Interfaces/ITimeoutHandler.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { EventsManager } from "./EventsManager.js";

/**
 * TimeoutManager class provides handling of setTimeout methods without window timeout errors.
 */
export class TimeoutManager extends BaseObject {
    private register : ITimeoutHandler[];
    private index : number;
    private size : number;
    private readonly id : string;
    private handle : any;

    /**
     * @param {number} [$time=1000] Specify time in milliseconds for which should be execution paused.
     * @returns {void}
     */
    public static async Sleep($time : number = 1000) : Promise<void> {
        return new Promise<void>(($resolve : any) : void => {
            setTimeout(() : void => {
                $resolve();
            }, $time);
        });
    }

    constructor() {
        super();
        this.register = [];
        this.index = -1;
        this.size = 0;
        this.id = this.getUID();
        this.handle = null;
    }

    /**
     * @returns {string} Returns manager's thread identification hash suitable for hook at manager's events.
     */
    public getId() : string {
        return this.id;
    }

    /**
     * @returns {number} Returns count of the registered handlers.
     */
    public Length() : number {
        return this.size;
    }

    /**
     * @param {ITimeoutHandler} $handler Register timeout handler.
     * @returns {void}
     */
    public Add($handler : ITimeoutHandler) : void {
        this.register[this.size] = $handler;
        this.size++;
    }

    /**
     * @returns {string} Returns manager's thread identification hash suitable for hook at manager's events.
     */
    public Execute() : string {
        EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), EventType.ON_START);
        EventsManager.getInstanceSingleton().FireEvent(this.id, EventType.ON_START);
        this.getNextFunction();
        return this.id;
    }

    /**
     * Clear registered handlers and kill execution.
     * @returns {void}
     */
    public Clear() : void {
        this.register = [];
        this.index = -1;
        this.size = 0;
        if (this.handle !== null) {
            clearTimeout(this.handle);
            this.handle = null;
        }
    }

    private getNextFunction() : void {
        const events : IEventsManager = EventsManager.getInstanceSingleton();
        this.handle = setTimeout(() : void => {
            try {
                const nextIndex : number = this.index++;
                if (nextIndex < this.size) {
                    this.getNextFunction();
                    if (ObjectValidator.IsSet(this.register[nextIndex])) {
                        this.register[nextIndex](nextIndex);
                    }
                } else {
                    this.handle = null;
                    this.Clear();
                    events.FireEvent(this.getClassName(), EventType.ON_COMPLETE);
                    events.FireEvent(this.getId(), EventType.ON_COMPLETE);
                }
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        }, 0);
    }
}
