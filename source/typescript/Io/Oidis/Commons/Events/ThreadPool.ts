/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IEventArgs } from "../Interfaces/IEventArgs.js";
import { IEventsHandler } from "../Interfaces/IEventsHandler.js";
import { IEventsManager } from "../Interfaces/IEventsManager.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";

/**
 * ThreadPool class provides asynchronous handling of custom events group.
 */
export class ThreadPool extends BaseObject {
    private static threadId : number;

    /**
     * @param {string} $owner Specify thread owner.
     * @param {string} $type Specify type of thread.
     * @returns {boolean} Returns true, if specified thread is running, otherwise false.
     */
    public static IsRunning($owner : string, $type : string) : boolean {
        return ThreadPool.getEvents().Exists($owner, $type);
    }

    /**
     * @param {string} $owner Specify thread owner.
     * @param {string} $type Specify type of thread.
     * @param {IEventsHandler} [$handler] Function suitable for handling of the thread.
     * @param {IEventArgs} [$args] Specify initial thread args.
     * @returns {void}
     */
    public static AddThread($owner : string, $type : string, $handler? : IEventsHandler,
                            $args? : IEventArgs) : void {
        ThreadPool.getEvents().setEvent($owner, $type, $handler, $args);
    }

    /**
     * @param {string} $owner Specify thread owner.
     * @param {string} $type Specify type of thread.
     * @param {IEventArgs} $args Specify current thread args.
     * @returns {void}
     */
    public static setThreadArgs($owner : string, $type : string, $args : IEventArgs) : void {
        ThreadPool.getEvents().setEventArgs($owner, $type, $args);
    }

    /**
     * @param {string} $owner Specify thread owner.
     * @param {string} $type Specify type of thread, which should be removed.
     * @returns {void}
     */
    public static RemoveThread($owner : string, $type : string) : void {
        ThreadPool.getEvents().Clear($owner, $type);
    }

    /**
     * Fire execution of all registered threads.
     * @returns {void}
     */
    public static Execute() : void {
        const events : IEventsManager = ThreadPool.getEvents();
        if (!events.getAll().IsEmpty()) {
            ThreadPool.threadId = events.FireAsynchronousMethod(() : void => {
                const owners : any[] = events.getAll().getKeys();
                let ownerIndex : number;
                let typeIndex : number;
                for (ownerIndex = 0; ownerIndex < owners.length; ownerIndex++) {
                    const types : any[] = events.getAll().getItem(owners[ownerIndex]).getKeys();
                    for (typeIndex = 0; typeIndex < types.length; typeIndex++) {
                        events.FireEvent(owners[ownerIndex], types[typeIndex]);
                    }
                }
                ThreadPool.Execute();
            }, 5);
        }
    }

    /**
     * Stop execution of all registered threads.
     * @returns {void}
     */
    public static Clear() : void {
        if (!ObjectValidator.IsEmptyOrNull(ThreadPool.threadId)) {
            clearTimeout(ThreadPool.threadId);
        }
        ThreadPool.getEvents().Clear();
    }

    private static getEvents() : IEventsManager {
        const instance : IEventsManager = new globalThis.Io.Oidis.Commons.Events.EventsManager();
        this.getEvents = () : IEventsManager => {
            return instance;
        };
        return instance;
    }
}

globalThis.Io.Oidis.Commons.Events.ThreadPool = ThreadPool;
