/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BuilderConnector } from "../Connectors/BuilderConnector.js";
import { BrowserType } from "../Enums/BrowserType.js";
import { EventType } from "../Enums/Events/EventType.js";
import { GeneralEventOwner } from "../Enums/Events/GeneralEventOwner.js";
import { HttpRequestConstants } from "../Enums/HttpRequestConstants.js";
import { LogSeverity } from "../Enums/LogSeverity.js";
import { ExceptionsManager } from "../Exceptions/ExceptionsManager.js";
import { FatalError } from "../Exceptions/Type/FatalError.js";
import { ResolverFatalException } from "../Exceptions/Type/ResolverFatalException.js";
import { HttpRequestParser } from "../HttpProcessor/HttpRequestParser.js";
import { IEventArgs } from "../Interfaces/IEventArgs.js";
import { IEventsHandler } from "../Interfaces/IEventsHandler.js";
import { IEventsManager } from "../Interfaces/IEventsManager.js";
import { Loader } from "../Loader.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { Convert } from "../Utils/Convert.js";
import { LogIt } from "../Utils/LogIt.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { Reflection } from "../Utils/Reflection.js";
import { StringUtils } from "../Utils/StringUtils.js";
import { AsyncRequestEventArgs } from "./Args/AsyncRequestEventArgs.js";
import { ErrorEventArgs } from "./Args/ErrorEventArgs.js";
import { EventArgs } from "./Args/EventArgs.js";
import { HttpRequestEventArgs } from "./Args/HttpRequestEventArgs.js";
import { MessageEventArgs } from "./Args/MessageEventArgs.js";

/**
 * EventsManager class provides handling of native and custom events.
 */
export class EventsManager extends BaseObject implements IEventsManager {
    private readonly eventsList : ArrayList<ArrayList<ArrayList<IEventsHandler>>>;
    private argsList : ArrayList<ArrayList<IEventArgs>>;
    private threadsRegister : ArrayList<number>;

    /**
     * @returns {IEventsManager} Returns singleton of events manager.
     */
    public static getInstanceSingleton() : IEventsManager {
        return Loader.getInstance().getHttpResolver().getEvents();
    }

    constructor() {
        super();

        this.eventsList = new ArrayList<ArrayList<ArrayList<IEventsHandler>>>();
        this.argsList = new ArrayList<ArrayList<IEventArgs>>();
        this.threadsRegister = new ArrayList<number>();
    }

    /**
     * Subscribe all global events managed by current instance
     * @returns {void}
     */
    public Subscribe() : void {
        this.setEvent(GeneralEventOwner.WINDOW, EventType.ON_ERROR,
            ($eventArgs : ErrorEventArgs) : void => {
                const exception : FatalError = new FatalError(
                    $eventArgs.Message() + StringUtils.NewLine() + "For more info see the console log.");
                exception.File($eventArgs.Exception().File());
                exception.Line($eventArgs.Exception().Line());
                ExceptionsManager.Throw(this.getClassName(), exception);
            });

        window.onerror = ($message : string, $filename : string, $lineno : number, $colno : number,
                          $error : Error) : boolean => {
            try {
                if (ExceptionsManager.IsNativeException($message)) {
                    let args : ErrorEventArgs;
                    if (!ObjectValidator.IsEmptyOrNull($error)) {
                        args = new ErrorEventArgs($error);
                    } else {
                        args = new ErrorEventArgs($message);
                        args.Exception().File($filename);
                        args.Exception().Line($lineno);
                    }
                    if (!StringUtils.Contains(args.Message(), "ResizeObserver loop limit exceeded") &&
                        !StringUtils.PatternMatched("*'#/*' is not a valid selector.", args.Message())) {
                        this.FireEvent(GeneralEventOwner.BODY, EventType.ON_ERROR, args, false);
                        this.FireEvent(GeneralEventOwner.WINDOW, EventType.ON_ERROR, args, false);
                    } else {
                        LogIt.Info("Detected known global error:\n" + args.Message(), LogSeverity.LOW);
                    }
                }
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
            return true;
        };
        window.onunhandledrejection = ($event : PromiseRejectionEvent) : void => {
            ExceptionsManager.HandleException(new Error($event.reason));
            $event.preventDefault();
        };

        window.onload = () : any => {
            try {
                this.FireEvent(GeneralEventOwner.WINDOW, EventType.ON_LOAD);
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        };

        window.onbeforeunload = ($eventArgs : BeforeUnloadEvent) : any => {
            try {
                const args : EventArgs = new EventArgs();
                args.NativeEventArgs($eventArgs);
                this.FireEvent(GeneralEventOwner.BODY, EventType.BEFORE_REFRESH, args, false);
                this.FireEvent(GeneralEventOwner.WINDOW, EventType.BEFORE_REFRESH, args, false);
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        };

        const hashHandler : any = () : any => {
            try {
                this.FireEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST,
                    new HttpRequestEventArgs(window.location.hash));
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        };

        const request : HttpRequestParser = Loader.getInstance().getHttpManager().getRequest();
        if (ObjectValidator.IsSet(window.onhashchange) && (
            request.getBrowserType() !== BrowserType.INTERNET_EXPLORER ||
            request.getBrowserType() === BrowserType.INTERNET_EXPLORER &&
            request.getBrowserVersion() > 7)) {
            window.onhashchange = hashHandler;
        } else {
            if (ObjectValidator.IsSet(window.onpopstate)) {
                window.onpopstate = hashHandler;
            } else {
                setTimeout(() : void => {
                    globalThis.Io.Oidis.Commons.Events.ThreadPool
                        .AddThread(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST, hashHandler);
                    globalThis.Io.Oidis.Commons.Events.ThreadPool.Execute();
                });
            }
        }

        if ("postMessage" in window) {
            const eventHandler : ($eventArgs : MessageEvent) => any = ($eventArgs : MessageEvent) : any => {
                try {
                    const args : MessageEventArgs = new MessageEventArgs();
                    if (ObjectValidator.IsSet($eventArgs)) {
                        args.NativeEventArgs($eventArgs);
                    } else {
                        args.NativeEventArgs(<any>event);
                    }

                    this.FireEvent(GeneralEventOwner.BODY, EventType.ON_MESSAGE, args, false);
                    this.FireEvent(GeneralEventOwner.WINDOW, EventType.ON_MESSAGE, args, false);
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };
            if (ObjectValidator.IsSet(window.addEventListener) && window instanceof EventTarget) {
                window.addEventListener("message", eventHandler);
            } else if (ObjectValidator.IsSet((<any>window).attachEvent)) {
                (<any>window).attachEvent("onmessage", eventHandler); // fallback under IE9+
            } else {
                window.onmessage = eventHandler;
            }
        }

        const onResolveRequestHandler : IEventsHandler = ($eventArgs? : HttpRequestEventArgs | AsyncRequestEventArgs) : void => {
            try {
                Loader.getInstance().getHttpResolver().ResolveRequest($eventArgs);
            } catch (ex) {
                try {
                    ExceptionsManager.Throw(this.getClassName(), new ResolverFatalException());
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            }
        };
        this.setEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST, onResolveRequestHandler);
        this.setEvent(GeneralEventOwner.WINDOW, EventType.ON_ASYNC_REQUEST, onResolveRequestHandler);

        if (request.IsPlugin() && request.getUrlArgs().KeyExists(HttpRequestConstants.DESIGNER)) {
            this.setEvent(GeneralEventOwner.BODY, EventType.ON_LOAD, () : void => {
                BuilderConnector.Connect().getEvents().OnBuildComplete(() : void => {
                    Loader.getInstance().getHttpManager().Reload();
                });
            });
        }
    }

    /**
     * @param {string} $owner Validate events subscribed to this owner value.
     * @param {string} $type Validate this type of subevents.
     * @returns {boolean} Returns true, if $owner and $type has been registered, otherwise false.
     */
    public Exists($owner : string, $type : string) : boolean {
        return this.eventsList.KeyExists($owner) && this.eventsList.getItem($owner).KeyExists($type);
    }

    /**
     * @returns {ArrayList<ArrayList<ArrayList<IEventsHandler>>>} Returns list of all registered events.
     */
    public getAll() : ArrayList<ArrayList<ArrayList<IEventsHandler>>> {
        return this.eventsList;
    }

    /**
     * @param {string} $owner Subscribed event to this owner value.
     * @param {string} $type Subscribe handler to this type of event.
     * @param {IEventsHandler} [$handler] Function suitable for handling of the event.
     * @param {IEventArgs} [$args] Set initial event args.
     * @returns {void}
     */
    public setEvent($owner : string, $type : string, $handler? : IEventsHandler, $args? : IEventArgs) : void {
        if (!this.eventsList.KeyExists($owner)) {
            this.eventsList.Add(new ArrayList<ArrayList<IEventsHandler>>(), $owner);
        }
        const owners : ArrayList<ArrayList<IEventsHandler>> = this.eventsList.getItem($owner);
        if (!owners.KeyExists($type)) {
            owners.Add(new ArrayList<IEventsHandler>(), $type);
        }
        const handlers : ArrayList<IEventsHandler> = owners.getItem($type);
        if (ObjectValidator.IsSet($handler)) {
            handlers.Add($handler, StringUtils.getSha1(Convert.FunctionToString($handler)));
        }

        if (!ObjectValidator.IsSet($args)) {
            this.setEventArgs($owner, $type, new EventArgs());
        } else {
            this.setEventArgs($owner, $type, $args);
        }
    }

    /**
     * @param {string} $owner Event args owner name.
     * @param {string} $type Event args type of.
     * @param {IEventArgs} $args Set current event args.
     * @returns {void}
     */
    public setEventArgs($owner : string, $type : string, $args : IEventArgs) : void {
        if (this.Exists($owner, $type)) {
            if (ObjectValidator.IsEmptyOrNull($args.Owner())) {
                $args.Owner($owner);
            }
            $args.Type($type);
            if (!this.getArgsList().KeyExists($owner)) {
                this.argsList.Add(new ArrayList<IEventArgs>(), $owner);
            }
            this.argsList.getItem($owner).Add($args, $type);
        }
    }

    public FireEvent($owner : string, $type : string, $args? : IEventArgs) : void;

    public FireEvent($owner : string, $type : string, $async? : boolean) : void;

    public FireEvent($owner : string, $type : string, $args? : IEventArgs, $async? : boolean) : void;

    /**
     * @param {string} $owner Specify fired event owner.
     * @param {string} $type Specify fired type of event.
     * @param {IEventArgs} [$args] Specify event args for current event process.
     * @param {boolean} [$async=true] Specify, if event handlers can be execute asynchronously.
     * @returns {void}
     */
    public FireEvent($owner : string, $type : string, $args? : any, $async? : boolean) : void {
        if (this.Exists($owner, $type)) {
            const reflection : Reflection = Reflection.getInstance();
            if (!ObjectValidator.IsSet($args) ||
                (ObjectValidator.IsBoolean($args) && !ObjectValidator.IsBoolean($async))) {
                if (ObjectValidator.IsBoolean($args) && !ObjectValidator.IsBoolean($async)) {
                    $async = $args;
                }
                if (this.getArgsList().KeyExists($owner)) {
                    const args : IEventArgs = this.getArgsList().getItem($owner).getItem($type);
                    if (!ObjectValidator.IsEmptyOrNull(args)) {
                        $args = args;
                    }
                }
            } else if (reflection.Implements($args, IEventArgs)) {
                this.setEventArgs($owner, $type, $args);
            }
            if (!ObjectValidator.IsBoolean($async)) {
                $async = true;
            }

            const handlers : ArrayList<IEventsHandler> = this.eventsList.getItem($owner).getItem($type);
            try {
                handlers.foreach(($handler : IEventsHandler) : void => {
                    if ($async) {
                        setTimeout(() : void => {
                            try {
                                $handler($args);
                            } catch (ex) {
                                ExceptionsManager.HandleException(ex);
                            }
                        }, 0);
                    } else {
                        $handler($args);
                    }
                });
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        }
    }

    public FireAsynchronousMethod($handler : () => void, $clearBefore? : boolean) : number;

    public FireAsynchronousMethod($handler : () => void, $waitForMilliseconds? : number) : number;

    public FireAsynchronousMethod($handler : () => void, $clearBefore? : boolean, $waitForMilliseconds? : number) : number;

    /**
     * @param {Function} $handler Function suitable for handling of the event.
     * @param {boolean} [$clearBefore=true] Clean up currently running thread with same handler.
     * @param {number} [$waitForMilliseconds] Specify wait time before execution of the handler in milliseconds.
     * @returns {number} Returns thread number allocated for asynchronous execution.
     */
    public FireAsynchronousMethod($handler : () => void, $clearBefore? : any, $waitForMilliseconds? : number) : number {
        if (ObjectValidator.IsInteger($clearBefore)) {
            $waitForMilliseconds = $clearBefore;
            $clearBefore = true;
        } else if (!ObjectValidator.IsSet($clearBefore)) {
            $clearBefore = true;
        }

        let handlerHash : string;
        if ($clearBefore) {
            handlerHash = StringUtils.getSha1(Convert.FunctionToString($handler));
            if (this.getThreadsRegister().KeyExists(handlerHash)) {
                this.Clear(this.threadsRegister.getItem(handlerHash));
                this.threadsRegister.RemoveAt(this.threadsRegister.getKeys().indexOf(handlerHash));
            }
        }
        if (!ObjectValidator.IsSet($waitForMilliseconds)) {
            $waitForMilliseconds = 0;
        }
        const threadNumber : any = setTimeout(() : void => {
            try {
                $handler();
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        }, $waitForMilliseconds);

        if ($clearBefore) {
            this.threadsRegister.Add(threadNumber, handlerHash);
        }
        return threadNumber;
    }

    /**
     * @param {string} $owner Event owner name of subscribed handler.
     * @param {string} $type Event type of subscribed handler.
     * @param {IEventsHandler} $handler Handler, which should be removed.
     * @returns {void}
     */
    public RemoveHandler($owner : string, $type : string, $handler : IEventsHandler) : void {
        if (this.Exists($owner, $type)) {
            const handlers : ArrayList<IEventsHandler> = this.eventsList.getItem($owner).getItem($type);
            const handlerName : string = StringUtils.getSha1(Convert.FunctionToString($handler));
            if (handlers.KeyExists(handlerName)) {
                handlers.RemoveAt(handlers.getKeys().indexOf(handlerName));
            }
        }
    }

    /**
     * Clear all events, if owner and type has not been specified.
     * Clear subset of events subscribed to owner name, if owner has been specified.
     * Clear subset of events, which belong to owner and event type, if owner and type has been specified.
     * @param {string|number} [$owner] Specify desired event or thread owner.
     * @param {string} [$type] Specify desired type of event.
     * @returns {void}
     */
    public Clear($owner? : string | number, $type? : string) : void {
        if (ObjectValidator.IsSet($owner)) {
            if (ObjectValidator.IsString($owner)) {
                if (this.eventsList.KeyExists($owner)) {
                    if (ObjectValidator.IsSet($type)) {
                        const owners : ArrayList<any> = this.eventsList.getItem(<string>$owner);
                        owners.RemoveAt(owners.getKeys().indexOf($type));
                        if (owners.IsEmpty()) {
                            this.eventsList.RemoveAt(this.eventsList.getKeys().indexOf($owner));
                        }
                    } else {
                        this.eventsList.RemoveAt(this.eventsList.getKeys().indexOf($owner));
                    }
                }
            } else if (ObjectValidator.IsInteger($owner)) {
                clearTimeout(<number>$owner);
            }
        } else {
            this.eventsList.Clear();
        }
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        let output : string = $prefix + "Registered events list:" + StringUtils.NewLine($htmlTag);
        this.eventsList.foreach(($value : ArrayList<any>, $owner? : string) : void => {
            $value.foreach(($value : ArrayList<any>, $type? : string) : void => {
                output += $prefix + StringUtils.Tab(1, $htmlTag) + "[\"" + $owner + "\"][\"" + $type + "\"]" +
                    " hooked handlers count: " + $value.Length() + StringUtils.NewLine($htmlTag);
            });
        });
        return output;
    }

    public toString() : string {
        return this.ToString();
    }

    protected excludeSerializationData() : string[] {
        return ["argsList", "threadsRegister"];
    }

    protected getArgsList() : ArrayList<ArrayList<IEventArgs>> {
        if (!ObjectValidator.IsSet(this.argsList)) {
            this.argsList = new ArrayList<ArrayList<IEventArgs>>();
            this.getArgsList = () : ArrayList<ArrayList<IEventArgs>> => {
                return this.argsList;
            };
        }
        return this.argsList;
    }

    protected getThreadsRegister() : ArrayList<number> {
        if (!ObjectValidator.IsSet(this.threadsRegister)) {
            this.threadsRegister = new ArrayList<number>();
            this.getThreadsRegister = () : ArrayList<number> => {
                return this.threadsRegister;
            };
        }
        return this.threadsRegister;
    }
}

globalThis.Io.Oidis.Commons.Events.EventsManager = EventsManager;
