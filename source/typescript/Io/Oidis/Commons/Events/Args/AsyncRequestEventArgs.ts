/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "../../Primitives/ArrayList.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { HttpRequestEventArgs } from "./HttpRequestEventArgs.js";

/**
 * AsyncRequestEventArgs class provides args connected with asynchronous request events.
 */
export class AsyncRequestEventArgs extends HttpRequestEventArgs {
    private result : any;

    /**
     * @param {string} $url Set url, which should be asynchronously resolved.
     * @param {ArrayList<any>} [$POST] Specify data, which should be passed to request resolver.
     */
    constructor($url : string, $POST? : ArrayList<any>) {
        super($url, $POST);
    }

    /**
     * @param {object} $value Set result of asynchronous request.
     * @returns {object} Returns result of asynchronous request.
     */
    public Result($value? : any) : any {
        if (ObjectValidator.IsSet($value)) {
            this.result = $value;
        }
        return this.result;
    }
}
