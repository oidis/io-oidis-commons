/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Exception } from "../../Exceptions/Type/Exception.js";
import { Convert } from "../../Utils/Convert.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { Property } from "../../Utils/Property.js";
import { StringUtils } from "../../Utils/StringUtils.js";
import { EventArgs } from "./EventArgs.js";

/**
 * ErrorEventArgs class provides args connected with asynchronous error.
 */
export class ErrorEventArgs extends EventArgs {
    private message : string;
    private exception : Exception;

    /**
     * @param {string|Exception|Error} $message Specify error message.
     */
    constructor($message : string | Exception | Error) {
        super();

        if (ObjectValidator.IsString($message)) {
            this.message = <string>$message;
            this.exception = new Exception(this.message);
        } else {
            this.Exception(<Exception>$message);
        }
    }

    /**
     * @param {string} [$value] Set error message.
     * @returns {string} Returns error message.
     */
    public Message($value? : string) : string {
        return this.message = Property.String(this.message, $value);
    }

    /**
     * @param {Exception|Error} [$value] Set exception object data.
     * @returns {Exception} Returns error exception object.
     */
    public Exception($value? : Exception | Error) : Exception {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            if (ObjectValidator.IsObject($value) && ObjectValidator.IsSet((<Exception>$value).IsMemberOf) &&
                (<Exception>$value).IsMemberOf(Exception)) {
                this.exception = <Exception>$value;
            } else {
                this.exception = new Exception((<Error>$value).message);
                if (ObjectValidator.IsSet((<any>$value).stack)) {
                    this.exception.Stack((<any>$value).stack);
                }
            }
            this.message = this.exception.Message();
        }
        if (!ObjectValidator.IsSet(this.exception)) {
            this.exception = new Exception();
        }
        return this.exception;
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        let output : string = "";
        output += $prefix + this.getClassName() + StringUtils.NewLine($htmlTag);
        output += $prefix + "[\"Message\"] " + this.Message() + StringUtils.NewLine($htmlTag);
        output += $prefix + "[\"Owner\"] ";
        if (ObjectValidator.IsObject(this.Owner())) {
            output += this.Owner().getClassName() + StringUtils.NewLine($htmlTag);
        } else {
            output += Convert.ObjectToString(this.Owner()) + StringUtils.NewLine($htmlTag);
        }
        return output;
    }
}
