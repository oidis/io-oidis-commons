/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "../../Enums/HttpStatusType.js";
import { ArrayList } from "../../Primitives/ArrayList.js";
import { Convert } from "../../Utils/Convert.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { Property } from "../../Utils/Property.js";
import { StringUtils } from "../../Utils/StringUtils.js";
import { EventArgs } from "./EventArgs.js";

/**
 * HttpRequestEventArgs class provides args connected with http request events.
 */
export class HttpRequestEventArgs extends EventArgs {
    private readonly url : string;
    private getData : ArrayList<string>;
    private postData : ArrayList<any>;
    private status : HttpStatusType;

    /**
     * @param {string} $url Set url, which should be synchronously resolved.
     * @param {ArrayList<any>} [$POST] Specify data, which should be passed to request resolver.
     */
    constructor($url : string, $POST? : ArrayList<any>) {
        super();
        this.url = StringUtils.Remove($url, "/#", "#");
        this.getData = new ArrayList<string>();
        this.postData = new ArrayList<any>();
        this.POST($POST);
        this.status = HttpStatusType.SUCCESS;
    }

    /**
     * @returns {string} Returns url of resolver, which should be or has been execute.
     */
    public Url() : string {
        return this.url;
    }

    /**
     * @param {HttpStatusType} $value Set async load result status code.
     * @returns {HttpStatusType} Returns status code of resolver result.
     */
    public Status($value? : HttpStatusType) : HttpStatusType {
        return this.status = Property.Integer(this.status, $value);
    }

    /**
     * @param {ArrayList<string>} [$GET] Set http get data.
     * @returns {ArrayList<string>} Returns http get data provided by requester.
     */
    public GET($GET? : ArrayList<string>) : ArrayList<string> {
        if (!ObjectValidator.IsEmptyOrNull($GET)) {
            this.getData = $GET;
        }
        return this.getData;
    }

    /**
     * @param {ArrayList<any>} [$POST] Set post data.
     * @returns {ArrayList<any>} Returns post data provided by requester.
     */
    public POST($POST? : ArrayList<any>) : ArrayList<any> {
        if (!ObjectValidator.IsEmptyOrNull($POST)) {
            this.postData = $POST;
        }
        return this.postData;
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        let output : string = "";
        output += $prefix + this.getClassName() + StringUtils.NewLine($htmlTag);
        output += $prefix + "[\"Url\"] " + this.Url() + StringUtils.NewLine($htmlTag);
        output += $prefix + "[\"Owner\"] ";
        if (ObjectValidator.IsObject(this.Owner())) {
            output += this.Owner().getClassName() + StringUtils.NewLine($htmlTag);
        } else {
            output += Convert.ObjectToString(this.Owner()) + StringUtils.NewLine($htmlTag);
        }
        output += $prefix + "[\"Status\"] " + this.Status() + StringUtils.NewLine($htmlTag);
        output += $prefix + "[\"GET\"] " + StringUtils.NewLine($htmlTag) +
            Convert.ObjectToString(this.getData, StringUtils.Tab(1, $htmlTag), $htmlTag) + StringUtils.NewLine($htmlTag);
        output += $prefix + "[\"POST\"] " + StringUtils.NewLine($htmlTag) +
            Convert.ObjectToString(this.postData, StringUtils.Tab(1, $htmlTag), $htmlTag);
        return output;
    }
}
