/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "../../Utils/Property.js";
import { EventArgs } from "./EventArgs.js";

/**
 * ProgressEventArgs class provides args connected with progress events.
 */
export class ProgressEventArgs extends EventArgs {
    private rangeStartValue : number;
    private rangeEndValue : number;
    private currentValue : number;

    constructor() {
        super();
        this.rangeStartValue = 0;
        this.rangeEndValue = 100;
        this.currentValue = 0;
    }

    /**
     * @param {number} [$value] Set start value for value change.
     * @returns {number} Returns start number value for value change.
     */
    public RangeStart($value? : number) : number {
        return this.rangeStartValue = Property.Integer(this.rangeStartValue, $value);
    }

    /**
     * @param {number} [$value] Set end value for value change.
     * @returns {number} Returns end number value for value change.
     */
    public RangeEnd($value? : number) : number {
        return this.rangeEndValue = Property.Integer(this.rangeEndValue, $value);
    }

    /**
     * @param {number} [$value] Set current value of change.
     * @returns {number} Returns number value of current change.
     */
    public CurrentValue($value? : number) : number {
        return this.currentValue = Property.Integer(this.currentValue, $value);
    }
}
