/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "./EventArgs.js";

/**
 * MessageEventArgs class provides args connected with post message events.
 */
export class MessageEventArgs extends EventArgs {
    /**
     * @param {MessageEvent} [$eventArgs] Specify native post message event args provided by native onmessage handler
     */
    constructor($eventArgs? : MessageEvent) {
        super();
        this.NativeEventArgs($eventArgs);
    }

    /**
     * @param {MessageEvent} [$value] Set MessageEvent instance.
     * @returns {MessageEvent} Returns MessageEvent args provided by native event handler.
     */
    public NativeEventArgs($value? : MessageEvent) : MessageEvent {
        return <MessageEvent>super.NativeEventArgs($value);
    }
}
