/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject, IContext } from "../../Interfaces/IBaseObject.js";
import { BaseObject } from "../../Primitives/BaseObject.js";
import { LogIt } from "../../Utils/LogIt.js";

export abstract class BaseConverter extends BaseObject implements IConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        LogIt.Warning("Skipping unsupported \"from()\" conversion in " + IConverter.ClassName() + ".");
        return $value;
    }

    public async to($value : any, $context : IContext) : Promise<any> {
        LogIt.Warning("Skipping unsupported \"to()\" conversion in " + IConverter.ClassName() + ".");
        return $value;
    }
}

export interface IConverter extends IBaseObject {
    to($value : any, $context : IContext) : Promise<any>;

    from($value : any, $context : IContext) : Promise<any>;
}

// generated-code-start
export const IConverter = globalThis.RegisterInterface(["to", "from"], <any>IBaseObject);
// generated-code-end
