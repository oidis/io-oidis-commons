/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IContext } from "../../Interfaces/IBaseObject.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { BaseConverter } from "./BaseConverter.js";

export abstract class BaseListToRefConverter extends BaseConverter {
    public async to($value : any, $context : IContext) : Promise<string[]> {
        const ids : string[] = [];
        const parentDocument : any = $context.parent.getDocument();
        const contextSymbol : string = $context.property.symbol;
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            for await(const model of $value) {
                if (ObjectValidator.IsObject(model)) {
                    if (ObjectValidator.IsEmptyOrNull(parentDocument[contextSymbol])) {
                        parentDocument[contextSymbol] = [];
                    }
                    if (parentDocument[contextSymbol].findIndex(($docId) => $docId === model.Id) === -1) {
                        parentDocument[contextSymbol].push(model.Id);
                    }
                    await model.Save((<any>$context.parent).getSaveOptions());
                    ids.push(model.Id);
                } else {
                    ids.push(model);
                }
            }
        }
        return ids;
    }
}
