/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IContext } from "../../Interfaces/IBaseObject.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { IdentityConverter } from "./IdentityConverter.js";

export class ArrayConverter extends IdentityConverter {

    public async from($value : any, $context : IContext) : Promise<any> {
        const array : string[] = [];
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            for (const item of $value) {
                array.push(item);
            }
        }
        return array;
    }
}
