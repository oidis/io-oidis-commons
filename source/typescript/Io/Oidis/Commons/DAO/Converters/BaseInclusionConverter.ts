/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IContext } from "../../Interfaces/IBaseObject.js";
import { BaseModel } from "../../Primitives/BaseModel.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { BaseConverter } from "./BaseConverter.js";

export abstract class BaseInclusionConverter extends BaseConverter {
    private readonly model : any;

    constructor($modelClass? : any) {
        super();
        if (ObjectValidator.IsEmptyOrNull($modelClass)) {
            $modelClass = BaseModel;
        }
        this.model = $modelClass;
    }

    public async from($value : any, $context : IContext) : Promise<any> {
        const instance : any = this.getInclusionInstance();
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            if ($context.dataTranslation) {
                instance.document = $value;
            } else {
                instance.document = Object.assign(instance.document, instance.context.DocumentToObject($value));
            }
            await instance.fromModel({dataTranslation: $context.dataTranslation});
        }
        return instance;
    }

    public async to($value : any, $context : IContext) : Promise<any> {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            await $value.toModel();
            await $value.context.Validate(); // call validate manually to rise potential errors directly
            return $value.context.DocumentToObject();
        }
        return null;
    }

    protected getInclusionInstance() : BaseModel<any> {
        return new this.model();
    }
}
