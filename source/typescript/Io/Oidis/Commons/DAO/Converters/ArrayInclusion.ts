/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IContext } from "../../Interfaces/IBaseObject.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { BaseInclusionConverter } from "./BaseInclusionConverter.js";

export class ArrayInclusion extends BaseInclusionConverter {

    public async from($value : any, $context : IContext) : Promise<any> {
        const items : any[] = [];
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            for await(const item of (<any>$context.parent).getContext().DocumentToObject($value)) {
                const instance : any = this.getInclusionInstance();
                instance.document = Object.assign(instance.document, item);
                await instance.fromModel();

                items.push(instance);
            }
        }
        return items;
    }

    public async to($value : any, $context : IContext) : Promise<any> {
        const objects : any[] = [];
        for await(const model of $value) {
            await model.toModel();
            objects.push(model.context.DocumentToObject());
        }
        return objects;
    }
}
