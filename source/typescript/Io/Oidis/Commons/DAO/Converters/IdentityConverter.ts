/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IContext } from "../../Interfaces/IBaseObject.js";
import { BaseConverter } from "./BaseConverter.js";

export class IdentityConverter extends BaseConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        return $value;
    }

    public async to($value : any, $context : IContext) : Promise<any> {
        return $value;
    }
}
