/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IContext } from "../../Interfaces/IBaseObject.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { BaseConverter } from "./BaseConverter.js";

export abstract class BaseToRefConverter extends BaseConverter {
    public async to($value : any, $context : IContext) : Promise<string> {
        if (!ObjectValidator.IsEmptyOrNull($value) && ObjectValidator.IsObject($value)) {
            if (ObjectValidator.IsEmptyOrNull($context.parent.getDocument()[$context.property.symbol])) {
                $context.parent.getDocument()[$context.property.symbol] = $value.Id;
            }
            await $value.Save((<any>$context.parent).getSaveOptions());
            return $value.Id;
        } else {
            return $value;
        }
    }
}
