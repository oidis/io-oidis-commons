/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IModel, IObjectProperty } from "../Interfaces/IBaseObject.js";
import { IDbConfiguration } from "../Interfaces/IProject.js";
import { Loader } from "../Loader.js";
import { BaseModel, IModelAggregateOptions, IModelFindOptions } from "../Primitives/BaseModel.js";
import { JsonUtils } from "../Utils/JsonUtils.js";
import { LogIt } from "../Utils/LogIt.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { Reflection } from "../Utils/Reflection.js";
import { BaseDbContext, BaseDbDriver } from "./BaseDbContext.js";

export class IndexedDbDriver extends BaseDbDriver {
    private driver : any;
    private onUpgradeHandler : any;
    private isOpen : boolean;

    public static IsSupported() : boolean {
        return "indexedDB" in window;
    }

    constructor() {
        super();
        this.driver = null;
        this.isOpen = false;
        this.onUpgradeHandler = ($db : any) : void => {
            // default handler
        };
    }

    public getModel($className : string) : any {
        const transaction : any = this.driver.transaction($className, "readwrite").objectStore($className);
        transaction.onerror = () : void => {
            throw new Error("Save transaction issue: " + transaction.error);
        };
        return transaction;
    }

    public IsOpen() : boolean {
        return this.isOpen;
    }

    public async Open($config : IDbConfiguration, $autoReconnect : boolean = false) : Promise<boolean> {
        if (!ObjectValidator.IsEmptyOrNull(this.driver)) {
            this.isOpen = true;
            return true;
        }
        return new Promise<boolean>(($resolve : ($status : boolean) => void) : void => {
            try {
                if (ObjectValidator.IsEmptyOrNull($config.dbName)) {
                    $config.dbName = Loader.getInstance().getEnvironmentArgs().getProjectName();
                }
                LogIt.Debug("Opening IndexDB: " + $config.dbName);
                const openRequest : IDBOpenDBRequest = window.indexedDB.open($config.dbName,
                    Loader.getInstance().getEnvironmentArgs().getProjectConfig().build.timestamp);
                openRequest.onerror = (ev) => {
                    LogIt.Error("Open IndexDB error: " + openRequest.error);
                    this.isOpen = false;
                    $resolve(false);
                };
                openRequest.onblocked = () : void => {
                    LogIt.Warning("DB transaction is blocked - waiting for end of unfinished job, maybe in another tab");
                };
                openRequest.onupgradeneeded = (event : any) : void => {
                    const db : any = event.target.result;
                    const transaction : any = event.target.transaction;
                    const reflection : Reflection = Reflection.getInstance();
                    const baseClasses : string[] = [
                        BaseModel.ClassName(), "Io.Oidis.Hub.Gui.Models.BaseModel", "Io.Oidis.Hub.DAO.Models.BaseModel"
                    ];
                    for (const modelClass of reflection.getAllClasses()) {
                        const modelPrototype : any = reflection.getClass(modelClass);
                        if (!ObjectValidator.IsEmptyOrNull(modelPrototype)) {
                            if (reflection.ClassHasInterface(modelPrototype, IModel) && !baseClasses.includes(modelClass)) {
                                const instance : BaseModel = new modelPrototype();
                                if (ObjectValidator.IsSet(instance.getContext) &&
                                    reflection.IsMemberOf(instance.getContext(), IndexedDbContext)) {
                                    let objectStore : any;
                                    if (!db.objectStoreNames.contains(instance.getSymbolicName())) {
                                        objectStore = db.createObjectStore(instance.getSymbolicName(), {
                                            autoIncrement: true,
                                            keyPath      : "_id"
                                        });
                                    } else {
                                        objectStore = transaction.objectStore(instance.getSymbolicName());
                                    }
                                    const props : IObjectProperty[] = instance.Properties;
                                    props.forEach(($prop : IObjectProperty) : void => {
                                        if (!objectStore.indexNames.contains($prop.symbol)) {
                                            objectStore.createIndex($prop.symbol, $prop.symbol, {unique: false});
                                        }
                                    });
                                    if (!this.models.includes(instance)) {
                                        this.models.push(instance);
                                    }
                                }
                            }
                        }
                    }
                    this.onUpgradeHandler(openRequest.result);
                };
                openRequest.onsuccess = (ev) => {
                    this.driver = openRequest.result;
                    this.driver.onversionchange = () : void => {
                        this.Close();
                        LogIt.Warning("Database is outdated please reload context");
                        if ($autoReconnect) {
                            this.Open($config, $autoReconnect);
                        }
                    };
                    this.isOpen = true;
                    $resolve(true);
                };
            } catch (ex) {
                LogIt.Error(ex);
                this.isOpen = false;
                $resolve(false);
            }
        });
    }

    public async Close($force : boolean = false) : Promise<boolean> {
        return new Promise<boolean>(($resolve : ($status : boolean) => void) : void => {
            try {
                if (!ObjectValidator.IsEmptyOrNull(this.driver)) {
                    this.driver.onclose = () : void => {
                        this.driver = null;
                        this.isOpen = false;
                        $resolve(true);
                    };
                    this.driver.close();
                } else {
                    $resolve(true);
                }
            } catch (ex) {
                LogIt.Error(ex);
                this.isOpen = false;
                $resolve(false);
            }
        });
    }

    public getDb() : any {
        return this.driver;
    }

    public setOnUpgradeHandler($handler : any) : void {
        if (!ObjectValidator.IsEmptyOrNull($handler)) {
            this.onUpgradeHandler = $handler;
        }
    }

    public getContextFor($model : any) : IndexedDbContext {
        return new IndexedDbContext(this, $model);
    }
}

export class IndexedDbContext extends BaseDbContext {

    public getDocument() : any {
        this.frontendOnlyAssert();
        return super.getDocument();
    }

    public CreateDocument($className : string, $schema : any, $collection : string) : any {
        return {
            _id         : this.getUID(),
            created     : new Date(),
            lastModified: new Date()
        };
    }

    public async FindById<T>($id : string, $options? : IModelFindOptions) : Promise<T> {
        const models : T[] = await this.Find($id, $options);
        if (!ObjectValidator.IsEmptyOrNull(models)) {
            if (models.length > 1) {
                LogIt.Warning("Found more than one document with id: " + $id);
            }
            return models[0];
        }
        return null;
    }

    public async Find<T>($query : any, $options? : IModelFindOptions) : Promise<T[]> {
        this.frontendOnlyAssert();
        $options = JsonUtils.Extend({
            limit : -1,
            offset: 0,
            sort  : "prev" // prevunique, next, nextunique | next is descendant, prev is ascendant
        }, $options);
        const models : any[] = await new Promise<T[]>(($resolve, $reject) : void => {
            try {
                const objectStore : any = this.getDocument();
                let targetStore : any = objectStore;
                const getAllRecords : boolean = ObjectValidator.IsEmptyOrNull($query) || JSON.stringify($query) === "{}";
                let find : any = $query;
                if (!getAllRecords && !ObjectValidator.IsString($query)) {
                    const keys : string[] = Object.keys($query);
                    if (keys.length > 1) {
                        LogIt.Warning("Current implementation of IndexedDB Context Find do not support more that one key! " +
                            "Try to use different approach for search, e.g. Model.Find({}).find({Native JS Array find syntax here ...})");
                    }
                    const key : string = Object.keys($query)[0];
                    find = $query[key];
                    targetStore = objectStore.index(key);
                }

                const request : IDBRequest = getAllRecords ? targetStore.openCursor() : targetStore.openCursor(find, $options.sort);
                const models : T[] = [];
                request.onsuccess = ($event : any) : void => {
                    const cursor = $event.target.result;
                    if (cursor) {
                        const model : T = this.getModelInstance();
                        (<any>model).document = cursor.value;
                        models.push(model);
                        if ($options.limit <= 0 || models.length < $options.limit) {
                            if ($options.offset <= 0) {
                                cursor.continue();
                            } else {
                                cursor.advance($options.offset);
                            }
                        }
                    } else {
                        $resolve(models);
                    }
                };
                request.onerror = () : void => {
                    $reject(new Error("Find error: " + request.error));
                };
            } catch (ex) {
                $reject(ex);
            }
        });
        for await (const model of models) {
            await model.fromModel();
        }
        return models;
    }

    public async Aggregate<T>($chain : any[] | IModelAggregateOptions, $options? : IModelFindOptions) : Promise<any> {
        this.frontendOnlyAssert();

        let queries : any[];
        if (ObjectValidator.IsArray($chain)) {
            queries = <any[]>$chain;
        } else {
            queries = (<IModelAggregateOptions>$chain).filterQuery;
        }
        let result : any = [];
        for await (const query of queries) {
            result = result.concat(await this.Find(query, $options));
        }
        return result;
    }

    public async Save() : Promise<void> {
        this.frontendOnlyAssert();
        return new Promise<void>(($resolve, $reject) : void => {
            try {
                this.Model.LastModified = new Date();
                const request : any = this.getDocument().put(this.Model.getDocument());
                request.onsuccess = ($event : any) : void => {
                    $resolve();
                };
                request.onerror = () : void => {
                    $reject(new Error(request.error));
                };
            } catch (ex) {
                $reject(ex);
            }
        });
    }

    public async Update($property : string) : Promise<void> {
        for await (const property of this.Model.Properties) {
            if (property.name === $property || property.symbol === $property) {
                const value : any = this[property.fieldKey];
                if (ObjectValidator.IsObject(property) && Reflection.getInstance().Implements(value, IModel)) {
                    this.Model.getDocument()[property.symbol] = await property.converter.to(value, {
                        dataTranslation: false,
                        parent         : this.Model,
                        property
                    });
                } else {
                    this.Model.getDocument()[property.symbol] = value;
                }
            }
        }
        return this.Save();
    }

    public async Refresh($options? : IModelFindOptions) : Promise<void> {
        this.frontendOnlyAssert();
        return new Promise<void>(($resolve, $reject) : void => {
            try {
                const objectStore = this.getDocument();
                const request : IDBRequest = objectStore.openCursor(this.Model.Id, "prev");
                request.onsuccess = ($event : any) : void => {
                    const cursor = $event.target.result;
                    if (cursor) {
                        (<any>this.Model).document = cursor.value;
                        (<any>this.Model).fromModel($options).then(() : void => {
                            $resolve();
                        }).catch(($ex : Error) : void => {
                            $reject($ex);
                        });
                    } else {
                        $reject(new Error("Failed to find model record for id: " + this.Model.Id));
                    }
                };
                request.onerror = () : void => {
                    $reject(new Error("Failed to sync model: " + request.error));
                };
            } catch (ex) {
                $reject(ex);
            }
        });
    }

    public async Remove($property? : string, $child? : string) : Promise<void> {
        this.frontendOnlyAssert();
        if (ObjectValidator.IsEmptyOrNull($property)) {
            return new Promise<void>(($resolve, $reject) : void => {
                const request : any = this.getDocument().delete(this.Model.Id);
                request.onsuccess = () : void => {
                    $resolve();
                };
                request.onerror = () : void => {
                    $reject(request.error);
                };
            });
        } else {
            for (const property of this.Model.Properties) {
                if (property.name === $property || property.symbol === $property) {
                    if (!ObjectValidator.IsEmptyOrNull($child)) {
                        delete (<any>this.Model).document()[property.symbol][$child];
                    } else {
                        delete (<any>this.Model).document[property.symbol];
                    }
                }
            }
            return new Promise<void>(($resolve, $reject) : void => {
                try {
                    const request : any = this.getDocument().put(this.Model.getDocument());
                    request.onsuccess = ($event : any) : void => {
                        $resolve();
                    };
                    request.onerror = () : void => {
                        $reject(request.error);
                    };
                } catch (ex) {
                    $reject(ex);
                }
            });
        }
    }
}
