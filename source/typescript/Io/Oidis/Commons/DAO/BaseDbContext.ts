/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IModel } from "../Interfaces/IBaseObject.js";
import { IDbConfiguration } from "../Interfaces/IProject.js";
import { BaseModel, IModelAggregateOptions, IModelFindOptions } from "../Primitives/BaseModel.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { isBrowser } from "../Utils/EnvironmentHelper.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { Reflection } from "../Utils/Reflection.js";
import { BaseInclusionConverter } from "./Converters/BaseInclusionConverter.js";

export abstract class BaseDbDriver extends BaseObject {
    protected readonly models : Array<BaseModel<any>>;

    protected constructor() {
        super();
        this.models = [];
    }

    public abstract getModel($className : string) : any;

    public abstract getDb() : any;

    public abstract Open($config : IDbConfiguration, $autoReconnect? : boolean) : Promise<boolean>;

    public abstract getContextFor($model : any) : BaseDbContext;

    public abstract IsOpen() : boolean ;

    public abstract Close($force? : boolean) : Promise<boolean>;

    public getAllModels() : Array<BaseModel<any>> {
        return this.models;
    }
}

export abstract class BaseDbContext extends BaseObject {

    public get Model() : BaseModel {
        return this.model;
    }

    protected readonly driver : BaseDbDriver;
    private schema : any;
    private model : any;

    constructor($driver : BaseDbDriver, $model? : any) {
        super();
        this.driver = $driver;
        this.schema = null;
        if (!ObjectValidator.IsEmptyOrNull($model)) {
            if (ObjectValidator.IsSet($model.ClassName) && ObjectValidator.IsFunction($model.ClassName)) {
                const prototype : any = Reflection.getInstance().getClass($model.ClassName());
                this.model = new prototype();
            } else {
                this.model = $model;
            }
        }
    }

    public setModel($model : any) : void {
        this.model = $model;
    }

    public abstract FindById<T>($id : string, $options? : IModelFindOptions) : Promise<T>;

    public abstract Find<T>($query : any, $options? : IModelFindOptions) : Promise<T[]>;

    public abstract Aggregate<T>($chain : any[] | IModelAggregateOptions, $options? : IModelFindOptions) : Promise<any>;

    public abstract Save() : Promise<void>;

    public abstract Update($property : string) : Promise<void>;

    public abstract Refresh($options? : IModelFindOptions) : Promise<void>;

    public abstract Remove($property? : string, $child? : string) : Promise<void>;

    public abstract CreateDocument($className : string, $schema : any, $collection : string) : any;

    public getSchema() : any {
        if (!ObjectValidator.IsEmptyOrNull(this.schema)) {
            return this.schema;
        }
        this.schema = {};
        for (const item of this.model.modelProperties) {
            if (!ObjectValidator.IsEmptyOrNull(item.schemaOptions.type)) {
                if (ObjectValidator.IsArray(item.schemaOptions.type)) {
                    // TODO(mkelnar) _id is forced to false because it is common in current use
                    //  check again if this should be forced or addId will be used control will be used instead.
                    //  exp: current use disables all _id generation for array props and all objects in array (not for inclusion)
                    // TODO(mkelnar) BaseModel derivatives validation is not optimal in such form, however type could be anything so
                    //  it looks that there is no other way except: why th reflection.Implements needs constructed instance and not
                    //  working with namespace-string or class prototype?
                    if (item.schemaOptions.type[0].hasOwnProperty("ClassName") &&
                        Reflection.getInstance().Implements(new (item.schemaOptions.type[0])(), IModel)) {
                        if (Reflection.getInstance().IsMemberOf(item.converter, BaseInclusionConverter)) {
                            this.schema[item.symbol] = [(new (item.schemaOptions.type[0])()).getSchema()];
                            item.isInclusion = true;
                        } else {
                            this.schema[item.symbol] = [
                                {
                                    _id     : false,
                                    required: item.schemaOptions.required,
                                    type    : String
                                }
                            ];
                        }
                    } else {
                        this.schema[item.symbol] = [
                            {
                                _id     : false,
                                required: item.schemaOptions.required,
                                type    : item.schemaOptions.type[0]
                            }
                        ];
                    }
                } else {
                    if (item.schemaOptions.type.hasOwnProperty("ClassName") &&
                        Reflection.getInstance().Implements(new (<any>item.schemaOptions.type)(), IModel)) {
                        if (Reflection.getInstance().IsMemberOf(item.converter, BaseInclusionConverter)) {
                            this.schema[item.symbol] = (new (<any>item.schemaOptions.type)()).getSchema();
                            item.isInclusion = true;
                        } else {
                            this.schema[item.symbol] = {
                                required: item.schemaOptions.required,
                                type    : String
                            };
                        }
                    } else {
                        this.schema[item.symbol] = {
                            required: item.schemaOptions.required,
                            type    : item.schemaOptions.type
                        };
                    }
                }
                if (!ObjectValidator.IsEmptyOrNull(item.schemaOptions.alias)) {
                    this.schema[item.symbol].alias = item.schemaOptions.alias;
                }
                if (item.symbol !== "_id") {
                    // turn of native id key generation for property (should have effect only for object types but has been
                    // set for every property from the BaseModel beginning so used it in the same way
                    if (!item.isInclusion && !ObjectValidator.IsArray(item.schemaOptions.type)) {
                        this.schema[item.symbol]._id = item.schemaOptions.addId;
                    }
                } else {
                    this.schema[item.symbol].default = item.schemaOptions.default;
                }
            }
        }
        return this.schema;
    }

    public DocumentToObject($property? : any) : any {
        if (ObjectValidator.IsSet($property)) {
            if (ObjectValidator.IsString($property)) {
                return this.Model.getDocument()[$property];
            }
            return $property;
        }
        return this.Model.getDocument();
    }

    public async Validate() : Promise<boolean> {
        /// TODO: implement validation based on metadata from getSchema instead of model.Validate()
        return this.Model.Validate();
    }

    public getDocument() : any {
        return this.driver.getModel(this.Model.getSymbolicName());
    }

    protected backendOnlyAssert() : void {
        if (isBrowser) {
            throw new Error("Method or feature is allowed only at backend. This is code design issue.");
        }
    }

    protected frontendOnlyAssert() : void {
        if (!isBrowser) {
            throw new Error("Method or feature is allowed only at frontend. This is code design issue.");
        }
    }

    protected getModelInstance() : any {
        const prototype : any = Reflection.getInstance().getClass(this.Model.getClassName());
        return new prototype();
    }
}
