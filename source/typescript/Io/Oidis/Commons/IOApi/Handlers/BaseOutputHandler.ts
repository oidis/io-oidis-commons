/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { NewLineType } from "../../Enums/NewLineType.js";
import { IOHandler } from "../../Interfaces/IOHandler.js";
import { BaseObject } from "../../Primitives/BaseObject.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { Property } from "../../Utils/Property.js";

/**
 * BaseOutputHandler class provides abstract API for handling output to custom resources types.
 */
export abstract class BaseOutputHandler extends BaseObject implements IOHandler {

    private static handlerIterator : number = 0;
    protected onPrintHandler : ($message : string) => void;
    private readonly name : string;
    private handler : any;
    private encoding : string;
    private newLineType : NewLineType;

    /**
     * @param {string} [$handlerName] Set handler name for future identification and internal use.
     */
    constructor($handlerName? : string) {
        super();
        BaseOutputHandler.handlerIterator++;
        if (ObjectValidator.IsSet($handlerName)) {
            this.name = $handlerName;
        } else {
            this.name = "OutputHandler_" + BaseOutputHandler.handlerIterator;
        }
        this.onPrintHandler = ($message : string) => void {};
    }

    /**
     * Provides initialization, which is specific for each type of output handler.
     * @returns {void}
     */
    public Init() : void {
        this.setEncoding("UTF-8");
    }

    /**
     * @returns {string} Returns handler's name used as unique identifier for IO factory.
     */
    public Name() : string {
        return this.name;
    }

    /**
     * @returns {string} Returns string representation of encoding specific for each type of handler.
     */
    public Encoding() : string {
        return this.encoding;
    }

    /**
     * @returns {NewLineType} Returns type of line ending specific for each type of handler.
     */
    public NewLineType() : NewLineType {
        return this.newLineType;
    }

    /**
     * @param {string} $message Value which should be printed to resource handled by handler.
     * @returns {void}
     */
    public Print($message : string) : void {
        this.onPrintHandler($message);
        throw new Error("'" + this.getClassName() + "' is abstract class and does not provides fully implemented Print method.");
    }

    /**
     * @param {Function} $handler Callback, which should be called in time on message print.
     * @returns {void}
     */
    public setOnPrint($handler : ($message : string) => void) : void {
        if (ObjectValidator.IsFunction($handler)) {
            this.onPrintHandler = $handler;
        }
    }

    /**
     * Clean up resource handled by handler.
     * @returns {void}
     */
    public Clear() : void {
        // provide implementation, which can clear handled resource
    }

    public toString() : string {
        return this.ToString();
    }

    protected setHandler($handler : any) : void {
        this.handler = $handler;
    }

    protected getHandler() : any {
        if (ObjectValidator.IsEmptyOrNull(this.handler)) {
            this.Init();
        }
        return this.handler;
    }

    protected setEncoding($value : string) : void {
        this.encoding = Property.String($value);
    }

    protected setNewLineType($value : NewLineType) : void {
        this.newLineType = $value;
    }
}
