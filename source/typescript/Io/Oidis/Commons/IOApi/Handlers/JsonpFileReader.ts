/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "../../Enums/Events/EventType.js";
import { EventArgs } from "../../Events/Args/EventArgs.js";
import { ExceptionsManager } from "../../Exceptions/ExceptionsManager.js";
import { Loader } from "../../Loader.js";
import { BaseObject } from "../../Primitives/BaseObject.js";
import { LogIt } from "../../Utils/LogIt.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { StringUtils } from "../../Utils/StringUtils.js";
import { ScriptHandler } from "./ScriptHandler.js";

/**
 * JsonpFileReader class provides reading data in JSONP format.
 */
export class JsonpFileReader extends BaseObject {
    /**
     * @param {object} $value Specify JSON data object, which should be read from the file.
     * @returns {void}
     */
    public static Data : ($value : any) => void;
    private static serviceTimeout : number = 60000;
    private static processId : number = null;
    private static query : any[] = [];

    /**
     * @param {string} $filePath Specify file path from, which should be data loaded.
     * @param {Function} $successHandler Specify asynchronous handler, which should be called,
     * when the data has been successfully loaded.
     * @param {Function} [$errorHandler] Specify error handler, which should be used instead of general error handler.
     * @returns {void}
     */
    public static Load($filePath : string, $successHandler : ($data : any, $filePath? : string) => void,
                       $errorHandler? : ($eventArgs? : ErrorEvent, $filePath? : string) => void) : void {
        const application : Loader = Loader.getInstance();
        const fileReader : ScriptHandler = new ScriptHandler();
        fileReader.Timeout(JsonpFileReader.serviceTimeout);

        let filePath : string = StringUtils.Replace(StringUtils.Remove($filePath, "file://", "http://", "https://"), "//", "/");
        if (StringUtils.StartsWith($filePath, "file://")) {
            filePath = "file://" + filePath;
        } else if (StringUtils.StartsWith($filePath, "http://")) {
            filePath = "http://" + filePath;
        } else if (StringUtils.StartsWith($filePath, "https://")) {
            filePath = "https://" + filePath;
        }
        if (!application.getEnvironmentArgs().IsProductionMode() && !StringUtils.Contains(filePath, "?dummy")) {
            filePath += "?dummy=" + (new Date().getTime());
        }

        if (application.getHttpManager().getRequest().IsIdeaHost() && StringUtils.Contains(filePath, ".jsonp") && (
            !StringUtils.Contains(filePath, "http://", "https://") || StringUtils.Contains(filePath, "file://", "localhost:"))) {
            filePath = StringUtils.Replace(filePath, ".jsonp", ".js");
        }

        filePath = this.filterPath(filePath);
        fileReader.Path(filePath);

        fileReader.ErrorHandler(($error : ErrorEvent) : void => {
            JsonpFileReader.processId = null;
            try {
                const eventArgs : EventArgs = new EventArgs();
                if (ObjectValidator.IsSet($error)) {
                    eventArgs.NativeEventArgs($error);
                } else {
                    eventArgs.NativeEventArgs(new ErrorEvent(""));
                }
                if (ObjectValidator.IsEmptyOrNull((<ErrorEvent>eventArgs.NativeEventArgs()).message) &&
                    StringUtils.Contains(filePath, ".jsonp") &&
                    (!StringUtils.Contains(filePath, "http://", "https://") ||
                        StringUtils.Contains(filePath, "file://", "localhost:"))) {
                    JsonpFileReader.Load(StringUtils.Replace(filePath, ".jsonp", ".js"), $successHandler, $errorHandler);
                } else {
                    application.getHttpResolver().getEvents()
                        .FireEvent(JsonpFileReader.ClassName(), EventType.ON_ERROR, eventArgs, false);
                    if (!ObjectValidator.IsEmptyOrNull($errorHandler)) {
                        $errorHandler($error, $filePath);
                    } else {
                        if (!ObjectValidator.IsEmptyOrNull((<ErrorEvent>eventArgs.NativeEventArgs())) &&
                            !ObjectValidator.IsEmptyOrNull((<ErrorEvent>eventArgs.NativeEventArgs()).message)) {
                            ExceptionsManager.Throw(
                                JsonpFileReader.ClassName(), (<ErrorEvent>eventArgs.NativeEventArgs()).message);
                        } else {
                            let notFoundFilePath : string = "";
                            if (StringUtils.StartsWith(filePath, "file://") ||
                                StringUtils.StartsWith(filePath, "http://") ||
                                StringUtils.StartsWith(filePath, "https://")) {
                                notFoundFilePath = filePath;
                            } else {
                                let rootPath : string = application.getHttpManager().getRequest().getHostUrl();
                                if (StringUtils.Contains(rootPath, ".html")) {
                                    rootPath = StringUtils.Substring(rootPath, 0, StringUtils.IndexOf(rootPath, "/", false));
                                }
                                notFoundFilePath = rootPath + "/" + filePath;
                                if (StringUtils.Contains(notFoundFilePath, "?dummy=")) {
                                    notFoundFilePath =
                                        StringUtils.Substring(notFoundFilePath, 0, StringUtils.IndexOf(notFoundFilePath, "?"));
                                }
                            }
                            application.getHttpManager().Return404NotFound(notFoundFilePath);
                        }
                    }
                }
            } catch (ex) {
                this.riseError($errorHandler, ex);
            }
        });
        if (ObjectValidator.IsEmptyOrNull(JsonpFileReader.processId)) {
            JsonpFileReader.processId = application.getHttpResolver().getEvents().FireAsynchronousMethod(() : void => {
                fileReader.DataHandler(JsonpFileReader, "Data");
                fileReader.SuccessHandler(() : void => {
                    JsonpFileReader.processId = null;
                    try {
                        application.getHttpResolver().getEvents().FireEvent(JsonpFileReader.ClassName(), EventType.ON_LOAD, false);
                        $successHandler(fileReader.Data(), $filePath);
                        if (JsonpFileReader.query.length > 0) {
                            LogIt.Warning("Executing {0} success handlers from query", this.ClassName());
                            JsonpFileReader.query.forEach(($handlers : any) : void => {
                                $handlers.success(fileReader.Data(), $filePath);
                            });
                        }
                    } catch (ex) {
                        this.riseError($errorHandler, ex);
                        if (JsonpFileReader.query.length > 0) {
                            LogIt.Warning("Executing {0} error handlers from query", this.ClassName());
                            JsonpFileReader.query.forEach(($handlers : any) : void => {
                                this.riseError($handlers.error, ex);
                            });
                        }
                    }
                    JsonpFileReader.query = [];
                });
                fileReader.Load();
            }, true);
        } else {
            JsonpFileReader.query.push({success: $successHandler, error: $errorHandler});
        }
    }

    /**
     * @param {string} $input Specify string, from which data should be loaded.
     * @param {Function} $successHandler Specify asynchronous handler, which should be called,
     * when the data has been successfully loaded.
     * @param {Function} [$errorHandler] Specify error handler, which should be used instead of general error handler.
     * @returns {void}
     */
    public static LoadString($input : string, $successHandler : ($data : any) => void,
                             $errorHandler? : ($eventArgs? : ErrorEvent) => void) : void {
        const application : Loader = Loader.getInstance();
        const scriptLoader : ScriptHandler = new ScriptHandler();
        scriptLoader.Timeout(JsonpFileReader.serviceTimeout);
        scriptLoader.ErrorHandler(($error : ErrorEvent) : void => {
            try {
                const eventArgs : EventArgs = new EventArgs();
                if (ObjectValidator.IsSet($error)) {
                    eventArgs.NativeEventArgs($error);
                } else {
                    eventArgs.NativeEventArgs(new ErrorEvent(""));
                }

                application.getHttpResolver().getEvents()
                    .FireEvent(JsonpFileReader.ClassName(), EventType.ON_ERROR, eventArgs, false);
                if (!ObjectValidator.IsEmptyOrNull($errorHandler)) {
                    $errorHandler($error);
                } else {
                    if (!ObjectValidator.IsEmptyOrNull((<ErrorEvent>eventArgs.NativeEventArgs())) &&
                        !ObjectValidator.IsEmptyOrNull((<ErrorEvent>eventArgs.NativeEventArgs()).message)) {
                        ExceptionsManager.Throw(JsonpFileReader.ClassName(), (<ErrorEvent>eventArgs.NativeEventArgs()).message);
                    } else {
                        ExceptionsManager.Throw(JsonpFileReader.ClassName(), "Unexpected read failure.");
                    }
                }
            } catch (ex) {
                this.riseError($errorHandler, ex);
            }
        });
        application.getHttpResolver().getEvents().FireAsynchronousMethod(() => {
            scriptLoader.DataHandler(JsonpFileReader, "Data");
            scriptLoader.Data($input);
            scriptLoader.SuccessHandler(() : void => {
                try {
                    application.getHttpResolver().getEvents().FireEvent(JsonpFileReader.ClassName(), EventType.ON_LOAD, false);
                    $successHandler(scriptLoader.Data());
                } catch (ex) {
                    this.riseError($errorHandler, ex);
                }
            });
            scriptLoader.Load();
        }, false);
    }

    protected static filterPath($value : string) : string {
        return $value;
    }

    protected static riseError($errorHandler : ($eventArgs? : ErrorEvent, $filePath? : string) => void,
                               $error : Error) : void {
        if (ObjectValidator.IsEmptyOrNull($errorHandler)) {
            ExceptionsManager.HandleException($error);
        } else {
            $errorHandler(new ErrorEvent($error.name, {
                error  : $error,
                message: $error.message
            }));
        }
    }
}

/**
 * Global function suitable for loading of data in JSON format.
 * @param {object} $value Specify data in JSON format, which should be loaded.
 * @returns {void}
 */
globalThis.JsonpData = ($value : any) : void => {
    "use strict";
    JsonpFileReader.Data($value);
};
