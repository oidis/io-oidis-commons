/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { NewLineType } from "../../Enums/NewLineType.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { BaseOutputHandler } from "./BaseOutputHandler.js";

/**
 * ConsoleHandler class provides handling of stdout.
 */
export class ConsoleHandler extends BaseOutputHandler {

    /**
     * @param {string} [$handlerName] Set handler name for future identification and internal use.
     */
    constructor($handlerName? : string) {
        super($handlerName);
    }

    /**
     * Provides initialization, which is specific for console type of output handler.
     * @returns {void}
     */
    public Init() : void {
        super.Init();
        try {
            /* eslint-disable no-console */
            const xConsole : any = console;
            if (!ObjectValidator.IsEmptyOrNull(xConsole._commandLineAPI)) {
                xConsole.API = xConsole._commandLineAPI;
            } else if (!ObjectValidator.IsEmptyOrNull(xConsole._inspectorCommandLineAPI)) {
                xConsole.API = xConsole._inspectorCommandLineAPI;
            } else if (!ObjectValidator.IsEmptyOrNull(console.clear)) {
                xConsole.API = console;
            }
            this.setHandler(console);
            /* eslint-enable */
        } catch (ex) {
            this.setHandler({
                error($message : string) : void {
                    // fallback for old browsers
                },
                log($message : string) : void {
                    // fallback for old browsers
                },
                clear() : void {
                    // fallback for old browsers
                }
            });
        }
        this.setNewLineType(NewLineType.LINUX);
    }

    /**
     * @param {string} $message Value which should be printed to resource handled by handler.
     * @returns {void}
     */
    public Print($message : string) : void {
        this.onPrintHandler($message);
        this.getHandler().log($message);
    }

    /**
     * Clean up resource handled by handler.
     * @returns {void}
     */
    public Clear() : void {
        try {
            this.getHandler().clear();
        } catch (ex) {
            this.getHandler().error("unable to clear console");
        }
    }
}
