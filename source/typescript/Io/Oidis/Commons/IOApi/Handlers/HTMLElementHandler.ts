/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { NewLineType } from "../../Enums/NewLineType.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { BaseOutputHandler } from "./BaseOutputHandler.js";

/**
 * HTMLElementHandler class provides handling of HTML element.
 */
export class HTMLElementHandler extends BaseOutputHandler {

    /**
     * @param {string} [$handlerName] Set handler name for future identification and internal use.
     */
    constructor($handlerName? : string) {
        super($handlerName);
    }

    /**
     * Provides initialization, which is specific for HTMLElement type of output handler.
     * @returns {void}
     */
    public Init() : void {
        super.Init();
        const target : HTMLElement = document.getElementById(this.Name());
        if (ObjectValidator.IsEmptyOrNull(target)) {
            throw new Error("Element \"" + this.Name() + "\" has not been found.");
        }
        this.setHandler(target);
        this.setNewLineType(NewLineType.HTML);
    }

    /**
     * @param {string} $message Value which should be printed to resource handled by handler.
     * @returns {void}
     */
    public Print($message : string) : void {
        this.onPrintHandler($message);
        const newContent : HTMLElement = document.createElement("span");
        newContent.setAttribute("guiType", "HtmlAppender");
        newContent.innerHTML = $message;
        this.getHandler().appendChild(newContent);
    }

    /**
     * Clean up resource handled by handler.
     * @returns {void}
     */
    public Clear() : void {
        this.getHandler().innerHTML = "";
    }
}
