/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "../../Enums/Events/EventType.js";
import { FileHandlerEventType } from "../../Enums/Events/FileHandlerEventType.js";
import { ErrorEventArgs } from "../../Events/Args/ErrorEventArgs.js";
import { EventArgs } from "../../Events/Args/EventArgs.js";
import { ProgressEventArgs } from "../../Events/Args/ProgressEventArgs.js";
import { EventsManager } from "../../Events/EventsManager.js";
import { ExceptionsManager } from "../../Exceptions/ExceptionsManager.js";
import { BaseObject } from "../../Primitives/BaseObject.js";
import { Convert } from "../../Utils/Convert.js";
import { ObjectDecoder } from "../../Utils/ObjectDecoder.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { StringUtils } from "../../Utils/StringUtils.js";

/**
 * FileHandler class provides handling of file data.
 */
export class FileHandler extends BaseObject {

    private readonly source : File;
    private reader : FileReader;
    private data : string;

    /**
     * @returns {boolean} Returns true, if File API is supported, otherwise false.
     */
    public static IsSupported() : boolean {
        const apiSupported : boolean =
            ObjectValidator.IsSet(globalThis.File) &&
            ObjectValidator.IsSet(globalThis.FileReader) &&
            ObjectValidator.IsSet(globalThis.FileList) &&
            ObjectValidator.IsSet(globalThis.Blob);
        FileHandler.IsSupported = () : boolean => {
            return apiSupported;
        };
        return apiSupported;
    }

    /**
     * @param {File} $value Specify File instance, which should be handled.
     */
    constructor($value : File) {
        super();
        this.source = $value;
        this.data = "";

        this.reader = new FileReader();
        let progress : number = 0;
        this.reader.onerror = ($event : Event) : void => {
            try {
                const name : string = ObjectValidator.IsEmptyOrNull(this.source) ? "undefined" : this.source.name;
                let message : string = "";
                const error : any = (<any>$event.target).error;
                switch (error.code) {
                case error.ENCODING_ERR:
                    message = "An encoding error occurred while reading the file '" + name + "'";
                    break;
                case error.NOT_FOUND_ERR:
                    message = "File '" + name + "' not found.";
                    break;
                case error.NOT_READABLE_ERR:
                    message = "File '" + name + "' is not readable.";
                    break;
                case error.SECURITY_ERR:
                    message = "Security issue with file '" + name + "'";
                    break;
                case error.ABORT_ERR:
                    break;
                default:
                    message = "An error occurred while reading the file '" + name + "'.";
                    break;
                }
                if (!ObjectValidator.IsEmptyOrNull(message)) {
                    const eventArgs : ErrorEventArgs = new ErrorEventArgs(message);
                    eventArgs.Owner(this);
                    eventArgs.NativeEventArgs($event);
                    EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), EventType.ON_ERROR, eventArgs);
                }
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        };
        this.reader.onprogress = ($event : ProgressEvent) : void => {
            try {
                const eventArgs : ProgressEventArgs = new ProgressEventArgs();
                eventArgs.Owner(this);
                eventArgs.NativeEventArgs($event);
                if ($event.lengthComputable) {
                    eventArgs.RangeEnd($event.total);
                    eventArgs.CurrentValue($event.loaded);
                } else {
                    eventArgs.CurrentValue(progress++);
                }
                EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), FileHandlerEventType.ON_CHANGE, eventArgs);
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        };
        this.reader.onabort = ($event : Event) : void => {
            try {
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.NativeEventArgs($event);
                eventArgs.Owner(this);
                EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), FileHandlerEventType.ON_ABOARD, eventArgs);
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        };
        this.reader.onloadstart = ($event : Event) : void => {
            try {
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.NativeEventArgs($event);
                eventArgs.Owner(this);
                EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), FileHandlerEventType.ON_START, eventArgs);
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        };
        this.reader.onload = ($event : Event) : void => {
            try {
                this.data += (<any>$event.target).result;
                if ((<any>$event.target).readyState === (<any>FileReader).DONE) {
                    if (!ObjectValidator.IsEmptyOrNull(this.data)) {
                        this.data = StringUtils.Split(this.data, ",")[1];
                    }
                    const eventArgs : EventArgs = new EventArgs();
                    eventArgs.Owner(this);
                    eventArgs.NativeEventArgs($event);
                    EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), FileHandlerEventType.ON_COMPLETE, eventArgs);
                }
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        };
    }

    /**
     * @returns {File} Returns source, which is handled by the FileHandler instance.
     */
    public getSource() : File {
        return this.source;
    }

    /**
     * @returns {string} Returns file name, which is handled by the FileHandler instance.
     */
    public getName() : string {
        if (!ObjectValidator.IsEmptyOrNull(this.source)) {
            return this.source.name;
        }
        return "";
    }

    /**
     * @returns {number} Returns file size, which is handled by the FileHandler instance
     */
    public getSize() : number {
        if (!ObjectValidator.IsEmptyOrNull(this.source)) {
            return this.source.size;
        }
        return 0;
    }

    /**
     * @returns {string} Returns file type, which is handled by the FileHandler instance.
     */
    public getType() : string {
        if (!ObjectValidator.IsEmptyOrNull(this.source)) {
            return this.source.type;
        }
        return "";
    }

    /**
     * @param {boolean} [$encoded=false] Specify, if data should be in base64 format.
     * @param {boolean} [$urlSafe=false] Specify, if base64 format should respect url formatting.
     * @returns {string} Returns data loaded from the file.
     */
    public Data($encoded : boolean = false, $urlSafe : boolean = false) : string {
        if (ObjectValidator.IsEmptyOrNull(this.data)) {
            return "";
        }
        if ($encoded) {
            if ($urlSafe) {
                this.data = StringUtils.Replace(this.data, "+", "-");
                this.data = StringUtils.Replace(this.data, "/", "_");
                this.data = StringUtils.Replace(this.data, "=", ".");
            }
            return this.data;
        }
        return ObjectDecoder.Base64(this.data);
    }

    /**
     * @param {number} [$start] Specify start of file read.
     * @param {number} [$end] Specify end of file read.
     * @returns {void}
     */
    public Load($start? : number, $end? : number) : void {
        this.data = "";
        if (!ObjectValidator.IsEmptyOrNull(this.source)) {
            if (ObjectValidator.IsEmptyOrNull($start)) {
                this.reader.readAsDataURL(this.source);
            } else {
                if (ObjectValidator.IsEmptyOrNull($end) || $end > this.getSize()) {
                    $end = this.getSize();
                }
                this.reader.readAsDataURL(this.source.slice($start, $end));
            }
        } else {
            const eventArgs : ErrorEventArgs = new ErrorEventArgs("File source is empty.");
            eventArgs.Owner(this);
            EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), EventType.ON_ERROR, eventArgs);
        }
    }

    /**
     * Stop file loading
     * @returns {void}
     */
    public Stop() : void {
        this.reader.abort();
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        let output : string = "";
        output += $prefix +
            "[\"name\"] " + Convert.ObjectToString(this.getName(), StringUtils.Tab(1, $htmlTag), $htmlTag) +
            StringUtils.NewLine($htmlTag);
        output += $prefix +
            "[\"type\"] " + Convert.ObjectToString(this.getType(), StringUtils.Tab(1, $htmlTag), $htmlTag) +
            StringUtils.NewLine($htmlTag);
        output += $prefix +
            "[\"size\"] " + Convert.ObjectToString(this.getSize(), StringUtils.Tab(1, $htmlTag), $htmlTag) +
            StringUtils.NewLine($htmlTag);
        output += $prefix +
            "[\"lastModifiedTime\"] " + StringUtils.Tab(1, $htmlTag) + Convert.TimeToGMTformat((<any>this.source).lastModified) +
            StringUtils.NewLine($htmlTag);
        return output;
    }
}
