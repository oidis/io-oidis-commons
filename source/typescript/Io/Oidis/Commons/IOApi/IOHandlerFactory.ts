/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IOHandlerType } from "../Enums/IOHandlerType.js";
import { IOHandler } from "../Interfaces/IOHandler.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { ConsoleHandler } from "./Handlers/ConsoleHandler.js";
import { HTMLElementHandler } from "./Handlers/HTMLElementHandler.js";

/**
 * IOHandlerFactory class provides factory for input and output resources handlers.
 */
export class IOHandlerFactory extends BaseObject {

    private static handlersList : ArrayList<IOHandler>;

    /**
     * @param {IOHandlerType} $handlerType Create or get this type of IO handler.
     * @param {string} [$owner] Set handler owner.
     * @returns {IOHandler} Returns instance of IO handler based on $handlerType.
     */
    public static getHandler($handlerType : IOHandlerType, $owner? : string) : IOHandler {
        const key : string = $handlerType + $owner;

        if (!ObjectValidator.IsSet(this.handlersList)) {
            this.handlersList = new ArrayList<IOHandler>();
        }
        if (!this.handlersList.KeyExists(key)) {
            const handlerName : string = this.handlerIdGenerator($handlerType, $owner);
            switch ($handlerType) {
            case IOHandlerType.CONSOLE:
                this.handlersList.Add(new ConsoleHandler(handlerName), key);
                break;
            case IOHandlerType.HTML_ELEMENT:
                this.handlersList.Add(new HTMLElementHandler(handlerName), key);
                break;
            default:
                break;
            }
        }

        return this.handlersList.getItem(key);
    }

    /**
     * @param {IOHandler} $handler Validate this type of IO handler.
     * @returns {IOHandlerType} Returns type of handler instance as value of IOHandlerType enum.
     */
    public static getHandlerType($handler : IOHandler) : IOHandlerType {
        if (!ObjectValidator.IsSet($handler)) {
            return null;
        }

        if ($handler.IsTypeOf(ConsoleHandler)) {
            return IOHandlerType.CONSOLE;
        }

        if ($handler.IsTypeOf(HTMLElementHandler)) {
            return IOHandlerType.HTML_ELEMENT;
        }
        return null;
    }

    /**
     * Clean up content of all registered handlers.
     * @returns {void}
     */
    public static DestroyAll() : void {
        if (ObjectValidator.IsSet(this.handlersList)) {
            this.handlersList.foreach(($handler : IOHandler) : void => {
                $handler.Clear();
            });
        }
    }

    private static handlerIdGenerator($handlerType : any, $owner : string) : string {
        let handlerName : string = "";
        switch ($handlerType) {
        case IOHandlerType.CONSOLE:
        case IOHandlerType.HTML_ELEMENT:
            if (!ObjectValidator.IsEmptyOrNull($owner)) {
                handlerName = $owner;
            } else {
                handlerName = $handlerType;
            }
            break;
        case IOHandlerType.OUTPUT_FILE:
            handlerName = "handlers\\output.txt";
            if (!ObjectValidator.IsEmptyOrNull($owner)) {
                handlerName = $owner + "\\" + handlerName;
            }
            break;
        case IOHandlerType.INPUT_FILE:
            handlerName = "inputhandler";
            if (!ObjectValidator.IsEmptyOrNull($owner)) {
                handlerName = $owner + "\\" + handlerName;
            }
            break;
        default:
            break;
        }
        return handlerName;
    }
}
