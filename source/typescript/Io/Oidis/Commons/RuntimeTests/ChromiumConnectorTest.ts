/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { RuntimeTestRunner } from "../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { IWebServiceClient } from "../Interfaces/IWebServiceClient.js";
import { Echo } from "../Utils/Echo.js";
import { WebServiceClientFactory } from "../WebServiceApi/WebServiceClientFactory.js";

export class ChromiumConnectorTest extends RuntimeTestRunner {

    public testConnect() : void {
        Echo.Println("Running test chromium connector");
        const client : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.CEF_QUERY);
        client.Send({test: "test"}, ($response : any) : void => {
            Echo.Printf($response);
        });
        client.Send({test: "test1"});
        client.Send({test: "test2"});
    }
}

/* dev:end */
