/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { LogSeverity } from "../Enums/LogSeverity.js";
import { RuntimeTestRunner } from "../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { HTMLAdapter } from "../LogProcessor/Adapters/HTMLAdapter.js";
import { Logger } from "../LogProcessor/Logger.js";
import { LogIt } from "../Utils/LogIt.js";

class PrintObject {
    public toString() : string {
        return "hello from print object";
    }
}

export class LogItTest extends RuntimeTestRunner {

    public AllLevelsTest() : void {
        const logger : Logger = new Logger();
        logger.AddAdapter(new HTMLAdapter());
        // logger.setLevels(LogLevel.ALL, LogSeverity.LOW);
        logger.setModes(true, true);
        // logger.setModes(false, true);
        // logger.setModes(true, false);
        LogIt.Init(logger);

        LogIt.Info("Info message");
        LogIt.Info("Info with severity low", LogSeverity.LOW);
        LogIt.Info("Info with severity medium", LogSeverity.MEDIUM);
        LogIt.Info("Info with severity high", LogSeverity.HIGH);
        LogIt.Info("Info with severity and format {0}", LogSeverity.LOW);
        LogIt.Info("Info with parameter same as severity", 2);
        LogIt.Info("Info with parameter same as severity and format {0}", 2, 1);
        LogIt.Info("Info with parameter {0}", -1);
        LogIt.Info("Info message {0} {1}", true, 1);

        LogIt.Warning("Warn message");
        LogIt.Warning("Warn message {0}", 123);

        LogIt.Error(new Error("report native error"));
        LogIt.Error("Error message");
        LogIt.Error("Error with trace", new Error());
        LogIt.Error("Error with params {0}", true);

        LogIt.Debug("Debug message");
        LogIt.Debug(new LogItTest());
        LogIt.Debug("Debug message {0}", true);

        LogIt.Verbose("Verbose message");
        LogIt.Verbose("Verbose message {0}", 1235);
    }
}
/* dev:end */
