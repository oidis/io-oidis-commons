/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { AsyncHttpResolver } from "../HttpProcessor/Resolvers/AsyncHttpResolver.js";
import { Convert } from "../Utils/Convert.js";
import { StringUtils } from "../Utils/StringUtils.js";

export class AsyncRequestTest extends AsyncHttpResolver {

    protected resolver() : void {
        let output : string = "";
        output += Convert.ObjectToString(this.getEnvironmentArgs()) + StringUtils.NewLine();
        output += Convert.ObjectToString(this.getRequest()) + StringUtils.NewLine();
        output += Convert.ObjectToString(this.RequestArgs()) + StringUtils.NewLine();
        this.RequestArgs().Result(output);
        this.success();
    }

}

/* dev:end */
