/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventType } from "../Enums/Events/EventType.js";
import { EventArgs } from "../Events/Args/EventArgs.js";
import { TimeoutManager } from "../Events/TimeoutManager.js";
import { RuntimeTestRunner } from "../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Echo } from "../Utils/Echo.js";

export class TimeoutTest extends RuntimeTestRunner {

    public TimeoutTest() : void {
        const startTime : number = new Date().getTime();
        const testObject : any = {testValue: false};

        let asyncExecutor : TimeoutManager = new TimeoutManager();
        for (let x : number = 0; x < 1000; x++) {
            asyncExecutor.Add(($x? : number) : void => {
                const asyncExecutor2 : TimeoutManager = new TimeoutManager();
                for (let y : number = 0; y <= 100; y++) {
                    asyncExecutor2.Add(($y? : number) : void => {
                        testObject.testValue = $x * $y % 1;
                        if ($x === 999 && $y === 100) {
                            Echo.Printf("async method finish time " + ((new Date().getTime() - startTime) / 1000));
                        }
                    });
                }
                asyncExecutor2.Execute();
                if ($x % 100 === 0) {
                    Echo.Println(".");
                } else {
                    Echo.Print(".");
                }
            });
        }

        this.getEventsManager().setEvent(asyncExecutor.getId(), EventType.ON_START,
            ($eventArgs : EventArgs) : void => {
                Echo.Printf("async process start for " + $eventArgs.Owner());
            });
        this.getEventsManager().setEvent(asyncExecutor.getId(), EventType.ON_COMPLETE,
            ($eventArgs : EventArgs) : void => {
                Echo.Printf("async process finished for " + $eventArgs.Owner());
                asyncExecutor = null;
            });
        asyncExecutor.Execute();
    }
}
/* dev:end */
