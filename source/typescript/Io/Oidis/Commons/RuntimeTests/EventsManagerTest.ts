/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventType } from "../Enums/Events/EventType.js";
import { EventArgs } from "../Events/Args/EventArgs.js";
import { ThreadPool } from "../Events/ThreadPool.js";
import { RuntimeTestRunner } from "../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Echo } from "../Utils/Echo.js";

export class EventsManagerTest extends RuntimeTestRunner {

    public testSetEvent() : void {
        this.getEventsManager().setEvent("test", EventType.ON_COMPLETE, ($eventArgs? : EventArgs) : void => {
            Echo.Print($eventArgs.Owner() + " " + $eventArgs.Type());
        });
        this.getEventsManager().FireEvent("test", EventType.ON_START, new EventArgs());
        this.getEventsManager().FireEvent("test", EventType.ON_COMPLETE, new EventArgs());
    }

    public testThreadPool() : void {
        let index : number = 1;
        ThreadPool.AddThread("test", "incrementer", ($eventArgs? : EventArgs) : void => {
            Echo.Println(index + ". " + $eventArgs.ToString());
            index++;
            if (index === 15) {
                ThreadPool.RemoveThread("test", "incrementer");
            }
        });
        ThreadPool.Execute();
    }

}
/* dev:end */
