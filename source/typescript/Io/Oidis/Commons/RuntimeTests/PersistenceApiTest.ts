/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { PersistenceType } from "../Enums/PersistenceType.js";
import { RuntimeTestRunner } from "../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { IPersistenceHandler } from "../Interfaces/IPersistenceHandler.js";
import { PersistenceFactory } from "../PersistenceApi/PersistenceFactory.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { Echo } from "../Utils/Echo.js";
import { ObjectDecoder } from "../Utils/ObjectDecoder.js";
import { ObjectEncoder } from "../Utils/ObjectEncoder.js";
import { Property } from "../Utils/Property.js";
import { StringUtils } from "../Utils/StringUtils.js";

class MockBaseObject extends BaseObject {
}

export class PersistenceApiTest extends RuntimeTestRunner {

    public testSerializationAtRuntime() : void {
        let holder : ArrayList<any> = new ArrayList<any>();
        const values : ArrayList<any> = new ArrayList<any>();
        values.Add("test");
        values.Add("按钮的工具提示文本");
        values.Add(1, 2);
        values.Add(true, "bool val key");
        values.Add(new ArrayList<string>(), "array");
        values.getItem("array").Add("value", "key");
        values.getItem("array").Add(new MockBaseObject(), "objectKey");

        holder.Add(Property.Time(null, "+1 h"), "ttl");
        holder.Add(StringUtils.getCrc(ObjectEncoder.Serialize(values)), "crc");
        holder.Add(values, "values");

        Echo.Println("Before serialization:");
        Echo.Printf(holder);
        const serialized : string = ObjectEncoder.Serialize(holder);
        Echo.Println("serialized:" + StringUtils.NewLine() + serialized + StringUtils.NewLine());
        const encoded : string = ObjectEncoder.Base64(serialized, true);
        Echo.Println("encoded:" + StringUtils.NewLine() + encoded + StringUtils.NewLine());
        holder = ObjectDecoder.Unserialize(ObjectDecoder.Base64(encoded));
        Echo.Println("After decoded unserialization:");
        Echo.Printf(holder);
    }

    public testAsyncSerializationAtRuntime() : void {
        const holder : ArrayList<any> = new ArrayList<any>();
        holder.Add(null);
        holder.Add("");
        holder.Add("test string");
        holder.Add(false);
        holder.Add(1);
        holder.Add(1.1, "float");
        holder.Add(() : number => {
            const testVar : number = 1;
            return testVar;
        });
        holder.Add({test: "value"});
        holder.Add(["test array 1"]);
        holder.Add(["test array 2"]);
        holder.Add([
            null, false, 1, 1.1, "", "test string", () : number => {
                const testVar : number = 1;
                return testVar;
            },
            new ArrayList<any>(),
            [
                true, "test", {}, [],
                [2, 2.5, "test"]
            ]
        ], "array");

        const values : ArrayList<any> = new ArrayList<any>();
        values.Add("test");
        values.Add("按钮的工具提示文本");
        values.Add(1, 2);
        values.Add(true, "bool val key");
        values.Add(new ArrayList<string>(), "array");
        values.getItem("array").Add("value", "key");
        values.getItem("array").Add({test: "value", test2: false}, "object");
        values.getItem("array").Add(new MockBaseObject(), "objectKey");
        holder.Add(values, "values");

        const persistence : ArrayList<any> = new ArrayList<any>();
        const variables : ArrayList<any> = new ArrayList<any>();
        variables.Add(holder, "holder");
        persistence.Add(variables, "variables");
        persistence.Add(StringUtils.getCrc(ObjectEncoder.Serialize(holder)), "crc");

        ObjectEncoder.Serialize(persistence, ($data : any) : void => {
            Echo.Println("After async serialization:");
            Echo.Printf($data);

            ObjectDecoder.Unserialize($data, ($data : any) : void => {
                Echo.Println("After async unserialization:");
                Echo.Printf($data);
            });
        });
    }

    public integralTest() : void {
        const persistenceManager : IPersistenceHandler = PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, "config");
        const persistenceManager2 : IPersistenceHandler = PersistenceFactory.getPersistence(PersistenceType.BROWSER);
        persistenceManager2.ExpireTime("1 sec");
        const persistenceManager3 : IPersistenceHandler = PersistenceFactory.getPersistence("GuiAutocomplete", "user1");
        persistenceManager3.ExpireTime("10 sec");
        const persistenceManager4 : IPersistenceHandler = PersistenceFactory.getPersistence("GuiAutocomplete", "user2");

        Echo.Println("persistent value: " + persistenceManager.Variable("test"));
        persistenceManager.Variable("test", "client value");
        Echo.Println("fresh value: " + persistenceManager.Variable("test"));

        Echo.Println(persistenceManager2.Variable("test"));
        persistenceManager2.Variable("test", "browser value");
        Echo.Println(persistenceManager2.Variable("test"));

        Echo.Println("per3 " + persistenceManager3.Variable("test"));
        Echo.Println("per4 " + persistenceManager4.Variable("test"));

        persistenceManager3.Variable("test", "value");
        const vars : ArrayList<string> = new ArrayList<string>();
        vars.Add("value2", "test");
        vars.Add("value3", "test2");
        vars.Add("value4", "test3");
        vars.Add("value5", "test4");
        persistenceManager4.Variable(vars);

        Echo.Println("per3 " + persistenceManager3.Variable("test"));
        Echo.Println("per4 " + persistenceManager4.Variable("test"));

        this.assertEquals(persistenceManager4.Exists("test"), true, "test val exist before destroy");
        persistenceManager4.Destroy("test");
        const vars2 : ArrayList<string> = new ArrayList<string>();
        vars2.Add("test2");
        vars2.Add("test3");
        persistenceManager4.Destroy(vars2);
        this.assertEquals(persistenceManager4.Exists("test"), false, "do not exist after destroy");
        this.assertEquals(persistenceManager4.Exists("test4"), true, "one value exist after destroy");

        Echo.Println("per1 (" + persistenceManager.getSessionId() + ") " + StringUtils.NewLine() + persistenceManager);
        Echo.Println("per2 (" + persistenceManager2.getSessionId() + ") " + StringUtils.NewLine() + persistenceManager2);
        Echo.Println("per3 (" + persistenceManager3.getSessionId() + ") " + StringUtils.NewLine() + persistenceManager3);
        Echo.Println("per4 (" + persistenceManager4.getSessionId() + ") " + StringUtils.NewLine() + persistenceManager4);
        persistenceManager4.Clear();
        Echo.Println("per4 after clear (" + persistenceManager4.getSessionId() + ") " + StringUtils.NewLine() + persistenceManager4);
    }

    public integralAsyncTest() : void {
        let holder : ArrayList<any> = new ArrayList<any>();
        const values : ArrayList<any> = new ArrayList<any>();
        values.Add("test");
        values.Add("按钮的工具提示文本");
        values.Add(1, 2);
        values.Add(true, "bool val key");
        values.Add(new ArrayList<string>(), "array");
        values.getItem("array").Add("value", "key");
        values.getItem("array").Add(new MockBaseObject(), "objectKey");
        holder.Add("test string");
        holder.Add(values);

        const persistenceManager : IPersistenceHandler = PersistenceFactory.getPersistence("asyncTestPersistence");
        persistenceManager.LoadPersistenceAsynchronously(() : void => {
            if (!persistenceManager.Exists("holder")) {
                persistenceManager.Variable("holder", holder, () : void => {
                    Echo.Printf("async persistence saved asynchronously - refresh the page for read of the saved data");
                });
            } else {
                Echo.Printf("persistence asynchronously loaded");
                holder = persistenceManager.Variable("holder");
                Echo.Printf(holder);
                persistenceManager.Clear();
            }
        });
    }
}
/* dev:end */
