/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { IRuntimeTestPromise, RuntimeTestRunner } from "../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Echo } from "../Utils/Echo.js";

export class AsyncRuntimeTestCase extends RuntimeTestRunner {

    public testCase01() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                Echo.Printf("test case 01");
                $done();
            }, false, 200);
        };
    }

    public testCase02() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                Echo.Printf("test case 02");
                $done();
            }, false, 200);
        };
    }

    protected before() : IRuntimeTestPromise {
        this.addMethod("dynamic01", () : void => {
            Echo.Printf("dynamic test 01");
        });
        return ($done : () => void) : void => {
            this.addMethod("dynamic02", () : void => {
                Echo.Printf("dynamic test 02");
            });
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                this.addMethod("dynamic03", () : IRuntimeTestPromise => {
                    return ($done : () => void) : void => {
                        this.getEventsManager().FireAsynchronousMethod(() : void => {
                            Echo.Printf("dynamic test 03");
                            $done();
                        }, 500);
                    };
                });
                Echo.Printf("before");
                $done();
            }, false, 200);
        };
    }

    protected setUp() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                Echo.Printf("set up");
                $done();
            }, false, 200);
        };
    }

    protected tearDown() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                Echo.Printf("tear down");
                $done();
            }, false, 200);
        };
    }

    protected after() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                Echo.Printf("after");
                $done();
            }, false, 200);
        };
    }
}

/* dev:end */
