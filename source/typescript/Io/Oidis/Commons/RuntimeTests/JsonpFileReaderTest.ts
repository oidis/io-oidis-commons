/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { IRuntimeTestPromise, RuntimeTestRunner } from "../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { JsonpFileReader } from "../IOApi/Handlers/JsonpFileReader.js";
import { Echo } from "../Utils/Echo.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";

export class JsonpFileReaderTest extends RuntimeTestRunner {

    constructor() {
        super();
        // this.setMethodFilter("testReadFromString");
    }

    public testSuccess() : Promise<void> {
        return new Promise<void>(($resolve : () => void) : void => {
            JsonpFileReader.Load(
                "https://hub.oidis.io/connector.config.jsonp",
                ($data : any, $filePath? : string) : void => {
                    Echo.Printf("success from {1}: {0}", $data, $filePath);
                    this.assertEquals(!ObjectValidator.IsEmptyOrNull($data), true);
                    $resolve();
                },
                ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                    Echo.Printf("error from {1}: {0}", $eventArgs, $filePath);
                    this.assertEquals(false, true);
                    $resolve();
                });
        });
    }

    public testError() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            JsonpFileReader.Load(
                "test/resource/data/Io/Oidis/Commons/InvalidData.jsonp",
                ($data : any, $filePath? : string) : void => {
                    Echo.Printf("success from {1}: {0}", $data, $filePath);
                    this.assertEquals(false, true);
                    $done();
                },
                ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                    Echo.Printf("error from {1}: {0}", $eventArgs, $filePath);
                    this.assertEquals(true, true);
                    $done();
                });
        };
    }

    public testTimeout() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            JsonpFileReader.Load(
                "https://localhost2.oidis.io/io-oidis-hub/Configuration/" +
                "io-oidis-commons/ValidData2",
                ($data : any, $filePath? : string) : void => {
                    Echo.Printf("success from {1}: {0}", $data, $filePath);
                    this.assertEquals(false, true);
                    $done();
                },
                ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                    Echo.Printf("error from {1}: {0}", $eventArgs, $filePath);
                    this.assertEquals(true, true);
                    $done();
                });
        };
    }

    public testDummy() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            JsonpFileReader.Load("test/resource/data/Io/Oidis/Commons/ValidData2.jsonp",
                ($data : any, $filePath? : string) : void => {
                    Echo.Printf("success from {1}: {0}", $data, $filePath);
                    this.assertEquals(!ObjectValidator.IsEmptyOrNull($data), true);
                    $done();
                },
                ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                    Echo.Printf("error from {1}: {0}", $eventArgs, $filePath);
                    this.assertEquals(false, true);
                    $done();
                });
        };
    }

    public testReadFromString() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            JsonpFileReader.LoadString("" +
                "JsonpData(\n" +
                "    {\n" +
                "        \"$interface\": \"IBasePageConfiguration\",\n" +
                "        \"title\": \"Oidis Services v{0}\",\n" +
                "        \"loading\": \"Loading, please wait ...\",\n" +
                "        \"loadingProgress\": \"Progress: {0}%\"\n" +
                "    }\n" +
                ");",
                ($data : any) : void => {
                    Echo.Printf("success: {0}", $data);
                    this.assertEquals(!ObjectValidator.IsEmptyOrNull($data), true);
                    $done();
                },
                ($eventArgs : ErrorEvent) : void => {
                    Echo.Printf("error from: {0}", $eventArgs);
                    this.assertEquals(false, true);
                    $done();
                });
        };
    }
}
/* dev:end */
