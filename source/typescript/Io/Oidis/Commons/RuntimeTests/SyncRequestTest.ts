/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { BaseHttpResolver } from "../HttpProcessor/Resolvers/BaseHttpResolver.js";
import { Echo } from "../Utils/Echo.js";

export class SyncRequestTest extends BaseHttpResolver {

    protected resolver() : void {
        Echo.Printf(this.getEnvironmentArgs());
        Echo.Printf(this.getRequest());
        Echo.Printf(this.RequestArgs());
    }

}
/* dev:end */
