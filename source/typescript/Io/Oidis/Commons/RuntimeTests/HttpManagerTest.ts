/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventType } from "../Enums/Events/EventType.js";
import { GeneralEventOwner } from "../Enums/Events/GeneralEventOwner.js";
import { AsyncRequestEventArgs } from "../Events/Args/AsyncRequestEventArgs.js";
import { HttpManager } from "../HttpProcessor/HttpManager.js";
import { IRuntimeTestPromise, RuntimeTestRunner } from "../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Index } from "../Index.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { Echo } from "../Utils/Echo.js";

export class HttpManagerTest extends RuntimeTestRunner {

    private object : HttpManager;

    public testResolveRequest() : void {
        this.object.OverrideResolver("/ServerError/Http/DefaultPage", "Http404NotFoundPage");
        this.object.OverrideResolver("/ServerError/Exception/{ErrorType}", "ExceptionErrorPage");
        const fileExtensions : string[] = [
            "*.htm", "*.html", "*.txt", "*.pdf",
            "*.min.css", "*.css",
            "*.min.js", "*.js", "*.json", "*.jsonp", "*.map", "*.ts",
            "*.bmp", "*.jpg", "*.jpe", "*.jpeg", "*.png", "*.gif", "*.ico", "*.icon", "*.svg",
            "*.woff", "*.woff2", "*.eot"
        ];
        for (const $extension of fileExtensions) {
            this.object.RegisterResolver("/**/" + $extension, "FileRequestResolver");
        }
        this.object.RegisterResolver("/Download/{fileId}", "FileDownloadResolver");

        this.assertEquals(this.object.getResolverClassName(
            "/resource/css/com-wui-framework-hub-2018-1-0-alpha.min.css"), "FileRequestResolver");
        this.assertEquals(this.object.getResolverClassName(
            "com-wui-framework-hub-2018-1-0-alpha.min.css"), "FileRequestResolver");
        this.assertEquals(this.object.getResolverClassName(
            "resource/css/com-wui-framework-hub-2018-1-0-alpha.min.css"), "FileRequestResolver");
        this.assertEquals(this.object.getResolverClassName("/ServerError/Http/DefaultPage"), "Http404NotFoundPage");
        this.assertEquals(this.object.getResolverClassName("/ServerError/Exception/1"), "ExceptionErrorPage");
        this.assertEquals(this.object.getResolverClassName("/Download/test.txt"), "FileDownloadResolver");
    }

    public testCreateLink() : void {
        this.assertEquals(this.createLink("/test"), "/com-wui-framework-commons/test",
            "absolute link '/test'");
        this.assertEquals(this.createLink("/com-wui-framework-commons/test"), "/com-wui-framework-commons/test",
            "absolute link '/com-wui-framework-commons/test'");
        this.assertEquals(this.createLink("com-wui-framework-commons/test"), "/com-wui-framework-commons/test",
            "relative link 'com-wui-framework-commons/test'");
        this.assertEquals(this.createLink("test"), "/com-wui-framework-commons/web/test",
            "relative link 'test'");
        this.assertEquals(this.createLink(""), this.getRequest().getUrl(),
            "current link by empty");
        this.assertEquals(this.createLink(null), this.getRequest().getUrl(),
            "current link by null");
        this.assertEquals(this.createLink(this.getRequest().getBaseUrl() + "test"), "/com-wui-framework-commons/web/test",
            "current http link with '/test'");
        this.assertEquals(this.createLink("http://www.wuiframework.com"), "http://www.wuiframework.com",
            "foreign link");
        this.assertEquals(this.createLink("https://www.wuiframework.com"), "https://www.wuiframework.com",
            "foreign https link");
        this.assertEquals(this.createLink("www.wuiframework.com"), "http://www.wuiframework.com",
            "foreign www link");
        this.assertEquals(this.createLink(this.createLink("/test")), "/com-wui-framework-commons/test",
            "recursive call test");
        this.assertEquals(this.createLink("//test"), "/com-wui-framework-commons/test",
            "double slash '//test'");
    }

    public testRegisterResolver() : void {
        this.object.RegisterResolver("1", "res");
        this.object.RegisterResolver("1/2", "res");
        this.object.RegisterResolver("1/2/3", "res");
        this.object.RegisterResolver("1/4/5/6", "res");
        this.object.RegisterResolver("1/3", "res");

        this.object.RegisterResolver("/7/8", "res");

        this.object.RegisterResolver("*6", "res");
        this.object.RegisterResolver("*7***8**9", "res");

        this.object.RegisterResolver("*", "res");
        this.object.RegisterResolver("*/1", "res");
        this.object.RegisterResolver("1*", "res");
        this.object.RegisterResolver("1*/*", "res");
        this.object.RegisterResolver("1*2", "res");
        this.object.RegisterResolver("1*2/34/*/5", "res");

        this.object.RegisterResolver("**/1", "res");
        this.object.RegisterResolver("**/1", "res3");
        this.object.OverrideResolver("**/**/1", "res2");

        this.object.RegisterResolver("test string", "res2");
        this.object.RegisterResolver("test", "result1");

        this.object.RegisterResolver("test234", "result2");
        this.object.RegisterResolver("test*2", "result2");
        this.object.RegisterResolver("test23", "result2");
        this.object.RegisterResolver("test2", "result2");

        this.object.RegisterResolver("**/TeSt3", "result3", false);
        this.object.RegisterResolver("**/Tes*4", "result4");
        this.object.RegisterResolver("**/**/**/Tes*5", "result5");
        this.object.RegisterResolver("**/**/**/Tes****6**", "result6");
        this.object.RegisterResolver("**/**/**/test7/**/*", "result7");
        this.object.RegisterResolver("**/**/**/test7/**/*/*", "result8");
        this.object.RegisterResolver("**/testStr/*", "result9");

        this.object.RegisterResolver("*test", "result10");
        this.object.RegisterResolver("tes*t", "result10");
        this.object.RegisterResolver("te*st", "result12");
        this.object.RegisterResolver("t*est", "result10");
        this.object.RegisterResolver("test*", "result11");

        this.object.RegisterResolver("/teststring", "result13");

        Echo.Printf(this.object.getResolversCollection());
    }

    public testOrderResolver() : void {
        this.object.RegisterResolver("/ServerError/Cookies", "ErrorPages.CookiesErrorPage");
        this.object.RegisterResolver("/ServerError/Browser", "ErrorPages.BrowserErrorPage");
        this.object.RegisterResolver("/ServerError/Http/Moved", "ErrorPages.Http301MovedPage");
        this.object.RegisterResolver("/ServerError/Http/Forbidden", "ErrorPages.Http403ForbiddenPage");
        this.object.RegisterResolver("/ServerError/Http/NotFound", "ErrorPages.Http404NotFoundPage");
        this.object.RegisterResolver("/ServerError/Exception/{ErrorType}", "ErrorPages.ExceptionErrorPage");
        this.object.RegisterResolver("/about/Cache", "Io.Oidis.Commons.PersistenceApi.CacheManager");
        this.object.RegisterResolver("/about/Env", "Io.Oidis.Commons.RuntimeTests.HttpRequestParserTest");
        this.object.RegisterResolver("/PersistenceManager", "Resolvers.PersistenceManager");

        this.object.RegisterResolver("/", Index);
        this.object.RegisterResolver("/index", Index);
        this.object.RegisterResolver("/web", Index);
        this.object.RegisterResolver("/web/*", "Io.Oidis.Gui.HttpProcessor.Resolvers.DrawGuiObject");
        this.object.RegisterResolver("/async/*", "Io.Oidis.Gui.HttpProcessor.Resolvers.AsyncDrawGuiObject");
        this.object.RegisterResolver("/RuntimeTest/*", "Io.Oidis.Gui.HttpProcessor.Resolvers.RuntimeTestRunner");

        Echo.Printf(this.object.getResolversCollection());
        this.object.getResolversCollection().getChildrenList().getFirst().OrderChildrenCollection();
        Echo.Printf(this.object.getResolversCollection());

        Echo.Printf(this.object.getResolverClassName("/index"));
    }

    public testGetResolverClassName() : void {
        this.object.RegisterResolver("/test", "result1");
        this.object.RegisterResolver("/*", "result3");

        this.object.RegisterResolver("/*t*e*st*", "result10");

        this.object.RegisterResolver("/*tes*t*", "result12");
        this.object.RegisterResolver("/*te*st*", "result4");
        this.object.RegisterResolver("/*t*est*", "result11");

        this.object.RegisterResolver("/*test*", "result5");

        this.object.RegisterResolver("/test*", "result7");
        this.object.RegisterResolver("/tes*t", "result9");
        this.object.RegisterResolver("/te*st", "result8");
        this.object.RegisterResolver("/*test", "result6");

        this.assertEquals(this.object.getResolverClassName("/test"), "result1");
        this.assertEquals(this.object.getResolverClassName("/web"), "result3");
        this.assertEquals(this.object.getResolverClassName("/2test"), "result6");
        this.assertEquals(this.object.getResolverClassName("/tes3t"), "result9");
        this.assertEquals(this.object.getResolverClassName("/4test4"), "result5");
        this.assertEquals(this.object.getResolverClassName("/te55st"), "result8");
        this.assertEquals(this.object.getResolverClassName("/6te6st6"), "result4");
        this.assertEquals(this.object.getResolverClassName("/7t77est7"), "result11");

        Echo.Printf(this.object.getResolversCollection());
    }

    public testGetResolverClassName2() : void {
        this.object.RegisterResolver("/web/test/test/test", "result1");
        this.object.RegisterResolver("/*/*/test", "result2");
        this.object.RegisterResolver("/web/*/test", "result3");
        // this.object.RegisterResolver("/web/*/test/*", "result8");
        this.object.RegisterResolver("/**/test", "result4");
        this.object.RegisterResolver("/**/*/test", "result5");
        this.object.RegisterResolver("/**/*/test/*", "result7");
        this.object.RegisterResolver("/web/{param1}/test/{param2}", "result6");

        this.assertEquals(this.object.getResolverClassName("/test/test/test"), "result2");
        this.assertEquals(this.object.getResolverClassName("/web/test/test"), "result3");
        this.assertEquals(this.object.getResolverClassName("/testtest1/testtest2/testtest3/testtest4/test"), "result5");
        this.assertEquals(this.object.getResolverClassName("/web/test/test/test"), "result1");
        this.assertEquals(this.object.getResolverClassName("/web/testparam1/test/testparam2"), "result6");

        Echo.Printf(this.object.getResolversCollection());
    }

    public testReloadTo() : void {
        Echo.Println("<div id=\"syncButton\" style=\"border: 1px solid red; color: red; width: 230px; cursor: pointer;\">" +
            "Reload to synchronously</div>");
        Echo.Println("<div id=\"asyncButton\" style=\"border: 1px solid red; color: red; width: 230px; cursor: pointer;\">" +
            "Reload to asynchronously</div>");
        Echo.Println("<div id=\"movedButton\" style=\"border: 1px solid red; color: red; width: 230px; cursor: pointer;\">" +
            "Http 301 Moved</div>");
        Echo.Println("<div id=\"forbiddenButton\" style=\"border: 1px solid red; color: red; width: 230px; cursor: pointer;\">" +
            "Http 403 Forbidden</div>");
        Echo.Println("<div id=\"notFoundButton\" style=\"border: 1px solid red; color: red; width: 230px; cursor: pointer;\">" +
            "Http 404 Not found</div>");
        Echo.Println("<div id=\"refreshButton\" style=\"border: 1px solid red; color: red; width: 230px; cursor: pointer;\">" +
            "Refresh page</div>");

        this.getEventsManager().setEvent(GeneralEventOwner.WINDOW, EventType.ON_ASYNC_REQUEST,
            ($args : AsyncRequestEventArgs) : void => {
                Echo.Println("Async load executed for: " + $args.Url());
            });

        this.getEventsManager().setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_START,
            ($args : AsyncRequestEventArgs) : void => {
                Echo.Println("Async request started at: " + $args.Url());
            });

        this.getEventsManager().setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_SUCCESS,
            ($args : AsyncRequestEventArgs) : void => {
                Echo.Println("Async success result:");
                Echo.Println($args.Result());
            });

        this.getEventsManager().FireAsynchronousMethod(() : void => {
            document.getElementById("syncButton").onclick = () : any => {
                const postData : ArrayList<string> = new ArrayList<string>();
                postData.Add("test post data", "testKey");
                this.getHttpManager().ReloadTo("/web/SynchronousRequestTest", postData);
            };
            document.getElementById("asyncButton").onclick = () : any => {
                const postData : ArrayList<string> = new ArrayList<string>();
                postData.Add("test post data", "testKey");
                this.getHttpManager().ReloadTo("/web/AsyncRequestTest", postData, true);
            };
            document.getElementById("movedButton").onclick = () : any => {
                this.getHttpManager().Return301Moved("/new/file/link");
            };
            document.getElementById("forbiddenButton").onclick = () : any => {
                this.getHttpManager().Return403Forbidden();
            };
            document.getElementById("notFoundButton").onclick = () : any => {
                this.getHttpManager().Return404NotFound();
            };
            document.getElementById("refreshButton").onclick = () : any => {
                this.getHttpManager().Refresh();
            };
        });
    }

    public testIsOnline() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            this.getHttpManager().IsOnline(($status : boolean) : void => {
                Echo.Printf("This application is online: {0}", $status);
                $done();
            });
        };
    }

    protected setUp() : void {
        this.object = new HttpManager(this.getRequest());
    }

    protected tearDown() : void {
        this.object = null;
    }
}

/* dev:end */
