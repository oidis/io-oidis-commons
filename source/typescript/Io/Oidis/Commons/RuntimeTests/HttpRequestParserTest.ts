/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { BrowserType } from "../Enums/BrowserType.js";
import { HttpRequestParser } from "../HttpProcessor/HttpRequestParser.js";
import { RuntimeTestRunner } from "../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Echo } from "../Utils/Echo.js";

export class HttpRequestParserTest extends RuntimeTestRunner {

    private object : HttpRequestParser;

    public testGetUrl() : void {
        Echo.Println(this.object.getUrl());
    }

    public testGetBaseUrl() : void {
        Echo.Println(this.object.getBaseUrl());
    }

    public testGetHostUrl() : void {
        Echo.Println(this.object.getHostUrl());
    }

    public testGetUrlArgs() : void {
        Echo.Printf(this.object.getUrlArgs());
    }

    public testIsOnServer() : void {
        Echo.Printf(this.object.IsOnServer());
    }

    public testGetBrowserType() : void {
        Echo.Println(BrowserType[this.object.getBrowserType()]);
    }

    public testGetBrowserVersion() : void {
        Echo.Printf(this.object.getBrowserVersion());
    }

    public testGetClientIP() : void {
        Echo.Println(this.object.getClientIP());
    }

    public testGetEtags() : void {
        Echo.Printf(this.object.getEtags());
    }

    public testGetLastModifiedTime() : void {
        Echo.Println(this.object.getLastModifiedTime());
    }

    public testGetScriptPath() : void {
        Echo.Println(this.object.getScriptPath());
    }

    public testGetRelativeDirectory() : void {
        Echo.Println(this.object.getRelativeDirectory());
    }

    public testGetRelativeRoot() : void {
        Echo.Println(this.object.getRelativeRoot());
    }

    public testToString() : void {
        Echo.Println(this.object.ToString());
    }

    protected setUp() : void {
        this.object = this.getRequest();
    }

    protected tearDown() : void {
        this.object = null;
    }
}
/* dev:end */
