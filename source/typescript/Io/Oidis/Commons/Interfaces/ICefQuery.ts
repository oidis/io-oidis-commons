/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "./IBaseObject.js";

export interface ICefQuery extends IBaseObject {

    /**
     * @param  {ICefQueryMessage} $message Data for cefQuery
     * @returns {number} Returns cefQuery id.
     */
    ($message : ICefQueryMessage) : number;
}

export interface ICefQueryMessage {
    request : string;
    persistent : boolean;
    onSuccess : ($response : string) => void;
    onFailure : ($code : number, $message? : string) => void;
}

// generated-code-start
export const ICefQuery = globalThis.RegisterInterface([], <any>IBaseObject);
export const ICefQueryMessage = globalThis.RegisterInterface(["request", "persistent", "onSuccess", "onFailure"]);
// generated-code-end
