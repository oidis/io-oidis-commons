/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "./IBaseObject.js";

export interface IEventArgs extends IBaseObject {
    Owner($owner? : any) : any;

    Type($value? : string) : string;

    NativeEventArgs($value? : Event) : Event;
}

// generated-code-start
export const IEventArgs = globalThis.RegisterInterface(["Owner", "Type", "NativeEventArgs"], <any>IBaseObject);
// generated-code-end
