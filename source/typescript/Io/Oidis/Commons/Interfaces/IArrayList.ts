/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "../Primitives/ArrayList.js";
import { IBaseObject } from "./IBaseObject.js";

export interface IArrayList<T> extends IBaseObject {
    Add($value : T, $key? : string | number) : void;

    getKeys() : Array<string | number>;

    getKey($value : T) : string | number;

    KeyExists($value : string | number) : boolean;

    Contains($value : T) : boolean;

    IndexOf($value : T) : number;

    getAll() : T[];

    getItem($key : string | number) : T;

    getFirst() : T;

    getLast() : T;

    Equal($compareWith : ArrayList<any>) : boolean;

    Length() : number;

    IsEmpty() : boolean;

    Clear() : void;

    Copy($copyFrom : ArrayList<T>, $count? : number,
         $clearBefore? : boolean) : void;

    RemoveAt($index : number) : void;

    RemoveLast() : void;

    SortByKeyUp() : void;

    SortByKeyDown() : void;

    foreach($handler : ($value : T, $key? : string | number) => void | boolean) : void;

    ToArray() : T[];
}

// generated-code-start
/* eslint-disable */
export const IArrayList = globalThis.RegisterInterface(["Add", "getKeys", "getKey", "KeyExists", "Contains", "IndexOf", "getAll", "getItem", "getFirst", "getLast", "Equal", "Length", "IsEmpty", "Clear", "Copy", "RemoveAt", "RemoveLast", "SortByKeyUp", "SortByKeyDown", "foreach", "ToArray"], <any>IBaseObject);
/* eslint-enable */
// generated-code-end
