/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "./IBaseObject.js";

export interface IException extends IBaseObject {
    Owner($value? : string | IBaseObject) : string;

    Message($value? : string) : string;

    Line($value? : number) : number;

    File($value? : string) : string;

    Code($value? : number) : number;

    Stack($value? : string) : string;
}

// generated-code-start
export const IException = globalThis.RegisterInterface(["Owner", "Message", "Line", "File", "Code", "Stack"], <any>IBaseObject);
// generated-code-end
