/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IEventArgs } from "./IEventArgs.js";

export interface IEventsHandler {
   
    (...$args : any[]) : void;

    ($eventArgs : IEventArgs, ...$args : any[]) : void;
}

// generated-code-start
export const IEventsHandler = globalThis.RegisterInterface([]);
// generated-code-end
