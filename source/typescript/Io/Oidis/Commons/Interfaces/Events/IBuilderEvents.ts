/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IWebServiceClientEvents } from "./IWebServiceClientEvents.js";

export interface IBuilderEvents extends IWebServiceClientEvents {
    OnBuildStart($eventHandler : () => void) : void;

    OnBuildComplete($eventHandler : () => void) : void;

    OnWarning($eventHandler : ($message? : string) => void) : void;

    OnFail($eventHandler : ($message? : string) => void) : void;

    OnMessage($eventHandler : ($message? : string) => void) : void;
}

// generated-code-start
/* eslint-disable */
export const IBuilderEvents = globalThis.RegisterInterface(["OnBuildStart", "OnBuildComplete", "OnWarning", "OnFail", "OnMessage"], <any>IWebServiceClientEvents);
/* eslint-enable */
// generated-code-end
