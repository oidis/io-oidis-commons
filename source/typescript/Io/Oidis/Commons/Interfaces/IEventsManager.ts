/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IArrayList } from "./IArrayList.js";
import { IBaseObject } from "./IBaseObject.js";
import { IEventArgs } from "./IEventArgs.js";
import { IEventsHandler } from "./IEventsHandler.js";

export interface IEventsManager extends IBaseObject {
    /**
     * @param {string} $owner Validate events subscribed to this owner value.
     * @param {string} $type Validate this type of subevents.
     * @returns {boolean} Returns true, if $owner and $type has been registered, otherwise false.
     */
    Exists($owner : string, $type : string) : boolean;

    /**
     * @returns {ArrayList<ArrayList<ArrayList<IEventsHandler>>>} Returns list of all registered events.
     */
    getAll() : IArrayList<IArrayList<IArrayList<IEventsHandler>>>;

    /**
     * @param {string} $owner Subscribed event to this owner value.
     * @param {string} $type Subscribe handler to this type of event.
     * @param {IEventsHandler} [$handler] Function suitable for handling of the event.
     * @param {IEventArgs} [$args] Set initial event args.
     * @returns {void}
     */
    setEvent($owner : string, $type : string, $handler : IEventsHandler, $args? : IEventArgs) : void;

    /**
     * @param {string} $owner Event args owner name.
     * @param {string} $type Event args type of.
     * @param {IEventArgs} $args Set current event args.
     * @returns {void}
     */
    setEventArgs($owner : string, $type : string, $args : IEventArgs) : void;

    FireEvent($owner : string, $type : string, $args? : IEventArgs) : void;

    FireEvent($owner : string, $type : string, $async? : boolean) : void;

    /**
     * @param {string} $owner Specify fired event owner.
     * @param {string} $type Specify fired type of event.
     * @param {IEventArgs} [$args] Specify event args for current event process.
     * @param {boolean} [$async=true] Specify, if event handlers can be execute asynchronously.
     * @returns {void}
     */
    FireEvent($owner : string, $type : string, $args? : IEventArgs, $async? : boolean) : void;

    FireEvent($owner : string, $type : string, $args? : any, $async? : boolean) : void;

    FireAsynchronousMethod($handler : () => void, $clearBefore? : boolean) : number;

    FireAsynchronousMethod($handler : () => void, $waitForMilliseconds? : number) : number;

    /**
     * @param {Function} $handler Function suitable for handling of the event.
     * @param {boolean} [$clearBefore=true] Clean up currently running thread with same handler.
     * @param {number} [$waitForMilliseconds] Specify wait time before execution of the handler in milliseconds.
     * @returns {number} Returns thread number allocated for asynchronous execution.
     */
    FireAsynchronousMethod($handler : () => void, $clearBefore? : boolean, $waitForMilliseconds? : number) : number;

    FireAsynchronousMethod($handler : () => void, $clearBefore? : any, $waitForMilliseconds? : number) : number;

    /**
     * @param {string} $owner Event owner name of subscribed handler.
     * @param {string} $type Event type of subscribed handler.
     * @param {IEventsHandler} $handler Handler, which should be removed.
     * @returns {void}
     */
    RemoveHandler($owner : string, $type : string, $handler : any) : void;

    /**
     * Clear all events, if owner and type has not been specified.
     * Clear subset of events subscribed to owner name, if owner has been specified.
     * Clear subset of events, which belong to owner and event type, if owner and type has been specified.
     * @param {string|number} [$owner] Specify desired event or thread owner.
     * @param {string} [$type] Specify desired type of event.
     * @returns {void}
     */
    Clear($owner? : string | number, $type? : string) : void;
}

// generated-code-start
/* eslint-disable */
export const IEventsManager = globalThis.RegisterInterface(["Exists", "getAll", "setEvent", "setEventArgs", "FireEvent", "FireAsynchronousMethod", "RemoveHandler", "Clear"], <any>IBaseObject);
/* eslint-enable */
// generated-code-end
