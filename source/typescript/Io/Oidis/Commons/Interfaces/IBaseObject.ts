/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IConverter } from "../DAO/Converters/BaseConverter.js";
import { IModelFindOptions, IModelSaveOptions } from "../Primitives/BaseModel.js";
import { IClassName, Interface } from "./Interface.js";
import { IValidator } from "./IValidator.js";

export interface IBaseObject {
    getClassName() : string;

    getNamespaceName() : string;

    getClassNameWithoutNamespace() : string;

    getClassHierarchy() : string[];

    getNamespaceHierarchy() : string[];

    getProperties() : string[];

    getMethods() : string[];

    IsTypeOf($className : IClassName | string) : boolean;

    IsMemberOf($className : IClassName | string) : boolean;

    Implements($className : Interface | string) : boolean;

    ToString($prefix? : string, $htmlTag? : boolean) : string;

    toString() : string;

    SerializationData() : object;

    getHash() : number;

    getUID() : string;

    ValidateProperties($property? : string, $verbose? : boolean) : boolean;

    ValidateDeep($property? : string, $verbose? : boolean) : string[];
}

export interface IModel extends IBaseObject {
    Validate($property? : string, $verbose ? : boolean) : boolean;

    ToJSON($options? : IObjectSerializerOptions) : Promise<any>;

    Save($options? : IModelSaveOptions) : Promise<void>;

    Update($property : string) : Promise<void>;

    Refresh($options? : IModelFindOptions) : Promise<void>;

    Remove($property? : string, $child? : string) : Promise<void>;

    Delete() : Promise<void>;

    getCollection() : string;

    getSymbolicName() : string;

    getDocument() : any;

    ToInterface($options? : IObjectSerializerOptions) : any;
}

declare interface ModelConstructor {
    new();
}

export interface ISchemaOptions {
    // TODO(mkelnar) tried to integrate TS compiler checks for supported types, if there will be discovered and issue then use "any"
    //  and remove ModelConstructor above. However, "any" will cause that also 123,true,{},... will be valid for compilation and maybe
    //  fail during schema validation or later.
    /**
     * Type could be String, Number, Date, Buffer, Boolean, BaseModel<>, [<of-supported-types>]
     * While Array type could be one of supported types (multidimensional arrays are possible too, but not recommended)
     */
    type? :
        ModelConstructor | BufferConstructor | StringConstructor | NumberConstructor | BooleanConstructor | DateConstructor |
        StringConstructor[] | NumberConstructor[] | BooleanConstructor[] | Date[] | ModelConstructor[] |
        BufferConstructor[];

    /**
     * Explicit definition of property value which will be initialized during construction.
     */
    default? : any;

    /**
     * If required flag is set to true then property needs to be not null during model save. Default value is true.
     */
    required? : boolean;

    /**
     * Same meaning as _id:true in mongoose schema definition. Default value is false.
     */
    addId? : boolean; // equivalent to _id:true in schema definition, default false

    /**
     * This is mongo schema alias, so property symbol defined in model could be accessed also by this alias.
     * It is not recommended to actively use this except really needed situations.
     * The alias option is now used by "Id" property which has symbol "_id" and alias set to "id".
     */
    alias? : string;
}

export interface IObjectProperty {
    name : string;
    symbol : string;
    fieldKey : string;
    validator : IValidator;
    noAutoListValidation : boolean;
    modifiers : IObjectModifiers;
    schemaOptions : ISchemaOptions;
    converter : IConverter;
    isInclusion : boolean;
}

export interface IObjectModifiers {
    /**
     * Exclude property from object serialization. (Could be overridden by ToJSON options)
     */
    noJSON? : boolean;
    /**
     * Referenced property data ID will be serialized instead of full property data
     */
    noSerialize? : boolean;
    /**
     * Property will be excluded from model resolver and ID will be returned instead. (Could be forced in Find invocation)
     */
    noResolve? : boolean;
}

export interface IObjectSerializerOptions {
    /**
     * Exclude object properties' data from serialization and use only referenced IDs. Exclusive option which suppress other ones.
     */
    idsOnly? : boolean;
    /**
     * Serialize all object properties. (applied also for properties in all referenced models)
     */
    expanded? : boolean;
}

export interface IContext {
    parent : IModel;
    property : IObjectProperty;
    dataTranslation? : boolean;
}

// generated-code-start
/* eslint-disable */
export const IBaseObject = globalThis.RegisterInterface(["getClassName", "getNamespaceName", "getClassNameWithoutNamespace", "getClassHierarchy", "getNamespaceHierarchy", "getProperties", "getMethods", "IsTypeOf", "IsMemberOf", "Implements", "ToString", "toString", "SerializationData", "getHash", "getUID", "ValidateProperties", "ValidateDeep"]);
export const IModel = globalThis.RegisterInterface(["Validate", "ToJSON", "Save", "Update", "Refresh", "Remove", "Delete", "getCollection", "getSymbolicName", "getDocument", "ToInterface"], <any>IBaseObject);
export const ISchemaOptions = globalThis.RegisterInterface(["type", "default", "required", "addId", "alias"]);
export const IObjectProperty = globalThis.RegisterInterface(["name", "symbol", "fieldKey", "validator", "noAutoListValidation", "modifiers", "schemaOptions", "converter", "isInclusion"]);
export const IObjectModifiers = globalThis.RegisterInterface(["noJSON", "noSerialize", "noResolve"]);
export const IObjectSerializerOptions = globalThis.RegisterInterface(["idsOnly", "expanded"]);
export const IContext = globalThis.RegisterInterface(["parent", "property", "dataTranslation"]);
/* eslint-enable */
// generated-code-end
