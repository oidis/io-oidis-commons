/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "./IBaseObject.js";

export interface IValidator extends IBaseObject {
    validate($value : any) : boolean;
}

// generated-code-start
export const IValidator = globalThis.RegisterInterface(["validate"], <any>IBaseObject);
// generated-code-end
