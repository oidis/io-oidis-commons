/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { NewLineType } from "../Enums/NewLineType.js";
import { IBaseObject } from "./IBaseObject.js";

export interface IOHandler extends IBaseObject {

    /**
     * Provides initialization, which is specific for each type of output handler.
     * @returns {void}
     */
    Init() : void;

    /**
     * @returns {string} Returns handler's name used as unique identifier for IO factory.
     */
    Name() : string;

    /**
     * @returns {string} Returns string representation of encoding specific for each type of handler.
     */
    Encoding() : string;

    /**
     * @returns {NewLineType} Returns type of line ending specific for each type of handler.
     */
    NewLineType() : NewLineType;

    /**
     * @param {string} $message Value which should be printed to resource handled by handler.
     * @returns {void}
     */
    Print($message : string) : void;

    /**
     * @param {Function} $handler Callback, which should be called in time on message print.
     * @returns {void}
     */
    setOnPrint($handler : ($message : string) => void) : void;

    /**
     * Clean up resource handled by handler.
     * @returns {void}
     */
    Clear() : void;
}

// generated-code-start
/* eslint-disable */
export const IOHandler = globalThis.RegisterInterface(["Init", "Name", "Encoding", "NewLineType", "Print", "setOnPrint", "Clear"], <any>IBaseObject);
/* eslint-enable */
// generated-code-end
