/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpResolversCollection } from "../HttpProcessor/HttpResolversCollection.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { IBaseObject } from "./IBaseObject.js";

export interface IHttpManager extends IBaseObject {
    RegisterResolver($httpRequest : string, $resolver : any, $ignoreCase? : boolean) : void;

    OverrideResolver($httpRequest : string, $resolver : any, $ignoreCase? : boolean) : void;

    getResolverClassName($requireLink : string) : string;

    getResolverParameters() : ArrayList<string>;

    getResolversCollection() : HttpResolversCollection;
}

// generated-code-start
/* eslint-disable */
export const IHttpManager = globalThis.RegisterInterface(["RegisterResolver", "OverrideResolver", "getResolverClassName", "getResolverParameters", "getResolversCollection"], <any>IBaseObject);
/* eslint-enable */
// generated-code-end
