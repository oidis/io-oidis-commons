/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { AsyncRequestEventArgs } from "../Events/Args/AsyncRequestEventArgs.js";
import { HttpRequestEventArgs } from "../Events/Args/HttpRequestEventArgs.js";
import { IBaseObject } from "./IBaseObject.js";

export interface IHttpRequestResolver extends IBaseObject {
    RequestArgs($value? : HttpRequestEventArgs | AsyncRequestEventArgs) : HttpRequestEventArgs | AsyncRequestEventArgs;

    Process() : void;
}

// generated-code-start
export const IHttpRequestResolver = globalThis.RegisterInterface(["RequestArgs", "Process"], <any>IBaseObject);
// generated-code-end
