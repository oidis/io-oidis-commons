/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IWebServiceConfigurationProtocol {
    location : string;
    responseUrl : string;
    address : string;
    port : number;
    base : string;
    protocol : string;
    timeout : number;
    version : string;
}

// generated-code-start
/* eslint-disable */
export const IWebServiceConfigurationProtocol = globalThis.RegisterInterface(["location", "responseUrl", "address", "port", "base", "protocol", "timeout", "version"]);
/* eslint-enable */
// generated-code-end
