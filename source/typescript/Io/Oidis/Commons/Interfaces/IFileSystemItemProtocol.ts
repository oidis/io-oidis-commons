/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { FileSystemItemType } from "../Enums/FileSystemItemType.js";

export interface IFileSystemItemProtocol {
    name : string;
    type : string | FileSystemItemType;
    value : string;
    attributes : Array<string | FileSystemItemType>;
    map : IFileSystemItemProtocol[] | string;
}

export interface IBaseFileSystemItem {
    name : string;
    type : string | FileSystemItemType;
}

export interface IFileSystemFileProtocol extends IBaseFileSystemItem {
    attributes : Array<string | FileSystemItemType>;
}

export interface IFileSystemDirectoryProtocol extends IFileSystemFileProtocol {
    map : Array<IFileSystemDirectoryProtocol | IFileSystemFileProtocol> | string;
}

export interface IFileSystemDriveProtocol extends IBaseFileSystemItem {
    value : string;
    map : Array<IFileSystemDirectoryProtocol | IFileSystemFileProtocol> | string;
}

// generated-code-start
export const IFileSystemItemProtocol = globalThis.RegisterInterface(["name", "type", "value", "attributes", "map"]);
export const IBaseFileSystemItem = globalThis.RegisterInterface(["name", "type"]);
export const IFileSystemFileProtocol = globalThis.RegisterInterface(["attributes"], <any>IBaseFileSystemItem);
export const IFileSystemDirectoryProtocol = globalThis.RegisterInterface(["map"], <any>IFileSystemFileProtocol);
export const IFileSystemDriveProtocol = globalThis.RegisterInterface(["value", "map"], <any>IBaseFileSystemItem);
// generated-code-end
