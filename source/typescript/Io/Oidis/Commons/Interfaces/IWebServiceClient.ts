/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Exception } from "../Exceptions/Type/Exception.js";
import { IWebServiceClientEvents } from "./Events/IWebServiceClientEvents.js";
import { IBaseObject } from "./IBaseObject.js";

export interface IWebServiceClient extends IBaseObject {

    /**
     * @returns {number} Returns client's id, which can be used for events handling.
     */
    getId() : number;

    /**
     * @returns {string} Returns target service url.
     */
    getServerUrl() : string;

    /**
     * @param {Function} $handler Specify async handler for handling of server path data.
     * @returns {void}
     */
    getServerPath($handler : ($path : string) => void) : void;

    /**
     * @returns {IWebServiceClientEvents} Returns events manager interface.
     */
    getEvents() : IWebServiceClientEvents;

    /**
     * Start Communication with target service
     * @returns {boolean} Returns true, if communication has been started, otherwise false.
     */
    StartCommunication() : void;

    /**
     * Stop Communication with target service
     * @returns {boolean} Returns true, if communication has been stopped, otherwise false.
     */
    StopCommunication() : void;

    /**
     * @returns {boolean} Returns true, if communication with target service is live, otherwise false.
     */
    CommunicationIsRunning() : boolean;

    /**
     * @param {string} $data Specify request command.
     * @param {IWebServiceResponseHandler} [$handler] Specify response handler.
     * @returns {void}
     */
    Send($data : any, $handler? : IWebServiceResponseHandler) : void;

    /**
     * @param {Function} $formatter Specify request formatter function
     * @returns {void}
     */
    setRequestFormatter($formatter : ($data : IWebServiceRequestFormatterData) => void) : void;

    /**
     * @param {Function} $formatter Specify response formatter function
     * @returns {void}
     */
    setResponseFormatter($formatter : ($data : any, $owner : IWebServiceClient,
                                       $onSuccess : ($value : any, $key? : number) => void,
                                       $onError : ($message : string | Error | Exception) => void) => void) : void;
}

/**
 * @param {any} $response Specify response function
 * @returns {void}
 */
export type IWebServiceResponseHandler = ($response? : any) => void;

export interface IWebServiceRequestFormatterData {
    value : any;
    key : number;
}

// generated-code-start
/* eslint-disable */
export const IWebServiceClient = globalThis.RegisterInterface(["getId", "getServerUrl", "getServerPath", "getEvents", "StartCommunication", "StopCommunication", "CommunicationIsRunning", "Send", "setRequestFormatter", "setResponseFormatter"], <any>IBaseObject);
export const IWebServiceRequestFormatterData = globalThis.RegisterInterface(["value", "key"]);
/* eslint-enable */
// generated-code-end
