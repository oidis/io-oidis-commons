/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "./IBaseObject.js";

export interface IJxbrowserBridge extends IBaseObject {
    send($clientId : number, $data : string) : void;
}

// generated-code-start
export const IJxbrowserBridge = globalThis.RegisterInterface(["send"], <any>IBaseObject);
// generated-code-end
