/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IArrayList } from "./IArrayList.js";
import { IBaseObject } from "./IBaseObject.js";

export interface IPersistenceHandler extends IBaseObject {
    getSessionId() : string;

    getSize() : number;

    getLoadTime() : number;

    getRawData($asyncHandler? : ($data : string) => void) : string;

    LoadPersistenceAsynchronously($asyncHandler : () => void, $sourceFilePath? : string) : void;

    DisableCRC() : void;

    ExpireTime($value? : string | number) : number;

    Variable($name : string | IArrayList<any>, $value? : any, $asyncHandler? : () => void) : any;

    Exists($name : string) : boolean;

    Destroy($name : string | IArrayList<string>) : void;

    Clear() : void;
}

// generated-code-start
/* eslint-disable */
export const IPersistenceHandler = globalThis.RegisterInterface(["getSessionId", "getSize", "getLoadTime", "getRawData", "LoadPersistenceAsynchronously", "DisableCRC", "ExpireTime", "Variable", "Exists", "Destroy", "Clear"], <any>IBaseObject);
/* eslint-enable */
// generated-code-end
