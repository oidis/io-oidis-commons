/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "./IBaseObject.js";
import { IClassName, Interface } from "./Interface.js";

export interface IReflection extends IBaseObject {
    getAllClasses() : string[];

    Exists($className : string) : boolean;

    getClass($className : string) : any;

    IsInstanceOf($instance : IBaseObject, $className : string | IClassName) : boolean;

    ClassHasInterface($className : string | IClassName, $interface : string | Interface) : boolean;

    IsMemberOf($instance : any, $className? : string | IClassName) : boolean;

    Implements($instance : any, $className? : Interface | string) : boolean;
}

// generated-code-start
/* eslint-disable */
export const IReflection = globalThis.RegisterInterface(["getAllClasses", "Exists", "getClass", "IsInstanceOf", "ClassHasInterface", "IsMemberOf", "Implements"], <any>IBaseObject);
/* eslint-enable */
// generated-code-end
