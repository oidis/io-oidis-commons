/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientEventType } from "../Enums/Events/WebServiceClientEventType.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { ErrorEventArgs } from "../Events/Args/ErrorEventArgs.js";
import { EventsManager } from "../Events/EventsManager.js";
import { ExceptionsManager } from "../Exceptions/ExceptionsManager.js";
import { Exception } from "../Exceptions/Type/Exception.js";
import { IBuilderEvents } from "../Interfaces/Events/IBuilderEvents.js";
import { IWebServiceClientEvents } from "../Interfaces/Events/IWebServiceClientEvents.js";
import { IWebServiceClient, IWebServiceRequestFormatterData, IWebServiceResponseHandler } from "../Interfaces/IWebServiceClient.js";
import { Loader } from "../Loader.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { Echo } from "../Utils/Echo.js";
import { LogIt } from "../Utils/LogIt.js";
import { ObjectDecoder } from "../Utils/ObjectDecoder.js";
import { ObjectEncoder } from "../Utils/ObjectEncoder.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { StringUtils } from "../Utils/StringUtils.js";
import { WebServiceClientFactory } from "../WebServiceApi/WebServiceClientFactory.js";

/**
 * BuilderConnector class provides bidirectional interface to instance of Oidis Builder server.
 */
export class BuilderConnector extends BaseObject {
    private static singleton : BuilderConnector;
    private subscribers : ArrayList<ArrayList<any>>;
    private readonly client : IWebServiceClient;

    /**
     * @returns {BuilderConnector} Returns singleton of connector instance.
     */
    public static Connect() : BuilderConnector {
        if (!ObjectValidator.IsSet(BuilderConnector.singleton)) {
            BuilderConnector.singleton = new BuilderConnector();
        }
        return BuilderConnector.singleton;
    }

    /**
     * @param {boolean} [$reconnect=true] Specify, if connection should be automatically reconnected in case of
     * lose of the connection or unavailability of Oidis Builder server.
     */
    constructor($reconnect : boolean = true) {
        super();

        this.subscribers = new ArrayList<ArrayList<any>>();

        this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
            "resource/data/Io/Oidis/Builder/connector.config.jsonp");

        if (!ObjectValidator.IsEmptyOrNull(this.client)) {
            this.client.setRequestFormatter(($data : IWebServiceRequestFormatterData) : void => {
                if (ObjectValidator.IsObject($data.value)) {
                    $data.value.id = StringUtils.getCrc(this.getUID());
                    $data.value.status = 201;
                    $data.key = $data.value.id;
                }
            });

            this.client.setResponseFormatter(($data : any, $owner : IWebServiceClient,
                                              $onSuccess : ($value : any, $key? : number) => void,
                                              $onError : ($message : string | Error | Exception) => void) : void => {
                if (ObjectValidator.IsString($data)) {
                    $onSuccess($data);
                } else {
                    if ($data.status === 200) {
                        $onSuccess($data, $data.id);
                    } else if ($data.status === 500) {
                        if ($data.type === "Server.Exception") {
                            $onError(new Exception(ObjectDecoder.Base64($data.data)));
                        } else if ($data.type === "Server.Timeout") {
                            EventsManager.getInstanceSingleton().FireEvent("" + $owner.getId(), WebServiceClientEventType.ON_TIMEOUT);
                        } else if ($data.type === "Request.Exception") {
                            $onError(ObjectDecoder.Base64(<string>$data.data));
                        } else {
                            $onError("Unsupported response error type: " + $data.type);
                        }
                    } else if ($data.status !== 201) {
                        $onError("Unsupported response status: " + $data.status);
                    }
                }
            });

            if ($reconnect) {
                this.client.getEvents().OnClose(() : void => {
                    this.client.StartCommunication();
                });
                this.client.getEvents().OnTimeout(() : void => {
                    this.client.StartCommunication();
                });
            }
            this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                if (this.client.CommunicationIsRunning()) {
                    if (Loader.getInstance().getEnvironmentArgs().IsProductionMode()) {
                        ExceptionsManager.Throw(BuilderConnector.ClassName(), $eventArgs.Exception());
                    } else {
                        LogIt.Error($eventArgs.Exception().ToString("", false));
                        Echo.Printf($eventArgs.Exception());
                    }
                }
            });
        } else {
            ExceptionsManager.Throw(BuilderConnector.ClassName(),
                "Client for Oidis Builder service is not supported by runtime environment.");
        }
    }

    /**
     * @returns {number} Returns connector id suitable for events handling and connectors factory.
     */
    public getId() : number {
        return this.client.getId();
    }

    /**
     * @returns {IBuilderEvents} Returns events interface connected with connector instance.
     */
    public getEvents() : IBuilderEvents {
        const superEvents : IWebServiceClientEvents = this.client.getEvents();
        this.extendClientEvents(superEvents, "OnBuildStart");
        this.extendClientEvents(superEvents, "OnBuildComplete");
        this.extendClientEvents(superEvents, "OnWarning", "OnBuilderWarning");
        this.extendClientEvents(superEvents, "OnFail", "OnBuilderFail");
        this.extendClientEvents(superEvents, "OnMessage");
        return <IBuilderEvents>superEvents;
    }

    /**
     * @param {string} $type Specify data type value.
     * @param {object} $data Specify data, which should be send to Oidis Builder server.
     * @param {IWebServiceResponseHandler} [$handler] Specify handler, which should be used for handling of response data.
     * @returns {void}
     */
    public Send($type : string, $data : any, $handler? : IWebServiceResponseHandler) : void {
        this.client.Send(this.getProtocol($type, $data), $handler);
    }

    /**
     * @param {string} $name Specify event name value.
     * @param {IWebServiceResponseHandler} [$handler] Specify handler, which should be used for handling of event data.
     * @returns {void}
     */
    public AddEventListener($name : string, $handler : IWebServiceResponseHandler) : void {
        this.client.Send(
            this.getProtocol("AddEventListener", {
                name: $name
            }),
            ($data : any) : void => {
                if ($data.type === "AddEventListener") {
                    if (ObjectDecoder.Base64($data.data) === "true") {
                        if (!this.subscribers.KeyExists($name)) {
                            this.subscribers.Add(new ArrayList<any>(), $name);
                        }
                        this.subscribers.getItem($name).Add($handler, $data.id);
                    }
                } else if ($data.type === "FireEvent") {
                    const data : any = JSON.parse(ObjectDecoder.Base64($data.data));
                    if (this.subscribers.KeyExists(data.name)) {
                        const handlers : ArrayList<any> = this.subscribers.getItem(data.name);
                        handlers.foreach(($handler : any, $id : number) : void => {
                            if ($id === $data.id) {
                                $handler(JSON.parse(ObjectDecoder.Base64(data.args)));
                            }
                        });
                    }
                }
            });
    }

    /**
     * @param {string} $name Specify event name value, which should be removed from events pool.
     * @returns {void}
     */
    public RemoveEventListener($name : string) : void {
        if (this.subscribers.KeyExists($name)) {
            this.subscribers.RemoveAt(this.subscribers.getKeys().indexOf($name));
        }
    }

    private getProtocol($type : string, $data : any) : any {
        return {
            data: ObjectEncoder.Base64(JSON.stringify($data)),
            type: $type
        };
    }

    private extendClientEvents($container : IWebServiceClientEvents, $eventType : string, $interfaceName? : string) : void {
        if (!ObjectValidator.IsSet($interfaceName)) {
            $interfaceName = $eventType;
        }
        $container[$eventType] = ($handler : IWebServiceResponseHandler) : void => {
            this.AddEventListener($interfaceName, $handler);
        };
    }
}
