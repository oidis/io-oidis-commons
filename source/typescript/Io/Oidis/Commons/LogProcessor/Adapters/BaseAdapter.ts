/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IOHandlerType } from "../../Enums/IOHandlerType.js";
import { LogLevel } from "../../Enums/LogLevel.js";
import { NewLineType } from "../../Enums/NewLineType.js";
import { IOHandler } from "../../Interfaces/IOHandler.js";
import { IOHandlerFactory } from "../../IOApi/IOHandlerFactory.js";
import { BaseObject } from "../../Primitives/BaseObject.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { Property } from "../../Utils/Property.js";
import { StringUtils } from "../../Utils/StringUtils.js";
import { ITraceBackEntry } from "../../Utils/TraceBack.js";
import { ILoggerTrace } from "../Logger.js";

export abstract class BaseAdapter extends BaseObject {
    protected debug : boolean;
    protected verbose : boolean;
    private output : IOHandler;
    private handlerType : IOHandlerType;
    private owner : any;
    private enabled : boolean;

    protected constructor() {
        super();
        this.enabled = true;
        this.setHandler(IOHandlerFactory.getHandler(IOHandlerType.CONSOLE));
    }

    public getConfigSHA() : string {
        return StringUtils.getSha1(this.HtmlSupported() + "" + <string>this.NewLineType());
    }

    public HtmlSupported() : boolean {
        return false;
    }

    public NewLineType() : NewLineType {
        return this.output.NewLineType();
    }

    public setModes($debug : boolean, $verbose : boolean) : void {
        this.debug = Property.Boolean(this.debug, $debug);
        this.verbose = Property.Boolean(this.verbose, $verbose);
    }

    public Enabled($value? : boolean) : boolean {
        return this.enabled = Property.Boolean(this.enabled, $value);
    }

    public Process($trace : ILoggerTrace) : void {
        if (!ObjectValidator.IsEmptyOrNull($trace.message) && this.enabled) {
            try {
                $trace.time = this.timeToString(<number>$trace.time);
                $trace.entryPoint = this.traceBackToString(<ITraceBackEntry>$trace.entryPoint);
                this.print(this.formatter($trace), this.getHandler());
            } catch (ex) {
                console.error(ex); // eslint-disable-line no-console
            }
        }
    }

    public setOwner($value : any) : void {
        this.owner = $value;
    }

    protected setHandler($handler : IOHandler) : void {
        if (!ObjectValidator.IsEmptyOrNull($handler)) {
            this.output = $handler;
            this.output.Init();
            this.handlerType = IOHandlerFactory.getHandlerType(this.output);
        }
    }

    protected getHandler() : IOHandler {
        return this.output;
    }

    protected getOwner() : any {
        return this.owner;
    }

    protected timeToString($value : number) : string {
        return new Date($value).toLocaleTimeString([], {
            hour12  : false,
            timeZone: "UTC",
            hour    : "2-digit",
            minute  : "2-digit",
            second  : "2-digit"
        });
    }

    protected traceBackToString($traceback : ITraceBackEntry) : string {
        let trace : string = "";
        if (this.verbose && !ObjectValidator.IsEmptyOrNull($traceback)) {
            if ($traceback.at !== "undefined" && $traceback.at !== "anonymous") {
                trace = $traceback.at;
            }
            if (!ObjectValidator.IsEmptyOrNull(trace)) {
                trace = "> " + trace + this.NewLineType();
            }
            if ($traceback.from !== "undefined" && $traceback.from !== "anonymous") {
                if (!ObjectValidator.IsEmptyOrNull(trace)) {
                    trace += "> from ";
                } else {
                    trace += "> ";
                }
                trace += $traceback.from + this.NewLineType();
            }
        }
        return trace;
    }

    protected formatter($trace : ILoggerTrace) : ILoggerTrace {
        if (this.debug) {
            $trace.message = StringUtils.Format(
                this.verbose && !ObjectValidator.IsEmptyOrNull($trace.entryPoint) ? "{2}[{0}][{1}] {3}" : "[{0}][{1}] {3}",
                $trace.time,
                LogLevel[$trace.level],
                $trace.entryPoint,
                $trace.message);
        }
        return $trace;
    }

    protected print($trace : ILoggerTrace, $handler : IOHandler) : void {
        $handler.Print($trace.message);
    }
}
