/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "../../Enums/LogLevel.js";
import { IOHandler } from "../../Interfaces/IOHandler.js";
import { ISentryConfiguration } from "../../Interfaces/IProject.js";
import { Loader } from "../../Loader.js";
import { isBrowser } from "../../Utils/EnvironmentHelper.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { StringUtils } from "../../Utils/StringUtils.js";
import { ITraceBackEntry } from "../../Utils/TraceBack.js";
import { ILoggerTrace } from "../Logger.js";
import { BaseAdapter } from "./BaseAdapter.js";

declare const Sentry : any;

export class SentryAdapter extends BaseAdapter {
    private context : string;

    constructor() {
        super();
        this.context = "";
    }

    public setContext($value : string) : void {
        this.context = $value;
    }

    protected getHandler() : IOHandler {
        const handler : IOHandler = super.getHandler();
        const config : ISentryConfiguration = Loader.getInstance().getEnvironmentArgs().getProjectConfig().sentry;
        if (!ObjectValidator.IsEmptyOrNull(config.dsn)) {
            const profile : any = {
                debug         : config.debug,
                dsn           : config.dsn,
                maxValueLength: config.maxMessageLength,
                release       : Loader.getInstance().getEnvironmentArgs().getProjectVersion()
            };
            if (config.tracesSampleRate > 0) {
                profile.tracesSampleRate = config.tracesSampleRate;
            }
            if (!isBrowser) {
                globalThis.Sentry = require("@sentry/node");
            } else if (!ObjectValidator.IsEmptyOrNull(config.tunnel)) {
                profile.tunnel = config.tunnel;
            }
            if (!ObjectValidator.IsEmptyOrNull(config.environment) && config.environment !== "none") {
                profile.environment = config.environment;
            }
            Sentry.init(profile);
            Sentry.setTag("version", Loader.getInstance().getEnvironmentArgs().getProjectVersion());
            for (const tagName in config.tags) {
                if (tagName.toLowerCase() !== "version" && config.tags.hasOwnProperty(tagName)) {
                    Sentry.setTag(tagName, config.tags[tagName]);
                }
            }
        } else {
            this.Enabled(false);
        }
        this.getHandler = () : IOHandler => {
            return handler;
        };
        return handler;
    }

    protected timeToString($value : number) : string {
        return <any>$value;
    }

    protected traceBackToString($traceback : ITraceBackEntry) : any {
        return $traceback;
    }

    protected formatter($trace : ILoggerTrace) : ILoggerTrace {
        return $trace;
    }

    protected print($trace : ILoggerTrace) : void {
        if (!(<any>$trace).isForwarded && ($trace.level === LogLevel.ERROR || $trace.level === LogLevel.WARNING) && this.Enabled()) {
            Sentry.withScope(($scope : any) : void => {
                if (!ObjectValidator.IsEmptyOrNull(this.context)) {
                    /// TODO: user can be described also by email and username, but context should be anonymous
                    $scope.setUser({
                        id: this.context
                    });
                }
                if ($trace.level === LogLevel.WARNING) {
                    $scope.setLevel("warning");
                    Sentry.captureMessage($trace.message);
                } else {
                    if (StringUtils.ContainsIgnoreCase($trace.message, "\n")) {
                        const error : Error = new Error($trace.message.split("\n")[0]);
                        error.stack = $trace.message;
                        Sentry.captureException(error);
                    } else {
                        $scope.setLevel("error");
                        Sentry.captureMessage($trace.message);
                    }
                }
            });
        }
    }
}
