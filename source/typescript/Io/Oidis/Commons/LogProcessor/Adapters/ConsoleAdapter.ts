/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BrowserType } from "../../Enums/BrowserType.js";
import { IOHandlerType } from "../../Enums/IOHandlerType.js";
import { LogLevel } from "../../Enums/LogLevel.js";
import { LogSeverity } from "../../Enums/LogSeverity.js";
import { IOHandlerFactory } from "../../IOApi/IOHandlerFactory.js";
import { Loader } from "../../Loader.js";
import { Property } from "../../Utils/Property.js";
import { StringUtils } from "../../Utils/StringUtils.js";
import { ILoggerTrace } from "../Logger.js";
import { BaseAdapter } from "./BaseAdapter.js";

export class ConsoleAdapter extends BaseAdapter {
    private readonly stripTags : boolean;
    private withColors : boolean;

    constructor($withHtmlTags : boolean = false) {
        super();
        this.setHandler(IOHandlerFactory.getHandler(IOHandlerType.CONSOLE));
        this.stripTags = !$withHtmlTags;
        this.withColors = true;
    }

    public ColorsEnabled($value? : boolean) : boolean {
        return this.withColors = Property.Boolean(this.withColors, $value);
    }

    protected formatter($trace : ILoggerTrace) : ILoggerTrace {
        super.formatter($trace);
        if (this.stripTags) {
            $trace.message = StringUtils.Replace($trace.message, StringUtils.NewLine(), <string>this.NewLineType());
            $trace.message = StringUtils.StripTags($trace.message);
        }
        return $trace;
    }

    protected print($trace : ILoggerTrace, $handler : any) : void {
        switch ($trace.level) {
        case LogLevel.INFO:
            if (this.colorSupport()) {
                if ($trace.severity === LogSeverity.LOW) {
                    $handler.debug("%c" + $trace.message, "color: silver");
                } else if ($trace.severity === LogSeverity.MEDIUM) {
                    $handler.debug("%c" + $trace.message, "color: gray");
                } else {
                    $handler.info($trace.message);
                }
            } else {
                if ($trace.severity !== LogSeverity.HIGH) {
                    $handler.debug($trace.message);
                } else {
                    $handler.info($trace.message);
                }
            }
            break;
        case LogLevel.WARNING:
            $handler.warn($trace.message);
            break;
        case LogLevel.ERROR:
            $handler.error($trace.message);
            break;
        case LogLevel.DEBUG:
        case LogLevel.VERBOSE:
            $handler.debug($trace.message);
            break;
        default:
            $handler.log($trace.message);
            break;
        }
    }

    protected getHandler() : any {
        return (<any>super.getHandler()).getHandler();
    }

    private colorSupport() : boolean {
        if (this.withColors) {
            const browser : BrowserType = Loader.getInstance().getHttpManager().getRequest().getBrowserType();
            const supported : boolean = browser === BrowserType.FIREFOX || browser === BrowserType.GOOGLE_CHROME;
            this.colorSupport = () : boolean => {
                return supported;
            };
            return supported;
        }
        return false;
    }
}
