/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IOHandlerType } from "../../Enums/IOHandlerType.js";
import { LogLevel } from "../../Enums/LogLevel.js";
import { LogSeverity } from "../../Enums/LogSeverity.js";
import { IOHandlerFactory } from "../../IOApi/IOHandlerFactory.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { StringUtils } from "../../Utils/StringUtils.js";
import { ITraceBackEntry } from "../../Utils/TraceBack.js";
import { ILoggerTrace } from "../Logger.js";
import { BaseAdapter } from "./BaseAdapter.js";

export class HTMLAdapter extends BaseAdapter {

    constructor() {
        super();
        this.setHandler(IOHandlerFactory.getHandler(IOHandlerType.HTML_ELEMENT, "Content"));
    }

    public HtmlSupported() : boolean {
        return true;
    }

    protected traceBackToString($traceback : ITraceBackEntry) : string {
        let trace : string = "";
        if (this.verbose) {
            const getUrl : any = ($value : string) : string => {
                let method : string = $value;
                let file : string = "";
                let url : string = $value;
                if (StringUtils.OccurrenceCount(url, " ") >= 1) {
                    file = StringUtils.Substring(url, StringUtils.IndexOf(url, " ") + 1);
                    url = file;
                    method = StringUtils.Remove($value, " " + url);
                }
                if (StringUtils.OccurrenceCount(url, ":") > 1) {
                    url = StringUtils.Substring(url, 0, StringUtils.IndexOf(url, ":", false));
                }
                if (StringUtils.OccurrenceCount(url, ":") > 1) {
                    url = StringUtils.Substring(url, 0, StringUtils.IndexOf(url, ":", false));
                }
                return method + " <a href=\"" + url + "\" target=\"_blank\">" + file + "</a>";
            };
            if ($traceback.at !== "undefined" && $traceback.at !== "anonymous") {
                trace = getUrl($traceback.at);
            }
            if (!ObjectValidator.IsEmptyOrNull(trace)) {
                trace = "> " + trace + this.NewLineType();
            }

            if ($traceback.from !== "undefined" && $traceback.from !== "anonymous") {
                if (!ObjectValidator.IsEmptyOrNull(trace)) {
                    trace += "> from ";
                } else {
                    trace += "> ";
                }
                trace += getUrl($traceback.from) + this.NewLineType();
            }
            trace = "<i>" + trace + "</i>";
        }
        return trace;
    }

    protected formatter($trace : ILoggerTrace) : ILoggerTrace {
        if (this.debug) {
            $trace.message = StringUtils.Format(
                this.verbose && !ObjectValidator.IsEmptyOrNull($trace.entryPoint) ? "{2}[{0}][{1}]{3}" : "[{0}][{1}]{3}",
                $trace.time,
                "<b>" + LogLevel[$trace.level] + "</b>",
                $trace.entryPoint,
                StringUtils.NewLine() + $trace.message
            );
        }
        $trace.message += StringUtils.NewLine();

        let color : string = "";
        if ($trace.level === LogLevel.ERROR) {
            color = "red";
        } else if ($trace.level === LogLevel.WARNING) {
            color = "orange";
        } else if ($trace.level === LogLevel.INFO) {
            if ($trace.severity === LogSeverity.LOW) {
                color = "silver";
            } else if ($trace.severity === LogSeverity.MEDIUM) {
                color = "gray";
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(color)) {
            $trace.message = "<span style=\"color: " + color + "\">" + $trace.message + "</span>";
        }
        return $trace;
    }
}
