/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogAdapterType } from "../Enums/LogAdapterType.js";
import { LogLevel } from "../Enums/LogLevel.js";
import { LogSeverity } from "../Enums/LogSeverity.js";
import { NewLineType } from "../Enums/NewLineType.js";
import { ILoggerConfiguration } from "../Interfaces/IProject.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { Convert } from "../Utils/Convert.js";
import { JsonUtils } from "../Utils/JsonUtils.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { Property } from "../Utils/Property.js";
import { Reflection } from "../Utils/Reflection.js";
import { StringUtils } from "../Utils/StringUtils.js";
import { ITraceBackEntry, StackTraceCallSite, TraceBack } from "../Utils/TraceBack.js";
import { BaseAdapter } from "./Adapters/BaseAdapter.js";
import { ConsoleAdapter } from "./Adapters/ConsoleAdapter.js";
import { HTMLAdapter } from "./Adapters/HTMLAdapter.js";
import { SentryAdapter } from "./Adapters/SentryAdapter.js";

export class Logger extends BaseObject {
    protected debug : boolean;
    protected verbose : boolean;
    private adapters : BaseAdapter[];
    private level : LogLevel;
    private severity : LogSeverity;
    private anonymizer : any;
    private trackBackEntryPoint : any;

    constructor() {
        super();
        this.debug = false;
        this.verbose = false;
        this.setAdapters(this.buildInAdapters().map(($type : LogAdapterType) : BaseAdapter => {
            return this.getAdapter($type);
        }));
        this.enableAdapters();
        this.setLevels(LogLevel.INFO, LogSeverity.HIGH);
    }

    public setConfig($value : ILoggerConfiguration) : void {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            const config : ILoggerConfiguration = JsonUtils.Clone($value);
            if (!ObjectValidator.IsEmptyOrNull(config.level)) {
                if (config.level > 6) {
                    config.level = 1;
                }
                if (config.level <= 0) {
                    config.level = -1;
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(config.severity)) {
                if (config.severity > 2) {
                    config.severity = 2;
                }
                if (config.severity < 0) {
                    config.severity = 0;
                }
            }
            this.setLevels(config.level, config.severity);
            this.setModes(config.debug, config.verbose);
            this.setAnonymizer(config.symbols);
            if (ObjectValidator.IsEmptyOrNull(config.adapters)) {
                config.adapters = [];
            }
            this.enableAdapters(config.adapters.map(($value : string) : string => {
                return StringUtils.ToUpperCase($value);
            }));
        }
    }

    public setLevels($level : LogLevel, $severity : LogSeverity) : void {
        if (!ObjectValidator.IsEmptyOrNull($level)) {
            this.level = $level;
        }
        if (!ObjectValidator.IsEmptyOrNull($severity)) {
            this.severity = $severity;
        }
    }

    public MuteToggle() : void {
        if (ObjectValidator.IsEmptyOrNull((<any>this)._level)) {
            (<any>this)._level = this.level;
            this.level = LogLevel.OFF;
        } else {
            this.level = (<any>this)._level;
            delete (<any>this)._level;
        }
    }

    public setModes($debug : boolean, $verbose : boolean) : void {
        this.debug = Property.Boolean(this.debug, $debug);
        this.verbose = Property.Boolean(this.verbose, $verbose);
        this.adapters.forEach(($adapter : BaseAdapter) : void => {
            $adapter.setModes(this.debug, this.verbose);
        });
    }

    public setTrackBackEntryPoint($class : any) : void {
        this.trackBackEntryPoint = $class;
    }

    public setAnonymizer($patterns? : any) : void {
        if (!ObjectValidator.IsSet(this.anonymizer)) {
            this.anonymizer = this.getAnonymizerSymbols();
        }
        if (!ObjectValidator.IsEmptyOrNull($patterns)) {
            let pattern : string;
            for (pattern in $patterns) {
                if ($patterns.hasOwnProperty(pattern) && !ObjectValidator.IsEmptyOrNull($patterns[pattern])) {
                    this.anonymizer.values[pattern] = $patterns[pattern];
                }
            }
        }
    }

    public AddAdapter($adapter : BaseAdapter) : void {
        $adapter.setModes(this.debug, this.verbose);
        $adapter.setOwner(this);
        this.adapters.push($adapter);
    }

    public setAdapters($adapters : BaseAdapter[]) : void {
        if (!ObjectValidator.IsEmptyOrNull($adapters)) {
            this.adapters = [];
            $adapters.forEach(($adapter : BaseAdapter) : void => {
                if (!ObjectValidator.IsEmptyOrNull($adapter)) {
                    $adapter.setOwner(this);
                    this.adapters.push($adapter);
                }
            });
            this.setModes(this.debug, this.verbose);
        } else {
            this.adapters = [];
        }
    }

    public Info($message : string, ...$parameters : any[]) : void {
        let severity = LogSeverity.HIGH;
        if ($parameters.length >= 1 && $parameters[0] in LogSeverity) {
            severity = $parameters[0];
            $parameters.shift();
        }
        this.process(LogLevel.INFO, StringUtils.Format.apply(StringUtils, [$message].concat($parameters)), severity); // eslint-disable-line
    }

    public Warning($message : string, ...$parameters : any[]) : void {
        this.process(LogLevel.WARNING, StringUtils.Format.apply(StringUtils, [$message].concat($parameters))); // eslint-disable-line
    }

    public Error($message : string | Error, ...$parameters : any[]) : void {
        let exception : Error = null;
        if (this.verbose && ObjectValidator.IsString($message)) {
            exception = new Error(<string>$message);
            exception.message = "";
        }
        if (!ObjectValidator.IsString($message)) {
            exception = <Error>$message;
            $message = (<Error>$message).message;
            try {
                exception.message = "";
            } catch (ex) {
                const errorCopy : Error = new Error(<string>$message);
                errorCopy.message = "";
                errorCopy.stack = exception.stack;
                exception = errorCopy;
            }
        }
        if ($parameters.length === 1 && ObjectValidator.IsError($parameters[0])) {
            exception = $parameters[0];
            $parameters.shift();
        }
        if ($parameters.length > 0) {
            $message = StringUtils.Format.apply(StringUtils, [$message].concat($parameters)); // eslint-disable-line
        }
        let data : any = $message;
        if (!ObjectValidator.IsEmptyOrNull(exception) && ObjectValidator.IsSet(exception.stack)) {
            data = {
                _isLoggerObject: true,
                error          : exception,
                message        : $message
            };
        }
        this.process(LogLevel.ERROR, data);
    }

    public Debug($messageOrObject : string | any, ...$parameters : any[]) : void {
        if (ObjectValidator.IsSet($parameters) && ObjectValidator.IsString($messageOrObject)) {
            this.process(LogLevel.DEBUG, {
                _isLoggerObject: true,
                args           : $parameters,
                message        : $messageOrObject
            });
        } else {
            this.process(LogLevel.DEBUG, $messageOrObject);
        }
    }

    public Verbose($message : string, ...$parameters : any[]) : void {
        this.process(LogLevel.VERBOSE, StringUtils.Format.apply(StringUtils, [$message].concat($parameters))); // eslint-disable-line
    }

    public getAdapter($type : LogAdapterType) : BaseAdapter {
        const name : string = "adapter" + $type;
        switch ($type) {
        case LogAdapterType.CONSOLE:
            if (ObjectValidator.IsEmptyOrNull(this[name])) {
                this[name] = new ConsoleAdapter();
            }
            return this[name];
        case LogAdapterType.HTML:
            if (ObjectValidator.IsEmptyOrNull(this[name])) {
                this[name] = new HTMLAdapter();
            }
            return this[name];
        case LogAdapterType.SENTRY:
            if (ObjectValidator.IsEmptyOrNull(this[name])) {
                this[name] = new SentryAdapter();
            }
            return this[name];
        default:
            return null;
        }
    }

    protected getAnonymizerSymbols() : any {
        return {
            format: "{0}",
            values: []
        };
    }

    protected serializeMessage($message : any, $htmlTagEnabled : boolean, $newLineType : NewLineType) : string {
        if (!ObjectValidator.IsString($message)) {
            if (ObjectValidator.IsSet($message._isLoggerObject)) {
                if (ObjectValidator.IsSet($message.error)) {
                    $message.message += $newLineType +
                        StringUtils.Tab(1, $htmlTagEnabled) + (ObjectValidator.IsEmptyOrNull($message.error.message) ? "" :
                            $message.error.message + $newLineType) +
                        "Stack trace: ";
                    if ($htmlTagEnabled) {
                        $message.message += StringUtils.Replace(StringUtils.Replace($message.error.stack, "</", ":"),
                            "\n", StringUtils.NewLine());
                    } else {
                        $message.message += $message.error.stack;
                    }
                    $message = $message.message;
                } else if (ObjectValidator.IsSet($message.args)) {
                    const args : any[] = [$message.message];
                    let index : number;
                    for (index = 0; index < $message.args.length; index++) {
                        args[index + 1] = Convert.ObjectToString($message.args[index], "", $htmlTagEnabled);
                    }
                    $message = StringUtils.Format.apply(StringUtils, args); // eslint-disable-line
                }
            } else {
                $message = Convert.ObjectToString($message, "", $htmlTagEnabled);
            }
        }
        return $message;
    }

    protected buildInAdapters() : LogAdapterType[] {
        return [LogAdapterType.CONSOLE, LogAdapterType.SENTRY];
    }

    protected defaultAdapters() : LogAdapterType[] {
        return [LogAdapterType.CONSOLE, LogAdapterType.SENTRY];
    }

    private process($level : LogLevel, $message : any, $severity? : LogSeverity) : void {
        const minLevel : LogLevel = this.debug || this.verbose ? LogLevel.ALL : this.level;
        if (minLevel === LogLevel.ALL ||
            minLevel === LogLevel.DEBUG ||
            minLevel === LogLevel.ERROR && $level === LogLevel.ERROR ||
            minLevel === LogLevel.WARNING && ($level === LogLevel.ERROR || $level === LogLevel.WARNING) ||
            minLevel === LogLevel.INFO && ($level === LogLevel.INFO || $level === LogLevel.WARNING || $level === LogLevel.ERROR)) {

            if ($level === LogLevel.INFO && !ObjectValidator.IsEmptyOrNull($severity)) {
                let minSeverity : LogSeverity = this.severity;
                if (this.verbose) {
                    minSeverity = LogSeverity.LOW;
                } else if (this.debug) {
                    minSeverity = LogSeverity.MEDIUM;
                }
                if ($severity < minSeverity) {
                    $message = "";
                }
            }

            if (!ObjectValidator.IsEmptyOrNull($message)) {
                const messages : ArrayList<string> = new ArrayList<string>();
                this.adapters.forEach(($adapter : BaseAdapter) : void => {
                    if ($adapter.Enabled()) {
                        const type : string = $adapter.getConfigSHA();
                        if (!messages.KeyExists(type)) {
                            messages.Add(
                                this.anonymize(this.serializeMessage($message, $adapter.HtmlSupported(), $adapter.NewLineType())),
                                type);
                        }
                    }
                });
                const entryPoint : ITraceBackEntry = TraceBack.CaptureEntry(this.toType());
                entryPoint.at = this.anonymize(entryPoint.at);
                entryPoint.from = this.anonymize(entryPoint.from);

                this.adapters.forEach(($adapter : BaseAdapter) : void => {
                    if ($adapter.Enabled()) {
                        $adapter.Process({
                            entryPoint,
                            level   : $level,
                            message : messages.getItem($adapter.getConfigSHA()),
                            severity: $severity,
                            time    : new Date().getTime(),
                            trace   : TraceBack.getTrace()
                        });
                    }
                });
            }
        }
    }

    private enableAdapters($allowed? : Array<LogAdapterType | string>) : void {
        if (ObjectValidator.IsEmptyOrNull($allowed)) {
            $allowed = this.defaultAdapters();
        }
        this.buildInAdapters().forEach(($type : LogAdapterType) : void => {
            const adapter : BaseAdapter = this.getAdapter($type);
            if (!ObjectValidator.IsEmptyOrNull(adapter)) {
                adapter.Enabled($allowed.indexOf(<string>$type) !== -1);
            }
        });
    }

    private anonymize($message : string) : string {
        if (!this.debug) {
            if (!ObjectValidator.IsSet(this.anonymizer)) {
                this.anonymizer = this.getAnonymizerSymbols();
            }

            let key : string;
            for (key in this.anonymizer.values) {
                if (this.anonymizer.values.hasOwnProperty(key) && StringUtils.Contains($message, this.anonymizer.values[key])) {
                    $message = StringUtils.Replace($message,
                        this.anonymizer.values[key], StringUtils.Format(this.anonymizer.format, key));
                }
            }
        }
        return $message;
    }

    private toType() : string {
        const entryPoint = ObjectValidator.IsEmptyOrNull(this.trackBackEntryPoint) ?
            Reflection.getInstance().getClass(this.getClassName()) : this.trackBackEntryPoint;
        let type : string = "Function.";
        let match : string[] = entryPoint.toString().match(/function\s(\w*)/);
        if (match === null) {
            match = (({}).toString.call(entryPoint) + "").match(/\s([a-zA-Z]+)/);
            type += (match[1] + "").toLowerCase();
        } else {
            type += match[1];
        }
        this.toType = () : string => {
            return type;
        };
        return type;
    }
}

export interface ILoggerTrace {
    level : LogLevel;
    message : string;
    severity : LogSeverity;
    time : number | string;
    trace : StackTraceCallSite[];
    entryPoint : ITraceBackEntry | string;
}

// generated-code-start
export const ILoggerTrace = globalThis.RegisterInterface(["level", "message", "severity", "time", "trace", "entryPoint"]);
// generated-code-end
