/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpRequestConstants } from "../Enums/HttpRequestConstants.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { StringUtils } from "../Utils/StringUtils.js";
import { BaseErrorPage } from "./BaseErrorPage.js";

export class Http404NotFoundPage extends BaseErrorPage {
    private filePath : string;

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        this.filePath = "";
        if ($POST.KeyExists(HttpRequestConstants.HTTP404_FILE_PATH)) {
            this.filePath = $POST.getItem(HttpRequestConstants.HTTP404_FILE_PATH);
        }
    }

    protected getPageBody() : string {
        if (!ObjectValidator.IsEmptyOrNull(this.filePath)) {
            return "<h1>HTTP status 404:</h1>" + StringUtils.NewLine(false) +
                "<h2>File has not been found. Required file path is:</h2>" + StringUtils.NewLine(false) +
                "<a href=\"" + this.filePath + "\">" + this.filePath + "</a>";

        }
        return "<h1>HTTP status 404:</h1>" + StringUtils.NewLine(false) +
            "<h2>File has not been found</h2>";
    }
}
