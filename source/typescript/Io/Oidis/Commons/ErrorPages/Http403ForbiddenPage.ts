/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "../Utils/StringUtils.js";
import { BaseErrorPage } from "./BaseErrorPage.js";

export class Http403ForbiddenPage extends BaseErrorPage {
    protected getPageBody() : string {
        return "<h1>HTTP status 403:</h1>" + StringUtils.NewLine(false) +
            "<h2>Access denied</h2>";
    }
}
