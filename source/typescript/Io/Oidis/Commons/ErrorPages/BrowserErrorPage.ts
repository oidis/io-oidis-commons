/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "../Utils/StringUtils.js";
import { BaseErrorPage } from "./BaseErrorPage.js";

export class BrowserErrorPage extends BaseErrorPage {
    protected getPageBody() : string {
        return "You are using unsupported type or version of the browser. " +
            "Please, choose one of the supported browser from the list below:" + StringUtils.NewLine() +
            "<a href=\"https://windows.microsoft.com/en-us/internet-explorer/download-ie\" " +
            "target=\"_blank\">Internet Explorer 5+</a>" + StringUtils.NewLine() +
            "<a href=\"https://www.mozilla.org/en-US/firefox/new/\" " +
            "target=\"_blank\">Firefox</a>" + StringUtils.NewLine() +
            "<a href=\"https://www.google.com/chrome/\" " +
            "target=\"_blank\">Google Chrome</a>" + StringUtils.NewLine() +
            "<a href=\"https://www.opera.com/\" " +
            "target=\"_blank\">Opera</a>" + StringUtils.NewLine() +
            "<a href=\"https://www.apple.com/safari/\" " +
            "target=\"_blank\">Safari</a>";
    }
}
