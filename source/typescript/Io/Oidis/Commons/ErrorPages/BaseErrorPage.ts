/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionsManager } from "../Exceptions/ExceptionsManager.js";
import { AsyncHttpResolver } from "../HttpProcessor/Resolvers/AsyncHttpResolver.js";
import { Echo } from "../Utils/Echo.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";

export abstract class BaseErrorPage extends AsyncHttpResolver {

    protected getEchoOutput() : string {
        let output : string = Echo.getStream();
        if (ObjectValidator.IsEmptyOrNull(output)) {
            output = "Nothing has been printed by Echo yet.";
        }
        return output;
    }

    protected getPageBody() : string {
        return "<h1>Something has went wrong</h1>";
    }

    protected resolver() : void {
        const body : string = this.getPageBody();

        try {
            ExceptionsManager.ThrowExit();
        } catch (ex) {
            // stop all background execution, but continue in execution of current resolver
        }
        Echo.ClearAll();
        Echo.Println(body);
    }
}
