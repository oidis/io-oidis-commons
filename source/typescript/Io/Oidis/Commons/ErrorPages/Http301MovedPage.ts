/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpRequestConstants } from "../Enums/HttpRequestConstants.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { StringUtils } from "../Utils/StringUtils.js";
import { BaseErrorPage } from "./BaseErrorPage.js";

export class Http301MovedPage extends BaseErrorPage {
    private link : string;

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        this.link = "";
        if ($POST.KeyExists(HttpRequestConstants.HTTP301_LINK)) {
            this.link = $POST.getItem(HttpRequestConstants.HTTP301_LINK);
        }
    }

    protected getPageBody() : string {
        let output : string = "";
        const EOL : string = StringUtils.NewLine(false);

        output +=
            "<h1>HTTP status 301:</h1>" + EOL +
            "<h2>File has been moved. New address is:</h2>" + EOL +
            "<a href=\"#" + this.link + "\">" + this.getRequest().getHostUrl() + "#" + this.link + "</a>";

        return output;
    }
}
