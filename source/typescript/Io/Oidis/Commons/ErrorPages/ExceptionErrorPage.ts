/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2098 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionCode } from "../Enums/ExceptionCode.js";
import { HttpRequestConstants } from "../Enums/HttpRequestConstants.js";
import { ExceptionsManager } from "../Exceptions/ExceptionsManager.js";
import { ErrorPageException } from "../Exceptions/Type/ErrorPageException.js";
import { Exception } from "../Exceptions/Type/Exception.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { StringUtils } from "../Utils/StringUtils.js";
import { BaseErrorPage } from "./BaseErrorPage.js";

export class ExceptionErrorPage extends BaseErrorPage {
    private fatalErrorExists : boolean;
    private exceptionsList : ArrayList<Exception>;

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        if (ObjectValidator.IsEmptyOrNull(this.fatalErrorExists)) {
            if ($GET.KeyExists(HttpRequestConstants.EXCEPTION_TYPE)) {
                this.fatalErrorExists =
                    StringUtils.ToInteger($GET.getItem(HttpRequestConstants.EXCEPTION_TYPE)) === ExceptionCode.FATAL_ERROR;
            } else {
                this.fatalErrorExists = false;
            }
        }
        if (ObjectValidator.IsEmptyOrNull(this.exceptionsList)) {
            if ($POST.KeyExists(HttpRequestConstants.EXCEPTIONS_LIST)) {
                this.exceptionsList = $POST.getItem(HttpRequestConstants.EXCEPTIONS_LIST);
            } else {
                this.exceptionsList = new ArrayList<Exception>();
            }
        }
    }

    protected getExceptionsList() : ArrayList<Exception> {
        return this.exceptionsList;
    }

    protected isFatalError() : boolean {
        return this.fatalErrorExists;
    }

    protected getPageBody() : string {
        let output : string = "";
        const EOL : string = StringUtils.NewLine(false);

        if (this.isFatalError()) {
            output += "<h1>FATAL Error!</h1>";
        } else {
            output += "<h1>Oops, something went wrong...</h1>";
        }

        this.getExceptionsList().foreach(($exception : Exception) : void => {
            output += "thrown by: <b>" + $exception.Owner() + "</b>:" + StringUtils.NewLine();
            try {
                output += $exception.ToString() + StringUtils.NewLine();
            } catch (ex) {
                output += ex.message + StringUtils.NewLine();
            }
        });

        output +=
            "<span onclick=\"" +
            "document.getElementById('exceptionEcho').style.display=" +
            "document.getElementById('exceptionEcho').style.display===" +
            "'block'?'none':'block';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
            "Echo output before exception" +
            "</span>" + EOL +
            "<div id=\"exceptionEcho\" style=\"border: 0 solid black; display: none;\">" + EOL +
            this.getEchoOutput() + EOL +
            "</div>" + StringUtils.NewLine() +
            "<a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; text-decoration: none;\" " +
            "href=\"#" + this.createLink("/about/Cache") + "\">Cache info</a>";

        ExceptionsManager.Clear();

        return output;
    }

    protected resolver() : void {
        try {
            super.resolver();
        } catch (ex) {
            try {
                ExceptionsManager.Throw(this.getClassName(), ex);
            } catch (ex) {
                // register error page self-error and continue with processing of general exception manager
            }
            try {
                ExceptionsManager.Throw(this.getClassName(), new ErrorPageException(this.getClassName() + " self error."));
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        }
    }
}
