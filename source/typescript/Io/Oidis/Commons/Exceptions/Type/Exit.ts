/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionCode } from "../../Enums/ExceptionCode.js";
import { Exception } from "./Exception.js";

/**
 * Exit class should be used to stop execution of current thread without thrown of exception.
 */
export class Exit extends Exception {

    constructor() {
        super();
        this.Code(ExceptionCode.EXIT);
    }
}
