/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionCode } from "../../Enums/ExceptionCode.js";
import { Exception } from "./Exception.js";

/**
 * ErrorPageException class should be used in case of, that error page resolver has failed or
 * if exception should be forced to print in string format.
 */
export class ErrorPageException extends Exception {

    constructor($message? : string) {
        super($message);
        this.Code(ExceptionCode.ERROR_PAGE_EXCEPTION);
    }
}
