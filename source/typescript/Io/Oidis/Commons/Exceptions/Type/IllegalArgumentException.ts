/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionCode } from "../../Enums/ExceptionCode.js";
import { Exception } from "./Exception.js";

/**
 * IllegalArgumentException class should be used in case of passing argument with incorrect type or value.
 */
export class IllegalArgumentException extends Exception {

    constructor($message? : string) {
        super($message);
        this.Code(ExceptionCode.ILLEGAL_ARGUMENT);
    }
}
