/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* istanbul ignore if */
if (!Array.prototype.indexOf) {
    /* istanbul ignore next : cross-browser compatibility bug fix can be covered only by selenium run */
    Array.prototype.indexOf = function (searchElement : any, fromIndex? : number) : number {
        let index : number;
        const length : number = this.length;
        for (index = (fromIndex || 0); index < length; index++) {
            if (this[index] === searchElement) {
                return index;
            }
        }
        return -1;
    };
}

/* istanbul ignore if */
if (!Array.prototype.forEach) {
    /* istanbul ignore next : cross-browser compatibility bug fix can be covered only by selenium run */
    Array.prototype.forEach = function (callbackfn : (value : any, index? : number, array? : any[]) => void, thisArg? : any) : void {
        let index : number;
        const length : number = this.length;
        for (index = 0; index < length; index++) {
            callbackfn(this[index], index, thisArg || this);
        }
    };
}
