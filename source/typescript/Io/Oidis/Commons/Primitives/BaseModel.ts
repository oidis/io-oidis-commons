/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseDbContext } from "../DAO/BaseDbContext.js";
import { LogSeverity } from "../Enums/LogSeverity.js";
import { IModel, IObjectProperty, IObjectSerializerOptions } from "../Interfaces/IBaseObject.js";
import { Loader } from "../Loader.js";
import { isBrowser } from "../Utils/EnvironmentHelper.js";
import { JsonUtils } from "../Utils/JsonUtils.js";
import { LogIt } from "../Utils/LogIt.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { Reflection } from "../Utils/Reflection.js";
import { BaseObject } from "./BaseObject.js";
import { Property } from "./Decorators.js";

/// TODO: implement flush mechanism hooked to change of LCP token,
//        where also property should have secret attribute where it will be not stored/auto-flushed and from BE to FE not send at all
export class BaseModel<T = any> extends BaseObject implements IModel {

    public get Properties() : IObjectProperty[] {
        return this.modelProperties;
    }

    protected get document() {
        if (ObjectValidator.IsEmptyOrNull(this.modelDocument)) {
            this.modelDocument = this.context.CreateDocument(this.getClassName(), this.getSchema(), this.getCollection());
            if (ObjectValidator.IsEmptyOrNull(this.Id)) {
                this.Id = this.modelDocument._id;
            }
        }
        return this.modelDocument;
    }

    protected set document($value : any) {
        this.modelDocument = $value;
    }

    @Property({type: String, symbol: "_id", alias: "id", default: isBrowser ? Reflection.UID() : require("uuid").v1})
    public declare Id : string;

    @Property({type: Boolean, required: false, default: false})
    public declare Deleted : boolean;

    @Property({type: Date, default: new Date()})
    public declare LastModified : Date;

    @Property({type: Date, default: new Date()})
    public declare Created : Date;

    @Property({type: String, required: false})
    public declare ClassName : string;

    protected readonly modelProperties : IObjectProperty[];
    protected context : BaseDbContext;
    private hasSyncedModel : boolean;
    private saveOptions : IModelSaveOptions;
    private lastUpdated : string[];
    private modelDocument : any;

    public static getCollection() : string {
        return this.getSymbolicName();
    }

    public static getSymbolicName() : string {
        let symbol : string = this.ClassName();
        const prototype : any = Reflection.getInstance().getClass(this.ClassName());
        if (prototype.hasOwnProperty("__entitySymbolicName")) {
            symbol = prototype.__entitySymbolicName();
        }
        return symbol.replace(/[._]/g, "");
    }

    public static async FindById<T extends BaseModel<T>>($id : string, $options? : IModelFindOptions) : Promise<T> {
        return Loader.getInstance().getDBDriver().getContextFor(this).FindById<T>($id, $options);
    }

    public static async Find<T extends BaseModel<T>>($query : any, $options? : IModelFindOptions) : Promise<T[]> {
        return Loader.getInstance().getDBDriver().getContextFor(this).Find<T>($query, $options);
    }

    public static async From<T extends BaseModel<T>>($data : any) : Promise<T> {
        const prototype : any = <any>this;
        const instance : T = new prototype();
        instance.document = $data;
        await instance.fromModel({dataTranslation: true});
        return instance;
    }

    public static async Aggregate<T extends BaseModel<T>>($chain : any[] | IModelAggregateOptions,
                                                          $options? : IModelFindOptions) : Promise<any> {
        return Loader.getInstance().getDBDriver().getContextFor(this).Aggregate<T>($chain, $options);
    }

    public static Model() : any {
        return Loader.getInstance().getDBDriver().getContextFor(this).getDocument();
    }

    constructor() {
        super();

        if (!ObjectValidator.IsEmptyOrNull(Loader.getInstance().getDBDriver())) {
            this.context = Loader.getInstance().getDBDriver().getContextFor(this);
        }
        this.hasSyncedModel = false;
        this.modelProperties = [];
        this.lastUpdated = [];
        if (!ObjectValidator.IsEmptyOrNull((<any>this)._modelProperties)) {
            this.modelProperties = (<any>this)._modelProperties;
        }
        if (!ObjectValidator.IsEmptyOrNull((<any>this)._lastUpdated)) {
            this.lastUpdated = (<any>this)._lastUpdated;
        }
        this.saveOptions = {};

        this.Created = new Date();
        this.LastModified = new Date();
        this.ClassName = this.getClassName();

        for (const property of this.modelProperties) {
            // DO NOT skip "_id" default value initialisation on FE
            if (ObjectValidator.IsSet(property.schemaOptions.type) &&
                property.schemaOptions.type.hasOwnProperty("ClassName") &&
                Reflection.getInstance().Implements(new (<any>property.schemaOptions.type)(), IModel) &&
                property.schemaOptions.required) {
                if (ObjectValidator.IsSet(property.schemaOptions.default)) {
                    if (!ObjectValidator.IsEmptyOrNull(property.schemaOptions.default)) {
                        this[property.name] = property.schemaOptions.default;
                        LogIt.Warning("Default value from @Property for BaseModel derivative types uses same reference for all instances.");
                    }
                } else {
                    // required BaseModel derivatives are constructed with default constructor, if this behaviour is not optimal for
                    // some specific usecase then definition is expected in model constructor.
                    this[property.name] = new (<any>property.schemaOptions.type)();
                }
            } else if (ObjectValidator.IsSet(property.schemaOptions.default) && ((!isBrowser && property.symbol !== "_id") || isBrowser)) {
                if (property.symbol === "_id" && isBrowser) {
                    this[property.name] = (<any>this).getUID();
                } else if (ObjectValidator.IsArray(property.schemaOptions.default) && property.schemaOptions.default.length === 0) {
                    this[property.name] = <any>[];
                } else if (property.schemaOptions.default instanceof Date) {
                    this[property.name] = <any>new Date(property.schemaOptions.default.getTime());
                } else if (!isBrowser && property.schemaOptions.default instanceof Buffer) {
                    this[property.name] = new Buffer(property.schemaOptions.default);
                } else if (ObjectValidator.IsObject(property.schemaOptions.default)) {
                    this[property.name] = {...property.schemaOptions.default};
                } else {
                    this[property.name] = property.schemaOptions.default;
                }
            }
        }

        this.attachStringify();
    }

    public getContext() : BaseDbContext {
        return this.context;
    }

    public setContext($context : BaseDbContext) : void {
        $context.setModel(this);
        this.context = $context;
    }

    public getLastUpdated() : string[] {
        return this.lastUpdated;
    }

    public getCollection() : string {
        return (<any>this.constructor).getCollection();
    }

    public getSymbolicName() : string {
        return (<any>this.constructor).getSymbolicName();
    }

    public getDocument() : any {
        return this.document;
    }

    public async Save($options? : IModelSaveOptions) : Promise<void> {
        this.saveOptions = JsonUtils.Extend(<IModelSaveOptions>{
            skipValidation: false,
            verbose       : true
        }, $options);

        if (!this.saveOptions.skipValidation) {
            if (!this.Validate()) {
                throw new Error("Database model " + this.getClassName() + " property values validation failed.");
            }
        } else if (this.saveOptions.verbose) {
            LogIt.Warning("Insecure model " + this.getClassName() + " save due to skipValidation option");
        }
        await this.toModel();
        try {
            await this.context.Save();
            LogIt.Info("Database saved: " + this.getClassName() + " - [" + this.getSymbolicName() +
                "]" + "; " + this.Id, LogSeverity.LOW);
            this.saveOptions.skipValidation = false;
        } catch (ex) {
            this.saveOptions.skipValidation = false;
            LogIt.Debug("Database {1} save error: {0}", ex.message, this.getClassNameWithoutNamespace());
            throw ex;
        }
        this.lastUpdated = [];
    }

    public async Update($property : string) : Promise<void> {
        if (this.hasSyncedModel) {
            try {
                await this.context.Update($property);
            } catch (ex) {
                LogIt.Debug("Database property {0} update error: {1}", $property, ex.message);
                throw ex;
            }
        }
    }

    public async Refresh($options? : IModelFindOptions) : Promise<void> {
        if (this.hasSyncedModel) {
            return this.context.Refresh($options);
        }
    }

    public async Delete() : Promise<void> {
        this.Deleted = true;
        return this.Save();
    }

    public async Remove($property? : string, $child? : string) : Promise<void> {
        if (this.hasSyncedModel) {
            try {
                await this.context.Remove($property, $child);
            } catch (ex) {
                LogIt.Debug("Database delete item error: {0}", ex.message);
                throw ex;
            }
        }
    }

    public SyncWith($model : T) : void {
        if (ObjectValidator.IsEmptyOrNull($model)) {
            return;
        }
        const reflection : Reflection = Reflection.getInstance();
        for (const prop of this.modelProperties) {
            try {
                let value : any = $model[prop.fieldKey];
                if (ObjectValidator.IsEmptyOrNull(value)) {
                    value = $model[prop.name];
                }

                if (!ObjectValidator.IsEmptyOrNull(this[prop.name]) && reflection.Implements(this[prop.name], IModel)) {
                    if (reflection.IsMemberOf(value, BaseModel)) {
                        this[prop.name] = value;
                    } else {
                        this[prop.name].SyncWith(value);
                    }
                } else if (!ObjectValidator.IsEmptyOrNull(value) && ObjectValidator.IsEmptyOrNull(this[prop.name]) &&
                    ObjectValidator.IsSet(prop.schemaOptions.type) &&
                    prop.schemaOptions.type.hasOwnProperty("ClassName") &&
                    reflection.Implements(new (<any>prop.schemaOptions.type)(), IModel)) {
                    if (reflection.IsMemberOf(value, BaseModel)) {
                        this[prop.name] = value;
                    } else {
                        let modelClassName : any = null;
                        if (!ObjectValidator.IsEmptyOrNull(value) && !ObjectValidator.IsEmptyOrNull(value.ClassName)) {
                            modelClassName = value.ClassName;
                        }
                        if (ObjectValidator.IsEmptyOrNull(modelClassName) && ObjectValidator.IsSet(prop.schemaOptions.type) &&
                            prop.schemaOptions.type.hasOwnProperty("ClassName")) {
                            modelClassName = prop.schemaOptions.type;
                        }
                        if (ObjectValidator.IsString(modelClassName)) {
                            modelClassName = reflection.getClass(modelClassName);
                        }
                        this[prop.name] = new (modelClassName)();
                        this[prop.name].SyncWith(value);
                    }
                } else if (ObjectValidator.IsArray(value)) {
                    if (ObjectValidator.IsArray(prop.schemaOptions.type) &&
                        prop.schemaOptions.type.length >= 1 &&
                        prop.schemaOptions.type[0].hasOwnProperty("ClassName") &&
                        reflection.Implements(new (<any>prop.schemaOptions.type[0])(), IModel)) {
                        if (ObjectValidator.IsEmptyOrNull(this[prop.name])) {
                            this[prop.name] = [];
                        }
                        const syncedArray : any[] = [];
                        value.forEach(($new : any) : void => {
                            if (ObjectValidator.IsEmptyOrNull($new)) {
                                syncedArray.push(null);
                            } else if (reflection.IsMemberOf($new, BaseModel)) {
                                syncedArray.push($new);
                            } else {
                                let itemModel : any = null;
                                this[prop.name].forEach(($current : any) : void => {
                                    if ($new.Id === $current.Id) {
                                        itemModel = $current;
                                    }
                                });
                                if (ObjectValidator.IsEmptyOrNull(itemModel)) {
                                    let itemClass : any = null;
                                    if (!ObjectValidator.IsEmptyOrNull($new.ClassName)) {
                                        itemClass = $new.ClassName;
                                    }
                                    if (ObjectValidator.IsEmptyOrNull(itemClass)) {
                                        itemClass = prop.schemaOptions.type[0];
                                    }
                                    if (ObjectValidator.IsString(itemClass)) {
                                        try {
                                            itemClass = reflection.getClass(itemClass);
                                        } catch (ex) {
                                            LogIt.Info("Failed to find item class: " + itemClass + " Using default schema type.",
                                                LogSeverity.LOW);
                                            itemClass = prop.schemaOptions.type[0];
                                        }
                                    }
                                    itemModel = new (itemClass)();
                                }
                                itemModel.SyncWith($new);
                                syncedArray.push(itemModel);
                            }
                        });
                        this[prop.name] = syncedArray;
                    } else {
                        this[prop.name] = $model[prop.name];
                    }
                } else {
                    if (!ObjectValidator.IsEmptyOrNull(value)) {
                        this[prop.name] = value;
                    } else if (!ObjectValidator.IsEmptyOrNull(this[prop.name]) && value !== undefined) {
                        this[prop.name] = null;
                    }
                }
            } catch (ex) {
                LogIt.Error("Sync error for: " + prop.name, ex);
                throw ex;
            }
        }
    }

    public Clone() : this {
        const prototype : any = Reflection.getInstance().getClass(this.getClassName());
        const instance : any = new prototype();
        instance.SyncWith(this);
        return instance;
    }

    /**
     * Validate model properties for value condition over defined validators. Validation runs by default also for array elements, and
     * this feature could be turned off by noAutoListValidation in model property options to use explicit array validator.
     * @param $property Specify property name, symbol, key or let empty to validate all properties in model.
     * @param $verbose Specify if model validation should print validation errors.
     * @returns Returns true if value(s) are valid or false when not.
     */
    public Validate($property? : string, $verbose : boolean = true) : boolean {
        return this.ValidateProperties($property, $verbose);
    }

    /**
     * ValidateDeep is similar to Validate, but it returns list of property symbols of founded issues.
     * @param $property Specify property name, symbol, key or let empty to validate all properties in model.
     * @param $verbose Specify if model validation should print validation errors.
     * @returns Returns empty array if value(s) are valid or findings when not.
     */
    public ValidateDeep($property? : string, $verbose : boolean = true) : string[] {
        if (!this.saveOptions.skipValidation) {
            return super.ValidateDeep($property, $verbose);
        } else {
            LogIt.Warning("Skipping model " + this.getClassName() + " validation due to skipValidation option");
        }
        return [];
    }

    public async ToJSON($options? : IObjectSerializerOptions) : Promise<any> {
        $options = JsonUtils.Extend({idsOnly: false, expanded: false}, $options);
        const retVal : any = {};
        const process : any = async ($key : string, $serialized : boolean) : Promise<any> => {
            if (ObjectValidator.IsArray(this[$key])) {
                const list : any[] = [];
                for await(const property of this[$key]) {
                    if (ObjectValidator.IsObject(property) && Reflection.getInstance().Implements(property, IModel)) {
                        list.push($serialized ? await property.ToJSON($options) : property.Id);
                    } else if ($serialized) {
                        list.push(property);
                    }
                }
                return list;
            } else if (ObjectValidator.IsObject(this[$key]) && Reflection.getInstance().Implements(this[$key], IModel)) {
                return $serialized ? this[$key].ToJSON($options) : this[$key].Id;
            } else {
                return this[$key];
            }
        };

        for await (const property of this.modelProperties) {
            if (!property.modifiers.noJSON) {
                retVal[property.name] = await process(property.fieldKey,
                    ((!property.modifiers.noSerialize || $options.expanded) && !$options.idsOnly));
            }
        }
        return retVal;
    }

    public ToInterface($options? : IObjectSerializerOptions) : any {
        $options = JsonUtils.Extend({idsOnly: false, expanded: false}, $options);
        const retVal : any = {};

        const process : any = ($key : string, $serialized : boolean) : any => {
            if (ObjectValidator.IsArray(this[$key])) {
                const list : any[] = [];
                for (const property of this[$key]) {
                    if (ObjectValidator.IsObject(property) && Reflection.getInstance().Implements(property, IModel)) {
                        list.push($serialized ? property.ToInterface($options) : property.Id);
                    } else if ($serialized) {
                        list.push(property);
                    }
                }
                return list;
            } else if (ObjectValidator.IsObject(this[$key]) && Reflection.getInstance().Implements(this[$key], IModel)) {
                return $serialized ? this[$key].ToInterface($options) : this[$key].Id;
            } else {
                return this[$key];
            }
        };

        for (const property of this.modelProperties) {
            if (!property.modifiers.noJSON) {
                const value : any = process(property.fieldKey,
                    ((!property.modifiers.noSerialize || $options.expanded) && !$options.idsOnly));
                if (ObjectValidator.IsSet(value)) {
                    retVal[property.name] = value;
                }
            }
        }

        return retVal;
    }

    protected getSchema() : any {
        return this.context.getSchema();
    }

    /**
     * This method is called from constructor and applies override for native toJSON function which is implicitly called by
     * JSON.stringify(). It is by default connected to BaseModel.ToInterface().
     */
    protected attachStringify() : void {
        (<any>this).toJSON = () => {
            return this.ToInterface();
        };
    }

    protected getSaveOptions() : IModelSaveOptions {
        return this.saveOptions;
    }

    protected async toModel() : Promise<void> {
        for await(const item of this.modelProperties) {
            let changed = false;
            const tmp = await item.converter.to(this[item.fieldKey], {parent: this, property: item, dataTranslation: false});
            if (!ObjectValidator.IsEmptyOrNull(tmp) || ObjectValidator.IsArray(tmp)) {
                if (ObjectValidator.IsArray(this.document[item.symbol])) {
                    changed = (JSON.stringify(tmp) !== JSON.stringify(this.context.DocumentToObject(item.symbol)));
                } else if (ObjectValidator.IsEmptyOrNull(this.document[item.symbol])) {
                    changed = true;
                } else {
                    changed = (tmp !== this.document[item.symbol]);
                }
                if (changed) {
                    this.document[item.symbol] = tmp;
                }
            } else if (!ObjectValidator.IsEmptyOrNull(this.document[item.symbol]) && tmp === null) {
                this.document[item.symbol] = null;
            }
        }
        this.hasSyncedModel = true;
    }

    protected async fromModel($options? : IModelFindOptions) : Promise<void> {
        $options = JsonUtils.Extend({partial: false, expanded: false, dataTranslation: false}, $options);
        for await(const item of this.modelProperties) {
            if (!item.isInclusion && (item.modifiers.noResolve || $options.partial) && !$options.expanded) {
                // this could be unsafe because it could return different (native) type of property!
                if (Array.isArray(this.document[item.symbol])) {
                    this[item.fieldKey] = <any>[];
                    for (const doc of this.document[item.symbol]) {
                        this[item.fieldKey].push(doc);
                    }
                } else {
                    this[item.fieldKey] = this.document[item.symbol];
                }
            } else {
                if (!ObjectValidator.IsEmptyOrNull(this.document) && (!ObjectValidator.IsEmptyOrNull(this.document[item.symbol]) ||
                    ObjectValidator.IsArray(this.document[item.symbol]))) {
                    this[item.fieldKey] = await item.converter.from(this.document[item.symbol], {
                        dataTranslation: ObjectValidator.IsEmptyOrNull($options.dataTranslation) ? false : $options.dataTranslation,
                        parent         : this,
                        property       : item
                    });
                }
            }
        }
        this.hasSyncedModel = true;
    }
}

export interface IModelSaveOptions {
    skipValidation? : boolean;
    verbose? : boolean;
}

export interface IModelFindOptions {
    /**
     * Forces model resolve into flat mirror while ignoring property options. Exclusive option which suppress other ones.
     */
    expanded? : boolean;

    /**
     * Forces resolving only parent model and rest will be ignored (only ID strings available)
     */
    partial? : boolean;

    offset? : number;

    limit? : number;

    sort? : any;

    dataTranslation? : boolean;
}

export interface IModelAggregateOptions {
    filterQuery : any[];
    resultQuery : any[];
    sortQuery? : any;
}

// generated-code-start
export const IModelSaveOptions = globalThis.RegisterInterface(["skipValidation", "verbose"]);
export const IModelFindOptions = globalThis.RegisterInterface(["expanded", "partial", "offset", "limit", "sort", "dataTranslation"]);
export const IModelAggregateOptions = globalThis.RegisterInterface(["filterQuery", "resultQuery", "sortQuery"]);
// generated-code-end
