/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { SyntaxConstants } from "../Enums/SyntaxConstants.js";
import { IBaseObject } from "../Interfaces/IBaseObject.js";
import { IClassName, Interface } from "../Interfaces/Interface.js";

/**
 * BaseObject class provides helper methods focused on validations and handling of class instance.
 */
export abstract class BaseObject implements IBaseObject {

    private static classNamespace : string = "";
    private static className : string = "";
    private objectNamespace : string;
    private objectClassName : string;

    /**
     * @returns {string} Returns class name with namespace in string format.
     */

    /* istanbul ignore next: function is called by ClassName and running in Reflection Class */
    public static ClassName() : string {
        globalThis.Io.Oidis.Commons.Utils.Reflection.getInstance();
        return this.classNamespace + "." + this.className;
    }

    /**
     * @returns {string[]} Returns list of classes hierarchy for current class.
     */
    public static ClassHierarchy() : string[] {
        return globalThis.Io.Oidis.Commons.Utils.Reflection.getInstance().getClassHierarchyOf(this.ClassName());
    }

    /**
     * @returns {string} Returns namespace of the class in string format.
     */

    /* istanbul ignore next: function is running in Reflection Class */
    public static NamespaceName() : string {
        globalThis.Io.Oidis.Commons.Utils.Reflection.getInstance();
        return this.classNamespace;
    }

    /**
     * @returns {string[]} Returns list of namespaces hierarchy for current class.
     */
    public static NamespaceHierarchy() : string[] {
        return globalThis.Io.Oidis.Commons.Utils.Reflection.getInstance().getNamespaceHierarchyOf(this.ClassName());
    }

    /**
     * @returns {string} Returns only the class name without namespace in string format.
     */

    /* istanbul ignore next: function is running in Reflection Class */
    public static ClassNameWithoutNamespace() : string {
        globalThis.Io.Oidis.Commons.Utils.Reflection.getInstance();
        return this.className;
    }

    /**
     * @returns {string} Returns unique identifier, based on class name and timestamp.
     */
    public static UID() : string {
        return globalThis.Io.Oidis.Commons.Utils.Reflection.UID(this.className);
    }

    /**
     * @param {any} [$dataObject] Data object suitable for data reconstruction.
     * @returns {any} Returns class instance in case of, that data object is suitable for object reconstruction,
     * otherwise null. This method is mainly focused to unserialization purposes, but it can also returns class singleton.
     */
    public static getInstance($dataObject? : any) : any {
        return globalThis.Io.Oidis.Commons.Utils.Reflection.getInstanceOf(this.ClassName(), $dataObject);
    }

    private static hashProcessor($input : any, $cache : any[], $cacheIndex : number) : string {
        if ($cache.indexOf($input) !== -1) {
            return "";
        }
        $cache[$cacheIndex] = $input;
        $cacheIndex++;

        let exclude : string[] = [];
        if ($input instanceof BaseObject) {
            exclude = (<BaseObject>$input).excludeIdentityHashData();
        }

        const parameters : string[] = [];
        Object.getOwnPropertyNames($input).forEach(($parameter : string) : void => {
            if (parameters.indexOf($parameter) === -1) {
                parameters.push($parameter);
            }
        });
        let output : string = "";
        parameters.forEach(($parameter : string) : void => {
            if ($input.hasOwnProperty($parameter) && $input[$parameter] !== null && exclude.indexOf($parameter) === -1) {
                const type : string = typeof $input[$parameter];
                if (type !== SyntaxConstants.FUNCTION) {
                    output += $parameter + ":";
                    if (type === SyntaxConstants.OBJECT) {
                        output += BaseObject.hashProcessor($input[$parameter], $cache, $cacheIndex);
                    } else {
                        output += $input[$parameter];
                    }
                    output += ",";
                }
            }
        });
        return output;
    }

    /**
     * @returns {string} Returns unique identifier, based on class name and timestamp.
     */
    public getUID() : string {
        return globalThis.Io.Oidis.Commons.Utils.Reflection.UID(this.getClassName());
    }

    /**
     * @returns {string} Returns class name with namespace for the class instance in string format.
     */

    /* istanbul ignore next: function is called by ClassName and running in Reflection Class */
    public getClassName() : string {
        if (SyntaxConstants.UNDEFINED.indexOf(this.objectClassName) > -1) {
            this.objectClassName = globalThis.Io.Oidis.Commons.Utils.Reflection.getInstance().getClassName(this);
        }
        if (this.objectClassName === "") {
            this.objectClassName = this[SyntaxConstants.CONSTRUCTOR].toString().match(/function\s(\w*)/)[1];
            this.objectClassName = SyntaxConstants.ALIASES.indexOf(this.objectClassName) > -1 ? "Function" : this.objectClassName;
        }
        return this.objectClassName;
    }

    /**
     * @returns {string[]} Returns list of classes hierarchy for current instance.
     */
    public getClassHierarchy() : string[] {
        return globalThis.Io.Oidis.Commons.Utils.Reflection.getInstance().getClassHierarchyOf(this);
    }

    /**
     * @returns {string} Returns namespace of the class in string format.
     */

    /* istanbul ignore next: function is running in Reflection Class */
    public getNamespaceName() : string {
        const fullName : string = this.getClassName();
        return (fullName + "").substring(0, (fullName + "").lastIndexOf("."));
    }

    /**
     * @returns {string[]} Returns list of namespaces hierarchy for current instance.
     */
    public getNamespaceHierarchy() : string[] {
        return globalThis.Io.Oidis.Commons.Utils.Reflection.getInstance().getNamespaceHierarchyOf(this);
    }

    /**
     * @returns {string} Returns only the class name without namespace in string format.
     */

    /* istanbul ignore next: function is running in Reflection Class */
    public getClassNameWithoutNamespace() : string {
        const fullName : string = this.getClassName();
        return (fullName + "").substr((this.getNamespaceName() + "").length + 1);
    }

    /**
     * @returns {string[]} Returns string array of all properties in the class instance.
     */
    public getProperties() : string[] {
        const output : string[] = [];
        Object.getOwnPropertyNames(Object.getPrototypeOf(this)).forEach(($parameter : string) : void => {
            if (output.indexOf($parameter) === -1 && typeof this[$parameter] !== SyntaxConstants.FUNCTION) {
                output.push($parameter);
                // console.log("Obj: add property: " + $parameter);
            }
        });
        let parameter : any;
        for (parameter in this) {
            if (output.indexOf(parameter) === -1 &&
                typeof this[parameter] !== SyntaxConstants.FUNCTION) {
                output.push(parameter);
                // console.log("Obj: add missing property: " + parameter);
            }
        }
        return output;
    }

    /**
     * @returns {string[]} Returns string array of all methods in the class instance.
     */
    public getMethods() : string[] {
        const output : string[] = [];
        Object.getOwnPropertyNames(Object.getPrototypeOf(this)).forEach(($parameter : string) : void => {
            if (output.indexOf($parameter) === -1 &&
                typeof this[$parameter] === SyntaxConstants.FUNCTION && $parameter !== SyntaxConstants.CONSTRUCTOR) {
                output.push($parameter);
                // console.log("Obj: add method: " + $parameter);
            }
        });
        let parameter : any;
        for (parameter in this) {
            if (output.indexOf(parameter) === -1 &&
                typeof this[parameter] === SyntaxConstants.FUNCTION && parameter !== SyntaxConstants.CONSTRUCTOR) {
                output.push(parameter);
                // console.log("Obj: add missing method: " + parameter);
            }
        }
        return output;
    }

    /**
     * @param {ClassName|string} $className Class name for validation.
     * @returns {boolean} Returns true if the class instance is type of $className, otherwise false.
     */
    public IsTypeOf($className : IClassName | string) : boolean {
        return globalThis.Io.Oidis.Commons.Utils.Reflection.getInstance().IsInstanceOf(this, $className);
    }

    /**
     * @param {ClassName|string} $className Class name for validation.
     * @returns {boolean} Returns true if the class instance is type of $className object or it's child, otherwise false.
     */
    public IsMemberOf($className : IClassName | string) : boolean {
        return globalThis.Io.Oidis.Commons.Utils.Reflection.getInstance().IsMemberOf(this, $className);
    }

    /**
     * @param {(Interface|string)} $className Interface class name in string format or as Object, which should be validated.
     * @returns {boolean} Returns true if the class instance implements $className interface, otherwise false.
     */
    public Implements($className : Interface | string) : boolean {
        return globalThis.Io.Oidis.Commons.Utils.Reflection.getInstance().Implements(this, $className);
    }

    /**
     * @returns {object} Returns data suitable for object serialization.
     */
    public SerializationData() : object {
        const properties : string[] = this.getProperties();
        const output : object = {};
        const exclude : string[] = this.excludeSerializationData();
        let index : number;
        const length : number = properties.length;
        for (index = 0; index < length; index++) {
            if (exclude.indexOf(properties[index]) === -1) {
                output[properties[index]] = this[properties[index]];
            }
        }
        return output;
    }

    /**
     * @param  {string} $prefix value converted to String.
     * @param {boolean} $htmlTag Specify, if output format should be HTML or plain text.
     * @returns String of prefix value and HTML tag.
     */
    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        return $prefix + "object type of '" + this.getClassName() + "'";
    }

    public toString() : string {
        return this.ToString("", false);
    }

    /**
     * @returns {number} Returns CRC calculated from data, which represents current object.
     */
    public getHash() : number {
        let cacheMap : any[] = [];
        const cacheMapIndex : number = 0;
        const output : number = globalThis.Io.Oidis.Commons.Utils.StringUtils
            .getCrc(BaseObject.hashProcessor(this, cacheMap, cacheMapIndex));
        cacheMap = null;
        return output;
    }

    /**
     * Validate object properties for value condition over defined validators. Validation runs by default also for array elements, and
     * this feature could be turned off by noAutoListValidation in model property options to use explicit array validator.
     * @param $property Specify property name, symbol, key or let empty to validate all properties in object.
     * @param $verbose Specify if object validation should print validation errors.
     * @returns Returns true if value(s) are valid or false when not.
     */
    public ValidateProperties($property? : string, $verbose : boolean = true) : boolean {
        return globalThis.Io.Oidis.Commons.Utils.ObjectValidator.IsEmptyOrNull(this.ValidateDeep($property, $verbose));
    }

    /**
     * ValidateDeep is similar to Validate, but it returns list of property symbols of founded issues.
     * @param $property Specify property name, symbol, key or let empty to validate all properties in model.
     * @param $verbose Specify if model validation should print validation errors.
     * @returns Returns empty array if value(s) are valid or findings when not.
     */
    public ValidateDeep($property? : string, $verbose : boolean = true) : string[] {
        let findings : string[] = [];
        const ObjectValidator : any = globalThis.Io.Oidis.Commons.Utils.ObjectValidator;
        const LogIt : any = globalThis.Io.Oidis.Commons.Utils.LogIt;
        if (!ObjectValidator.IsEmptyOrNull((<any>this)._modelProperties)) {
            const StringUtils : any = globalThis.Io.Oidis.Commons.Utils.StringUtils;
            const Reflection : any = globalThis.Io.Oidis.Commons.Utils.Reflection;

            const validateElement : any = ($element : any, $item : any, $index? : number) : void => {
                const symbolSuffix : string = ObjectValidator.IsSet($index) ? "[" + $index + "]" : "";
                let validationStatus : boolean = true;
                let isSubModel : boolean = false;
                if (ObjectValidator.IsObject($element) &&
                    Reflection.getInstance().Implements($element, IBaseObject)) {
                    findings = findings.concat($element.ValidateDeep(null, $verbose)
                        .map(($record : string) : string => $item.symbol + symbolSuffix + "." + $record));
                    isSubModel = true;
                } else if (ObjectValidator.IsArray($element) && !$item.noAutoListValidation) {
                    let index : number = 0;
                    for (const element of $element) {
                        validateElement(element, $item, index);
                        index++;
                    }
                } else {
                    validationStatus = $item.validator.validate($element);
                }
                if (!validationStatus) {
                    if ($verbose) {
                        LogIt.Info(">> object validation for " + this.getClassName() + " " +
                            $item.symbol + symbolSuffix + " failed: " + $element);
                    }
                    if (!isSubModel) {
                        findings.push($item.symbol);
                    }
                }
            };
            for (const item of (<any>this)._modelProperties) {
                if (ObjectValidator.IsEmptyOrNull($property) ||
                    (StringUtils.ContainsIgnoreCase($property, item.symbol, item.name, item.fieldKey))) {
                    validateElement(this[item.fieldKey], item);
                }
            }
        } else {
            LogIt.Warning("Skipping object " + this.getClassName() + " validation: no decorated properties found");
        }
        return findings;
    }

    protected excludeSerializationData() : string[] {
        return ["objectNamespace", "objectClassName"];
    }

    protected excludeIdentityHashData() : string[] {
        return [];
    }
}
