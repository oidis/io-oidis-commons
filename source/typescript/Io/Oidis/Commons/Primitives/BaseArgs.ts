/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { SyntaxConstants } from "../Enums/SyntaxConstants.js";
import { Convert } from "../Utils/Convert.js";
import { StringUtils } from "../Utils/StringUtils.js";
import { BaseObject } from "./BaseObject.js";

/**
 * BaseArgs class is abstract class providing automatic convert of whole args to the string.
 */
export abstract class BaseArgs extends BaseObject {

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        const methods : string[] = this.getMethods();
        let output : string = "";
        let index : number;
        for (index = 0; index < methods.length; index++) {
            const methodName : string = methods[index];
            if (this.toStringFilter(methodName)) {
                let methodValue : any;
                try {
                    methodValue = this[methodName]();
                } catch (ex) {
                    methodValue = Convert.FunctionToString(this[methodName]);
                }
                output += $prefix + "[" + methodName + "] " +
                    Convert.ObjectToString(methodValue, $prefix, $htmlTag) + StringUtils.NewLine($htmlTag);
            }
        }
        return output;
    }

    public toString() : string {
        return this.ToString();
    }

    protected toStringFilter($methodName : string) : boolean {
        return (
            $methodName !== SyntaxConstants.CONSTRUCTOR &&
            $methodName !== "toStringFilter" &&
            $methodName !== "getMethods" &&
            $methodName !== "getProperties" &&
            $methodName !== "SerializationData" &&
            $methodName !== "getUID" &&
            StringUtils.ToLowerCase($methodName) !== StringUtils.ToLowerCase(SyntaxConstants.TO_STRING) &&
            $methodName !== SyntaxConstants.IS_TYPE_OF
        );
    }
}
