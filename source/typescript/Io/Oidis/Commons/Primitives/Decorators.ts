/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IConverter } from "../DAO/Converters/BaseConverter.js";
import { IdentityConverter } from "../DAO/Converters/IdentityConverter.js";
import { IObjectProperty, ISchemaOptions } from "../Interfaces/IBaseObject.js";
import { IClassName } from "../Interfaces/Interface.js";
import { IValidator } from "../Interfaces/IValidator.js";
import { JsonUtils } from "../Utils/JsonUtils.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { PropertyValidator } from "../Utils/PropertyValidator.js";

export function Property($options? : IPropertyOptions) : any {
   
    return function ($target : any, $propertyKey : string, $descriptor : PropertyDescriptor) : any {
        if (ObjectValidator.IsEmptyOrNull($target._modelProperties)) {
            $target._modelProperties = [];
        } else {
            // copy of array causes that actual property will not be written into base
            // and thus also not into different child of the same base class
            $target._modelProperties = $target._modelProperties.slice();
        }
        if (ObjectValidator.IsEmptyOrNull($target._lastUpdated)) {
            $target._lastUpdated = [];
        }
        const fieldKey : string = "_" + $propertyKey;
        const options : IPropertyOptions = <IPropertyOptions>{
            converter           : new IdentityConverter(),
            symbol              : $propertyKey.charAt(0).toLowerCase() + $propertyKey.slice(1),
            noJSON              : false,
            noResolve           : false,
            noSerialize         : false,
            validator           : new PropertyValidator(),
            noAutoListValidation: false,
            addId               : false,
            required            : true
        };
        if (!ObjectValidator.IsEmptyOrNull($options)) {
            for (const $item in $options) {
                if ($options.hasOwnProperty($item)) {
                    options[$item] = $options[$item];
                }
            }
        }
        const index = $target._modelProperties.findIndex(($item : IObjectProperty) : boolean => {
            return $item.name === $propertyKey;
        });
        if (index < 0) {
            $target._modelProperties.push(<IObjectProperty>{
                name                : $propertyKey,
                fieldKey,
                symbol              : options.symbol,
                converter           : options.converter,
                modifiers           : {
                    noJSON     : options.noJSON,
                    noResolve  : options.noResolve,
                    noSerialize: options.noSerialize
                },
                validator           : options.validator,
                noAutoListValidation: options.noAutoListValidation,
                schemaOptions       : {
                    default : options.default,
                    addId   : options.addId,
                    type    : options.type,
                    required: options.required,
                    alias   : options.alias
                },
                isInclusion         : false
            });
        } else {
            const item : IObjectProperty = $target._modelProperties[index];
            item.symbol = options.symbol;
            item.converter = options.converter;
            item.modifiers.noJSON = options.noJSON;
            item.modifiers.noResolve = options.noResolve;
            item.modifiers.noSerialize = options.noSerialize;
            item.validator = options.validator;
            item.noAutoListValidation = options.noAutoListValidation;
            item.schemaOptions.default = options.default;
            item.schemaOptions.addId = options.addId;
            item.schemaOptions.type = options.type;
            item.schemaOptions.required = options.required;
            item.schemaOptions.alias = options.alias;
        }
        return {
            set($value : any) {
                if (this.hasOwnProperty(fieldKey) && this[fieldKey] !== $value && !this._lastUpdated.includes(options.symbol)) {
                    this._lastUpdated.push(options.symbol);
                }
                this[fieldKey] = $value;
            },
            get() {
                if (options.symbol === "_id") {
                    if (this.document) {
                        // just dummy call of getter to init schema
                    }
                }
                return this[fieldKey];
            },
            configurable: true,
            enumerable  : true
        };
    };
}

export function Entity($shortcut? : string) : any {
    return function ($target : any) : any {
        $target.__entitySymbolicName = () : string => {
            return ObjectValidator.IsEmptyOrNull($shortcut) ? $target.ClassName() : $shortcut;
        };
    };
}

export function Resolver($links? : string | string[], $options? : IResolverOptions) : any;
export function Resolver($defaultPath? : string, $altLinks? : string[], $options? : IResolverOptions) : any;
export function Resolver($options? : IResolverOptions) : any;
export function Resolver($linksOrOptions? : string | string[] | IResolverOptions, $altLinksOrOptions? : string[] | IResolverOptions,
                         $options? : IResolverOptions) : any {
    return function ($target : any, $propertyKey : string, $descriptor : PropertyDescriptor) : any {
        if (!globalThis.hasOwnProperty("oidisResolvers")) {
            globalThis.oidisResolvers = [];
        }
        $options = JsonUtils.Extend(<IResolverOptions>{
            link         : "",
            altLinks     : [],
            type         : ResolverType.UNIVERSAL,
            scope        : ResolverScope.ALL_PROFILES,
            forceRegister: false,
            priority     : 1
        }, $options);
        if (ObjectValidator.IsString($linksOrOptions)) {
            $options.link = <string>$linksOrOptions;
        } else if (ObjectValidator.IsArray($linksOrOptions)) {
            $options.link = (<string[]>$linksOrOptions)[0];
            $options.altLinks = <string[]>$linksOrOptions;
            $options.altLinks.shift();
        } else if (!ObjectValidator.IsEmptyOrNull($linksOrOptions)) {
            JsonUtils.Extend($options, $linksOrOptions);
        }
        if (ObjectValidator.IsArray($altLinksOrOptions)) {
            $options.altLinks = <string[]>$altLinksOrOptions;
        } else if (!ObjectValidator.IsEmptyOrNull($altLinksOrOptions)) {
            JsonUtils.Extend($options, $altLinksOrOptions);
        }

        $target.getLink = () : string => {
            return $options.link;
        };
        $target.getAltLinks = () : string[] => {
            return $options.altLinks;
        };
        globalThis.oidisResolvers.push(<IResolverDescriptor>{
            ClassName: () : string => {
                return $target.ClassName();
            },
            instance : $target,
            options  : $options
        });
    };
}

export function Partial(...$subclasses : any[]) : any {
   
    return function ($target : any) : any {
        $target.prototype.isPartial = true;

        if (!globalThis.hasOwnProperty("oidisPartials")) {
            globalThis.oidisPartials = [];
        }
        globalThis.oidisPartials.push(<IPartialDescriptor>{
            ClassName : () : string => {
                return $target.ClassName();
            },
            instance  : $target,
            subclasses: $subclasses
        });
    };
}

export interface IPropertyOptions extends ISchemaOptions {
    /**
     * Symbolic nome used by model schema generators, automatically constructed from property name with first character transformed to
     * lowercase. Symbolic name could be defined by @Property decorator with explicit definition of "symbol" parameter.
     */
    symbol? : string;

    /**
     * Exclude property from object serialization. (Could be overridden by ToJSON options)
     */
    noJSON? : boolean;

    /**
     * Referenced property data ID will be serialized instead of full property data
     */
    noSerialize? : boolean;

    /**
     * Custom property value validator (implements IValidator). Is called during object.Validate() or implicitly during object.Save() on BE.
     */
    validator? : IValidator;

    /**
     * Validation runs by default also for array elements, this feature could be turned off by noAutoListValidation option.
     */
    noAutoListValidation? : boolean;
    /**
     * Custom property converter (implements IConverter). It is IdentityConverter by default and purpose is to transform
     * data before sync to mongo model.
     */
    converter? : IConverter;

    /**
     * Exclude property from automatic resolve (only ID will be returned by Find method results for referenced objects).
     */
    noResolve? : boolean;
}

export interface IResolverDescriptor {
    ClassName : () => string;
    instance : IClassName;
    options : IResolverOptions;
}

export interface IResolverOptions {
    link? : string;
    altLinks? : string[];
    type? : ResolverType;
    scope? : ResolverScope;
    forceRegister? : boolean;
    priority? : number;
}

export enum ResolverType {
    UNIVERSAL,
    BACKEND_ONLY,
    FRONTEND_ONLY
}

export enum ResolverScope {
    ALL_PROFILES,
    PROD,
    DEV
}

export interface IPartialDescriptor {
    ClassName : () => string;
    instance : IClassName;
    subclasses : IClassName[];
}

// generated-code-start
/* eslint-disable */
export const IPropertyOptions = globalThis.RegisterInterface(["symbol", "noJSON", "noSerialize", "validator", "noAutoListValidation", "converter", "noResolve"], <any>ISchemaOptions);
export const IResolverDescriptor = globalThis.RegisterInterface(["ClassName", "instance", "options"]);
export const IResolverOptions = globalThis.RegisterInterface(["link", "altLinks", "type", "scope", "forceRegister", "priority"]);
export const IPartialDescriptor = globalThis.RegisterInterface(["ClassName", "instance", "subclasses"]);
/* eslint-enable */
// generated-code-end
