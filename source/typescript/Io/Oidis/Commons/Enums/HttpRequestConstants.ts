/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "../Primitives/BaseEnum.js";

export class HttpRequestConstants extends BaseEnum {
    public static readonly EXCEPTION_TYPE : string = "ErrorType";
    public static readonly EXCEPTIONS_LIST : string = "ErrorsList";
    public static readonly HTTP301_LINK : string = "Http301Link";
    public static readonly HTTP404_FILE_PATH : string = "Http404FilePath";
    public static readonly APP_NAME : string = "AppName";
    public static readonly APP_PID : string = "AppPid";
    public static readonly DESIGNER : string = "Designer";
    public static readonly RELEASE_NAME : string = "ReleaseName";
    public static readonly PLATFORM : string = "Platform";
}
