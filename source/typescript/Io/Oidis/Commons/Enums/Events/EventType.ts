/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "../../Primitives/BaseEnum.js";

export class EventType extends BaseEnum {
    public static readonly ON_START : string = "onstart";
    public static readonly ON_CHANGE : string = "onchange";
    public static readonly ON_COMPLETE : string = "oncomplete";
    public static readonly BEFORE_LOAD : string = "beforereload";
    public static readonly ON_LOAD : string = "onload";
    public static readonly BEFORE_REFRESH : string = "beforerefresh";
    public static readonly ON_HTTP_REQUEST : string = "onhttprequest";
    public static readonly ON_ASYNC_REQUEST : string = "onasyncrequest";
    public static readonly ON_SUCCESS : string = "onsuccess";
    public static readonly ON_ERROR : string = "onerror";
    public static readonly ON_MESSAGE : string = "onmessage";
}
