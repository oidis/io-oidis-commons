/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export class SyntaxConstants {
    public static readonly UNDEFINED : any[] = ["undefined", undefined];
    public static readonly CONSTRUCTOR : string = "constructor";
    public static readonly ALIASES : string[] = ["", "anonymous", "Anonymous"];
    public static readonly FUNCTION : string = "function";
    public static readonly BOOLEAN : string = "boolean";
    public static readonly NUMBER : string = "number";
    public static readonly STRING : string = "string";
    public static readonly ARRAYS : string[] = ["array", "[object Array]"];
    public static readonly OBJECT : string = "object";
    public static readonly CLASS_NAME : string = "getClassName";
    public static readonly TO_STRING : string = "ToString";
    public static readonly IS_EMPTY : string = "IsEmpty";
    public static readonly IS_TYPE_OF : string = "IsTypeOf";
    public static readonly LENGTH : string = "Length";
    public static readonly ERROR : string = "[object Error]";
}
