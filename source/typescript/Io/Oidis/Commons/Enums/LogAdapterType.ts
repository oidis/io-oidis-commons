/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "../Primitives/BaseEnum.js";

export class LogAdapterType extends BaseEnum {
    public static readonly CONSOLE : string = "CONSOLE";
    public static readonly HTML : string = "HTML";
    public static readonly SENTRY : string = "SENTRY";
}
