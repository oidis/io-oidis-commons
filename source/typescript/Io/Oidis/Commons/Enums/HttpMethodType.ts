/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "../Primitives/BaseEnum.js";

export class HttpMethodType extends BaseEnum {
    public static readonly OPTIONS : string = "OPTIONS";
    public static readonly GET : string = "GET";
    public static readonly HEAD : string = "HEAD";
    public static readonly POST : string = "POST";
    public static readonly PUT : string = "PUT";
    public static readonly PATCH : string = "PATCH";
    public static readonly DELETE : string = "DELETE";
    public static readonly TRACE : string = "TRACE";
    public static readonly CONNECT : string = "CONNECT";
}
