/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "../Primitives/BaseEnum.js";

export class IOHandlerType extends BaseEnum {
    public static readonly CONSOLE : string = "Console";
    public static readonly HTML_ELEMENT : string = "HtmlElement";
    public static readonly OUTPUT_FILE : string = "OutputFile";
    public static readonly INPUT_FILE : string = "InputFile";
}
