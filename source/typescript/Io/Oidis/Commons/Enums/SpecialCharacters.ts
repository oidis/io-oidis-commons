/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "../Primitives/BaseEnum.js";

export class SpecialCharacters extends BaseEnum {
    public static readonly WHITESPACE : string = "&nbsp;";
    public static readonly TAB : string = "&nbsp;&nbsp;&nbsp;";
    public static readonly AMPERSAND : string = "&amp;";
    public static readonly COPYRIGHT : string = "&copy;";
    public static readonly REGISTERED : string = "&reg;";
}
