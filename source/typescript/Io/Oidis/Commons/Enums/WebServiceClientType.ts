/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "../Primitives/BaseEnum.js";

export class WebServiceClientType extends BaseEnum {
    public static readonly AJAX : string = "Ajax";
    public static readonly AJAX_SYNCHRONOUS : string = "AjaxSync";
    public static readonly IFRAME : string = "Iframe";
    public static readonly POST_MESSAGE : string = "PostMessage";
    public static readonly HTTP : string = "Http";
    public static readonly WEB_SOCKETS : string = "WebSockets";
    public static readonly CEF_QUERY : string = "CefQuery";
    public static readonly JXBROWSER_BRIDGE : string = "JxbrowserBridge";
}
