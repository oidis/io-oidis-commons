/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionsManager } from "../../Exceptions/ExceptionsManager.js";
import { FatalError } from "../../Exceptions/Type/FatalError.js";
import { IllegalArgumentException } from "../../Exceptions/Type/IllegalArgumentException.js";
import { JsonpFileReader } from "../../IOApi/Handlers/JsonpFileReader.js";
import { Loader } from "../../Loader.js";
import { ArrayList } from "../../Primitives/ArrayList.js";
import { Convert } from "../../Utils/Convert.js";
import { ObjectDecoder } from "../../Utils/ObjectDecoder.js";
import { ObjectEncoder } from "../../Utils/ObjectEncoder.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { Property } from "../../Utils/Property.js";
import { StringUtils } from "../../Utils/StringUtils.js";
import { BasePersistenceHandler } from "./BasePersistenceHandler.js";

/**
 * CookiesHandler class provides API for handling of cookies.
 */
export class CookiesHandler extends BasePersistenceHandler {
    private session : ArrayList<any>;
    private size : number;
    private loadTime : number;

    public static setCookie(name : string, value : string, ttl : number = 0) : void {
        let expires : string = "";
        if (ttl !== 0) {
            expires = " expires=" + Convert.TimeToGMTformat(ttl);
        }
        document.cookie = name + "=" + value + ";" + expires;
    }

    /**
     * @param {string} [$sessionId] If not defined, session id will be generated.
     */
    constructor($sessionId? : string) {
        super($sessionId);
        this.size = 0;
        this.loadTime = 0;
        if (ObjectValidator.IsEmptyOrNull(this.sessionId)) {
            this.sessionId = this.getUID();
        }
    }

    /**
     * @returns {number} Returns original or modified value as integer.
     */
    public getSize() : number {
        return this.size;
    }

    /**
     * @returns {number} Returns time needed for load of the Cookies.
     */
    public getLoadTime() : number {
        return this.loadTime;
    }

    /**
     * @param {Function} $asyncHandler Callback, which should be executed when persistence has been loaded.
     * @param {string} [$sourceFilePath] Specify path to data in expected format, which should be loaded as persistence resource.
     * @returns {void}
     */
    public LoadPersistenceAsynchronously($asyncHandler : () => void, $sourceFilePath? : string) : void {
        if (ObjectValidator.IsEmptyOrNull(this.session)) {
            if (!ObjectValidator.IsEmptyOrNull($sourceFilePath)) {
                JsonpFileReader.Load($sourceFilePath, ($data : string) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($data)) {
                        ObjectDecoder.Unserialize(ObjectDecoder.Base64($data), ($object : any) : void => {
                            this.session = $object;
                            $asyncHandler();
                        });
                    } else {
                        this.initSession($asyncHandler);
                    }
                }, () : void => {
                    this.initSession($asyncHandler);
                });
            } else {
                this.initSession($asyncHandler);
            }
        } else {
            $asyncHandler();
        }
    }

    /**
     * @param {string|ArrayList<any>} $name Specify variable name, which should be managed.
     * List of variable/values pairs is allowed for ability to store multiple values at once.
     * @param {any} [$value] Specify value, which should be stored.
     * @param {Function} [$asyncHandler] Enable asynchronous load of variable.
     * @returns {any} Returns value associated with variable name, if persistence has been loaded and variable name exists,
     * otherwise null.
     */
    public Variable($name : string | ArrayList<any>, $value? : any, $asyncHandler? : () => void) : any {
        const async : boolean = ObjectValidator.IsSet($value) && ObjectValidator.IsSet($asyncHandler);

        this.initSession();
        let updateSession : boolean = false;
        if (!ObjectValidator.IsSet($value)) {
            if (ObjectValidator.IsArray($name)) {
                const keys : string[] = <string[]>(<ArrayList<any>>$name).getKeys();
                const data : string[] = (<ArrayList<any>>$name).getAll();
                let index : number;
                for (index = 0; index < (<ArrayList<any>>$name).Length(); index++) {
                    this.addVariable(keys[index], data[index]);
                    updateSession = true;
                }
            }
        } else if (ObjectValidator.IsString($name) || ObjectValidator.IsInteger($name)) {
            this.addVariable(<string>$name, $value);
            updateSession = true;
        } else {
            ExceptionsManager.Throw("CookiesHandler",
                new IllegalArgumentException("Persistence name is supposed to be type of string or integer"));
        }

        if (updateSession) {
            this.session.Add(this.variables, "variables");
            if (this.crcEnabled()) {
                if (async) {
                    ObjectEncoder.Serialize(this.variables, ($data : string) : void => {
                        this.session.Add(StringUtils.getCrc($data), "crc");
                        this.save($asyncHandler);
                    });
                } else {
                    this.session.Add(StringUtils.getCrc(ObjectEncoder.Serialize(this.variables)), "crc");
                    this.save();
                }
            } else {
                this.save($asyncHandler);
            }
        }

        if ((ObjectValidator.IsString($name) || ObjectValidator.IsInteger($name)) &&
            this.variables.KeyExists(<string>$name)) {
            return this.variables.getItem(<string>$name);
        } else {
            return null;
        }
    }

    /**
     * @param {string} $name Key of exists handler in string format, which should be validated.
     * @returns {boolean} Returns true, if key has been mapped, otherwise false.
     */
    public Exists($name : string) : boolean {
        this.initSession();
        return super.Exists($name);
    }

    /**
     * Clean up content of  registered handler.
     * @param {string|ArrayList<string>} [$name] Specify variable name or list of variables, which should be destroyed.
     * @returns {void}
     */
    public Destroy($name : string | ArrayList<string>) : void {
        if (!ObjectValidator.IsEmptyOrNull($name)) {
            this.initSession();
            let updateSession : boolean = false;
            if (ObjectValidator.IsArray($name)) {
                const removeNames : string[] = (<ArrayList<string>>$name).getAll();
                let index : number;
                for (index = 0; index < removeNames.length; index++) {
                    this.removeVariable(removeNames[index]);
                    updateSession = true;
                }
            } else {
                this.removeVariable(<string>$name);
                updateSession = true;
            }

            if (updateSession) {
                this.session.Add(this.variables, "variables");
                if (this.crcEnabled()) {
                    this.session.Add(StringUtils.getCrc(ObjectEncoder.Serialize(this.variables)), "crc");
                }
                this.save();
            }
        }
    }

    /**
     * Clean up resource handled by handler.
     * @returns {void}
     */
    public Clear() : void {
        this.remove();
        this.session = null;
        this.variables = null;
        this.initSession();
    }

    public toString() : string {
        this.initSession();
        return this.ToString();
    }

    /**
     * @param {Function} [$asyncHandler] Callback, which should be executed in case of asynchronous data load.
     * @returns {string} Returns raw persistence data synchronously in case of that $asyncHandler has not been specified,
     * otherwise empty string.
     */
    public getRawData($asyncHandler? : ($data : string) => void) : string {
        if (!ObjectValidator.IsSet($asyncHandler)) {
            return ObjectEncoder.Serialize(this.session);
        } else {
            ObjectEncoder.Serialize(this.session, $asyncHandler);
            return "";
        }
    }

    protected encode($value : ArrayList<any>, $asyncHandler? : ($data : string) => void) : string {
        if (ObjectValidator.IsSet($asyncHandler)) {
            super.encode($value, ($data : string) : void => {
                $asyncHandler(ObjectEncoder.Base64($data, true));
            });
            return "";
        } else {
            return ObjectEncoder.Base64(super.encode($value), true);
        }
    }

    protected decode($value : string, $asyncHandler? : ($data : any) => void) : ArrayList<any> {
        return super.decode(ObjectDecoder.Base64($value), $asyncHandler);
    }

    private load() : string {
        let data : string = "";
        const cookies : ArrayList<string> = Loader.getInstance().getHttpManager().getRequest().getCookies();
        const sessionId : string = this.sessionId;
        cookies.foreach(($value : any, $key? : any) : void => {
            if (StringUtils.StartsWith($key, sessionId)) {
                data += $value;
            }
        });
        this.size = StringUtils.Length(data);

        return data;
    }

    private save($asyncHandler? : () => void) : void {
        const sessionId : string = this.sessionId;
        const saveCookies : ($encoded : string) => void = ($encoded : string) : void => {
            const contentSize : number = 4 * 1000;
            const cookiesCount : number = Math.ceil(StringUtils.Length($encoded) / contentSize);
            let index : number;
            for (index = 0; index < cookiesCount; index++) {
                CookiesHandler.setCookie(sessionId + index,
                    StringUtils.Substring($encoded, index * contentSize, (index + 1) * contentSize),
                    Property.Time(null, "+5 year"));
            }
        };
        if (ObjectValidator.IsSet($asyncHandler)) {
            this.encode(this.session, ($data : string) : void => {
                saveCookies($data);
                $asyncHandler();
            });
        } else {
            saveCookies(this.encode(this.session));
        }
    }

    private remove() : void {
        const cookies : ArrayList<string> = Loader.getInstance().getHttpManager().getRequest().getCookies();
        const sessionId : string = this.sessionId;
        cookies.foreach(($value : any, $key? : any) : void => {
            if (StringUtils.StartsWith($key, sessionId)) {
                CookiesHandler.setCookie($key, "", Property.Time(null, "-1 year"));
            }
        });
    }

    private backup() : void {
        const cookies : ArrayList<string> = Loader.getInstance().getHttpManager().getRequest().getCookies();
        const sessionId : string = this.sessionId;
        cookies.foreach(($value : any, $key? : any) : void => {
            if (StringUtils.StartsWith($key, sessionId)) {
                CookiesHandler.setCookie(StringUtils.getSha1("back" + $key), $value, Property.Time(null, "+5 year"));
            }
        });
        this.remove();
    }

    private initSession($asyncHandler? : () => void) : void {
        if (!Loader.getInstance().getHttpManager().getRequest().IsCookieEnabled()) {
            Loader.getInstance().getHttpManager().ReloadTo("/ServerError/Cookies", null, true);
            ExceptionsManager.ThrowExit();
        }

        const setVariables : () => void = () : void => {
            if (ObjectValidator.IsEmptyOrNull(this.variables)) {
                this.variables = this.session.getItem("variables");
            }

            if (!ObjectValidator.IsEmptyOrNull(this.getExpireTime())) {
                if (this.session.KeyExists("ttl") && this.session.getItem("ttl") < new Date().getTime()) {
                    this.remove();
                    this.session.Add(this.getExpireTime(), "ttl");
                    this.session.Add(null, "crc");
                    this.session.Add(new ArrayList<any>(), "variables");
                    this.variables = null;
                    this.initSession($asyncHandler);
                } else {
                    this.session.Add(this.getExpireTime(), "ttl");
                }
            }
        };
        if (ObjectValidator.IsEmptyOrNull(this.session)) {
            const startTime : number = new Date().getTime();
            const session : string = this.load();
            if (!ObjectValidator.IsEmptyOrNull(session)) {
                const validateCrc : ($values : string) => void = ($values : string) : void => {
                    if (this.session.getItem("crc") !== StringUtils.getCrc($values)) {
                        this.backup();
                        ExceptionsManager.Throw("Cookies Handler",
                            new FatalError("Cookies data has been externally changed. " +
                                "We can not trust them, so they has been removed!"));
                    }
                };
                const initData : ($asyncHandler? : () => void) => void = ($asyncHandler? : () => void) : void => {
                    if (this.crcEnabled()) {
                        if (ObjectValidator.IsSet($asyncHandler)) {
                            ObjectEncoder.Serialize(this.session.getItem("variables"),
                                ($data : string) : void => {
                                    validateCrc($data);
                                    this.loadTime = new Date().getTime() - startTime;
                                    setVariables();
                                    $asyncHandler();
                                });
                        } else {
                            validateCrc(ObjectEncoder.Serialize(this.session.getItem("variables")));
                            this.loadTime = new Date().getTime() - startTime;
                            setVariables();
                        }
                    } else {
                        this.loadTime = new Date().getTime() - startTime;
                        setVariables();
                        if (ObjectValidator.IsSet($asyncHandler)) {
                            $asyncHandler();
                        }
                    }
                };
                if (ObjectValidator.IsSet($asyncHandler)) {
                    this.decode(session, ($decoded : any) : void => {
                        this.session = $decoded;
                        initData($asyncHandler);
                    });
                } else {
                    this.session = this.decode(session);
                    initData();
                }
            } else {
                this.session = new ArrayList<any>();
                this.session.Add(new ArrayList<any>(), "variables");
                this.loadTime = new Date().getTime() - startTime;
            }
        }
        if (!ObjectValidator.IsSet($asyncHandler)) {
            setVariables();
        } else if (!ObjectValidator.IsEmptyOrNull(this.session)) {
            $asyncHandler();
        }
    }
}
