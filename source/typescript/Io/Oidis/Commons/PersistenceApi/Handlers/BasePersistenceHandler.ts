/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IPersistenceHandler } from "../../Interfaces/IPersistenceHandler.js";
import { JsonpFileReader } from "../../IOApi/Handlers/JsonpFileReader.js";
import { ArrayList } from "../../Primitives/ArrayList.js";
import { BaseObject } from "../../Primitives/BaseObject.js";
import { ObjectDecoder } from "../../Utils/ObjectDecoder.js";
import { ObjectEncoder } from "../../Utils/ObjectEncoder.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { Property } from "../../Utils/Property.js";
import { StringUtils } from "../../Utils/StringUtils.js";

/**
 * BasePersistenceHandler class provides abstract API for handling of persistence resources.
 */
export abstract class BasePersistenceHandler extends BaseObject implements IPersistenceHandler {

    protected sessionId : string;
    protected variables : ArrayList<any>;

    private crcCheckEnabled : boolean;
    private expireTime : string;
    private currentExpireTime : number;

    /**
     * @param {string} [$sessionId] If not defined, session is persistent for browser instance
     * (depends on allowed cookies), otherwise it is persistent for server.
     */
    protected constructor($sessionId? : string) {
        super();

        if (!ObjectValidator.IsEmptyOrNull($sessionId)) {
            this.sessionId = $sessionId;
        } else {
            this.sessionId = "";
        }

        this.crcCheckEnabled = true;
    }

    /**
     * @returns {string} Returns session id, which should be used as part of http argument in case of,
     * that session id can not be provided by cookies.
     */
    public getSessionId() : string {
        return this.sessionId;
    }

    /**
     * @returns {number} Returns original or modified value as integer.
     */
    public getSize() : number {
        return 0;
    }

    /**
     * @returns {number} Returns time needed for load.
     */
    public getLoadTime() : number {
        return 0;
    }

    /**
     * @param {Function} $asyncHandler Callback, which should be executed when persistence has been loaded.
     * @param {string} [$sourceFilePath] Specify path to data in expected format, which should be loaded as persistence resource.
     * @returns {void}
     */
    public LoadPersistenceAsynchronously($asyncHandler : () => void, $sourceFilePath? : string) : void {
        if (!ObjectValidator.IsEmptyOrNull($sourceFilePath)) {
            JsonpFileReader.Load($sourceFilePath, ($data : string) : void => {
                if (!ObjectValidator.IsEmptyOrNull($data)) {
                    ObjectDecoder.Unserialize(ObjectDecoder.Base64($data), ($object : any) : void => {
                        this.variables = $object;
                        $asyncHandler();
                    });
                } else {
                    $asyncHandler();
                }
            });
        } else {
            $asyncHandler();
        }
    }

    /**
     * @returns {void}
     */
    public DisableCRC() : void {
        this.crcCheckEnabled = false;
    }

    /**
     * @param {string|number} [$value] Specify expiration time for data stored in current persistent resource.
     * @returns {number} Returns expiration time in seconds.
     */
    public ExpireTime($value? : string | number) : number {
        if (ObjectValidator.IsSet($value)) {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                if (!StringUtils.StartsWith(<string>$value, "+") || !StringUtils.StartsWith(<string>$value, "-")) {
                    $value = "+" + $value;
                }
                this.expireTime = Property.String(this.expireTime, $value + "");
            } else {
                this.expireTime = "";
            }
        }
        return this.getExpireTime();
    }

    /**
     * @param {string|ArrayList<any>} $name Specify variable name, which should be managed.
     * List of variable/values pairs is allowed for ability to store multiple values at once.
     * @param {any} [$value] Specify value, which should be stored.
     * @param {Function} [$asyncHandler] Enable asynchronous load of variable.
     * @returns {any} Returns value associated with variable name, if persistence has been loaded and variable name exists,
     * otherwise null.
     */
    public Variable($name : string | ArrayList<any>, $value? : any, $asyncHandler? : () => void) : any {
        if (!ObjectValidator.IsEmptyOrNull($name) && ObjectValidator.IsString($name)) {
            if (ObjectValidator.IsSet($value)) {
                this.addVariable(<string>$name, $value);
            }
            return this.variables.getItem(<string>$name);
        } else {
            return null;
        }
    }

    /**
     * @param {string} $name Key of exists handler in string format, which should be validated.
     * @returns {boolean} Returns true, if key has been mapped, otherwise false.
     */
    public Exists($name : string) : boolean {
        return this.variables.KeyExists($name);
    }

    /**
     * Clean up content of  registered handler.
     * @param {string|ArrayList<string>} [$name] Specify variable name or list of variables, which should be destroyed.
     * @returns {void}
     */
    public Destroy($name : string | ArrayList<string>) : void {
        if (!ObjectValidator.IsEmptyOrNull($name) && ObjectValidator.IsString($name)) {
            this.removeVariable(<string>$name);
        }
    }

    /**
     * Clean up resource handled by handler.
     * @returns {void}
     */
    public Clear() : void {
        this.variables = null;
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        if (!ObjectValidator.IsSet(this.variables)) {
            this.variables = new ArrayList<any>();
        }
        return this.variables.ToString($prefix, $htmlTag);
    }

    public toString() : string {
        return this.ToString();
    }

    /**
     * @param {Function} [$asyncHandler] Callback, which should be executed in case of asynchronous data load.
     * @returns {string} Returns raw persistence data synchronously in case of that $asyncHandler has not been specified,
     * otherwise empty string.
     */
    public getRawData($asyncHandler? : ($data : string) => void) : string {
        if (!ObjectValidator.IsSet($asyncHandler)) {
            return ObjectEncoder.Serialize(this.variables);
        } else {
            ObjectEncoder.Serialize(this.variables, $asyncHandler);
            return "";
        }
    }

    protected crcEnabled() : boolean {
        return this.crcCheckEnabled;
    }

    protected encode($value : ArrayList<any>, $asyncHandler? : ($data : string) => void) : string {
        if (!ObjectValidator.IsSet($asyncHandler)) {
            return ObjectEncoder.Serialize($value);
        } else {
            ObjectEncoder.Serialize($value, $asyncHandler);
            return "";
        }
    }

    protected decode($value : string, $asyncHandler? : ($data : any) => void) : ArrayList<any> {
        if (!ObjectValidator.IsSet($asyncHandler)) {
            return ObjectDecoder.Unserialize($value);
        } else {
            ObjectDecoder.Unserialize($value, $asyncHandler);
            return new ArrayList<any>();
        }
    }

    protected addVariable($key : string, $value : any) : void {
        this.variables.Add($value, $key);
    }

    protected removeVariable($key : string) : void {
        if (this.variables.KeyExists($key)) {
            this.variables.RemoveAt(this.variables.getKeys().indexOf($key));
        }
    }

    protected getExpireTime() : number {
        return this.currentExpireTime = Property.Time(this.currentExpireTime, this.expireTime);
    }
}
