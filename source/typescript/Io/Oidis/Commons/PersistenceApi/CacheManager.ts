/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseHttpResolver } from "../HttpProcessor/Resolvers/BaseHttpResolver.js";
import { IHttpRequestResolver } from "../Interfaces/IHttpRequestResolver.js";
import { Loader } from "../Loader.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { Convert } from "../Utils/Convert.js";
import { Echo } from "../Utils/Echo.js";
import { Property } from "../Utils/Property.js";
import { StorageHandler } from "./Handlers/StorageHandler.js";

/**
 * CacheManager class is providing master management for all storage types.
 */
export class CacheManager extends BaseHttpResolver implements IHttpRequestResolver {

    private static clearCookies() : void {
        if (Loader.getInstance().getHttpManager().getRequest().IsCookieEnabled()) {
            const cookies : ArrayList<string> = Loader.getInstance().getHttpManager().getRequest().getCookies();
            cookies.foreach(($value : any, $key? : any) : void => {
                document.cookie = $key + "=; expires=" + Convert.TimeToGMTformat(Property.Time(null, "-1 year"));
            });
        }
        Loader.getInstance().getHttpManager().Refresh();
    }

    private static clearStorage() : void {
        if (StorageHandler.IsSupported()) {
            const items : ArrayList<string> = Loader.getInstance().getHttpManager().getRequest().getStorageItems();
            items.foreach(($value : any, $key? : any) : void => {
                localStorage.removeItem($key);
            });
        }
        Loader.getInstance().getHttpManager().Refresh();
    }

    protected resolver() : void {
        Echo.Println("<h1>Local cache manager</h1>");

        Echo.Println("<h2>Domain cookies</h2>");
        Echo.Printf(this.getRequest().getCookies());
        Echo.Println("<span style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\" " +
            "onclick=\"" + this.getClassName() + ".clearCookies();\">Clear cookies</span>");

        Echo.Println("<h2>Local storage items</h2>");
        Echo.Printf(this.getRequest().getStorageItems());
        Echo.Println("<span style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\" " +
            "onclick=\"" + this.getClassName() + ".clearStorage();\">Clear storage</span>");
    }
}
