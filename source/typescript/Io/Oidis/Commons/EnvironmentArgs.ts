/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpRequestConstants } from "./Enums/HttpRequestConstants.js";
import { IOHandlerType } from "./Enums/IOHandlerType.js";
import { INamespaceMapping, IProject } from "./Interfaces/IProject.js";
import { ScriptHandler } from "./IOApi/Handlers/ScriptHandler.js";
import { IOHandlerFactory } from "./IOApi/IOHandlerFactory.js";
import { Loader } from "./Loader.js";
import { BaseObject } from "./Primitives/BaseObject.js";
import { Convert } from "./Utils/Convert.js";
import { JsonUtils } from "./Utils/JsonUtils.js";
import { ObjectValidator } from "./Utils/ObjectValidator.js";
import { StringUtils } from "./Utils/StringUtils.js";

/**
 * EnvironmentArgs class provides environment args mainly set at build time.
 */
export class EnvironmentArgs extends BaseObject {
    public static AppConfigData : ($data : any) => void;
    private appConfigData : IProject;
    private appName : string;
    private processId : number;
    private releaseName : string;
    private platform : string;
    private readonly htmlAllowed : boolean;

    constructor() {
        super();
        this.htmlAllowed = false;
        try {
            /* istanbul ignore else : bulletproof condition */
            if (ObjectValidator.IsSet(window)) {
                this.htmlAllowed = true;
            }
        } catch (ex) {
            // missing window should be interpreted as runtime without HTML support
        }
    }

    /**
     * Load all settings provided by embedded and external configuration.
     * @param {string} $appConfig Specify project configuration in JSON format.
     * @param {Function} $handler Specify handler which will be executed after environment load
     * @returns {void}
     */
    public Load($appConfig : string | IProject, $handler : () => void) : void {
        if (ObjectValidator.IsString($appConfig)) {
            this.appConfigData = JSON.parse(<string>$appConfig);
        } else {
            this.appConfigData = <IProject>$appConfig;
        }
        if (ObjectValidator.IsEmptyOrNull(this.appConfigData.target)) {
            this.appConfigData.target = <any>{};
        }
        if (ObjectValidator.IsEmptyOrNull(this.appConfigData.namespaces)) {
            this.appConfigData.namespaces = [];
        }

        const paths : string[] = [];
        const configs : IEnvironmentConfig[] = [];
        const loaderClass : any = this.fileLoaderClass();
        let isInitLoad : boolean = true;

        const normalizePaths : any = () : void => {
            this.getConfigPaths().forEach(($path : string) : void => {
                if (paths.indexOf($path) === -1) {
                    paths.push($path);
                }
            });
        };
        const mergeConfigs : any = () : void => {
            configs.forEach(($config : IEnvironmentConfig) : void => {
                if (!$config.processed) {
                    if (this.validate($config)) {
                        JsonUtils.Extend(this.appConfigData, $config.data);
                        this.printHandler("> processed: " + $config.path);
                    }
                    $config.processed = true;
                }
            });
        };
        const loadConfig : any = ($index : number, $callback : () => void) : void => {
            if ($index < paths.length) {
                const loader : ScriptHandler = new loaderClass();
                loader.Path(paths[$index]);
                loader.DataHandler(EnvironmentArgs, "AppConfigData");
                loader.SuccessHandler(() : void => {
                    configs.push({
                        data     : loader.Data(),
                        path     : loader.Path(),
                        processed: false
                    });
                    loadConfig($index + 1, $callback);
                });
                loader.ErrorHandler(($error : Error | ErrorEvent) : void => {
                    if (this.errorHandler($error, loader.Path())) {
                        loadConfig($index + 1, $callback);
                    }
                });
                this.printHandler("> load config from: " + loader.Path());
                loader.Load();
            } else if (isInitLoad) {
                isInitLoad = false;
                mergeConfigs();
                normalizePaths();
                if ($index < paths.length) {
                    loadConfig($index, $callback);
                } else {
                    $callback();
                }
            } else {
                $callback();
            }
        };
        normalizePaths();
        loadConfig(0, () : void => {
            try {
                mergeConfigs();
                IOHandlerFactory.getHandler(IOHandlerType.CONSOLE).Clear();
            } catch (ex) {
                this.errorHandler(ex);
            }
            $handler();
        });
    }

    /**
     * @returns {string} Returns project name
     */
    public getProjectName() : string {
        return this.appConfigData.name;
    }

    /**
     * @returns {string} Returns project version
     */
    public getProjectVersion() : string {
        return this.appConfigData.version;
    }

    /**
     * @returns {string[]} Returns expected namespaces at project
     */
    public getNamespaces() : string[] {
        if (this.appConfigData.namespaces.indexOf("Io.Oidis") === -1) {
            this.appConfigData.namespaces.unshift("Io.Oidis");
        }
        this.getNamespaces = () : string[] => {
            return this.appConfigData.namespaces;
        };
        return this.appConfigData.namespaces;
    }

    /**
     * @returns {string[]} Returns fallback namespaces of project
     */
    public getNamespaceMappings() : INamespaceMapping[] {
        return this.appConfigData.namespaceMappings;
    }

    /**
     * @returns {string} Returns application name based on window title
     */
    public getAppName() : string {
        this.appName = this.appConfigData.target.name;
        if (ObjectValidator.IsEmptyOrNull(this.appName) &&
            Loader.getInstance().getHttpManager().getRequest().getUrlArgs().KeyExists(HttpRequestConstants.APP_NAME)) {
            this.appName = decodeURIComponent(Loader.getInstance().getHttpManager().getRequest().getUrlArgs()
                .getItem(HttpRequestConstants.APP_NAME));
        }
        return this.appName;
    }

    /**
     * @returns {string} Returns release name based on target settings
     */
    public getReleaseName() : string {
        this.releaseName = this.appConfigData.build.releaseName;
        if (ObjectValidator.IsEmptyOrNull(this.releaseName)) {
            this.releaseName = decodeURIComponent(Loader.getInstance().getHttpManager().getRequest().getUrlArgs()
                .getItem(HttpRequestConstants.RELEASE_NAME));
        }
        return this.releaseName;
    }

    /**
     * @returns {string} Returns platform type based on build settings
     */
    public getPlatform() : string {
        this.platform = this.appConfigData.build.platform;
        if (ObjectValidator.IsEmptyOrNull(this.platform)) {
            this.platform = decodeURIComponent(Loader.getInstance().getHttpManager().getRequest().getUrlArgs()
                .getItem(HttpRequestConstants.PLATFORM));
        }
        return this.platform;
    }

    /**
     * @returns {number} Returns process ID for stand-alone application
     */
    public getProcessID() : number {
        if (ObjectValidator.IsEmptyOrNull(this.processId)) {
            this.processId = StringUtils.ToInteger(Loader.getInstance().getHttpManager().getRequest()
                .getUrlArgs().getItem(HttpRequestConstants.APP_PID));
        }
        return this.processId;
    }

    /**
     * @returns {string} Returns time of build
     */
    public getBuildTime() : string {
        return this.appConfigData.build.time;
    }

    /**
     * @returns {boolean} Returns true, if type of runtime mode is production, otherwise false.
     */
    public IsProductionMode() : boolean {
        return this.appConfigData.build.type === "prod";
    }

    /**
     * @returns {boolean} Returns true, if html content is allowed at runtime, otherwise false.
     */
    public HtmlOutputAllowed() : boolean {
        return this.htmlAllowed;
    }

    /**
     * @returns {IProject} Returns configuration object which is used by current instance after merge with all external configurations.
     */
    public getProjectConfig() : IProject {
        return this.appConfigData;
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        let output : string = "";
        output += $prefix + "[\"getProjectName\"]" +
            StringUtils.Tab(1, $htmlTag) + this.getProjectName() + StringUtils.NewLine($htmlTag);
        output += $prefix + "[\"getProjectVersion\"]" +
            StringUtils.Tab(1, $htmlTag) + this.getProjectVersion() + StringUtils.NewLine($htmlTag);
        output += $prefix + "[\"getBuildTime\"]" +
            StringUtils.Tab(1, $htmlTag) + this.getBuildTime() + StringUtils.NewLine($htmlTag);
        output += $prefix + "[\"IsProductionMode\"]" +
            StringUtils.Tab(1, $htmlTag) + Convert.ObjectToString(this.IsProductionMode(), "", $htmlTag) +
            StringUtils.NewLine($htmlTag);
        output += $prefix + "[\"HtmlOutputAllowed\"]" +
            StringUtils.Tab(1, $htmlTag) + Convert.ObjectToString(this.HtmlOutputAllowed(), "", $htmlTag) +
            StringUtils.NewLine($htmlTag);
        return output;
    }

    protected getConfigPaths() : string[] {
        const paths : string[] = [];
        if (!ObjectValidator.IsEmptyOrNull(this.appConfigData.target.appDataPath)) {
            this.appConfigData.target.appDataPath = StringUtils.Replace(this.appConfigData.target.appDataPath, "\\", "/");
            paths.push(this.appConfigData.target.appDataPath + "/" + this.getAppName() + ".config.jsonp");
        }
        return paths;
    }

    protected fileLoaderClass() : any {
        return ScriptHandler;
    }

    protected validate($config : IEnvironmentConfig) : boolean {
        return true;
    }

    protected printHandler($message : string) : void {
        IOHandlerFactory.getHandler(IOHandlerType.CONSOLE).Print($message);
    }

    protected errorHandler($error : Error | ErrorEvent, $path? : string) : boolean {
        this.printHandler(">> " + $error.message);
        return true;
    }
}

export interface IEnvironmentConfig {
    path : string;
    data : any;
    processed : boolean;
}

// generated-code-start
export const IEnvironmentConfig = globalThis.RegisterInterface(["path", "data", "processed"]);
// generated-code-end
