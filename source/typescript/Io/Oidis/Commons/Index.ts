/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseHttpResolver } from "./HttpProcessor/Resolvers/BaseHttpResolver.js";
import { Echo } from "./Utils/Echo.js";

/**
 * Index request resolver class provides handling of web index page.
 */
export class Index extends BaseHttpResolver {

    protected resolver() : void {
        /* dev:start */
        Echo.Print("<H3>Runtime tests</H3>");
        Echo.Print("<a href=\"#" + this.createLink("/web/HttpRequestParserTest") + "\">HttpRequestParserTest</a>");
        Echo.Println("<a href=\"#" + this.createLink("/web/HttpManagerTest") + "\">HttpManagerTest</a>");
        Echo.Println("<a href=\"#" + this.createLink("/web/PersistenceApiTest") + "\">PersistenceApiTest</a>");
        Echo.Println("<a href=\"#" + this.createLink("/web/EventsManagerTest") + "\">EventsManagerTest</a>");
        Echo.Println("<a href=\"#" + this.createLink("/web/ExceptionsManagerTest") + "\">ExceptionsManagerTest</a>");
        Echo.Println("<a href=\"#" + this.createLink("/web/TimeoutTest") + "\">TimeoutTest</a>");
        Echo.Println("<a href=\"#" + this.createLink("/web/ChromiumConnectorTest") + "\">ChromiumConnectorTest</a>");
        Echo.Println("<a href=\"#" + this.createLink("/web/CoverageTest") + "\">CoverageTest</a>");
        Echo.Println("<a href=\"#" + this.createLink("/web/AsyncRuntimeTest") + "\">AsyncRuntimeTest</a>");
        Echo.Println("<a href=\"#" + this.createLink("/web/JsonpFileReaderTest") + "\">JsonpFileReaderTest</a>");
        Echo.Println("<a href=\"#" + this.createLink("/web/JxbrowserBridgeClientTest") + "\">JxbrowserBridgeClientTest</a>");
        Echo.Println("<a href=\"#" + this.createLink("/web/LogItTest") + "\">LogItTest</a>");
        /* dev:end */
        Echo.Print("<H3>About resolvers</H3>");
        Echo.Print("<a href=\"#" + this.createLink("/about/Cache") + "\">/about/Cache</a>");
        Echo.Println("<a href=\"#" + this.createLink("/about/Env") + "\">/about/Env</a>");
    }
}
