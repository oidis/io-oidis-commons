/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "../Primitives/ArrayList.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { Convert } from "../Utils/Convert.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { Property } from "../Utils/Property.js";
import { StringUtils } from "../Utils/StringUtils.js";

/**
 * HttpResolverCollection class should be used as structure object describing particular part of request pattern.
 */
export class HttpResolversCollection extends BaseObject {

    private readonly requestPattern : string;
    private resolverClassName : any;
    private parameterName : string;
    private ignoreCase : boolean;
    private isOrdered : boolean;
    private levelsCount : string;
    private childrenCollection : ArrayList<HttpResolversCollection>;

    /**
     * @param {string} $httpKeyPattern Http request key pattern value for resolver subscription.
     * @param {any} [$resolverClassName] Resolver class name or callback.
     */
    constructor($httpKeyPattern : string, $resolverClassName? : any) {
        super();

        if ($httpKeyPattern === null) {
            throw new Error("HttpKeyPattern can not be null.");
        }

        this.requestPattern = $httpKeyPattern;
        if (ObjectValidator.IsSet($resolverClassName)) {
            this.resolverClassName = $resolverClassName;
        } else {
            this.resolverClassName = null;
        }
        this.parameterName = null;
        this.ignoreCase = true;
        this.childrenCollection = new ArrayList<HttpResolversCollection>();
        this.isOrdered = false;
        this.levelsCount = "0";
    }

    /**
     * @returns {string} Returns http request key pattern subscribed to resolver callback.
     */
    public Pattern() : string {
        return this.requestPattern;
    }

    /**
     * @param {string} $httpKeyPattern Request http pattern which should be validated.
     * @returns {boolean} Returns true if http key request pattern is already registered otherwise false.
     */
    public Contains($httpKeyPattern : string) : boolean {
        const childrenList : HttpResolversCollection[] = this.childrenCollection.getAll();
        let index : number;
        for (index = 0; index < childrenList.length; index++) {
            if (childrenList[index].Pattern() === $httpKeyPattern) {
                return true;
            }
        }
        return false;
    }

    /**
     * Set or get resolver class name or callback.
     * @param {any} [$value] If specified set resolver class name or callback.
     * @returns {any} Returns resolver class name or callback subscribed to the http request key pattern.
     */
    public ResolverClassName($value? : any) : any {
        if (ObjectValidator.IsSet($value)) {
            this.resolverClassName = $value;
        }
        return this.resolverClassName;
    }

    /**
     * Set or get http request key pattern index name.
     * @param {string} [$value] If specified set http request key pattern index name.
     * @returns {string} Returns http request key pattern index name.
     */
    public ParameterName($value? : string) : string {
        return this.parameterName = Property.NullString(this.parameterName, $value);
    }

    /**
     * Set or get http request pattern resolving behaviour.
     * @param {boolean} [$value] If specified set ignore case behaviour.
     * @returns {boolean} Returns true if http request pattern should be resolved in ignore case otherwise false.
     */
    public IgnoreCase($value? : boolean) : boolean {
        return this.ignoreCase = Property.Boolean(this.ignoreCase, $value);
    }

    /**
     * Set or get count of subrequests grouped by current http request key pattern.
     * Flexible count of levels is marked by '+'.
     * @param {number|string} [$value] If specified set count of subrequests levels.
     * @param {boolean} [$flexible] If specified mark leves count as flexible.
     * @returns {string} Returns levels count number with optional flexible mark.
     */
    public LevelsCount($value? : number | string, $flexible? : boolean) : string {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            if (ObjectValidator.IsString($value)) {
                $value = StringUtils.Remove(<string>$value, "+");
                $value = StringUtils.ToInteger(<string>$value);
            }

            if (StringUtils.Contains(this.levelsCount, "+")) {
                $flexible = true;
                this.levelsCount = StringUtils.Remove(this.levelsCount, "+");
            }

            if ($value > this.levelsCount) {
                this.levelsCount = (<string>$value);
            }

            if (ObjectValidator.IsSet($flexible) && $flexible) {
                this.levelsCount += "+";
            }
        }
        return this.levelsCount;
    }

    /**
     * @param {HttpResolversCollection} $resolverCollection Sub group of resolvers, which should be owned by
     * current http request key pattern.
     * @returns {void}
     */
    public AddChild($resolverCollection : HttpResolversCollection) : void {
        if (!ObjectValidator.IsEmptyOrNull($resolverCollection)) {
            if (!this.Contains($resolverCollection.Pattern())) {
                this.childrenCollection.Add($resolverCollection);
                this.isOrdered = false;
            }
        }
    }

    /**
     * @returns {ArrayList<HttpResolversCollection>} Returns resolvers collection grouped by
     * current http request key pattern.
     */
    public getChildrenList() : ArrayList<HttpResolversCollection> {
        return this.childrenCollection;
    }

    /**
     * @param {string} $httpKeyPattern Http request key pattern which should be found in subresolvers collection.
     * @returns {HttpResolversCollection} Returns resolvers collection if http request ey pattern has been found,
     * otherwise null.
     */
    public getChild($httpKeyPattern : string) : HttpResolversCollection {
        const childrenList : HttpResolversCollection[] = this.childrenCollection.getAll();
        let index : number;
        for (index = 0; index < childrenList.length; index++) {
            if (childrenList[index].Pattern() === $httpKeyPattern) {
                return childrenList[index];
            }
        }

        return null;
    }

    /**
     * Order registered resolvers collection by count of http key pattern flexible levels.
     * @returns {void}
     */
    public OrderChildrenCollection() : void {
        if (!this.isOrdered) {
            let fullString : ArrayList<ArrayList<HttpResolversCollection>> = new ArrayList<ArrayList<HttpResolversCollection>>();
            const wildcharString : ArrayList<ArrayList<HttpResolversCollection>> = new ArrayList<ArrayList<HttpResolversCollection>>();
            let flexPathCollection : HttpResolversCollection = null;

            this.childrenCollection.foreach(($child : HttpResolversCollection) : void => {
                const key : string = $child.Pattern();
                const childKey : number = StringUtils.Length(key);
                if (key === "**") {
                    flexPathCollection = $child;
                } else if (StringUtils.Contains(key, "*")) {
                    if (!wildcharString.KeyExists(childKey)) {
                        wildcharString.Add(new ArrayList<HttpResolversCollection>($child), childKey);
                    } else {
                        wildcharString.getItem(childKey).Add($child);
                    }
                } else {
                    if (!fullString.KeyExists(childKey)) {
                        fullString.Add(new ArrayList($child), childKey);
                    } else {
                        fullString.getItem(childKey).Add($child);
                    }
                }
            });

            let orderedArray : string[] = [];
            let index : number;
            let length : number;
            let keys : string[];
            let currentKey : string;
            let nextKey : string;
            let orderedArrayList : ArrayList<ArrayList<HttpResolversCollection>>;
            let resolversOrdered : ArrayList<HttpResolversCollection>;

            length = fullString.Length();
            if (length > 0) {
                keys = <string[]>fullString.getKeys();
                for (index = 0; index < length; index++) {
                    orderedArray.push(keys[index]);
                }
                for (index = 0; index < length - 1; index++) {
                    currentKey = orderedArray[index];
                    nextKey = orderedArray[index + 1];
                    if (StringUtils.ToInteger(currentKey) < StringUtils.ToInteger(nextKey)) {
                        orderedArray[index] = nextKey;
                        orderedArray[index + 1] = currentKey;
                        index = -1;
                    }
                }
                orderedArrayList = new ArrayList<ArrayList<HttpResolversCollection>>();
                for (index = 0; index < length; index++) {
                    currentKey = orderedArray[index];
                    orderedArrayList.Add(fullString.getItem(currentKey), currentKey);
                }
                fullString = orderedArrayList;
                fullString.foreach(($value : ArrayList<HttpResolversCollection>, $key? : number) : void => {
                    length = $value.Length();
                    orderedArray = [];
                    for (index = 0; index < length; index++) {
                        orderedArray.push($value.getItem(index).Pattern());
                    }
                    for (index = 0; index < length - 1; index++) {
                        currentKey = orderedArray[index];
                        nextKey = orderedArray[index + 1];
                        if (currentKey > nextKey) {
                            orderedArray[index] = nextKey;
                            orderedArray[index + 1] = currentKey;
                            index = -1;
                        }
                    }
                    resolversOrdered = new ArrayList<HttpResolversCollection>();
                    for (index = 0; index < length; index++) {
                        $value.foreach(($resolver : HttpResolversCollection) : boolean => {
                            if (orderedArray[index] === $resolver.Pattern()) {
                                resolversOrdered.Add($resolver);
                                return false;
                            }
                            return true;
                        });
                    }
                    fullString.Add(resolversOrdered, $key);
                });
            }

            wildcharString.SortByKeyDown();
            wildcharString.foreach(($value : ArrayList<HttpResolversCollection>) : void => {
                length = $value.Length();
                let index : number;
                for (index = 0; index < length - 1; index++) {
                    const current : HttpResolversCollection = $value.getItem(index);
                    const next : HttpResolversCollection = $value.getItem(index + 1);
                    if (current.Pattern() < next.Pattern() ||
                        StringUtils.IndexOf(current.Pattern(), "*") < StringUtils.IndexOf(next.Pattern(), "*")) {
                        $value.Add(next, index);
                        $value.Add(current, index + 1);
                        index = -1;
                    }
                }
            });

            const childrenCollection : ArrayList<HttpResolversCollection> = new ArrayList<HttpResolversCollection>();
            fullString.foreach(($values : ArrayList<HttpResolversCollection>) : void => {
                $values.foreach(($part : HttpResolversCollection) : void => {
                    childrenCollection.Add($part);
                });
            });

            wildcharString.foreach(($values : ArrayList<HttpResolversCollection>) : void => {
                $values.foreach(($part : HttpResolversCollection) : void => {
                    childrenCollection.Add($part);
                });
            });
            this.childrenCollection = childrenCollection;

            if (!ObjectValidator.IsEmptyOrNull(flexPathCollection)) {
                this.childrenCollection.Add(flexPathCollection);
            }

            this.isOrdered = true;
        }
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        let resolver : string = "";
        if (ObjectValidator.IsEmptyOrNull(this.resolverClassName)) {
            resolver = "resolver not assigned";
        } else if (ObjectValidator.IsString(this.resolverClassName)) {
            resolver = "\"" + this.resolverClassName + "\"";
        } else {
            resolver = "\"" + Convert.ToType(this.resolverClassName) + "\"";
        }

        if ($htmlTag) {
            return Convert.ToHtmlContentBlock("\"" + this.requestPattern + "\" => " + resolver + StringUtils.Space(3),
                $prefix + StringUtils.Tab(2) + "<i>ignore case:</i> " +
                Convert.BooleanToString(this.ignoreCase) + StringUtils.NewLine() +
                $prefix + StringUtils.Tab(2) + "<i>count of levels:</i> " +
                Convert.ObjectToString(this.levelsCount) + StringUtils.NewLine() +
                $prefix + StringUtils.Tab(2) + "<i>parameter name:</i> " +
                Convert.ObjectToString(this.parameterName) + StringUtils.NewLine() +
                $prefix + StringUtils.Tab(2) + "<i>child list:</i> " +
                this.childrenCollection.ToString($prefix + StringUtils.Tab(4)));
        } else {
            let output : string = "\"" + this.requestPattern + "\" => " + resolver + StringUtils.Space(3, false) +
                StringUtils.NewLine(false) +
                $prefix + StringUtils.Tab(2, false) + "ignore case: " +
                Convert.BooleanToString(this.ignoreCase) + StringUtils.NewLine(false) +
                $prefix + StringUtils.Tab(2, false) + "count of levels: " +
                Convert.ObjectToString(this.levelsCount, "", false) + StringUtils.NewLine(false) +
                $prefix + StringUtils.Tab(2, false) + "parameter name: " +
                Convert.ObjectToString(this.parameterName, "", false) + StringUtils.NewLine(false) +
                $prefix + StringUtils.Tab(2, false) + "child list: ";
            if (!ObjectValidator.IsEmptyOrNull(this.childrenCollection)) {
                output += this.childrenCollection.ToString($prefix + StringUtils.Tab(2, false), false);
            } else {
                output += "EMPTY" + StringUtils.NewLine(false);
            }
            return output;
        }
    }

    public toString() : string {
        return this.ToString();
    }
}
