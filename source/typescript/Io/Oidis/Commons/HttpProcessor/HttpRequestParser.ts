/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BrowserType } from "../Enums/BrowserType.js";
import { HttpMethodType } from "../Enums/HttpMethodType.js";
import { ExceptionsManager } from "../Exceptions/ExceptionsManager.js";
import { Loader } from "../Loader.js";
import { StorageHandler } from "../PersistenceApi/Handlers/StorageHandler.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { Convert } from "../Utils/Convert.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { StringUtils } from "../Utils/StringUtils.js";

/**
 * HttpRequestParser class provides parsing methods of current http request.
 */
export class HttpRequestParser extends BaseObject {

    protected parsedInfo : any;
    private readonly owner : any;
    private headersParsed : boolean;

    /**
     * @param {string} [$urlBase] Force url base to parsed request info.
     * @param {any} [$owner] Specify request owner.
     */
    constructor($urlBase? : string, $owner? : any) {
        super();
        this.parsedInfo = {
            uri               : "",
            url               : "",
            scheme            : "http://",
            hostName          : "",
            port              : 80,
            relativeRoot      : "",
            relativeDirectory : "",
            scriptFileName    : "",
            args              : new ArrayList<string>(),
            headers           : new ArrayList<string>(),
            eTags             : new ArrayList<string>(),
            lastModifiedTime  : Convert.TimeToGMTformat(new Date()),
            hostIpAddress     : "127.0.0.1",
            clientIpAddress   : "127.0.0.1",
            language          : "",
            platform          : "",
            userAgent         : "",
            browserType       : BrowserType.UNKNOWN,
            browserVersion    : -1,
            cookieEnabled     : false,
            isOnServer        : false,
            isOnLine          : false,
            isSearchBot       : false,
            isMobileDevice    : false,
            isJre             : false,
            isJreSimulator    : false,
            isConnector       : false,
            isConnectorDebug  : false,
            isPlugin          : false,
            isIdeaHost        : false,
            isLocalhostHosting: false,
            isIOS             : false
        };
        this.headersParsed = false;

        if (ObjectValidator.IsSet($urlBase)) {
            this.parsedInfo.relativeRoot = $urlBase;
        }
        this.owner = ObjectValidator.IsSet($owner) ? $owner : window;

        this.parseUri();
        this.parseUserAgent();
    }

    /**
     * @returns {any} Returns requested owner.
     */
    public getOwner() : any {
        return this.owner;
    }

    /**
     * @returns {string} Returns requested URI.
     */
    public getUri() : string {
        return this.parsedInfo.uri;
    }

    /**
     * @returns {string} Returns requested URL.
     */
    public getUrl() : string {
        return this.parsedInfo.url;
    }

    /**
     * @returns {string} Returns scheme + host name + port, ends with /.
     */
    public getHostUrl() : string {
        return this.parsedInfo.hostName;
    }

    /**
     * @returns {string} Returns host URL + relative directory, ends with /.
     */
    public getBaseUrl() : string {
        let url : string = this.parsedInfo.hostName;
        if (!ObjectValidator.IsEmptyOrNull(this.parsedInfo.relativeDirectory)) {
            url += this.parsedInfo.relativeDirectory;
        }
        return url + "/";
    }

    /**
     * @returns {string} Returns relative path to required script file name.
     */
    public getScriptPath() : string {
        return this.parsedInfo.scriptFileName;
    }

    /**
     * @returns {ArrayList} Returns array of passed arguments by required URL.
     */
    public getUrlArgs() : ArrayList<string> {
        return this.parsedInfo.args;
    }

    /**
     * @returns {string} Returns directory relative to URL root path.
     */
    public getRelativeDirectory() : string {
        return this.parsedInfo.relativeDirectory;
    }

    /**
     * @returns {string} Returns root path of required URL.
     */
    public getRelativeRoot() : string {
        return this.parsedInfo.relativeRoot;
    }

    /**
     * @returns {string} Returns client IP address.
     */
    public getClientIP() : string {
        return this.parsedInfo.clientIpAddress;
    }

    /**
     * @param {boolean} [$simulator=false] Specify, if should be validated Oidis hosting simulator.
     * @returns {boolean} Returns true, if runtime environment is type of server, otherwise false.
     */
    public IsOnServer($simulator : boolean = false) : boolean {
        if ($simulator) {
            this.parseHeaders();
            return this.parsedInfo.isLocalhostHosting;
        }
        return this.parsedInfo.isOnServer;
    }

    /**
     * @returns {boolean} Returns true, if client has been detected as search bot, otherwise false.
     */
    public IsSearchBot() : boolean {
        return this.parsedInfo.isSearchBot;
    }

    /**
     * @returns {boolean} Returns true, if client device has been detected as mobile device, otherwise false.
     */
    public IsMobileDevice() : boolean {
        return this.parsedInfo.isMobileDevice;
    }

    /**
     * @param {boolean} [$simulator=false] Specify, if should be validated Oidis JRE simulator.
     * @returns {boolean} Returns true, if client device has been detected as Oidis JRE, otherwise false.
     */
    public IsJre($simulator : boolean = false) : boolean {
        if ($simulator) {
            return this.parsedInfo.isJreSimulator;
        } else {
            return this.parsedInfo.isJre;
        }
    }

    /**
     * @returns {boolean} Returns true, if runtime environment has been detected as Oidis Plugin, otherwise false.
     */
    public IsPlugin() : boolean {
        return this.parsedInfo.isPlugin;
    }

    /**
     * @param {boolean} [$remote=false] Specify, if should be validated Remote Oidis Connector instance.
     * @returns {boolean} Returns true, if server has been detected as Oidis Connector, otherwise false.
     */
    public IsConnector($remote : boolean = false) : boolean {
        if ($remote) {
            return this.parsedInfo.isConnectorDebug;
        } else {
            this.parseHeaders();
            return this.parsedInfo.isConnector;
        }
    }

    /**
     * @returns {boolean} Returns true, if server has been detected as IntelliJ IDEA, otherwise false.
     */
    public IsIdeaHost() : boolean {
        this.parseHeaders();
        return this.parsedInfo.isIdeaHost;
    }

    /**
     * @returns {boolean} Returns true, if server has been detected as Oidis Localhost, otherwise false.
     */
    public IsLocalhostHosting() : boolean {
        this.parseHeaders();
        return this.parsedInfo.isLocalhostHosting;
    }

    /**
     * @returns {boolean} Returns true, if device OS has been detected as iOS, otherwise false.
     */
    public IsIOS() : boolean {
        return this.parsedInfo.isIOS;
    }

    /**
     * @returns {boolean} Returns true, if cookies are enabled at client browser, otherwise false.
     */
    public IsCookieEnabled() : boolean {
        return this.parsedInfo.cookieEnabled;
    }

    /**
     * @returns {string} Returns user agent string provided by the browser.
     */
    public getUserAgent() : string {
        return this.parsedInfo.userAgent;
    }

    /**
     * @returns {BrowserType} Returns detected type of browser.
     */
    public getBrowserType() : BrowserType {
        return this.parsedInfo.browserType;
    }

    /**
     * @returns {number} Returns detected version number of client browser.
     */
    public getBrowserVersion() : number {
        return this.parsedInfo.browserVersion;
    }

    /**
     * @returns {ArrayList} Returns list of all headers passed by current request.
     */
    public getHeaders() : ArrayList<string> {
        this.parseHeaders();
        return this.parsedInfo.headers;
    }

    /**
     * @returns {ArrayList} Returns list of Etags filtered from current headers.
     */
    public getEtags() : ArrayList<string> {
        this.parseHeaders();
        return this.parsedInfo.eTags;
    }

    /**
     * @returns {string} Returns last modified time parsed from headers in GMT format.
     */
    public getLastModifiedTime() : string {
        this.parseHeaders();
        return this.parsedInfo.lastModifiedTime;
    }

    /**
     * @returns {ArrayList<string>} Returns ArrayList of all cookies stored in the browser.
     */
    public getCookies() : ArrayList<string> {
        const cookies : ArrayList<string> = new ArrayList<string>();
        let data : string = null;
        if (this.IsCookieEnabled() && !ObjectValidator.IsEmptyOrNull(document.cookie)) {
            data = document.cookie;
        } else if (this.getHeaders().KeyExists("cookie")) {
            data = this.getHeaders().getItem("cookie");
        }
        if (!ObjectValidator.IsEmptyOrNull(data)) {
            data.split(";").forEach(($value : string) : void => {
                const valueStart : number = $value.indexOf("=");
                if (valueStart !== -1) {
                    const key : string = decodeURIComponent($value.substring(0, valueStart).trim());
                    if (!ObjectValidator.IsEmptyOrNull(key)) {
                        const value : string = decodeURIComponent($value.substring(valueStart + 1).trim());
                        cookies.Add(value, key);
                    }
                }
            });
        }
        return cookies;
    }

    /**
     * @param {string} $name Cookie name, which should be found.
     * @returns {string} Returns string value subscribed to cookie name, if name has been found,
     * otherwise returns empty string.
     */
    public getCookie($name : string) : string {
        if (!ObjectValidator.IsEmptyOrNull($name)) {
            const cookies : ArrayList<string> = this.getCookies();
            if (cookies.KeyExists($name)) {
                return cookies.getItem($name);
            }
        }
        return "";
    }

    /**
     * Find cookie by name and immediately remove it after retrieving
     * @param {string} $name Cookie name, which should be found.
     * @returns {string} Returns string value subscribed to cookie name, if name has been found,
     * otherwise returns empty string.
     */
    public getCookieOnce($name : string) : string {
        const cookies : ArrayList<string> = this.getCookies();
        let value : string = "";
        if (cookies.KeyExists($name)) {
            value = cookies.getItem($name);
            document.cookie = $name + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT";
            cookies.RemoveAt(cookies.getKeys().indexOf($name));
        }
        return value;
    }

    /**
     * @returns {ArrayList<string>} Returns ArrayList of all locally stored items.
     */
    public getStorageItems() : ArrayList<string> {
        const items : ArrayList<string> = new ArrayList<string>();
        if (StorageHandler.IsSupported()) {
            let index : number;
            for (index = 0; index < localStorage.length; index++) {
                const key : string = localStorage.key(index);
                items.Add(localStorage.getItem(key), key);
            }
        }
        return items;
    }

    /**
     * @param {string} $name Storage item name, which should be found.
     * @returns {string} Returns string value subscribed to storage item name, if name has been found,
     * otherwise returns empty string.
     */
    public getStorageItem($name : string) : string {
        if (!ObjectValidator.IsEmptyOrNull($name)) {
            const items : ArrayList<string> = this.getStorageItems();
            if (items.KeyExists($name)) {
                return items.getItem($name);
            }
        }
        return "";
    }

    /**
     * @param {string} $uri Uri, which should be validated.
     * @returns {boolean} Returns true, if uri has same origin as host otherwise false.
     */
    public IsSameOrigin($uri : string) : boolean {
        const server : Location = this.getServerLocation();
        const parser : HTMLAnchorElement = document.createElement("a");
        parser.href = $uri;
        return server.protocol === parser.protocol && server.hostname === parser.hostname && server.port === parser.port;
    }

    /**
     * @returns {string} Returns formatted string with all parsed information in HTML format.
     */
    public ToString() : string {
        this.parseHeaders();
        let output : string = "uri: " + this.parsedInfo.uri;
        output += StringUtils.NewLine() + "url: " + this.parsedInfo.url;
        output += StringUtils.NewLine() + "scheme: " + this.parsedInfo.scheme;
        output += StringUtils.NewLine() + "host: " + this.parsedInfo.hostName;
        output += StringUtils.NewLine() + "port: " + Convert.ObjectToString(this.parsedInfo.port);
        output += StringUtils.NewLine() + "script: " + this.parsedInfo.scriptFileName;
        output += StringUtils.NewLine() + "args: " + Convert.ObjectToString(this.parsedInfo.args);
        output += StringUtils.NewLine() + "relative dir: " + this.parsedInfo.relativeDirectory;
        output += StringUtils.NewLine() + "relative root: " + this.parsedInfo.relativeRoot;

        output += StringUtils.NewLine() + StringUtils.NewLine() + "server IP: " + this.parsedInfo.hostIpAddress;
        output += StringUtils.NewLine() + "client IP: " + this.parsedInfo.clientIpAddress;

        output += StringUtils.NewLine();
        output += StringUtils.NewLine() + "cookieEnabled: " + Convert.BooleanToString(this.parsedInfo.cookieEnabled);
        output += StringUtils.NewLine() + "language: " + this.parsedInfo.language;
        output += StringUtils.NewLine() + "onServer: " + Convert.BooleanToString(this.parsedInfo.isOnServer);
        output += StringUtils.NewLine() + "onLine: " + Convert.BooleanToString(this.parsedInfo.isOnLine);
        output += StringUtils.NewLine() + "platform: " + this.parsedInfo.platform;
        output += StringUtils.NewLine() + "user agent: " + this.parsedInfo.userAgent;
        output += StringUtils.NewLine() + "is bot: " + Convert.BooleanToString(this.parsedInfo.isSearchBot);
        output += StringUtils.NewLine() + "is mobile: " + Convert.BooleanToString(this.parsedInfo.isMobileDevice);
        output += StringUtils.NewLine() + "is JRE: " + Convert.BooleanToString(this.parsedInfo.isJre);
        output += StringUtils.NewLine() + "is Plugin: " + Convert.BooleanToString(this.parsedInfo.isPlugin);
        output += StringUtils.NewLine() + "browser type: " + BrowserType[this.parsedInfo.browserType];
        output += StringUtils.NewLine() + "browser version: " + Convert.ObjectToString(this.parsedInfo.browserVersion);

        output += StringUtils.NewLine() + StringUtils.NewLine() + "headers: " + Convert.ObjectToString(this.parsedInfo.headers);
        output += StringUtils.NewLine() + "is Connector: " + Convert.BooleanToString(this.parsedInfo.isConnector);
        output += StringUtils.NewLine() + "is IDEA Hosting: " + Convert.BooleanToString(this.parsedInfo.isIdeaHost);
        output += StringUtils.NewLine() + "eTags: " + Convert.ObjectToString(this.parsedInfo.eTags);
        output += StringUtils.NewLine() + "last modified time: " + Convert.ObjectToString(this.parsedInfo.lastModifiedTime);

        output += StringUtils.NewLine() + StringUtils.NewLine() + "document cookies: " + Convert.ObjectToString(document.cookie);
        output += StringUtils.NewLine() + "parsed cookies: " + Convert.ObjectToString(this.getCookies());

        return output;
    }

    public toString() : string {
        return this.ToString();
    }

    protected getServerLocation() : Location {
        return window.location;
    }

    protected getNavigator() : Navigator {
        return window.navigator;
    }

    protected getRawHeaders() : ArrayList<string> {
        let request : XMLHttpRequest;
        if (Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed() && (
            this.parsedInfo.scheme !== "file://" ||
            (this.parsedInfo.scheme === "file://" &&
                this.parsedInfo.browserType !== BrowserType.INTERNET_EXPLORER &&
                this.parsedInfo.browserType !== BrowserType.GOOGLE_CHROME))) {
            try {
                // firefox, opera 8.0+, safari
                request = new XMLHttpRequest();
            } catch (e) {
                // internet explorer
                try {
                    request = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                    try {
                        request = new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (e) {
                        ExceptionsManager.Throw("parser", e);
                    }
                }
            }
        }
        const headers : ArrayList<string> = new ArrayList<string>();
        try {
            if (ObjectValidator.IsSet(request)) {
                request.open(HttpMethodType.HEAD, window.location.href, false);
                request.send(null);
                const tmp : ArrayList<string> = ArrayList.ToArrayList(StringUtils.Split(request.getAllResponseHeaders(), "\n"));

                tmp.foreach((value : string) : void => {
                    const index : number = StringUtils.IndexOf(value, ":");
                    const key : string = StringUtils.Substring(value, 0, index);
                    value = StringUtils.Substring(value, index + 1);
                    if (StringUtils.StartsWith(value, " ")) {
                        value = StringUtils.Substring(value, 1);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(value)) {
                        headers.Add(value, StringUtils.ToLowerCase(StringUtils.Replace(StringUtils.Remove(key, "HTTP_"), "_", "-")));
                    }
                });
            }
        } catch (e) {
            // unable to read or parse headers
        }
        return headers;
    }

    protected parseUri() : void {
        const server : Location = this.getServerLocation();

        if (!ObjectValidator.IsEmptyOrNull(server.port)) {
            this.parsedInfo.port = StringUtils.ToInteger(server.port);
        } else {
            this.parsedInfo.port = 80;
        }
        this.parsedInfo.uri = server.href;
        while (StringUtils.Contains(this.parsedInfo.uri, "//")) {
            this.parsedInfo.uri = StringUtils.Replace(this.parsedInfo.uri, "//", "/");
        }
        this.parsedInfo.uri = StringUtils.Replace(this.parsedInfo.uri, server.protocol + "/", server.protocol + "//");
        this.parsedInfo.scheme = server.protocol + "//";

        this.parsedInfo.isOnServer = !StringUtils.Contains(StringUtils.ToLowerCase(this.parsedInfo.scheme), "file");

        this.parsedInfo.hostName = this.parsedInfo.uri;
        if (StringUtils.Contains(this.parsedInfo.uri, "#")) {
            this.parsedInfo.url = StringUtils.Split(this.parsedInfo.uri, "#")[1];
            this.parsedInfo.hostName = StringUtils.Remove(this.parsedInfo.hostName, "#" + this.parsedInfo.url);
        } else {
            this.parsedInfo.url = "";
        }

        this.parsedInfo.scriptFileName = this.parsedInfo.url;

        this.parsedInfo.args = new ArrayList<string>();
        if (StringUtils.Contains(this.parsedInfo.uri, "?")) {
            let query : string = StringUtils.Split(this.parsedInfo.uri, "?")[1];
            if (StringUtils.Contains(query, "#")) {
                query = StringUtils.Split(query, "#")[0];
            }
            StringUtils.Split(query, "&").forEach(($argument : string) : void => {
                if (!ObjectValidator.IsEmptyOrNull($argument)) {
                    const split : string[] = StringUtils.Split($argument, "=");
                    if (!ObjectValidator.IsEmptyOrNull(split[0])) {
                        let value : string = "";
                        // + is allowed in urlencoded query (only after ?) as symbol for "space"
                        if (split.length > 1 && !ObjectValidator.IsEmptyOrNull(split[1])) {
                            value = decodeURIComponent(split[1].replace("+", "%20"));
                        }
                        this.parsedInfo.args.Add(value, split[0]);
                    }
                }
            });
            this.parsedInfo.hostName = StringUtils.Remove(this.parsedInfo.hostName, "?" + query);
            this.parsedInfo.scriptFileName = StringUtils.Remove(this.parsedInfo.scriptFileName, "?" + query);
        }

        this.parsedInfo.relativeDirectory = StringUtils.Replace(StringUtils.Substring(this.parsedInfo.url, 0,
            StringUtils.IndexOf(this.parsedInfo.url, "/", false)), "\\", "/");

        this.parsedInfo.isJreSimulator = StringUtils.ContainsIgnoreCase(this.parsedInfo.args.getItem("debug"), "JRESimulator");
        this.parsedInfo.isConnectorDebug = StringUtils.ContainsIgnoreCase(this.parsedInfo.args.getItem("debug"), "Connector");
        this.parsedInfo.isLocalhostHosting = StringUtils.ContainsIgnoreCase(this.parsedInfo.args.getItem("debug"), "Hosting");
    }

    protected parseUserAgent() : void {
        const navigator : Navigator = this.getNavigator();

        this.parsedInfo.cookieEnabled = navigator.cookieEnabled;
        this.parsedInfo.language = navigator.language;
        this.parsedInfo.isOnLine = navigator.onLine;
        this.parsedInfo.platform = navigator.platform;

        this.parsedInfo.userAgent = navigator.userAgent;
        this.parsedInfo.isMobileDevice = StringUtils.Contains(this.parsedInfo.userAgent, "Mobile", "iPad");
        this.parsedInfo.isJre = false;
        this.parsedInfo.isPlugin = false;
        this.parsedInfo.isSearchBot = false;
        this.parsedInfo.browserType = BrowserType.UNKNOWN;
        this.parsedInfo.browserVersion = -1;
        let browserVersion : string = "-1";

        if (StringUtils.Contains(this.parsedInfo.userAgent, "com-wui-framework-plugin", "io-oidis-plugin")) {
            this.parsedInfo.isPlugin = true;
        }
        if (StringUtils.Contains(this.parsedInfo.userAgent, "com-wui-framework-jre", "io-oidis-jre")) {
            this.parsedInfo.isJre = true;
            this.parsedInfo.browserType = BrowserType.JAVA_RE;
        } else if (StringUtils.Contains(this.parsedInfo.userAgent, "bot")) {
            this.parsedInfo.isSearchBot = true;
        } else if (StringUtils.Contains(this.parsedInfo.userAgent, ".NET", "MSIE") ||
            StringUtils.Contains(this.parsedInfo.userAgent, "Mozilla/5.0") &&
            StringUtils.Contains(this.parsedInfo.userAgent, "rv:") &&
            !StringUtils.Contains(this.parsedInfo.userAgent, "Firefox")) {
            this.parsedInfo.browserType = BrowserType.INTERNET_EXPLORER;
            if (StringUtils.Contains(this.parsedInfo.userAgent, "MSIE")) {
                browserVersion = StringUtils.Split(this.parsedInfo.userAgent, "MSIE ")[1];
            } else {
                browserVersion = StringUtils.Split(this.parsedInfo.userAgent, "rv:")[1];
            }
        } else if (StringUtils.Contains(this.parsedInfo.userAgent, "Opera", "OPR")) {
            this.parsedInfo.browserType = BrowserType.OPERA;
            if (StringUtils.Contains(this.parsedInfo.userAgent, "OPR")) {
                browserVersion = StringUtils.Split(this.parsedInfo.userAgent, "OPR/")[1];
            }
        } else if (StringUtils.Contains(this.parsedInfo.userAgent, "Edge", "Edg/")) {
            this.parsedInfo.browserType = BrowserType.EDGE;
            const versionParts : string[] = StringUtils.Split(this.parsedInfo.userAgent, "Edg/");
            browserVersion = versionParts.length >= 2 ? versionParts[1] : "";
            if (ObjectValidator.IsEmptyOrNull(browserVersion)) {
                browserVersion = StringUtils.Split(this.parsedInfo.userAgent, "Edge/")[1];
            }
        } else if (StringUtils.Contains(this.parsedInfo.userAgent, "Chrome")) {
            this.parsedInfo.browserType = BrowserType.GOOGLE_CHROME;
            browserVersion = StringUtils.Split(this.parsedInfo.userAgent, "Chrome/")[1];
        } else if (StringUtils.Contains(this.parsedInfo.userAgent, "Safari")) {
            this.parsedInfo.browserType = BrowserType.SAFARI;
        } else if (StringUtils.Contains(this.parsedInfo.userAgent, "Firefox", "Gecko")) {
            this.parsedInfo.browserType = BrowserType.FIREFOX;
            browserVersion = StringUtils.Split(this.parsedInfo.userAgent, "Firefox/")[1];
        }

        if (browserVersion === "-1" && StringUtils.Contains(this.parsedInfo.userAgent, "Version")) {
            browserVersion = StringUtils.Split(StringUtils.Split(this.parsedInfo.userAgent, "Version")[1], "/")[1];
        }

        if (StringUtils.Contains(browserVersion, " ")) {
            browserVersion = StringUtils.Split(browserVersion, " ")[0];
        }
        if (StringUtils.Contains(browserVersion, ")")) {
            browserVersion = StringUtils.Split(browserVersion, ")")[0];
        }
        browserVersion = StringUtils.Remove(browserVersion, ";");
        this.parsedInfo.browserVersion = StringUtils.ToInteger(browserVersion);

        this.parsedInfo.isIOS = [
            "iPad Simulator",
            "iPhone Simulator",
            "iPod Simulator",
            "iPad",
            "iPhone",
            "iPod"
        ].includes(this.parsedInfo.platform) || (this.parsedInfo.userAgent.includes("Mac") && "ontouchend" in document);
    }

    protected parseHeaders() : void {
        if (!this.headersParsed) {
            this.parsedInfo.headers = this.getRawHeaders();
            if (this.parsedInfo.headers.KeyExists("if-none-match")) {
                this.parsedInfo.eTags = ArrayList.ToArrayList(
                    StringUtils.Split(StringUtils.StripSlashes(this.parsedInfo.headers.getItem("if-none-match")), ", "));
            }
            if (this.parsedInfo.headers.KeyExists("if-modified-since")) {
                this.parsedInfo.lastModifiedTime = StringUtils.StripSlashes(this.parsedInfo.headers.getItem("if-modified-since"));
            }
            const server : string = this.parsedInfo.headers.getItem("server");
            this.parsedInfo.isConnector = StringUtils.ContainsIgnoreCase(server, "Connector");
            this.parsedInfo.isIdeaHost = StringUtils.ContainsIgnoreCase(server, "IntelliJ IDEA");
            if (!this.parsedInfo.isLocalhostHosting) {
                this.parsedInfo.isLocalhostHosting = StringUtils.ContainsIgnoreCase(server, "Localhost");
            }
            this.headersParsed = true;
        }
    }
}
