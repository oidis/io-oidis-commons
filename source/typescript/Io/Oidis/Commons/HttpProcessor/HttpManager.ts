/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "../Enums/Events/EventType.js";
import { GeneralEventOwner } from "../Enums/Events/GeneralEventOwner.js";
import { HttpRequestConstants } from "../Enums/HttpRequestConstants.js";
import { BrowserErrorPage } from "../ErrorPages/BrowserErrorPage.js";
import { CookiesErrorPage } from "../ErrorPages/CookiesErrorPage.js";
import { ExceptionErrorPage } from "../ErrorPages/ExceptionErrorPage.js";
import { Http301MovedPage } from "../ErrorPages/Http301MovedPage.js";
import { Http403ForbiddenPage } from "../ErrorPages/Http403ForbiddenPage.js";
import { Http404NotFoundPage } from "../ErrorPages/Http404NotFoundPage.js";
import { AsyncRequestEventArgs } from "../Events/Args/AsyncRequestEventArgs.js";
import { EventsManager } from "../Events/EventsManager.js";
import { ExceptionsManager } from "../Exceptions/ExceptionsManager.js";
import { IHttpManager } from "../Interfaces/IHttpManager.js";
import { Loader } from "../Loader.js";
import { CacheManager } from "../PersistenceApi/CacheManager.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { HttpRequestParserTest } from "../RuntimeTests/HttpRequestParserTest.js";
import { Convert } from "../Utils/Convert.js";
import { LogIt } from "../Utils/LogIt.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { StringUtils } from "../Utils/StringUtils.js";
import { HttpRequestParser } from "./HttpRequestParser.js";
import { HttpResolversCollection } from "./HttpResolversCollection.js";
import { BaseHttpResolver } from "./Resolvers/BaseHttpResolver.js";

/**
 * HttpManager class manages registration of http request resolvers,
 * provides finding of appropriate resolver and parsing of request parameters.
 */
export class HttpManager extends BaseObject implements IHttpManager {

    private processStartTime : number;
    private request : HttpRequestParser;
    private readonly resolversCollection : HttpResolversCollection;
    private readonly parameters : ArrayList<string>;

    /**
     * @param {HttpRequestParser} $httpRequest Http request info which should be managed by manager instance.
     */
    constructor($httpRequest : HttpRequestParser) {
        super();

        if (ObjectValidator.IsEmptyOrNull(this.processStartTime)) {
            this.processStartTime = new Date().getTime();
        }

        this.request = $httpRequest;

        this.resolversCollection = new HttpResolversCollection(this.request.getHostUrl());
        this.registerGeneralResolvers();
        this.parameters = new ArrayList<string>();
    }

    /**
     * @returns {number} Returns time needed for process of the script.
     */
    public getProcessTime() : number {
        return new Date().getTime() - this.processStartTime;
    }

    /**
     * Helper method providing creation of link in correct format. Recursive calls are supported.
     * Link, which does not point to localhost is automatically detected and passed without formatting.
     * Double slashes are replaced by single slashes.
     * Path directory separators are replaced by http request separators.
     * @param {string} $link Link which should be formatted. Link can be created as relative, if does not
     * starts with '/'. If link starts with '/', it will be appended to the base url.
     * @returns {string} Returns correctly formatted link.
     */
    public CreateLink($link : string) : string {
        if (StringUtils.IsEmpty($link)) {
            $link = this.request.getUrl();
        } else {
            if (this.request.getRelativeRoot() !== "/" &&
                StringUtils.Contains("/" + $link, this.request.getRelativeRoot())) {
                const relativeRoot : string = this.request.getRelativeRoot();
                $link = StringUtils.Substring($link,
                    StringUtils.IndexOf($link, relativeRoot, false) + StringUtils.Length(relativeRoot),
                    StringUtils.Length($link));
            }

            if (!StringUtils.StartsWith($link, "mailto:")) {
                if (StringUtils.Contains($link, "http://", "https://", "ssh://", "www.")) {
                    if (StringUtils.Contains($link, this.request.getBaseUrl())) {
                        $link = StringUtils.Remove($link, this.request.getHostUrl());
                    } else if (!StringUtils.StartsWith($link, "http://") &&
                        !StringUtils.StartsWith($link, "https://") &&
                        !StringUtils.StartsWith($link, "ssh://")) {
                        $link = "http://" + $link;
                    }
                } else if (StringUtils.StartsWith($link, "/")) {
                    $link = "/" + this.request.getRelativeRoot() + $link;
                } else {
                    $link = "/" + this.request.getRelativeDirectory() + "/" + $link;
                }
            }

            $link = StringUtils.Replace($link, "\\", "/");
            while (StringUtils.Contains($link, "//")) {
                $link = StringUtils.Replace($link, "//", "/");
            }
            $link = StringUtils.Replace($link, ":/", "://");
        }

        return $link;
    }

    /**
     * Reload current page content.
     * @returns {void}
     */
    public Reload() : void {
        this.Refresh();
    }

    /**
     * Redirect page to destination link or just refresh the page.
     * @param {string} [$link] Destination link value. If not specified page will be refreshed.
     * @param {boolean} [$openNewWindow=false] Specify, if request should be opened at new window.
     * @returns {void}
     */
    public ReloadTo($link? : string, $openNewWindow? : boolean) : void;

    /**
     * Redirect page to destination link or just refresh the page.
     * @param {string} [$link] Destination link value. If not specified page will be refreshed.
     * @param {ArrayList<any>} [$POST] Post data, which should be passed to the desired resolver.
     * @param {boolean} [$async] Specify, if request should be executed asynchronously.
     * @returns {void}
     */
    public ReloadTo($link? : string, $POST? : ArrayList<any>, $async? : boolean) : void;

    /**
     * Redirect page to destination link or just refresh the page.
     * @param {string} [$link] Destination link value. If not specified page will be refreshed.
     * @param {any} [$data] Data, which should be passed to the desired resolver.
     * @param {boolean} [$async] Specify, if request should be executed asynchronously.
     * @returns {void}
     */
    public ReloadTo($link? : string, $data? : any, $async? : boolean) : void {
        $link = this.CreateLink($link);
        if (ObjectValidator.IsSet($async) && $async) {
            if (ObjectValidator.IsEmptyOrNull($data)) {
                $data = new ArrayList<any>();
            }
            EventsManager.getInstanceSingleton().FireEvent(GeneralEventOwner.WINDOW, EventType.ON_ASYNC_REQUEST,
                new AsyncRequestEventArgs($link, $data), false);
        } else {
            if ((!ObjectValidator.IsSet($data) || !ObjectValidator.IsBoolean($data)) &&
                StringUtils.Contains($link, this.request.getRelativeRoot())) {
                window.location.hash = $link;
                if (!ObjectValidator.IsEmptyOrNull($data)) {
                    (<any>window.location).hashData = $data;
                }
            } else if (ObjectValidator.IsBoolean($data) && $data) {
                window.open($link, "_blank");
            } else {
                window.location.href = $link;
            }
        }
    }

    /**
     * Navigate to relative destination and wait for resolve.
     * @param {string} $link Destination link value.
     * @param {any} [$data] Data, which should be passed to the desired resolver.
     * @param {number} [$timeout=5000] Specify, max timeout in milliseconds for navigation finish.
     * @returns {Promise<void>}
     */
    public async NavigateTo($link : string, $data? : ArrayList<any>, $timeout : number = 5000) : Promise<void> {
        $link = this.CreateLink($link);
        if (!StringUtils.Contains($link, this.request.getRelativeRoot())) {
            throw new Error("NavigateTo is supported only by history API at same origin - use relative link instead. " +
                "Required link was: " + $link);
        }
        return new Promise<void>(($resolve : any, $reject : any) : void => {
            const timeout : number = EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                $reject(new Error("NavigateTo timeout reached."));
            }, $timeout);
            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST, () : void => {
                clearTimeout(timeout);
                $resolve();
            });
            this.ReloadTo($link, $data);
        });
    }

    /**
     * Go to correct destination link instead of incorrect requested value.
     * @param {string} $link Correct destination link value.
     * @returns {void}
     */
    public Return301Moved($link : string) : void {
        const data : ArrayList<any> = new ArrayList<any>();
        data.Add(this.CreateLink($link), HttpRequestConstants.HTTP301_LINK);
        this.ReloadTo("/ServerError/Http/Moved", data, true);
    }

    /**
     * Inform about access denied and stop the thread execution.
     * @returns {void}
     */
    public Return403Forbidden() : void {
        this.ReloadTo("/ServerError/Http/Forbidden", null, true);
    }

    /**
     * Inform file has not been found and stop the thread execution.
     * @param {string} [$requiredFilePath] Specify file path, which has been required,
     * so it can be reported by the error page handler.
     * @returns {void}
     */
    public Return404NotFound($requiredFilePath? : string) : void {
        let data : ArrayList<any> = null;
        if (!ObjectValidator.IsEmptyOrNull($requiredFilePath)) {
            data = new ArrayList<any>();
            data.Add($requiredFilePath, HttpRequestConstants.HTTP404_FILE_PATH);
        }
        this.ReloadTo("/ServerError/Http/NotFound", data, true);
    }

    /**
     * @returns {HttpRequestParser} Returns parsed information about current request.
     */
    public getRequest() : HttpRequestParser {
        return this.request;
    }

    /**
     * Refresh current page content.
     * @returns {void}
     */
    public Refresh() : void {
        try {
            window.location.reload();
        } catch (ex) {
            ExceptionsManager.Throw(this.getClassName(), ex);
        }
    }

    /**
     * Go back in browser history and refresh current page content.
     * @returns {void}
     */
    public ReloadBack() : void {
        try {
            window.history.back();
        } catch (ex) {
            ExceptionsManager.Throw(this.getClassName(), ex);
        }
    }

    /**
     * Refresh information about current request and restart process time counter without page reload.
     * @returns {void}
     */
    public RefreshWithoutReload() : void {
        this.processStartTime = new Date().getTime();
        this.request = Loader.getInstance().getHttpResolver().CreateRequest();
    }

    /**
     * @param {Callback} $callback Returns true, if connection is alive, otherwise false.
     * @returns {void}
     */
    public IsOnline($callback : ($status : boolean) => void) : void {
        if (this.getRequest().IsOnServer()) {
            $callback(true);
        } else if (window.navigator.onLine) {
            let testData : any = document.createElement("iframe");
            let iframeValidation : boolean = true;
            let timeoutId : number; // eslint-disable-line prefer-const
            const cleanup : any = () : void => {
                if (!ObjectValidator.IsEmptyOrNull(timeoutId)) {
                    clearTimeout(timeoutId);
                }
                if (iframeValidation && !ObjectValidator.IsEmptyOrNull(testData.parentNode)) {
                    testData.parentNode.removeChild(testData);
                }
            };
            if (ObjectValidator.IsEmptyOrNull(testData)) {
                testData = document.createElement("img");
                testData.crossOrigin = "Anonymous";
                iframeValidation = false;
            } else {
                testData.width = 0;
                testData.height = 0;
                document.body.appendChild(testData);
            }
            testData.onload = () : void => {
                cleanup();
                $callback(true);
            };
            testData.onerror = () : void => {
                cleanup();
                if (iframeValidation) {
                    $callback(false);
                } else {
                    LogIt.Debug("IsOnline validation based on image failure. " +
                        "Fallback to navigator.onLine state reached maybe do to CORS policy.");
                    $callback(true);
                }
            };
            testData.src = "https://hub.oidis.io/ping." + (iframeValidation ? "txt" : "png") +
                "?dummy=" + (new Date().getTime());
            timeoutId = <any>setTimeout(() : void => {
                cleanup();
                LogIt.Debug("IsOnline validation failure. Timeout reached.");
                $callback(false);
            }, 2500);
        } else {
            $callback(false);
        }
    }

    /**
     * @param {string} $httpRequest Http patter subscribed to resolver. Pattern can contains wilcards '*' as placeholder
     * for characters and fixed http parts. Pattern can contains double start '**' as placeholder for floating http path parts.
     * Pattern can contains '{paramName}' as placeholder for fixed http path parts, which will be served as resolver variables.
     * Pattern is resolved as absolute path, if starts with '/' otherwise is resolved as relative path.
     * If pattern has been registered already it will not be overridden (use OverrideResolver method for force resolver registration).
     * @param {any} $resolver Resolver callback subscribed to http pattern.
     * @param {boolean} [$ignoreCase = true] If true, resolver http pattern and http request without case insensitive,
     * otherwise as case sensitive.
     * @returns {void}
     */
    public RegisterResolver($httpRequest : string, $resolver : any, $ignoreCase : boolean = true) : void {
        if (!ObjectValidator.IsEmptyOrNull($resolver)) {
            const subRequests : string[] = this.splitRequest($httpRequest);
            const subRequestsCount : number = <number>subRequests.length;
            let collection : HttpResolversCollection = this.resolversCollection;
            let requestIndex : number;
            for (requestIndex = 1; requestIndex < subRequestsCount; requestIndex++) {
                const formatted : string[] = this.formatSubrequest(subRequests[requestIndex]);
                const subRequest : string = formatted[0];
                const parameterName : string = formatted[1];

                const collectionParent : HttpResolversCollection = new HttpResolversCollection(subRequest);
                if (requestIndex === subRequestsCount - 1) {
                    collectionParent.ResolverClassName($resolver);
                    collectionParent.IgnoreCase($ignoreCase);
                }

                collection.AddChild(collectionParent);
                const flexibleRequest : boolean = subRequest === "**";
                collection.LevelsCount(subRequestsCount - requestIndex, flexibleRequest);
                collection = collection.getChild(subRequest);
                collection.ParameterName(parameterName);

                if (flexibleRequest) {
                    let parent : HttpResolversCollection = this.resolversCollection;
                    parent.LevelsCount(parent.LevelsCount(), true);
                    let parentIndex : number;
                    for (parentIndex = 1; parentIndex < requestIndex; parentIndex++) {
                        const parentKey : string = subRequests[parentIndex];
                        parent = parent.getChild(parentKey);
                        parent.LevelsCount(parent.LevelsCount(), true);
                    }
                }
            }
        }
    }

    /**
     * @param {string} $httpRequest Http patter subscribed to resolver. Pattern can contains wilcards '*' as placeholder
     * for characters and fixed http parts. Pattern can contains double start '**' as placeholder for floating http path parts.
     * Pattern can contains '{paramName}' as placeholder for fixed http path parts, which will be served as resolver variables.
     * Pattern is resolved as absolute path, if starts with '/' otherwise is resolved as relative path.
     * If pattern has not been registered already it will not be overridden (use RegisterResolver method for resolver registration).
     * @param {any} $resolver Resolver callback subscribed to http pattern.
     * @param {boolean} [$ignoreCase = true] If true, resolver http pattern and http request without case insensitive,
     * otherwise as case sensitive.
     * @returns {void}
     */
    public OverrideResolver($httpRequest : string, $resolver : any, $ignoreCase : boolean = true) : void {
        if (!ObjectValidator.IsEmptyOrNull($resolver)) {
            const subRequests : string[] = this.splitRequest($httpRequest);
            const subRequestsCount : number = <number>subRequests.length;
            let collection : HttpResolversCollection = this.resolversCollection;
            let requestIndex : number;
            for (requestIndex = 1; requestIndex < subRequestsCount; requestIndex++) {
                const formatted : string[] = this.formatSubrequest(subRequests[requestIndex]);
                const subRequest : string = formatted[0];
                const parameterName : string = formatted[1];

                if (collection.Contains(subRequest)) {
                    collection = collection.getChild(subRequest);
                } else {
                    break;
                }

                if (requestIndex === subRequestsCount - 1) {
                    collection.ResolverClassName($resolver);
                    collection.IgnoreCase($ignoreCase);
                }
                collection.ParameterName(parameterName);
            }
        }
    }

    /**
     * @param {string} $pattern Specify http pattern, which should be validated.
     * @returns {boolean} Returns true, if http pattern has been already used for registration of Resolver callback, otherwise false.
     */
    public HttpPatternExists($pattern : string) : boolean {
        const subRequests : string[] = this.splitRequest($pattern);
        const subRequestsCount : number = <number>subRequests.length;
        let collection : HttpResolversCollection = this.resolversCollection;
        let requestIndex : number;
        for (requestIndex = 1; requestIndex < subRequestsCount; requestIndex++) {
            const formatted : string[] = this.formatSubrequest(subRequests[requestIndex]);
            if (collection.Contains(formatted[0])) {
                collection = collection.getChild(formatted[0]);
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * @param {string} $requireLink Require link which should be matched with registered resolvers.
     * @returns {any} Returns Resolver callback in case of that require link is in match with one of registered resolver,
     * otherwise returns null.
     */
    public getResolverClassName($requireLink : string) : any {
        $requireLink = this.CreateLink($requireLink);
        this.parameters.Clear();
        let className : any = null;
        const subRequires : string[] = StringUtils.Split($requireLink, "/");
        const getNextCollection : any = ($subRequireIndex : number, $collectionIndex : number,
                                         $collections : HttpResolversCollection[], $isFlexible : boolean) : void => {
            if ($collectionIndex < $collections.length && $subRequireIndex < subRequires.length) {
                const collection : HttpResolversCollection = $collections[$collectionIndex];
                const pattern : string = collection.Pattern();
                let nextPattern : string = "";
                if ($collectionIndex + 1 < $collections.length) {
                    nextPattern = $collections[$collectionIndex + 1].Pattern();
                }
                const subRequire : string = subRequires[$subRequireIndex];
                if (pattern === subRequire || StringUtils.PatternMatched(pattern, subRequire)) {
                    let assign : boolean = true;
                    if (pattern === "*" &&
                        ObjectValidator.IsEmptyOrNull(collection.ParameterName()) &&
                        !ObjectValidator.IsEmptyOrNull(className)) {
                        assign = false;
                    }
                    if (pattern === "**") {
                        assign = false;
                        $isFlexible = true;
                    }
                    if (assign) {
                        if (!ObjectValidator.IsEmptyOrNull(collection.ParameterName())) {
                            this.parameters.Add(subRequire, collection.ParameterName());
                        }
                        if (!ObjectValidator.IsEmptyOrNull(collection.ResolverClassName()) &&
                            $subRequireIndex === subRequires.length - 1) {
                            className = collection.ResolverClassName();
                        }
                    }
                    if (!collection.getChildrenList().IsEmpty()) {
                        collection.OrderChildrenCollection();
                        getNextCollection($subRequireIndex + 1, 0, collection.getChildrenList().getAll(), $isFlexible);
                    }
                }
                if (ObjectValidator.IsEmptyOrNull(className) ||
                    (pattern !== subRequire && nextPattern !== "*" && StringUtils.Contains(nextPattern, "*"))) {
                    getNextCollection($subRequireIndex, $collectionIndex + 1, $collections, $isFlexible);
                }
            } else if ($isFlexible && $subRequireIndex + 1 < subRequires.length) {
                getNextCollection($subRequireIndex + 1, 0, $collections, $isFlexible);
            }
        };
        try {
            this.resolversCollection.OrderChildrenCollection();
            getNextCollection(StringUtils.StartsWith($requireLink, "/") ? 1 : 0, 0,
                this.resolversCollection.getChildrenList().getAll(), false);
        } catch (ex) {
            LogIt.Error(this.getClassName(), ex);
            className = null;
            this.parameters.Clear();
        }
        return className;
    }

    /**
     * @returns {ArrayList<string>} Returns ArrayList of parsed parameters in case of, that current resolver callback
     * has been found and resolver pattern contains parameters names, otherwise returns empty string.
     */
    public getResolverParameters() : ArrayList<string> {
        return this.parameters;
    }

    /**
     * @returns {HttpResolversCollection} Returns all registered resolver to the current manager.
     */
    public getResolversCollection() : HttpResolversCollection {
        return this.resolversCollection;
    }

    public ToString() : string {
        return Convert.ObjectToString(this.request) + StringUtils.NewLine() +
            Convert.ObjectToString(this.getResolversCollection());
    }

    public toString() : string {
        return this.ToString();
    }

    protected registerGeneralResolvers() : void {
        this.RegisterResolver("/ServerError/Cookies", CookiesErrorPage);
        this.RegisterResolver("/ServerError/Browser", BrowserErrorPage);
        this.RegisterResolver("/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}",
            ExceptionErrorPage);
        this.RegisterResolver("/ServerError/Http/Moved", Http301MovedPage);
        this.RegisterResolver("/ServerError/Http/Forbidden", Http403ForbiddenPage);
        this.RegisterResolver("/ServerError/Http/NotFound", Http404NotFoundPage);
        this.RegisterResolver("/ServerError/Http/DefaultPage", BaseHttpResolver);
        this.RegisterResolver("/about/Cache", CacheManager);
        /* dev:start */
        this.RegisterResolver("/about/Env", HttpRequestParserTest);
        /* dev:end */
    }

    private splitRequest($httpRequest : string) : string[] {
        while (StringUtils.Contains($httpRequest, "**/**")) {
            $httpRequest = StringUtils.Replace($httpRequest, "**/**", "**");
        }
        $httpRequest = this.CreateLink($httpRequest);

        return StringUtils.Split($httpRequest, "/");
    }

    private formatSubrequest($subRequest : string) : string[] {
        if ($subRequest !== "**" && $subRequest !== "*" && StringUtils.Contains($subRequest, "*")) {
            while (StringUtils.Contains($subRequest, "**")) {
                $subRequest = StringUtils.Replace($subRequest, "**", "*");
            }
        }

        let parameterName : string = null;
        if (StringUtils.Contains($subRequest, "{", "}")) {
            parameterName = StringUtils.Substring($subRequest,
                StringUtils.IndexOf($subRequest, "{") + 1, StringUtils.IndexOf($subRequest, "}"));
            $subRequest = "*";
        }

        return [$subRequest, parameterName];
    }
}
