/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { AsyncRequestEventArgs } from "../Events/Args/AsyncRequestEventArgs.js";
import { HttpRequestEventArgs } from "../Events/Args/HttpRequestEventArgs.js";
import { EventsManager } from "../Events/EventsManager.js";
import { ExceptionsManager } from "../Exceptions/ExceptionsManager.js";
import { IHttpRequestResolver } from "../Interfaces/IHttpRequestResolver.js";
import { IClassName } from "../Interfaces/Interface.js";
import { Loader } from "../Loader.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { IResolverDescriptor, ResolverScope, ResolverType } from "../Primitives/Decorators.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { Reflection } from "../Utils/Reflection.js";
import { StringUtils } from "../Utils/StringUtils.js";
import { HttpManager } from "./HttpManager.js";
import { HttpRequestParser } from "./HttpRequestParser.js";
import { BaseHttpResolver } from "./Resolvers/BaseHttpResolver.js";

/**
 * HttpResolver class provides handling of current http request and hash on change events.
 */
export class HttpResolver extends BaseObject {

    private static baseUrl : string;
    private readonly manager : HttpManager;
    private readonly events : EventsManager;
    private readonly resolvers : any;

    /**
     * @param {string} [$baseUrl] Set baseUrl for http resolver
     */
    constructor($baseUrl? : string) {
        super();

        if (!ObjectValidator.IsEmptyOrNull($baseUrl) && $baseUrl !== "#") {
            if (Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed() && ObjectValidator.IsEmptyOrNull(window.location.hash)) {
                if (!StringUtils.StartsWith($baseUrl, "/")) {
                    $baseUrl = "/" + $baseUrl;
                }
                if (!StringUtils.EndsWith($baseUrl, "/")) {
                    $baseUrl += "/";
                }
                window.location.hash = $baseUrl;
            }
        }

        const httpManger : any = this.getHttpManagerClass();
        this.manager = new httpManger(this.CreateRequest($baseUrl));
        const eventsManger : any = this.getEventsManagerClass();
        this.events = new eventsManger();
        this.resolvers = {};
    }

    /**
     * @param {string} [$baseUrl] Specify base url for new instance of request parser.
     * @returns {HttpRequestParser} Returns new instance of request parser based on HttpResolver definition.
     */
    public CreateRequest($baseUrl? : string) : HttpRequestParser {
        if (!ObjectValidator.IsEmptyOrNull($baseUrl) && HttpResolver.baseUrl !== $baseUrl) {
            HttpResolver.baseUrl = $baseUrl;
        }
        const parserClass : any = this.getRequestParserClass();
        return new parserClass(HttpResolver.baseUrl);
    }

    /**
     * @returns {EventsManager} Returns EventsManager instance used for management of request events.
     */
    public getEvents() : EventsManager {
        return this.events;
    }

    /**
     * @returns {HttpManager} Returns HttpManager instance used for management of resolved requests.
     */
    public getManager() : HttpManager {
        return this.manager;
    }

    /**
     * Find out appropriate request resolver class based on current http request and execute resolver Process method.
     * @param {HttpRequestEventArgs|AsyncRequestEventArgs} [$eventArgs] Specify request args, which should be resolved.
     * @returns {void}
     */
    public ResolveRequest($eventArgs? : HttpRequestEventArgs | AsyncRequestEventArgs) : void {
        try {
            if (ObjectValidator.IsEmptyOrNull($eventArgs) ||
                !Reflection.getInstance().IsMemberOf($eventArgs, <IClassName>HttpRequestEventArgs)) {
                $eventArgs = new HttpRequestEventArgs(this.manager.getRequest().getScriptPath());
            }

            const isAsync : boolean = Reflection.getInstance().IsMemberOf($eventArgs, <IClassName>AsyncRequestEventArgs);
            if (!isAsync) {
                window.location.hash = $eventArgs.Url();
                this.manager.RefreshWithoutReload();
            }
            let resolver : IHttpRequestResolver;
            let resolverName : any = this.manager.getResolverClassName($eventArgs.Url());

            if (ObjectValidator.IsEmptyOrNull(resolverName)) {
                resolverName = this.manager.getResolverClassName("/ServerError/Http/DefaultPage");
                if (ObjectValidator.IsEmptyOrNull(resolverName)) {
                    resolverName = BaseHttpResolver;
                }
            } else {
                const getData : ArrayList<string> = this.manager.getResolverParameters();
                getData.Copy(this.manager.getRequest().getUrlArgs());
                $eventArgs.GET(getData);
            }
            if (!ObjectValidator.IsEmptyOrNull((<any>window.location).hashData)) {
                $eventArgs.POST((<any>window.location).hashData);
                delete (<any>window.location).hashData;
            }
            resolver = resolverName.getInstance(); // eslint-disable-line prefer-const
            let canResolve : boolean = true;
            if (!ObjectValidator.IsEmptyOrNull(globalThis.Io.Oidis.Gui)) {
                canResolve = !resolver.IsMemberOf("Io.Oidis.Gui.HttpProcessor.Resolvers.AsyncBaseHttpController");
            }
            if (!isAsync && canResolve) {
                let lastTimeoutId : any = setTimeout(
                    /* istanbul ignore next : callback placeholder for ability to get lastTimeoutId */
                    () : void => {
                        // force invoke creation of last timeout id
                    }, 0);
                do {
                    clearTimeout(lastTimeoutId);
                }
                while (lastTimeoutId--);
                let lastIntervalId : any = setInterval(
                    /* istanbul ignore next : callback placeholder for ability to get lastIntervalId */
                    () : void => {
                        // force invoke creation of last interval id
                    }, 0);
                do {
                    clearInterval(lastIntervalId);
                }
                while (lastIntervalId--);
            }
            resolver.RequestArgs($eventArgs);
            resolver.Process();
        } catch (ex) {
            ExceptionsManager.HandleException(ex);
        }
    }

    public toString() : string {
        return this.ToString();
    }

    public async RegisterResolvers() : Promise<void> {
        const context : IHttpResolverContext = {
            isBrowser: globalThis.isBrowser,
            isProd   : Loader.getInstance().getEnvironmentArgs().IsProductionMode()
        };
        await this.getStartupResolvers(context);
        await this.processDecoratedResolvers(context);
        Reflection.Refresh();
        let httpPattern : string;
        for (httpPattern in this.resolvers) {
            /* istanbul ignore else : bulletproof condition */
            if (ObjectValidator.IsSet(this.resolvers[httpPattern])) {
                const resolver : any = this.resolvers[httpPattern];
                if (!ObjectValidator.IsEmptyOrNull(resolver) && ObjectValidator.IsSet(resolver.ClassName)) {
                    if (this.manager.HttpPatternExists(httpPattern)) {
                        this.manager.OverrideResolver(httpPattern, resolver);
                    } else {
                        this.manager.RegisterResolver(httpPattern, resolver);
                    }
                }
            }
        }
    }

    protected getEventsManagerClass() : any {
        return EventsManager;
    }

    protected getHttpManagerClass() : any {
        return HttpManager;
    }

    protected getRequestParserClass() : any {
        return HttpRequestParser;
    }

    protected async getStartupResolvers($context : IHttpResolverContext) : Promise<any> {
        /// TODO: how to add to context something like bootstrap, react, GQL etc.? -> by solution?
        if ($context.isBrowser && !$context.isProd) {
            await this.registerResolver(async () => (await import("../Index.js")).Index,
                "/", "/web", "/web/index");
            await this.registerResolver(async () => (await import("../RuntimeTests/HttpManagerTest.js")).HttpManagerTest,
                "/web/HttpManagerTest");
            await this.registerResolver(async () => (await import("../RuntimeTests/HttpRequestParserTest.js")).HttpRequestParserTest,
                "/web/HttpRequestParserTest");
            await this.registerResolver(async () => (await import("../RuntimeTests/PersistenceApiTest.js")).PersistenceApiTest,
                "/web/PersistenceApiTest");
            await this.registerResolver(async () => (await import("../RuntimeTests/EventsManagerTest.js")).EventsManagerTest,
                "/web/EventsManagerTest");
            await this.registerResolver(async () => (await import("../RuntimeTests/ExceptionsManagerTest.js")).ExceptionsManagerTest,
                "/web/ExceptionsManagerTest");
            await this.registerResolver(async () => (await import("../RuntimeTests/SyncRequestTest.js")).SyncRequestTest,
                "/web/SynchronousRequestTest");
            await this.registerResolver(async () => (await import("../RuntimeTests/AsyncRequestTest.js")).AsyncRequestTest,
                "/web/AsyncRequestTest");
            await this.registerResolver(async () => (await import("../RuntimeTests/TimeoutTest.js")).TimeoutTest,
                "/web/TimeoutTest");
            await this.registerResolver(async () => (await import("../RuntimeTests/ChromiumConnectorTest.js")).ChromiumConnectorTest,
                "/web/ChromiumConnectorTest");
            await this.registerResolver(async () => (await import("../RuntimeTests/CoverageTest.js")).CoverageTest,
                "/web/CoverageTest");
            await this.registerResolver(async () => (await import("../RuntimeTests/AsyncRuntimeTestCase.js")).AsyncRuntimeTestCase,
                "/web/AsyncRuntimeTest");
            await this.registerResolver(async () => (await import("../RuntimeTests/JsonpFileReaderTest.js")).JsonpFileReaderTest,
                "/web/JsonpFileReaderTest");
            await this.registerResolver(async () =>
                    (await import("../RuntimeTests/JxbrowserBridgeClientTest.js")).JxbrowserBridgeClientTest,
                "/web/JxbrowserBridgeClientTest");
            return this.registerResolver(async () => (await import("../RuntimeTests/LogItTest.js")).LogItTest,
                "/web/LogItTest");
        }

        return {};
    }

    protected async registerResolver($from : () => Promise<any>, ...$to : string[]) : Promise<any> {
        return this.addResolver($from, $to);
    }

    protected async overrideResolver($from : () => Promise<any>, ...$to : string[]) : Promise<any> {
        return this.addResolver($from, $to, true);
    }

    private async addResolver($from : () => Promise<any>, $to : string[], $force : boolean = false) : Promise<any> {
        let resolverClass : any = null;
        for await (const path of $to) {
            if (!this.resolvers.hasOwnProperty(path) || $force) {
                if (resolverClass === null) {
                    resolverClass = await $from();
                    Reflection.Refresh();
                }
                this.resolvers[path] = resolverClass;
            }
        }
        return this.resolvers;
    }

    private async processDecoratedResolvers($context : IHttpResolverContext) : Promise<void> {
        if (!globalThis.hasOwnProperty("oidisResolvers")) {
            globalThis.oidisResolvers = [];
        }
        const reflection : Reflection = Reflection.getInstance();
        for (const resolver of globalThis.oidisResolvers) {
            if (resolver.options.type === ResolverType.UNIVERSAL) {
                const memberOf : string[] = reflection.getClassHierarchyOf(resolver.instance);
                if (memberOf.includes("Io.Oidis.Services.HttpProcessor.Resolvers.AsyncBasePageController") ||
                    memberOf.includes("Io.Oidis.Gui.Bootstrap.Primitives.BasePanelController")) {
                    resolver.options.type = ResolverType.FRONTEND_ONLY;
                } else if (memberOf.includes("Io.Oidis.Localhost.HttpProcessor.Resolvers.BaseHttpResolver")) {
                    resolver.options.type = ResolverType.BACKEND_ONLY;
                }
            }
        }

        const loaderNamespaces : string[] = Loader.getInstance().getNamespaceHierarchy();
        loaderNamespaces.pop();
        let sorted : IResolverDescriptor[] = [];
        const sortResolvers : any = ($scopes : ResolverScope[]) : void => {
            for (const loaderNamespace of loaderNamespaces) {
                for (const resolver of globalThis.oidisResolvers) {
                    if (StringUtils.StartsWith(resolver.ClassName(), loaderNamespace) && $scopes.includes(resolver.options.scope)) {
                        if (resolver.options.type === ResolverType.UNIVERSAL ||
                            $context.isBrowser && resolver.options.type === ResolverType.FRONTEND_ONLY ||
                            !$context.isBrowser && resolver.options.type === ResolverType.BACKEND_ONLY) {
                            sorted.push(resolver);
                        }
                    }
                }
            }
        };
        if (!$context.isProd) {
            sortResolvers([ResolverScope.DEV]);
        }
        sortResolvers([ResolverScope.ALL_PROFILES, ResolverScope.PROD]);
        sorted = sorted.sort(($a : any, $b : any) : number => {
            return ($a.options.priority < $b.options.priority) ? -1 : ($a.options.priority > $b.options.priority) ? 1 : 0;
        });
        for await (const resolver of sorted) {
            const instanceClass : any = resolver.instance;
            const links : string[] = [];
            const addToList : any = (...$links : string[]) : void => {
                $links.forEach(($link : string) : void => {
                    if (!StringUtils.StartsWith($link, "/")) {
                        $link = "/" + $link;
                    }
                    $link = StringUtils.Replace($link, "/#/", "/");
                    if (!ObjectValidator.IsEmptyOrNull($link) && !links.includes($link)) {
                        links.push($link);
                    }
                });
            };
            if (!$context.isBrowser && resolver.options.type !== ResolverType.FRONTEND_ONLY ||
                $context.isBrowser && resolver.options.type !== ResolverType.BACKEND_ONLY) {
                const instance : any = new instanceClass();
                if (!ObjectValidator.IsEmptyOrNull(globalThis.Io.Oidis.Gui) &&
                    !reflection.IsMemberOf(instance, "Io.Oidis.Gui.Bootstrap.Primitives.BasePanelController")) {
                    addToList(instanceClass.getLink());
                    addToList(...instanceClass.getAltLinks());
                    if (!ObjectValidator.IsEmptyOrNull((<any>globalThis).Io.Oidis.Hub) &&
                        !ObjectValidator.IsEmptyOrNull((<any>globalThis).Io.Oidis.Hub.Gui) &&
                        reflection.IsMemberOf(instance, "Io.Oidis.Hub.Gui.Primitives.BasePageLayoutController")) {
                        instance.InstanceOwner().getTabsList().forEach(($tab : any) : void => {
                            if (!ObjectValidator.IsEmptyOrNull($tab.controller) && ObjectValidator.IsSet((<any>$tab.controller).getLink)) {
                                addToList((<any>$tab.controller).getLink());
                                addToList(...(<any>$tab.controller).getAltLinks());
                            }
                        });
                    }
                }
                if (!ObjectValidator.IsEmptyOrNull(links)) {
                    if (!resolver.options.forceRegister) {
                        await this.registerResolver(async () => instanceClass, ...links);
                    } else {
                        await this.overrideResolver(async () => instanceClass, ...links);
                    }
                }
            }
        }
    }
}

export interface IHttpResolverContext {
    isBrowser : boolean;
    isProd : boolean;
}

// generated-code-start
export const IHttpResolverContext = globalThis.RegisterInterface(["isBrowser", "isProd"]);
// generated-code-end
