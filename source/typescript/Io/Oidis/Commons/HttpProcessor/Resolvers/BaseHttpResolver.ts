/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BrowserType } from "../../Enums/BrowserType.js";
import { EventType } from "../../Enums/Events/EventType.js";
import { GeneralEventOwner } from "../../Enums/Events/GeneralEventOwner.js";
import { EnvironmentArgs } from "../../EnvironmentArgs.js";
import { HttpRequestEventArgs } from "../../Events/Args/HttpRequestEventArgs.js";
import { EventsManager } from "../../Events/EventsManager.js";
import { ExceptionsManager } from "../../Exceptions/ExceptionsManager.js";
import { Exit } from "../../Exceptions/Type/Exit.js";
import { IHttpRequestResolver } from "../../Interfaces/IHttpRequestResolver.js";
import { Loader } from "../../Loader.js";
import { ArrayList } from "../../Primitives/ArrayList.js";
import { BaseObject } from "../../Primitives/BaseObject.js";
import { Echo } from "../../Utils/Echo.js";
import { LogIt } from "../../Utils/LogIt.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { HttpManager } from "../HttpManager.js";
import { HttpRequestParser } from "../HttpRequestParser.js";

/**
 * BaseHttpResolver class is abstract class providing basic http request resolving and
 * each http resolver should be extending this class for security reason.
 */
export class BaseHttpResolver extends BaseObject implements IHttpRequestResolver {
    private readonly events : EventsManager;
    private readonly environment : EnvironmentArgs;
    private readonly httpManager : HttpManager;
    private request : HttpRequestParser;
    private requestArgs : HttpRequestEventArgs;
    private nonce : string;

    public static getInstance() : BaseHttpResolver {
        return new this();
    }

    constructor() {
        super();
        this.environment = Loader.getInstance().getEnvironmentArgs();
        this.httpManager = Loader.getInstance().getHttpManager();
        this.request = this.httpManager.getRequest();
        this.events = Loader.getInstance().getHttpResolver().getEvents();
        this.requestArgs = new HttpRequestEventArgs(this.httpManager.getRequest().getScriptPath());
    }

    /**
     * @param {HttpRequestEventArgs} $value Specify request arguments, which should be processed by resolver instance.
     * @returns {HttpRequestEventArgs} Returns current request arguments
     */
    public RequestArgs($value? : HttpRequestEventArgs) : HttpRequestEventArgs {
        if (ObjectValidator.IsSet($value)) {
            this.requestArgs = $value;
            if ($value.Owner().IsMemberOf(HttpRequestParser)) {
                this.request = $value.Owner();
                this.requestArgs.GET().Copy(this.request.getUrlArgs());
            }
            if (this.requestArgs.POST().KeyExists("Protocol")) {
                const protocol : any = this.requestArgs.POST().getItem("Protocol");
                if (!ObjectValidator.IsEmptyOrNull(protocol.nonce)) {
                    this.nonce = protocol.nonce;
                }
            }
            this.argsHandler(this.requestArgs.GET(), this.requestArgs.POST());
        }
        return this.requestArgs;
    }

    /**
     * Execute resolver implementation.
     * @returns {void}
     */
    public Process() : void {
        if (!this.browserValidator()) {
            LogIt.Error("Unsupported browser. User Agent: " + this.request.getUserAgent());
            this.httpManager.ReloadTo("/ServerError/Browser", null, true);
            if (this.getEnvironmentArgs().HtmlOutputAllowed()) {
                this.stop();
            }
        } else {
            const resolve : any = () : void => {
                this.events.FireEvent(GeneralEventOwner.BODY, EventType.ON_LOAD);
            };
            const reject : any = ($ex : Error) : void => {
                ExceptionsManager.HandleException($ex);
            };
            this.accessValidator().then(($status : boolean) : void => {
                if ($status) {
                    Echo.ClearAll();
                    try {
                        const promiseInterface : any = this.resolver();
                        if (!ObjectValidator.IsEmptyOrNull(promiseInterface)) {
                            if (promiseInterface instanceof Promise || promiseInterface.constructor.name === "Promise") {
                                promiseInterface.then(resolve).catch(reject);
                            } else {
                                reject(new Error("Standard Promise interface is required for asynchronous http resolver."));
                            }
                        } else {
                            resolve();
                        }
                    } catch (ex) {
                        reject(ex);
                    }
                } else {
                    this.httpManager.Return403Forbidden();
                    if (this.getEnvironmentArgs().HtmlOutputAllowed()) {
                        this.stop();
                    }
                }
            }).catch(reject);
        }
    }

    public toString() : string {
        return this.ToString();
    }

    /**
     * @returns {boolean} Returns true, if browser is valid for execution the resolver, otherwise false.
     */
    protected browserValidator() : boolean {
        return !(
            this.request.getBrowserType() === BrowserType.UNKNOWN ||
            (this.request.getBrowserType() === BrowserType.INTERNET_EXPLORER && this.request.getBrowserVersion() < 5));
    }

    protected async accessValidator() : Promise<boolean> {
        return true;
    }

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        // handle request args
    }

    protected resolver() : void {
        Echo.Println("This is abstract BaseHttpResolver, which has been executed at: " + this.request.getUrl());
    }

    protected stop() : void {
        ExceptionsManager.Throw(this.getClassName(), new Exit());
    }

    protected exit() : void {
        this.stop();
    }

    protected getRequest() : HttpRequestParser {
        return this.request;
    }

    protected getHttpManager() : HttpManager {
        return this.httpManager;
    }

    protected getEventsManager() : EventsManager {
        return this.events;
    }

    protected getEnvironmentArgs() : EnvironmentArgs {
        return this.environment;
    }

    protected createLink($link : string) : string {
        return this.httpManager.CreateLink($link);
    }

    protected registerResolver($httpRequest : string, $resolver : any, $ignoreCase : boolean = true) : void {
        this.httpManager.RegisterResolver($httpRequest, $resolver, $ignoreCase);
    }

    protected overrideResolver($httpRequest : string, $resolver : any, $ignoreCase : boolean = true) : void {
        this.httpManager.OverrideResolver($httpRequest, $resolver, $ignoreCase);
    }

    protected getNonce() : string {
        return this.nonce;
    }
}
