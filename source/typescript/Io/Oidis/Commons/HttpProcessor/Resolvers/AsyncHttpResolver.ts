/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "../../Enums/Events/EventType.js";
import { GeneralEventOwner } from "../../Enums/Events/GeneralEventOwner.js";
import { HttpStatusType } from "../../Enums/HttpStatusType.js";
import { AsyncRequestEventArgs } from "../../Events/Args/AsyncRequestEventArgs.js";
import { HttpRequestEventArgs } from "../../Events/Args/HttpRequestEventArgs.js";
import { BaseHttpResolver } from "./BaseHttpResolver.js";

/**
 * AsyncHttpResolver class is abstract class providing basic asynchronous request.
 */
export abstract class AsyncHttpResolver extends BaseHttpResolver {

    /**
     * @param {AsyncRequestEventArgs} $value Specify request arguments, which should be processed by async resolver instance.
     * @returns {AsyncRequestEventArgs} Returns current request arguments
     */
    public RequestArgs($value? : AsyncRequestEventArgs) : AsyncRequestEventArgs {
        return <AsyncRequestEventArgs>super.RequestArgs($value);
    }

    /**
     * Execute asynchronous resolver implementation.
     * @returns {void}
     */
    public Process() : void {
        if (!this.RequestArgs().IsMemberOf(AsyncRequestEventArgs)) {
            const origArgs : HttpRequestEventArgs = this.RequestArgs();
            const newArgs : AsyncRequestEventArgs = this.RequestArgs(new AsyncRequestEventArgs(origArgs.Url(), origArgs.POST()));
            this.argsHandler(newArgs.GET(), newArgs.POST());
        }
        this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_START, this.RequestArgs());
        this.resolver();
    }

    protected success() : void {
        this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_SUCCESS, this.RequestArgs());
        this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_COMPLETE, this.RequestArgs());
    }

    protected error() : void {
        this.RequestArgs().Status(HttpStatusType.ERROR);
        this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_ERROR, this.RequestArgs());
        this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_COMPLETE, this.RequestArgs());
    }

    protected resolver() : void {
        this.success();
    }
}
