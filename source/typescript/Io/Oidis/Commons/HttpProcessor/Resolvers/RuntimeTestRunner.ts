/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BuilderConnector } from "../../Connectors/BuilderConnector.js";
import { ErrorEventArgs } from "../../Events/Args/ErrorEventArgs.js";
import { ExceptionsManager } from "../../Exceptions/ExceptionsManager.js";
import { Loader } from "../../Loader.js";
import { ArrayList } from "../../Primitives/ArrayList.js";
import { Convert } from "../../Utils/Convert.js";
import { Echo } from "../../Utils/Echo.js";
import { LogIt } from "../../Utils/LogIt.js";
import { ObjectDecoder } from "../../Utils/ObjectDecoder.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { Property } from "../../Utils/Property.js";
import { Reflection } from "../../Utils/Reflection.js";
import { StringUtils } from "../../Utils/StringUtils.js";
import { BaseHttpResolver } from "./BaseHttpResolver.js";

declare function getCoverageDataId() : string;

/**
 * RuntimeTestRunner class provides abstract layer for light version of runtime unit testing.
 */
export abstract class RuntimeTestRunner extends BaseHttpResolver {

    private static connector : BuilderConnector;
    private passingCount : number = 0;
    private failingCount : number = 0;
    private skippedCount : number = 0;
    private assertionsCount : number = 0;
    private testButtonId : number = 0;
    private eventHandlers : any;
    private methodFilter : string[];
    private defaultTimeout : number = 10000; // ms
    private currentTimeout : number;
    private processedMethods : string[];

    /**
     * Provided code instrumentation suitable for code coverage or generate code coverage report, if instrumented code is available.
     * @returns {void}
     */
    public static GenerateCoverage() : void {
        if (ObjectValidator.IsEmptyOrNull(RuntimeTestRunner.connector)) {
            RuntimeTestRunner.connector = BuilderConnector.Connect();
            RuntimeTestRunner.connector.getEvents().OnError(($data : ErrorEventArgs) : void => {
                const message : string = $data.Message();
                if (StringUtils.Contains(message, "is not running")) {
                    Echo.Printf(
                        "<span style=\"color: red;\">" +
                        "Code coverage requires running Oidis Builder server, but its instance has not been found. " +
                        "Please, validate, that sever has been launched for this project correctly." +
                        "</span>");
                } else {
                    Echo.Printf(message);
                }
            });
            RuntimeTestRunner.connector.getEvents().OnMessage(($data : string) : void => {
                if ($data === ".") {
                    Echo.Print($data);
                } else {
                    Echo.Printf($data);
                }
            });
        }

        const root : string = StringUtils.Remove(Loader.getInstance().getHttpManager().getRequest().getHostUrl(),
            "file:///", "/build/target/index.html");
        if (!ObjectValidator.IsSet((<any>window).getCoverageDataId)) {
            Echo.ClearAll();
            Echo.Printf("Processing code instrumentation, please wait ...");
            RuntimeTestRunner.connector.Send("Spawn", {
                args: ["typescript-test:instrument"],
                cmd : "wui",
                cwd : root
            }, () : void => {
                Loader.getInstance().getHttpManager().Reload();
            });
        } else {
            const coverageData : any = window[getCoverageDataId()];
            const path : string = StringUtils.Replace(coverageData.path, "/", "\\");
            coverageData.path = path;
            const data : any = {};
            data[path] = coverageData;

            Echo.Printf("Generating coverage data ...");
            RuntimeTestRunner.connector.Send("FileSystemHandler", {
                path : root + "/build/reports/remap-coverage.json",
                type : "Write",
                value: JSON.stringify(data)
            }, ($data : any) : void => {
                const status : boolean = ObjectDecoder.Base64($data.data) === "true";
                Echo.Printf("coverage data send: {0}", status);
                if (status) {
                    Echo.Printf("Generating coverage report ...");
                    RuntimeTestRunner.connector.Send("Spawn", {
                        args: ["grunt", "typescript-test:remap"],
                        cmd : "wui",
                        cwd : root
                    }, ($data : any) : void => {
                        const status : boolean = ObjectDecoder.Base64($data.data) === "0";
                        if (status) {
                            Echo.Printf("report url: <a href=\"{0}\" target=\"_blank\">{0}</a>",
                                "file:///" + root + "/build/reports/coverage/typescript/index.html");
                        } else {
                            Echo.Printf("ERROR: Coverage report has not been generated correctly.");
                        }
                    });
                } else {
                    Echo.Printf("ERROR: Coverage data has not been generated correctly.");
                }
            });
        }
    }

    constructor() {
        super();

        this.eventHandlers = {
            onSuiteStart() : void {
                // default event handler
            },
            onSuiteEnd($passing : number, $failing : number) : void {
                // default event handler
            },
            onTestCaseStart($testName : string) : void {
                // default event handler
            },
            onTestCaseEnd($testName : string) : void {
                // default event handler
            },
            onAssert($status : boolean) : void {
                // default event handler
            }
        };
        this.methodFilter = [];
        this.processedMethods = [];
        this.currentTimeout = this.defaultTimeout;
    }

    public toString() : string {
        return this.ToString();
    }

    protected timeoutLimit($value? : number) : number {
        return this.currentTimeout = Property.Integer(this.currentTimeout, $value);
    }

    protected getEvents() : IRuntimeTestEvents {
        return {
            setOnAssert       : ($callback? : any) : void => {
                this.eventHandlers.onAssert = $callback;
            },
            setOnSuiteEnd     : ($callback? : any) : void => {
                this.eventHandlers.onSuiteEnd = $callback;
            },
            setOnSuiteStart   : ($callback? : any) : void => {
                this.eventHandlers.onSuiteStart = $callback;
            },
            setOnTestCaseEnd  : ($callback? : any) : void => {
                this.eventHandlers.onTestCaseEnd = $callback;
            },
            setOnTestCaseStart: ($callback? : any) : void => {
                this.eventHandlers.onTestCaseStart = $callback;
            }
        };
    }

    protected before() : void | IRuntimeTestPromise | Promise<void> {
        // override this method for ability to execute code BEFORE run of ALL tests in current RuntimeTest
        return;
    }

    protected setUp() : void | IRuntimeTestPromise | Promise<void> {
        // override this method for ability to execute code BEFORE EACH test in current RuntimeTest
        return;
    }

    protected tearDown() : void | IRuntimeTestPromise | Promise<void> {
        // override this method for ability to execute code AFTER EACH test in current RuntimeTest
        return;
    }

    protected after() : void | IRuntimeTestPromise | Promise<void> {
        // override this method for ability to execute code AFTER run of ALL tests in current RuntimeTest
        return;
    }

    protected setMethodFilter(...$names : string[]) : void {
        $names.forEach(($name : string) : void => {
            this.methodFilter.push(StringUtils.ToLowerCase($name));
        });
    }

    protected addButton($text : string, $onClick : (() => void) | (() => Promise<void>)) : void {
        const buttonId : number = this.testButtonId++;
        Echo.Println("<div style=\"border: 1px solid red; cursor: pointer; " +
            "width: 250px; overflow-x: hidden; overflow-y: hidden; " +
            "text-align: center; font-size: 16px; font-family: Consolas; color: red;\" " +
            "id=\"TestButton" + buttonId + "\">" + $text + "</div>");

        this.getEventsManager().FireAsynchronousMethod(() : void => {
            const button : HTMLElement = document.getElementById("TestButton" + buttonId);
            button.onclick = () : any => {
                new Promise<void>(($resolve : () => void, $reject : ($error : Error) => void) => {
                    try {
                        const promiseInterface : any = $onClick.apply(this, []);
                        if (!ObjectValidator.IsEmptyOrNull(promiseInterface)) {
                            if (promiseInterface instanceof Promise || promiseInterface.constructor.name === "Promise") {
                                promiseInterface.then($resolve).catch($reject);
                            }
                        } else {
                            $resolve();
                        }
                    } catch (ex) {
                        $reject(ex);
                    }
                }).catch(($error) : void => {
                    Echo.Println($error.message);
                    LogIt.Error($error.message, $error);
                });
            };
        }, false);
    }

    protected resolver() : void {
        document.body.className = "RuntimeTest";
        Echo.Print("<h2>Runtime UNIT Test<h2>");

        const tests : ArrayList<string> = new ArrayList<string>();
        let output : string = "";

        const handlePromiseCall : ($promise : any) => Promise<void> = ($promise : any) : Promise<void> => {
            return new Promise<void>(($resolve : () => void, $reject : ($error : Error) => void) => {
                try {
                    const promiseInterface : any = $promise.apply(this, []);
                    if (!ObjectValidator.IsEmptyOrNull(promiseInterface)) {
                        if (promiseInterface instanceof Promise || promiseInterface.constructor.name === "Promise") {
                            promiseInterface.then($resolve).catch($reject);
                        } else if (ObjectValidator.IsFunction(promiseInterface)) {
                            promiseInterface.apply(this, [$resolve]);
                        } else {
                            $reject(new Error("IRuntimeTestPromise or standard Promise interface is required " +
                                "for asynchronous tests."));
                        }
                    } else {
                        $resolve();
                    }
                } catch (ex) {
                    $reject(ex);
                }
            });
        };

        const getNextTest : any = async ($index : number = 0) : Promise<void> => {
            if ($index < tests.Length()) {
                const method : string = tests.getItem($index);
                const testName : string = StringUtils.Remove(method, "test", "__Ignore");
                Echo.Print("<h3>" + this.getClassName() + " - " + testName + "</h3>");

                if (this.methodFilter.length === 0 && StringUtils.StartsWith(method, "__Ignore") ||
                    this.methodFilter.length !== 0 && (
                        this.methodFilter.indexOf(StringUtils.ToLowerCase(testName)) === -1 &&
                        this.methodFilter.indexOf(StringUtils.ToLowerCase("test" + testName)) === -1 &&
                        this.methodFilter.indexOf(StringUtils.ToLowerCase(method)) === -1)) {
                    this.skippedCount++;
                    output =
                        "<h4><i style=\"color: orange;\">skipped</i></h4>" +
                        "<hr>";
                    Echo.Print(output);
                    await getNextTest($index + 1);
                } else {
                    await handlePromiseCall(this.setUp);
                    this.eventHandlers.onTestCaseStart(method);

                    try {
                        let timeoutId : number = null;
                        await Promise.race([
                            handlePromiseCall(this[method]),
                            new Promise<void>(($resolve : () => void) : void => {
                                if (this.currentTimeout > -1) {
                                    timeoutId = <any>setTimeout(() : void => {
                                        this.failingCount++;
                                        Echo.Println(
                                            "<i id=\"" + this.getClassNameWithoutNamespace() + "_Assert_Fail_" + this.failingCount + "\" " +
                                            "style=\"color: red;\">Test case/suite has reached timeout " +
                                            "(" + this.currentTimeout + " ms)</i>");
                                        $resolve();
                                    }, this.currentTimeout);
                                } else {
                                    Echo.Println("<i style=\"color: orange;\">WARNING: " +
                                        "executing test with infinitive timeout [see this.timeoutLimit(-1)] " +
                                        "- it can lead to test execution block</i>");
                                }
                            })
                        ]);
                        if (!ObjectValidator.IsEmptyOrNull(timeoutId)) {
                            clearTimeout(timeoutId);
                        }
                    } catch (ex) {
                        Echo.Println(ex.message);
                        LogIt.Error(ex.message, ex);
                    }
                    this.currentTimeout = this.defaultTimeout;
                    await handlePromiseCall(this.tearDown);

                    Echo.Print("<hr>");
                    this.processedMethods.push(testName);
                    this.eventHandlers.onTestCaseEnd(method);
                    await getNextTest($index + 1);
                }
            }
        };

        const generateReport : any = () : void => {
            output = "<span class=\"Result\">";
            if (this.failingCount === 0) {
                output += "SUCCESS" + StringUtils.NewLine();
            } else {
                output += "FAILURES!" + StringUtils.NewLine();
            }
            output += "</span>";
            if (this.skippedCount !== 0) {
                output += "Skipped tests: " + this.skippedCount + StringUtils.NewLine();
            }
            output +=
                "Tests: " + tests.Length() + ", Assertions: " + this.assertionsCount + ", Failures: " + this.failingCount + ".";
            Echo.Println(output);
            Echo.Println(StringUtils.NewLine() +
                StringUtils.Format("Page was generated in {0} seconds.", this.getHttpManager().getProcessTime() / 1000));

            if (this.failingCount !== 0) {
                const navigationFirstButtonId : string = this.getClassNameWithoutNamespace() + "_GoToFirstFailing";
                Echo.Print("<div id=\"" + navigationFirstButtonId + "\" " +
                    "class=\"GoToFirstFailing\">Go to first failing assert</div>");
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    const button : HTMLElement = document.getElementById(navigationFirstButtonId);
                    button.onclick = () : any => {
                        try {
                            const assert : HTMLElement =
                                document.getElementById(this.getClassNameWithoutNamespace() + "_Assert_Fail_1");
                            assert.style.paddingTop = "80px";
                            assert.scrollIntoView();
                        } catch (ex) {
                            ExceptionsManager.HandleException(ex);
                        }
                    };
                }, false);

                const navigationNextButtonId : string = this.getClassNameWithoutNamespace() + "_GoToNextFailing";
                Echo.Print("<div id=\"" + navigationNextButtonId + "\" " +
                    "class=\"GoToNextFailing\">Go to next failing assert</div>");
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    const button : HTMLElement = document.getElementById(navigationNextButtonId);
                    let currentIndex : number = 1;
                    button.onclick = () : any => {
                        try {
                            if (currentIndex > this.failingCount) {
                                currentIndex = 1;
                            }
                            for (let index : number = 1; index <= this.failingCount; index++) {
                                const assert : HTMLElement =
                                    document.getElementById(this.getClassNameWithoutNamespace() + "_Assert_Fail_" + index);
                                assert.style.paddingTop = index !== currentIndex ? "0px" : "80px";
                            }
                            document.getElementById(this.getClassNameWithoutNamespace() + "_Assert_Fail_" + currentIndex)
                                .scrollIntoView();
                            currentIndex++;
                        } catch (ex) {
                            ExceptionsManager.HandleException(ex);
                        }
                    };
                }, false);

                const navigationLastButtonId : string = this.getClassNameWithoutNamespace() + "_GoToLastFailing";
                Echo.Print("<div id=\"" + navigationLastButtonId + "\" " +
                    "class=\"GoToLastFailing\">Go to last failing assert</div>");
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    const button : HTMLElement = document.getElementById(navigationLastButtonId);
                    button.onclick = () : any => {
                        try {
                            const assert : HTMLElement = document.getElementById(this.getClassNameWithoutNamespace() +
                                "_Assert_Fail_" + this.failingCount);
                            assert.style.paddingTop = "80px";
                            assert.scrollIntoView();
                        } catch (ex) {
                            ExceptionsManager.HandleException(ex);
                        }
                    };
                }, false);

                const navigationDropId : string = this.getClassNameWithoutNamespace() + "_GoToSpecific";
                Echo.Print("<select id=\"" + navigationDropId + "\" class=\"GoToSpecific\"/>");
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    const drop : HTMLSelectElement = <HTMLSelectElement>document.getElementById(navigationDropId);
                    this.processedMethods.forEach(($item : string) : void => {
                        const option : HTMLOptionElement = document.createElement("option");
                        option.value = $item;
                        option.text = $item;
                        drop.appendChild(option);
                    });
                    drop.onchange = () : any => {
                        const selection : string = (<any>document.getElementById(navigationDropId)).value;
                        const methods : any = document.getElementsByTagName("h3");
                        for (const item in methods) {
                            if (methods.hasOwnProperty(item)) {
                                const entry : HTMLElement = methods[item];
                                if (StringUtils.EndsWith((<any>entry).childNodes[0].data, selection)) {
                                    entry.style.paddingTop = "80px";
                                    entry.scrollIntoView();
                                    break;
                                }
                            }
                        }
                    };
                }, false);
            }
        };

        (async () : Promise<void> => {
            await handlePromiseCall(this.before);
            let index : number;
            const methods : string[] = this.getMethods();
            for (index = 0; index < methods.length; index++) {
                if (StringUtils.Contains(StringUtils.ToLowerCase(methods[index]), "test")) {
                    tests.Add(methods[index]);
                }
            }
            this.eventHandlers.onSuiteStart();

            if (tests.Length() > 0) {
                /// TODO: handle assert errors in before method
                /// TODO: handle timeout uncatched errors
                await getNextTest();
                await handlePromiseCall(this.after);

                generateReport();
                this.eventHandlers.onSuiteEnd(this.passingCount, this.failingCount);
            } else {
                Echo.Println("Suite \"" + this.getClassName() + "\" skipped: no test methods found");
            }
        })().catch(($error : Error) : void => {
            Echo.Println($error.message);
            LogIt.Error($error.message, $error);
        });
    }

    protected assertDeepEqual($actual : any, $expected : any, $message? : string) : void {
        if (Reflection.getInstance().IsMemberOf($actual, ArrayList) && Reflection.getInstance().IsMemberOf($expected, ArrayList)) {
            if ((<ArrayList<any>>$actual).Equal($expected)) {
                $actual = $expected;
            }
        }

        if (ObjectValidator.IsObject($actual) && ObjectValidator.IsObject($expected) ||
            ObjectValidator.IsNativeArray($actual) && ObjectValidator.IsNativeArray($expected)) {
            $actual = JSON.stringify($actual);
            $expected = JSON.stringify($expected);
        }

        this.assertEquals($actual, $expected, $message);
    }

    protected assertEquals($actual : any, $expected : any, $message? : string) : void {
        const assertId : string = this.getClassNameWithoutNamespace() + "_Assert_";
        this.assertionsCount++;
        let output : string = "<h4 id=\"" + assertId + this.assertionsCount + "\">";
        if (ObjectValidator.IsSet($message)) {
            output += " - " + $message + ":" + StringUtils.NewLine();
        }

        let status : boolean = true;
        if ($actual !== $expected) {
            status = false;
            this.failingCount++;
            output += "<i id=\"" + assertId + "Fail_" + this.failingCount + "\" " +
                "style=\"color: red;\">assert #" + this.assertionsCount + " has failed</i></h4>";
            Echo.Print(output);
            this.printFailure($actual, $expected);
        } else {
            this.passingCount++;
            output += "<i id=\"" + assertId + "Pass_" + this.passingCount + "\" " +
                "style=\"color: green;\">assert #" + this.assertionsCount + " has passed</i></h4>";
            Echo.Print(output);
        }
        this.eventHandlers.onAssert(status);
    }

    protected assertOk($actual : any, $message? : string) : void {
        this.assertEquals($actual, true, $message);
    }

    protected getAbsoluteRoot() : string {
        return StringUtils.Remove(ObjectDecoder.Url(this.getRequest().getHostUrl()), "file:///", "file://", "/index.html");
    }

    protected printFailure($actual : any, $expected : any) : void {
        $actual = Convert.ObjectToString($actual);
        if (StringUtils.Length($actual) > 1024) {
            $actual = StringUtils.Substring($actual, 0, 1024) + " ...";
        }
        $expected = Convert.ObjectToString($expected);
        if (StringUtils.Length($expected) > 1024) {
            $expected = StringUtils.Substring($expected, 0, 1024) + " ...";
        }

        Echo.Println("<u>actual:</u>" + StringUtils.NewLine() + $actual);
        Echo.Println("<u>expected:</u>" + StringUtils.NewLine() + $expected);
    }

    protected addMethod($name : string, $method : () => void | IRuntimeTestPromise) : void {
        if (!StringUtils.ContainsIgnoreCase($name, "test")) {
            $name = "test" + $name;
        }
        this[$name] = $method;
    }
}

export type IRuntimeTestPromise = ($done : () => void) => void;

export interface IRuntimeTestEvents {
    setOnSuiteStart($callback : () => void) : void;

    setOnSuiteEnd($callback : ($passing : number, $failing : number) => void) : void;

    setOnTestCaseStart($callback : ($testName? : string) => void) : void;

    setOnTestCaseEnd($callback : ($testName? : string) => void) : void;

    setOnAssert($callback : ($status? : boolean) => void) : void;
}

// generated-code-start
/* eslint-disable */
export const IRuntimeTestEvents = globalThis.RegisterInterface(["setOnSuiteStart", "setOnSuiteEnd", "setOnTestCaseStart", "setOnTestCaseEnd", "setOnAssert"]);
/* eslint-enable */
// generated-code-end
