/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import "./Utils/ReflectionEmitter.js";
import "./Interfaces/Interface.js";

import { BaseDbDriver } from "./DAO/BaseDbContext.js";
import { IndexedDbDriver } from "./DAO/IndexedDbContext.js";
import { EnvironmentArgs } from "./EnvironmentArgs.js";
import { ThreadPool } from "./Events/ThreadPool.js";
import { ExceptionsManager } from "./Exceptions/ExceptionsManager.js";
import "./ForceCompile.js";
import { HttpManager } from "./HttpProcessor/HttpManager.js";
import { HttpResolver } from "./HttpProcessor/HttpResolver.js";
import { INamespaceMapping, IProject } from "./Interfaces/IProject.js";
import { BaseObject } from "./Primitives/BaseObject.js";
import { Counters } from "./Utils/Counters.js";
import { appExceptionHandler, isBrowser } from "./Utils/EnvironmentHelper.js";
import { LogIt } from "./Utils/LogIt.js";
import { ObjectValidator } from "./Utils/ObjectValidator.js";
import { Reflection } from "./Utils/Reflection.js";

/**
 * Loader class provides handling of application content singleton.
 */
export class Loader extends BaseObject {

    private static singleton : Loader;
    private environment : EnvironmentArgs;
    private resolver : HttpResolver;
    private dbDriver : BaseDbDriver;

    /**
     * Create new application instance from this entry point.
     * @param {string} $appConfig Specify project configuration in JSON format.
     */
    public static Load($appConfig : string | IProject) : void {
        try {
            Loader.singleton = new this();
            Loader.singleton.Process($appConfig);
        } catch (ex) {
            LogIt.Error("Application start error.", ex);
        }
    }

    /**
     * @returns {Loader} Returns application singleton
     */
    public static getInstance() : Loader {
        return Loader.singleton;
    }

    /**
     * Start application process
     * @param {string} $appConfig Specify project configuration in JSON format.
     * @returns {void}
     */
    public Process($appConfig : string | IProject) : void {
        this.environment = this.initEnvironment();
        this.environment.Load($appConfig, () : void => {
            this.onEnvironmentLoad().catch((ex) : void => {
                if (ObjectValidator.IsSet(appExceptionHandler)) {
                    appExceptionHandler(ex.message, ex.stack);
                } else {
                    LogIt.Error("Application load error.", ex);
                }
            });
        });
    }

    /**
     * @returns {EnvironmentArgs} Returns singleton of EnvironmentArgs used by current application.
     */
    public getEnvironmentArgs() : EnvironmentArgs {
        return this.environment;
    }

    /**
     * @returns {HttpResolver} Returns singleton of HttpResolver used by current application.
     */
    public getHttpResolver() : HttpResolver {
        return this.resolver;
    }

    /**
     * @returns {HttpResolver} Returns singleton of HttpManager used by current application.
     */
    public getHttpManager() : HttpManager {
        return this.resolver.getManager();
    }

    public getDBDriver() : BaseDbDriver {
        return this.dbDriver;
    }

    protected initResolver() : HttpResolver {
        return new HttpResolver(this.environment.getProjectName());
    }

    protected initEnvironment() : EnvironmentArgs {
        return new EnvironmentArgs();
    }

    protected initDbDriver() : BaseDbDriver {
        return new IndexedDbDriver();
    }

    protected async onEnvironmentLoad() : Promise<void> {
        delete (<any>Reflection).singleton;
        const targetNamespaces : string[] = this.environment.getNamespaces();
        const namespaceMappings : INamespaceMapping[] = this.environment.getNamespaceMappings();
        Reflection.setInstanceNamespaces.apply(Reflection, targetNamespaces); // eslint-disable-line
        Reflection.setNamespaceMappings.apply(Reflection, namespaceMappings); // eslint-disable-line
        Reflection.getInstance();
        LogIt.getLogger().setConfig(this.environment.getProjectConfig().logger);
        Counters.Clear();
        ExceptionsManager.Clear();
        ThreadPool.Clear();
        this.dbDriver = this.initDbDriver();
        if (isBrowser) {
            try {
                if (!await this.getDBDriver().Open({dbName: this.environment.getProjectConfig().dbConfigs.dbName})) {
                    LogIt.Warning("Can not connect to FE database. Some features will not be available.");
                }
            } catch (ex) {
                LogIt.Warning("Failed to connect to FE database: {0}", ex.message);
            }
        }
        this.resolver = this.initResolver();
        await this.resolver.RegisterResolvers();
        this.resolver.getEvents().Subscribe();
        if (this.environment.HtmlOutputAllowed()) {
            this.resolver.ResolveRequest();
        }
    }
}
