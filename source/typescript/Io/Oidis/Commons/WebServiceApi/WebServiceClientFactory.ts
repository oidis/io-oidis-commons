/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { HttpRequestParser } from "../HttpProcessor/HttpRequestParser.js";
import { IWebServiceClient } from "../Interfaces/IWebServiceClient.js";
import { Loader } from "../Loader.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { StringUtils } from "../Utils/StringUtils.js";
import { AjaxClient } from "./Clients/AjaxClient.js";
import { CefQueryClient } from "./Clients/CefQueryClient.js";
import { IframeClient } from "./Clients/IframeClient.js";
import { JxbrowserBridgeClient } from "./Clients/JxbrowserBridgeClient.js";
import { PostMessageClient } from "./Clients/PostMessageClient.js";
import { WebSocketsClient } from "./Clients/WebSocketsClient.js";
import { WebServiceConfiguration } from "./WebServiceConfiguration.js";

export class WebServiceClientFactory extends BaseObject {

    private static clientsList : ArrayList<IWebServiceClient>;

    /**
     * @param {WebServiceClientType} $clientType Create or get this type of web service client.
     * @param {string|WebServiceConfiguration} [$serverConfigurationSource] Specify source for configuration of
     * server-client communication.
     * @returns {IWebServiceClient} Returns instance of web service client based on $clientType,
     * if is supported by runtime environment otherwise null.
     */
    public static getClient($clientType : WebServiceClientType,
                            $serverConfigurationSource? : string | WebServiceConfiguration) : IWebServiceClient {
        let key : string = <string>$clientType;
        if (!ObjectValidator.IsEmptyOrNull($serverConfigurationSource)) {
            if (ObjectValidator.IsString($serverConfigurationSource)) {
                key += <string>$serverConfigurationSource;
            } else {
                key += (<WebServiceConfiguration>$serverConfigurationSource).getServerUrl();
            }
        }
        key = StringUtils.getSha1(key);
        if (!ObjectValidator.IsSet(WebServiceClientFactory.clientsList)) {
            WebServiceClientFactory.clientsList = new ArrayList<IWebServiceClient>();
        }
        if (!WebServiceClientFactory.clientsList.KeyExists(key) && !ObjectValidator.IsEmptyOrNull($clientType)) {
            const client : IWebServiceClient = this.createClientInstance($clientType, $serverConfigurationSource);
            if (!ObjectValidator.IsEmptyOrNull(client)) {
                WebServiceClientFactory.clientsList.Add(client, key);
            }
        }

        return WebServiceClientFactory.clientsList.getItem(key);
    }

    /**
     * @param {WebServiceClientType} $clientType Specify client type, which should be validated.
     * @returns {boolean} Returns true, if required client type is supported by runtime environment, othewise false.
     */
    public static IsSupported($clientType : WebServiceClientType) : boolean {
        const request : HttpRequestParser = Loader.getInstance().getHttpManager().getRequest();
        switch ($clientType) {
        case WebServiceClientType.AJAX:
        case WebServiceClientType.AJAX_SYNCHRONOUS:
            return request.IsOnServer();
        case WebServiceClientType.IFRAME:
            return !!document.createElement("iframe");
        case WebServiceClientType.POST_MESSAGE:
            return "postMessage" in globalThis;
        case WebServiceClientType.WEB_SOCKETS:
            return "WebSocket" in globalThis;
        case WebServiceClientType.HTTP:
            return WebServiceClientFactory.IsSupported(WebServiceClientType.AJAX) ||
                WebServiceClientFactory.IsSupported(WebServiceClientType.IFRAME) ||
                WebServiceClientFactory.IsSupported(WebServiceClientType.POST_MESSAGE);
        case WebServiceClientType.CEF_QUERY:
            return StringUtils.Contains(request.getUserAgent(), "Chrome") && ("cefQuery" in globalThis);
        case WebServiceClientType.JXBROWSER_BRIDGE:
            return StringUtils.Contains(request.getUserAgent(), "Chrome") && ("java" in globalThis);
        default:
            break;
        }
        return false;
    }

    /**
     * @param {number} $clientId Specify id of the desired service client.
     * @returns {boolean} Returns true if instance of web service client with desired $clientId exists, otherwise false.
     */
    public static Exists($clientId : number) : boolean {
        return !ObjectValidator.IsEmptyOrNull(WebServiceClientFactory.getClientById($clientId));
    }

    /**
     * @param {number} $clientId Specify id of the desired service client.
     * @returns {IWebServiceClient} Returns instance of web service client based on $clientId,
     * if client has been found, otherwise null.
     */
    public static getClientById($clientId : number) : IWebServiceClient {
        let client : IWebServiceClient = null;
        if (!ObjectValidator.IsEmptyOrNull($clientId) && ObjectValidator.IsSet(WebServiceClientFactory.clientsList)) {
            WebServiceClientFactory.clientsList.foreach(($client : IWebServiceClient) : boolean => {
                if (!ObjectValidator.IsEmptyOrNull($client) && $client.getId() === $clientId) {
                    client = $client;
                    return false;
                }
                return true;
            });
        }

        return client;
    }

    /**
     * Close all currently running server-client communications.
     * @returns {void}
     */
    public static CloseAllConnections() : void {
        if (ObjectValidator.IsSet(WebServiceClientFactory.clientsList)) {
            WebServiceClientFactory.clientsList.foreach(($client : IWebServiceClient) : void => {
                $client.StopCommunication();
            });
        }
    }

    protected static createClientInstance($clientType : WebServiceClientType,
                                          $serverConfigurationSource? : string | WebServiceConfiguration) : IWebServiceClient {
        let configuration : WebServiceConfiguration;
        if (ObjectValidator.IsString($serverConfigurationSource) ||
            ObjectValidator.IsEmptyOrNull($serverConfigurationSource)) {
            configuration = new WebServiceConfiguration(<string>$serverConfigurationSource);
        } else {
            configuration = <WebServiceConfiguration>$serverConfigurationSource;
        }
        let client : IWebServiceClient = null;

        const request : HttpRequestParser = Loader.getInstance().getHttpManager().getRequest();

        const getRestClient : ($configuration : WebServiceConfiguration) => IWebServiceClient =
            ($configuration : WebServiceConfiguration) : IWebServiceClient => {
                if (StringUtils.StartsWith(request.getHostUrl(), "https://")) {
                    $configuration.ServerProtocol("https");
                } else {
                    $configuration.ServerProtocol("http");
                }
                if (request.IsSameOrigin($configuration.getServerUrl())) {
                    return new AjaxClient($configuration);
                } else if (WebServiceClientFactory.IsSupported(WebServiceClientType.POST_MESSAGE)) {
                    return new PostMessageClient($configuration);
                } else {
                    return new IframeClient($configuration);
                }
            };

        if (WebServiceClientFactory.IsSupported($clientType)) {
            switch ($clientType) {
            case WebServiceClientType.AJAX:
            case WebServiceClientType.AJAX_SYNCHRONOUS:
                if (request.IsSameOrigin(configuration.getServerUrl())) {
                    client = new AjaxClient(configuration, $clientType === WebServiceClientType.AJAX_SYNCHRONOUS);
                }
                break;
            case WebServiceClientType.IFRAME:
                client = new IframeClient(configuration);
                break;
            case WebServiceClientType.POST_MESSAGE:
                client = new PostMessageClient(configuration);
                break;
            case WebServiceClientType.WEB_SOCKETS:
                if (StringUtils.StartsWith(request.getHostUrl(), "https://") ||
                    configuration.ServerProtocol() === "https" ||
                    configuration.ServerPort() === 443) {
                    configuration.ServerProtocol("wss");
                } else {
                    configuration.ServerProtocol("ws");
                }
                client = new WebSocketsClient(configuration);
                break;
            case WebServiceClientType.HTTP:
                client = getRestClient(configuration);
                break;
            case WebServiceClientType.CEF_QUERY:
                client = new CefQueryClient();
                break;
            case WebServiceClientType.JXBROWSER_BRIDGE:
                client = new JxbrowserBridgeClient();
                break;
            default:
                break;
            }
        }

        return client;
    }
}
