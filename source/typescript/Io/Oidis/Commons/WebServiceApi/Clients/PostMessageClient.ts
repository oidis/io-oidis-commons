/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "../../Enums/Events/EventType.js";
import { GeneralEventOwner } from "../../Enums/Events/GeneralEventOwner.js";
import { MessageEventArgs } from "../../Events/Args/MessageEventArgs.js";
import { EventsManager } from "../../Events/EventsManager.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { WebServiceConfiguration } from "../WebServiceConfiguration.js";
import { BaseWebServiceClient } from "./BaseWebServiceClient.js";

/**
 * PostMessageClient class provides asynchronous http client for bidirectional cross-origin
 * communication with REST services.
 */
export class PostMessageClient extends BaseWebServiceClient {

    /**
     * @param {WebServiceConfiguration} $configuration Specify client-server communication parameters
     */
    constructor($configuration : WebServiceConfiguration) {
        super($configuration);

        this.getEvents().OnTimeout(() : void => {
            this.StopCommunication();
        });
    }

    public StartCommunication() : void {
        if (this.clientInstance() === null) {
            this.getConfiguration().Load(() : void => {
                const client : HTMLIFrameElement = this.clientInstance(document.createElement("iframe"));
                document.body.appendChild(client);
                client.contentWindow.name = "PostMessageConnector" + this.getId();

                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_MESSAGE,
                    ($eventArgs : MessageEventArgs) : void => {
                        this.onResponse($eventArgs.NativeEventArgs());
                    });

                let isLoaded : boolean = false;
                client.onload = () : void => {
                    try {
                        if (!isLoaded) {
                            isLoaded = true;
                            super.StartCommunication();
                        }
                    } catch (ex) {
                        this.throwError(ex);
                    }
                };

                this.resetTimeout();
                client.src = this.getServerUrl();
            }, () : void => {
                this.throwError("Unable to load client configuration from: " + this.getConfiguration().getSource());
            });
        }
    }

    public StopCommunication() : void {
        const client : HTMLIFrameElement = this.clientInstance();
        if (client !== null) {
            if (!ObjectValidator.IsEmptyOrNull(client) && !ObjectValidator.IsEmptyOrNull(client.parentNode)) {
                client.parentNode.removeChild(client);
            }
            super.StopCommunication();
        }
    }

    protected clientInstance($value? : HTMLIFrameElement) : HTMLIFrameElement {
        return <HTMLIFrameElement>super.clientInstance($value);
    }

    protected sendRequest($data : string) : void {
        this.clientInstance().contentWindow.postMessage($data, this.getServerUrl());
    }

    protected onResponse($event? : MessageEvent) : any {
        try {
            if (this.CommunicationIsRunning()) {
                const eventData : any = $event.data;
                EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                    this.responseResolver(eventData);
                }, false);
                super.onResponse();
            }
        } catch (ex) {
            this.throwError(ex);
        }
    }
}
