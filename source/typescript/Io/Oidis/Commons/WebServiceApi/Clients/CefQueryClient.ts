/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ICefQuery, ICefQueryMessage } from "../../Interfaces/ICefQuery.js";
import { ArrayList } from "../../Primitives/ArrayList.js";
import { Echo } from "../../Utils/Echo.js";
import { WebServiceConfiguration } from "../WebServiceConfiguration.js";
import { BaseWebServiceClient } from "./BaseWebServiceClient.js";

/**
 * CefQueryClient class provides client for bidirectional cross-origin communication with web sockets services.
 */
export class CefQueryClient extends BaseWebServiceClient {
    private requestIds : ArrayList<number>;

    constructor() {
        super(new WebServiceConfiguration());
        this.requestIds = new ArrayList<number>();
    }

    public StartCommunication() : void {
        if (this.clientInstance() === null) {
            this.clientInstance((<any>window).cefQuery);
            try {
                super.StartCommunication();
            } catch (ex) {
                this.throwError(ex);
            }
        }
    }

    public StopCommunication() : void {
        try {
            this.requestIds.foreach(($id : number) : void => {
                (<any>window).cefQueryCancel($id);
            });
            super.StopCommunication();
        } catch (ex) {
            this.throwError(ex);
        }
    }

    protected clientInstance($value? : ICefQuery) : ICefQuery {
        return <ICefQuery>super.clientInstance($value);
    }

    protected sendRequest($data : string) : void {
        const client : ICefQuery = this.clientInstance();
        try {
            this.requestIds.Add(client.apply(this, [
                <ICefQueryMessage>{
                    onFailure : ($code : number, $message? : string) : void => {
                        this.throwError($message);
                    },
                    onSuccess : ($response : string) : void => {
                        this.onResponse($response);
                    },
                    persistent: true,
                    request   : $data
                }
            ]));
        } catch (ex) {
            Echo.Printf(ex.message);
            this.throwError(ex);
        }
    }

    protected onResponse($message : string) : void {
        try {
            if ($message === "OK") {
                super.onResponse($message);
            } else {
                this.responseResolver($message);
            }
        } catch (ex) {
            this.throwError(ex);
        }
    }
}
