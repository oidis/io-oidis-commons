/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientEventType } from "../../Enums/Events/WebServiceClientEventType.js";
import { ErrorEventArgs } from "../../Events/Args/ErrorEventArgs.js";
import { EventArgs } from "../../Events/Args/EventArgs.js";
import { EventsManager } from "../../Events/EventsManager.js";
import { Exception } from "../../Exceptions/Type/Exception.js";
import { IWebServiceClientEvents } from "../../Interfaces/Events/IWebServiceClientEvents.js";
import { IWebServiceClient, IWebServiceRequestFormatterData, IWebServiceResponseHandler } from "../../Interfaces/IWebServiceClient.js";
import { ArrayList } from "../../Primitives/ArrayList.js";
import { BaseObject } from "../../Primitives/BaseObject.js";
import { Convert } from "../../Utils/Convert.js";
import { LogIt } from "../../Utils/LogIt.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { WebServiceConfiguration } from "../WebServiceConfiguration.js";

/**
 * BaseWebServiceClient class provides general http client for bidirectionl communication with web services.
 */
export abstract class BaseWebServiceClient extends BaseObject implements IWebServiceClient {

    private readonly configuration : WebServiceConfiguration;
    private client : any;
    private isRunning : boolean;
    private timeoutId : number;
    private requestSend : boolean;
    private requestsBuffer : ArrayList<any>;
    private responseBuffer : ArrayList<($response : any) => void>;
    private requestFormatter : ($data : IWebServiceRequestFormatterData) => void;
    private responseFormatter : ($data : any, $owner : IWebServiceClient,
                                 $onSuccess : ($value : any, $key? : number) => void,
                                 $onError : ($message : string | Error | Exception) => void) => void;

    /**
     * @param {WebServiceConfiguration} $configuration Specify client-server communication parameters
     */
    constructor($configuration : WebServiceConfiguration) {
        super();

        this.configuration = $configuration;
        this.client = null;
        this.isRunning = false;
        this.requestSend = false;
        this.requestsBuffer = new ArrayList<any>();
        this.responseBuffer = new ArrayList<($response : any) => void>();
        this.requestFormatter = ($data : IWebServiceRequestFormatterData) : void => {
            // default request data formatter will not effect data
        };
        this.responseFormatter = ($data : any, $owner : IWebServiceClient,
                                  $onSuccess : ($value : any, $key? : number) => void,
                                  $onError : ($message : string | Error | Exception) => void) : void => {
            $onSuccess($data);
        };
    }

    /**
     * @returns {number} Returns client's id, which can be used for events handling.
     */
    public getId() : number {
        return this.configuration.getId();
    }

    /**
     * @returns {string} Returns target service url.
     */
    public getServerUrl() : string {
        return this.configuration.getServerUrl();
    }

    /**
     * @param {Function} $handler Specify async handler for handling of server path data.
     * @returns {void}
     */
    public getServerPath($handler : ($path : string) => void) : void {
        return this.configuration.Load(
            () : void => {
                $handler(this.configuration.ServerLocation());
            }, () : void => {
                this.throwError("Unable to get server path from configuration: " + this.configuration.getSource());
            });
    }

    /**
     * @returns {string} Returns target service version.
     */
    public getServerVersion() : string {
        return this.configuration.Version();
    }

    /**
     * @returns {IWebServiceClientEvents} Returns events manager interface.
     */
    public getEvents() : IWebServiceClientEvents {
        return <IWebServiceClientEvents>{
            OnClose  : ($eventHandler : ($eventArgs : EventArgs) => void) : void => {
                EventsManager.getInstanceSingleton().setEvent("" + this.getId(), WebServiceClientEventType.ON_CLOSE, $eventHandler);
            },
            OnError  : ($eventHandler : ($eventArgs : ErrorEventArgs) => void) : void => {
                EventsManager.getInstanceSingleton().setEvent("" + this.getId(), WebServiceClientEventType.ON_ERROR, $eventHandler);
            },
            OnStart  : ($eventHandler : ($eventArgs : EventArgs) => void) : void => {
                EventsManager.getInstanceSingleton().setEvent("" + this.getId(), WebServiceClientEventType.ON_START, $eventHandler);
            },
            OnTimeout: ($eventHandler : ($eventArgs : EventArgs) => void) : void => {
                EventsManager.getInstanceSingleton().setEvent("" + this.getId(), WebServiceClientEventType.ON_TIMEOUT, $eventHandler);
            }
        };
    }

    /**
     * Start Communication with target service
     * @returns {void}
     */
    public StartCommunication() : void {
        if (!this.isRunning) {
            this.isRunning = true;
            this.requestSend = false;
            LogIt.Info("Started " + this.getClassNameWithoutNamespace() + " communication with server " + this.getServerUrl());
            EventsManager.getInstanceSingleton().FireEvent("" + this.getId(), WebServiceClientEventType.ON_START);
            this.nextRequest();
        }
    }

    /**
     * Stop Communication with target service
     * @returns {void}
     */
    public StopCommunication() : void {
        if (this.isRunning) {
            this.client = null;
            this.isRunning = false;
            EventsManager.getInstanceSingleton().FireEvent("" + this.getId(), WebServiceClientEventType.ON_CLOSE);
        }
    }

    /**
     * @returns {boolean} Returns true, if communication with target service is live, otherwise false.
     */
    public CommunicationIsRunning() : boolean {
        return this.isRunning;
    }

    /**
     * @param {any} $data Specify request command.
     * @param {IWebServiceResponseHandler} [$handler] Specify response handler.
     * @returns {void}
     */
    public Send($data : any, $handler? : IWebServiceResponseHandler) : void {
        if (!ObjectValidator.IsSet($handler)) {
            $handler = null;
        }
        this.requestsBuffer.Add({data: $data, handler: $handler});
        if (!this.CommunicationIsRunning()) {
            this.StartCommunication();
        } else if (this.requestsBuffer.Length() === 1) {
            this.nextRequest();
        }
    }

    /**
     * @param {Function} $formatter Specify request formatter function
     * @returns {void}
     */
    public setRequestFormatter($formatter : ($data : IWebServiceRequestFormatterData) => void) : void {
        if (!ObjectValidator.IsEmptyOrNull($formatter)) {
            this.requestFormatter = $formatter;
        }
    }

    /**
     * @param {Function} $formatter Specify response formatter function
     * @returns {void}
     */
    public setResponseFormatter($formatter : ($data : any, $owner : IWebServiceClient,
                                              $onSuccess : ($value : any, $key? : number) => void,
                                              $onError : ($message : string | Error | Exception) => void) => void) : void {
        if (!ObjectValidator.IsEmptyOrNull($formatter)) {
            this.responseFormatter = $formatter;
        }
    }

    public toString() : string {
        return "WebServiceClient(" + this.getId() + ")";
    }

    protected getConfiguration() : WebServiceConfiguration {
        return this.configuration;
    }

    protected clientInstance($value? : any) : any {
        if (ObjectValidator.IsSet($value)) {
            this.client = $value;
        }
        return this.client;
    }

    protected sendRequest($data : string) : void {
        // place holder for handling of data, which should be send
    }

    protected onResponse($eventData? : any) : any {
        if (this.requestSend) {
            this.requestSend = false;
            if (ObjectValidator.IsSet(this.timeoutId)) {
                clearTimeout(this.timeoutId);
            }
            this.requestsBuffer.RemoveAt(0);
            this.nextRequest();
        }
    }

    protected responseResolver($data : any) : void {
        if (!ObjectValidator.IsEmptyOrNull($data)) {
            if (ObjectValidator.IsString($data) || ObjectValidator.IsObject($data)) {
                let response : any;
                if (ObjectValidator.IsString($data)) {
                    try {
                        response = JSON.parse(<string>$data);
                    } catch (ex) {
                        response = $data;
                    }
                } else {
                    response = $data;
                }
                this.responseFormatter(response, this,
                    ($value : any, $key? : number) : void => {
                        if (!ObjectValidator.IsSet($key)) {
                            $key = 0;
                        }
                        if (this.responseBuffer.KeyExists($key)) {
                            this.responseBuffer.getItem($key)($value);
                        }
                        if (ObjectValidator.IsString($value)) {
                            if (!this.requestsBuffer.IsEmpty()) {
                                this.responseBuffer.RemoveAt(0);
                            }
                        }
                    },
                    ($message : string | Error | Exception) : void => {
                        this.throwError($message);
                    });
            } else {
                this.throwError("Unsupported response data type \"" + Convert.ToType($data) + "\".");
            }
        } else {
            this.throwError("Data transfer failure.");
        }
    }

    protected resetTimeout() : void {
        if (ObjectValidator.IsSet(this.timeoutId)) {
            clearTimeout(this.timeoutId);
        }
        this.timeoutId = <any>setTimeout(() : void => {
            try {
                EventsManager.getInstanceSingleton().FireEvent("" + this.getId(), WebServiceClientEventType.ON_TIMEOUT);
            } catch (ex) {
                this.throwError(ex);
            }
        }, this.configuration.TimeoutLimit());
    }

    protected throwError($message : string | Exception | Error) : void {
        const eventArgs : ErrorEventArgs = new ErrorEventArgs($message);
        eventArgs.Owner(this);
        LogIt.Debug(this.getClassName() + " error: " + eventArgs.Message());
        EventsManager.getInstanceSingleton().FireEvent("" + this.getId(), WebServiceClientEventType.ON_ERROR, eventArgs);
    }

    private nextRequest() : void {
        if (!this.requestsBuffer.IsEmpty() && this.isRunning) {
            this.requestSend = true;
            try {
                const buffer : any = this.requestsBuffer.getFirst();
                const data : any = {value: buffer.data, key: -1};
                this.requestFormatter(data);
                if (!ObjectValidator.IsEmptyOrNull(buffer.handler)) {
                    if (data.key === -1) {
                        this.responseBuffer.Add(buffer.handler);
                    } else {
                        this.responseBuffer.Add(buffer.handler, data.key);
                    }
                }
                let sendData : string;
                if (ObjectValidator.IsString(data.value)) {
                    sendData = <string>data.value;
                } else {
                    sendData = JSON.stringify(data.value);
                }
                this.resetTimeout();
                this.sendRequest(sendData);
            } catch (ex) {
                this.throwError(ex);
            }
        }
    }
}
