/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpMethodType } from "../../Enums/HttpMethodType.js";
import { EventsManager } from "../../Events/EventsManager.js";
import { JsonpFileReader } from "../../IOApi/Handlers/JsonpFileReader.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { StringUtils } from "../../Utils/StringUtils.js";
import { WebServiceConfiguration } from "../WebServiceConfiguration.js";
import { BaseWebServiceClient } from "./BaseWebServiceClient.js";

/**
 * IframeClient class provides asynchronous http client for bidirectional communication with
 * REST services for old browsers.
 */
export class IframeClient extends BaseWebServiceClient {
    private sendCommand : HTMLFormElement;
    private dataInput : HTMLInputElement;

    /**
     * @param {WebServiceConfiguration} $configuration Specify client-server communication parameters
     */
    constructor($configuration : WebServiceConfiguration) {
        super($configuration);

        this.getEvents().OnTimeout(() : void => {
            this.StopCommunication();
        });
    }

    public StartCommunication() : void {
        if (this.clientInstance() === null) {
            this.getConfiguration().Load(() : void => {
                const client : HTMLIFrameElement = this.clientInstance(document.createElement("iframe"));
                document.body.appendChild(client);
                const connectorId : string = "IframeConnector" + this.getId();
                client.contentWindow.name = connectorId;

                client.onload = () : void => {
                    try {
                        if (this.CommunicationIsRunning()) {
                            this.onResponse();
                        } else {
                            super.StartCommunication();
                        }
                    } catch (ex) {
                        this.throwError(ex);
                    }
                };

                this.sendCommand = document.createElement("form");
                this.sendCommand.target = connectorId;
                this.sendCommand.action = this.getServerUrl();
                this.sendCommand.method = HttpMethodType.POST;

                this.dataInput = document.createElement("input");
                this.dataInput.type = "hidden";
                this.dataInput.name = "jsonData";
                this.sendCommand.appendChild(this.dataInput);

                client.appendChild(this.sendCommand);

                this.resetTimeout();
                client.src = this.getServerUrl();
            }, () : void => {
                this.throwError("Unable to load client configuration from: " + this.getConfiguration().getSource());
            });
        }
    }

    public StopCommunication() : void {
        const client : HTMLIFrameElement = this.clientInstance();
        if (client !== null) {
            if (!ObjectValidator.IsEmptyOrNull(client) && !ObjectValidator.IsEmptyOrNull(client.parentNode)) {
                client.parentNode.removeChild(client);
            }
            super.StopCommunication();
        }
    }

    protected clientInstance($value? : HTMLIFrameElement) : HTMLIFrameElement {
        return <HTMLIFrameElement>super.clientInstance($value);
    }

    protected sendRequest($data : string) : void {
        this.dataInput.value = $data;
        this.sendCommand.submit();
    }

    protected onResponse($event? : MessageEvent) : any {
        try {
            if (this.CommunicationIsRunning()) {
                if (!ObjectValidator.IsEmptyOrNull(this.getConfiguration().ResponseUrl())) {
                    JsonpFileReader.Load(StringUtils.Format(this.getConfiguration().ResponseUrl(),
                            StringUtils.Replace(this.getId().toString(), "-", "_")),
                        ($data : string | object) : void => {
                            EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                                this.responseResolver($data);
                            }, false);
                            super.onResponse();
                        }, () : void => {
                            this.throwError("Unable to get response from: " + this.getConfiguration().ResponseUrl());
                        });
                } else {
                    super.onResponse();
                }
            }
        } catch (ex) {
            this.throwError(ex);
        }
    }
}
