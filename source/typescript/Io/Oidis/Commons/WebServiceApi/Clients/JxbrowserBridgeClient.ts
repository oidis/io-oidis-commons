/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionsManager } from "../../Exceptions/ExceptionsManager.js";
import { IJxbrowserBridge } from "../../Interfaces/IJxbrowserBridge.js";
import { ArrayList } from "../../Primitives/ArrayList.js";
import { ObjectDecoder } from "../../Utils/ObjectDecoder.js";
import { ObjectValidator } from "../../Utils/ObjectValidator.js";
import { WebServiceConfiguration } from "../WebServiceConfiguration.js";
import { BaseWebServiceClient } from "./BaseWebServiceClient.js";

/**
 * JxbrowserBridgeClient class provides client for bidirectional communication between services and Java projects through
 * Jxbrowser bridge.
 */
export class JxbrowserBridgeClient extends BaseWebServiceClient {

    private static responseHandlers : ArrayList<JxbrowserBridgeClient>;

    public static Callback($data : string) : string {
        setTimeout(() => {
            try {
                const response : any = JSON.parse($data);
                const client : JxbrowserBridgeClient = JxbrowserBridgeClient.responseHandlers.getItem(response.clientId);
                if (client !== null) {
                    try {
                        if (response.data === "OK") {
                            client.onResponse();
                        } else {
                            response.data = <any>JSON.parse(ObjectDecoder.Base64(response.data));
                            client.responseResolver(response);
                            client.onResponse();
                        }
                    } catch (ex) {
                        client.throwError(ex);
                    }
                }
            } catch (ex) {
                ExceptionsManager.Throw(JxbrowserBridgeClient.ClassName(), ex);
            }
        });
        return "OK";
    }

    constructor() {
        super(new WebServiceConfiguration());
    }

    public StartCommunication() : void {
        try {
            if (this.clientInstance() === null) {
                this.clientInstance((<any>window).java);
                if (!ObjectValidator.IsSet(JxbrowserBridgeClient.responseHandlers)) {
                    JxbrowserBridgeClient.responseHandlers = new ArrayList<JxbrowserBridgeClient>();
                }
                super.StartCommunication();
            }
        } catch (ex) {
            this.throwError(ex);
        }
    }

    public StopCommunication() : void {
        try {
            if (ObjectValidator.IsSet(JxbrowserBridgeClient.responseHandlers)) {
                JxbrowserBridgeClient.responseHandlers.RemoveAt(
                    JxbrowserBridgeClient.responseHandlers.IndexOf(
                        JxbrowserBridgeClient.responseHandlers.getItem(this.getId())));
                if (JxbrowserBridgeClient.responseHandlers.IsEmpty()) {
                    JxbrowserBridgeClient.responseHandlers = undefined;
                }
            }
            super.StopCommunication();
        } catch (ex) {
            this.throwError(ex);
        }
    }

    protected clientInstance($value? : IJxbrowserBridge) : IJxbrowserBridge {
        return <IJxbrowserBridge>super.clientInstance($value);
    }

    protected sendRequest($data : string) : void {
        setTimeout(() => {
            try {
                JxbrowserBridgeClient.responseHandlers.Add(this, this.getId());
                this.clientInstance().send(this.getId(), $data);
            } catch (ex) {
                JxbrowserBridgeClient.responseHandlers.RemoveAt(JxbrowserBridgeClient.responseHandlers.IndexOf(this));
                this.throwError(ex);
            }
        });
    }
}
