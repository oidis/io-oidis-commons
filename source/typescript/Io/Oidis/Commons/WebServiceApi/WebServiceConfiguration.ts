/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IWebServiceConfigurationProtocol } from "../Interfaces/IWebServiceConfigurationProtocol.js";
import { JsonpFileReader } from "../IOApi/Handlers/JsonpFileReader.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { ObjectValidator } from "../Utils/ObjectValidator.js";
import { Property } from "../Utils/Property.js";
import { StringUtils } from "../Utils/StringUtils.js";

/**
 * WebServiceConfiguration class provides structure for specification of client-server communication.
 */
export class WebServiceConfiguration extends BaseObject {

    private readonly id : number;
    private location : string;
    private protocol : string;
    private address : string;
    private base : string;
    private port : number;
    private readonly configPath : string;
    private timeout : number;
    private responseUrl : string;
    private version : string;

    /**
     * @param {string} [$loadFromPath] Specify path to external JSONP configuration, which should be loaded.
     */
    constructor($loadFromPath? : string) {
        super();

        this.id = StringUtils.getCrc(this.getUID());
        if (!ObjectValidator.IsEmptyOrNull($loadFromPath)) {
            this.configPath = $loadFromPath;
        } else {
            this.configPath = null;
        }
        this.location = "";
        this.protocol = "http";
        this.address = "127.0.0.1";
        this.base = "";
        this.port = 80;
        this.timeout = 2000;
        this.responseUrl = "";
        this.version = "";
    }

    /**
     * @returns {number} Returns configuration's id, which should be used as client id.
     */
    public getId() : number {
        return this.id;
    }

    /**
     * @returns {string} Returns path to external configuration if configuration has been load from file otherwise null.
     */
    public getSource() : string {
        return this.configPath;
    }

    /**
     * @param {string} [$value] Specify server file system location.
     * @returns {string} Returns location of server in target file system.
     */
    public ServerLocation($value? : string) : string {
        return this.location = Property.String(this.location, $value);
    }

    /**
     * @param {string} [$value] Specify protocol for server-client communication.
     * @returns {string} Returns server-client communication protocol.
     */
    public ServerProtocol($value? : string) : string {
        return this.protocol = Property.String(this.protocol, $value);
    }

    /**
     * @param {string} [$value] Specify server url address.
     * @returns {string} Returns url address of the target server.
     */
    public ServerAddress($value? : string) : string {
        return this.address = Property.String(this.address, $value);
    }

    /**
     * @param {string} [$value] Specify server base url path.
     * @returns {string} Returns base url path of the target server.
     */
    public ServerBase($value? : string) : string {
        return this.base = Property.String(this.base, $value);
    }

    /**
     * @param {number} [$value] Specify port for server-client communication.
     * @returns {number} Returns server-client communication port.
     */
    public ServerPort($value? : number) : number {
        return this.port = Property.PositiveInteger(this.port, $value);
    }

    /**
     * @returns {string} Returns full url path to the target server.
     */
    public getServerUrl() : string {
        let base : string = this.base;
        if (!ObjectValidator.IsEmptyOrNull(base)) {
            if (!StringUtils.StartsWith(base, "/")) {
                base = "/" + base;
            }
            if (!StringUtils.EndsWith(base, "/")) {
                base += "/";
            }
        } else {
            base = "/";
        }
        let url : string = this.protocol + "://" + this.address;
        if (this.port !== 80) {
            url += ":" + this.port;
        }
        return url + base;
    }

    /**
     * @param {string} [$value] Specify url path from wich can be reed last server response in JSONP format.
     * @returns {string} Returns url path to the last server response in JSONP format.
     */
    public ResponseUrl($value? : string) : string {
        return this.responseUrl = Property.String(this.responseUrl, $value);
    }

    /**
     * @param {number} [$value] Specify timeout limit for server-client communication.
     * @returns {number} Returns server-client communication timeout limit.
     */
    public TimeoutLimit($value? : number) : number {
        return this.timeout = Property.PositiveInteger(this.timeout, $value);
    }

    /**
     * @returns {string} Returns target server version declared by web service configuration.
     */
    public Version() : string {
        return this.version;
    }

    /**
     * @param {Callback} $onSuccess Specify handler for successful load of the configuration.
     * @param {Callback} $onError Specify handler for error in loading of the configuration.
     * @returns {void}
     */
    public Load($onSuccess : () => void, $onError : () => void) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.configPath)) {
            JsonpFileReader.Load(this.configPath, ($data : IWebServiceConfigurationProtocol) : void => {
                if (!ObjectValidator.IsEmptyOrNull($data)) {
                    this.ServerLocation($data.location);
                    this.ServerAddress($data.address);
                    this.ServerBase($data.base);
                    this.ServerPort($data.port);
                    this.ServerProtocol($data.protocol);
                    this.ResponseUrl($data.responseUrl);
                    this.TimeoutLimit($data.timeout * 1000);
                    this.version = Property.String(this.version, $data.version);
                    $onSuccess();
                } else {
                    $onError();
                }
            }, $onError);
        } else {
            $onSuccess();
        }
    }
}
