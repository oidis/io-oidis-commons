/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { SyntaxConstants } from "../Enums/SyntaxConstants.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { Counters } from "./Counters.js";
import { ObjectValidator } from "./ObjectValidator.js";
import { Property } from "./Property.js";
import { Reflection } from "./Reflection.js";
import { StringUtils } from "./StringUtils.js";

/**
 * Convert class provides static methods focused on object conversions to string or to other type of object.
 */
export class Convert extends BaseObject {

    /**
     * @param {any} $object Object for parsing.
     * @returns {string} Returns object type of in string format.
     */
    public static ToType($object : any) : string {
        Reflection.getInstance();
        let type : string = "";
        if (ObjectValidator.IsSet($object.ClassName)) {
            type = $object.ClassName();
        } else if (ObjectValidator.IsClass($object)) {
            type = $object.getClassName();
        }
        if (ObjectValidator.IsEmptyOrNull(type)) {
            let match : string[] = $object.toString().match(/function\s(\w*)/);
            if (match === null) {
                match = (({}).toString.call($object) + "").match(/\s([a-zA-Z]+)/);
                type = (match[1] + "").toLowerCase();
            } else {
                type = match[1];
            }
            type = SyntaxConstants.ALIASES.indexOf(type) > -1 ? "Function" : type;
        }
        return type;
    }

    /**
     * @param {any} $input Input object for conversion to string.
     * @param {string} [$prefix=""] Prefix for each line of the output.
     * @param {boolean} [$htmlTag=true] Choose, if output will be in HTML format or in plain text.
     * @returns {string} Returns object type of Array and ArrayList converted to HTML string.
     */
    public static ArrayToString($input : any, $prefix : string = "", $htmlTag : boolean = true) : string {
        let output : string = "";
        let size : number;
        let keys : Array<string | number>;
        let data : any[];
        let key : any;
        let value : any;
        let index : number;

        if (ObjectValidator.IsArray($input)) {
            if (ObjectValidator.IsNativeArray($input)) {
                size = $input.length;
                keys = null;
                data = $input;
            } else {
                size = $input.Length();
                keys = $input.getKeys();
                data = $input.getAll();
            }
        } else if (ObjectValidator.IsObject($input)) {
            index = 0;
            keys = [];
            data = [];
            for (key in $input) {
                /* istanbul ignore else : bulletproof condition */
                if (key !== null && $input.hasOwnProperty(key)) {
                    keys[index] = key;
                    data[index] = $input[key];
                    index++;
                }
            }
            size = index;
        }

        for (index = 0; index < size; index++) {
            key = keys !== null ? keys[index] : index;
            value = data[index];
            output += $prefix;

            if (ObjectValidator.IsString(key)) {
                key = "\"" + key + "\"";
            }

            if (value !== null) {
                if (ObjectValidator.IsArray(value) && !ObjectValidator.IsNativeArray(value)) {
                    value = value.ToString($prefix + StringUtils.Tab(2, $htmlTag), $htmlTag);
                } else if (ObjectValidator.IsNativeArray(value)) {
                    value = this.ArrayToString(value, $prefix + StringUtils.Tab(2, $htmlTag), $htmlTag);
                } else if (!ObjectValidator.IsString(value)) {
                    value = this.ObjectToString(value, $prefix, $htmlTag) + StringUtils.NewLine($htmlTag);
                } else if (value === "") {
                    if ($htmlTag) {
                        value += "<b>EMPTY</b>";
                    } else {
                        value += "EMPTY";
                    }
                    value += StringUtils.NewLine($htmlTag);
                } else {
                    value += StringUtils.NewLine($htmlTag);
                }
            } else {
                value = "NULL" + StringUtils.NewLine($htmlTag);
            }
            output += "[ " + key + " ]" + StringUtils.Space(4, $htmlTag) + value;
        }

        if (StringUtils.IsEmpty(output)) {
            output = $prefix + "Data object ";
            if ($htmlTag) {
                output += "<b>EMPTY</b>";
            } else {
                output += "EMPTY";
            }
        }

        if (ObjectValidator.IsArray($input) && !ObjectValidator.IsNativeArray($input)) {
            if ($htmlTag) {
                output = Convert.ToHtmlContentBlock("<i>" + $input.getClassName() + " object</i> ", output);
            } else {
                output = $input.getClassName() + " object" + StringUtils.NewLine(false) + output;
            }
        } else if (ObjectValidator.IsNativeArray($input)) {
            if ($htmlTag) {
                output = Convert.ToHtmlContentBlock("<i>Array object</i> ", output);
            } else {
                output = "Array object" + StringUtils.NewLine(false) + output;
            }
        }

        return output;
    }

    /**
     * @param {string} $input Input string for conversion.
     * @returns {number} Returns number representation of input string. Conversion is done in numeral system 10.
     */
    public static StringToInteger($input : string) : number {
        return StringUtils.ToInteger($input);
    }

    /**
     * @param {string} $input Input string for conversion.
     * @returns {number} Returns number representation of input string. Conversion is done in numeral system 10.
     */
    public static StringToDouble($input : string) : number {
        return StringUtils.ToDouble($input);
    }

    /**
     * @param {string} $input Input string for conversion.
     * @returns {boolean} Returns boolean representation of input string.
     */
    public static StringToBoolean($input : string) : boolean {
        return StringUtils.ToBoolean($input);
    }

    /**
     * @param {boolean} $input Input boolean for conversion.
     * @returns {string} Returns string representation of boolean input.
     */
    public static BooleanToString($input : boolean) : string {
        if (!ObjectValidator.IsEmptyOrNull($input) && ObjectValidator.IsBoolean($input)) {
            if ($input) {
                return "true";
            } else {
                return "false";
            }
        }
        return "";
    }

    /**
     * @param {Function} $input Input function for conversion.
     * @param {boolean} [$multiline=false] Specify, if new lines in function declaration should be preserved.
     * @returns {string} Returns string representation of function input.
     */
    public static FunctionToString($input : any, $multiline : boolean = false) : string {
        let output : string = "";
        if (ObjectValidator.IsFunction($input)) {
            if (ObjectValidator.IsSet($input.toSource)) {
                output = $input.toSource();
            }

            if (StringUtils.IsEmpty(output) || StringUtils.Contains(output, "\n")) {
                output = $input.toString();
                let index : number;
                const strings : string[] = (output + "").match(/"(.*?)"/gm);
                if (strings !== null) {
                    for (index = 0; index < strings.length; index++) {
                        output = StringUtils.Replace(output, strings[index], "##STR" + index + "##");
                    }
                }
                if (!$multiline) {
                    output = StringUtils.Remove(output, "\r");
                    output = StringUtils.Remove(output, "\n");
                }
                output = StringUtils.Remove(output, "  ");
                if (strings !== null) {
                    for (index = 0; index < strings.length; index++) {
                        output = StringUtils.Replace(output, "##STR" + index + "##", strings[index]);
                    }
                }
            }
            output = StringUtils.Replace(output, "() => {", "function () {");
            output = output.replace(/^\((.*)\) => \{/gmi, "function ($1) {");
            output = StringUtils.Replace(output, "function function ()", "function ()");
            if (StringUtils.StartsWith(output, "(function")) {
                output = StringUtils.Substring(output, 1, StringUtils.Length(output) - 1);
            }
        }
        return output;
    }

    /**
     * @param {any} $object Object for conversion to string.
     * @param {string} [$prefix=""] Prefix for each line of the output.
     * @param {boolean} [$htmlTag=true] Choose, if output will be in HTML format or in plain text.
     * @returns {string} Returns object type of any converted to HTML string.
     */
    public static ObjectToString($object : any, $prefix : string = "", $htmlTag : boolean = true) : string {
        if (ObjectValidator.IsSet($object)) {
            if ($object !== null) {
                if (ObjectValidator.IsString($object)) {
                    if (!StringUtils.IsEmpty($object)) {
                        return $object;
                    } else {
                        return "EMPTY";
                    }
                } else {
                    if (ObjectValidator.IsObject($object)) {
                        if (typeof $object[SyntaxConstants.TO_STRING] === SyntaxConstants.FUNCTION) {
                            return $object.ToString($prefix, $htmlTag);
                        } else if (typeof $object[SyntaxConstants.IS_TYPE_OF] === SyntaxConstants.FUNCTION &&
                            $object.IsMemberOf(BaseObject)) {
                            return "Object \"" + $object.getClassName() + "\" does not implement method ToString.";
                        } else {
                            if ($htmlTag) {
                                return StringUtils.Replace(
                                    StringUtils.Replace(
                                        StringUtils.Replace(JSON.stringify($object, null, 3), "\r\n", "\n"), "   ", StringUtils.Tab()),
                                    "\n", StringUtils.NewLine());
                            } else {
                                return JSON.stringify($object);
                            }
                        }
                    } else {
                        let output : string = "";
                        if ($htmlTag) {
                            output = "<i>Object type:</i> " + this.ToType($object) + ". ";
                            if (ObjectValidator.IsArray($object)) {
                                output += "<i>Return values:</i>" + StringUtils.NewLine();
                                output += this.ArrayToString($object, $prefix, true);
                            } else if (ObjectValidator.IsBoolean($object)) {
                                output += "<i>Return value:</i> " + this.BooleanToString($object);
                            } else if (ObjectValidator.IsFunction($object)) {
                                output += "<i>Source code:</i> " + this.FunctionToString($object);
                            } else {
                                output += "<i>Return value:</i> " + $object;
                            }
                        } else {
                            output = "Object type: " + this.ToType($object) + ". ";
                            if (ObjectValidator.IsArray($object)) {
                                output += "Return values: " + StringUtils.NewLine(false);
                                output += this.ArrayToString($object, $prefix, false);
                            } else if (ObjectValidator.IsBoolean($object)) {
                                output += "Return value: " + this.BooleanToString($object);
                            } else if (ObjectValidator.IsFunction($object)) {
                                output += "Source code: " + this.FunctionToString($object);
                            } else {
                                output += "Return value: " + $object;
                            }
                        }
                        return output;
                    }
                }
            } else {
                return "NULL";
            }
        } else {
            return "NOT DEFINED";
        }

    }

    /**
     * @param {(number|string|Date)} $time Object for conversion.
     * @returns {string} Returns GMT format of input time.
     */
    public static TimeToGMTformat($time : number | string | Date) : string {
        let date : Date;
        if ((<Date>$time) instanceof Date) {
            date = (<Date>$time);
        } else {
            date = new Date(Property.Time(null, <number>$time));
        }
        return date.toUTCString();
    }

    /**
     * @param {number} $time Time stamp in milliseconds for conversion.
     * @returns {number} Returns time stamp converted to seconds.
     */
    public static TimeToSeconds($time : number) : number {
        if ($time <= 0) {
            return 0;
        }
        return $time / 1000;
    }

    /**
     * @param {number} $input Input number for conversion.
     * @returns {string} Returns string representation of hexadecimal input.
     */
    public static HexadecimalToString($input : number) : string {
        if (ObjectValidator.IsEmptyOrNull($input)) {
            return "";
        }
        let output : string = "";
        let index : number;
        for (index = 7; index >= 0; index--) {
            output += (($input >>> (index * 4)) & 0x0f).toString(16);
        }
        return output;
    }

    /**
     * @param {string} $input Input string for conversion.
     * @returns {number} Returns hexadecimal representation of input string.
     */
    public static StringToHexadecimal($input : string) : number {
        return StringUtils.ToHexadecimal($input);
    }

    /**
     * @param {...number} $input Open-ended args for for conversion.
     * @returns {string} Returns string composed from unicode representation of characters.
     */
    public static UnicodeToString(...$input : number[]) : string {
        let output : string = "";
        let index : number;
        for (index = 0; index < $input.length; index++) {
            output += String.fromCharCode($input[index]);
        }
        return output;
    }

    /**
     * @param {number} $input Input number for conversion.
     * @param {number} $decimalPoints Specify decimal precision.
     * @param {number} [$base=10] Specify output radix.
     * @returns {number} Returns string composed from unicode representation of characters.
     */
    public static ToFixed($input : number, $decimalPoints : number, $base? : number) : number {
        if ($decimalPoints === 0) {
            return Math.trunc(Math.round($input));
        }
        const pow : number = Math.pow($base || 10, $decimalPoints);
        return +(Math.round($input * pow) / pow);
    }

    /**
     * @param {number} $input Input number for conversion.
     * @returns {number} Returns number in radians from degrees.
     */
    public static DegToRad($input : number) : number {
        return $input * .017453292519943295;
    }

    /**
     * @param {number} $input Input number for conversion.
     * @returns {number} Returns number in degrees from radians.
     */
    public static RadToDeg($input : number) : number {
        return $input * 57.29577951308232;
    }

    /**
     * @param {number} $input Input number for conversion.
     * @param {number} [$decimalPoints] Specify decimal precision.
     * @returns {string} Returns rounded number in string format with excepted size unit.
     */
    public static IntegerToSize($input : number, $decimalPoints? : number) : string {
        if ($input >= 0) {
            if ($input < 1024) {
                return $input + " B";
            } else if ($input < 1024 * 1024) {
                return Convert.ToFixed($input / 1024,
                    ObjectValidator.IsEmptyOrNull($decimalPoints) ? 0 : $decimalPoints) + " kB";
            } else if ($input < 1024 * 1024 * 1024) {
                return Convert.ToFixed($input / (1024 * 1024),
                    ObjectValidator.IsEmptyOrNull($decimalPoints) ? 1 : $decimalPoints) + " MB";
            } else if ($input < 1024 * 1024 * 1024 * 1024) {
                return Convert.ToFixed($input / (1024 * 1024 * 1024),
                    ObjectValidator.IsEmptyOrNull($decimalPoints) ? 2 : $decimalPoints) + " GB";
            } else if ($input < 1024 * 1024 * 1024 * 1024 * 1024) {
                return Convert.ToFixed($input / (1024 * 1024 * 1024 * 1024),
                    ObjectValidator.IsEmptyOrNull($decimalPoints) ? 2 : $decimalPoints) + " TB";
            } else {
                return $input + " B";
            }
        }
        return "0 B";
    }

    /**
     * @param {string} $header Specify header text for wrapped content.
     * @param {string} $content Specify value which should be wrapped.
     * @param {string} [$toggleText="Open/Close"] Specify text which should be used as link for content show and hide.
     * @returns {string} Returns content encapsulated by simple HTML wrapper enabling content show and hide.
     */
    public static ToHtmlContentBlock($header : string, $content : string, $toggleText : string = "Open/Close") : string {
        const id : string = "ContentBlock_" + Counters.getNext(Convert.ClassName());
        return "" +
            $header +
            "<span onclick=\"" +
            "document.getElementById('" + id + "').style.display=" +
            "document.getElementById('" + id + "').style.display==='block'?'none':'block';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
            $toggleText +
            "</span>" +
            StringUtils.NewLine() +
            "<span id=\"" + id + "\" style=\"border: 0 solid black; display: none;\">" + $content + "</span>";
    }

    /**
     * @param {number} $time Specify time in milliseconds which should be converted.
     * @returns {string} Returns time in long string format.
     */
    public static TimeToLongString($time : number) : string {
        if ($time < 1000) {
            return "0 seconds";
        }
        const dur : number[] = <any>{};
        const units : any[] = [
            {label: "millis", mod: 1000},
            {label: "seconds", mod: 60},
            {label: "minutes", mod: 60},
            {label: "hours", mod: 24},
            {label: "days", mod: 31}
        ];
        units.forEach(($unit : any) : void => {
            $time = ($time - (dur[$unit.label] = ($time % $unit.mod))) / $unit.mod;
        });
        return units
            .reverse()
            .filter(($unit : any) : number => {
                return $unit.label === "millis" ? 0 : dur[$unit.label];
            })
            .map(($unit : any) : string => {
                return dur[$unit.label] + " " + (dur[$unit.label] === 1 ? $unit.label.slice(0, -1) : $unit.label);
            })
            .join(", ");
    }

    /**
     * @param {string} $data Specify data in base64 format which should be converted.
     * @returns {ArrayBuffer} Returns data in binary format.
     */
    public static Base64ToArrayBuffer($data : string) : ArrayBuffer {
        const binaryString : string = window.atob($data);
        const length : number = binaryString.length;
        const bytes : Uint8Array = new Uint8Array(length);
        for (let i : number = 0; i < length; i++) {
            bytes[i] = binaryString.charCodeAt(i);
        }
        return bytes.buffer;
    }

    /**
     * @param {string} $input Specify data in base64 format which should be converted.
     * @param {string} [$contentType=application/octet-stream] Specify output content type.
     * @param {number} [$sliceSize=512] Specify size of data chunks for blob creation.
     * @returns {Blob} Returns data in blob format.
     */
    public static Base64ToBlob($input : string, $contentType : string = "application/octet-stream", $sliceSize : number = 512) : Blob {
        const byteCharacters : string = window.atob($input);
        const byteArrays : Uint8Array[] = [];

        for (let offset : number = 0; offset < byteCharacters.length; offset += $sliceSize) {
            const slice : string = byteCharacters.slice(offset, offset + $sliceSize);
            const byteNumbers : number[] = new Array(slice.length);
            for (let index : number = 0; index < slice.length; index++) {
                byteNumbers[index] = slice.charCodeAt(index);
            }
            byteArrays.push(new Uint8Array(byteNumbers));
        }

        return new Blob(byteArrays, {type: $contentType});
    }

    /**
     * @param {number} $time Specify time in milliseconds which should be converted.
     * @returns {string} Returns time in time zone string format.
     */
    public static TimeToLocalFormat($time : number | Date) : string {
        return new Date($time).toLocaleTimeString([], {
            hour12  : false,
            timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
            hour    : "2-digit",
            minute  : "2-digit",
            second  : "2-digit"
        });
    }
}
