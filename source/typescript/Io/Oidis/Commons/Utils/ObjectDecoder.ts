/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "../Enums/Events/EventType.js";
import { SyntaxConstants } from "../Enums/SyntaxConstants.js";
import { EventsManager } from "../Events/EventsManager.js";
import { TimeoutManager } from "../Events/TimeoutManager.js";
import { ExceptionsManager } from "../Exceptions/ExceptionsManager.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { Convert } from "./Convert.js";
import { ObjectEncoder } from "./ObjectEncoder.js";
import { ObjectValidator } from "./ObjectValidator.js";
import { Reflection } from "./Reflection.js";
import { StringUtils } from "./StringUtils.js";

/**
 * ObjectDecoder class provides static methods focused on decoding of encoded object or strings.
 */
export class ObjectDecoder extends BaseObject {

    /**
     * @param {string} $input String representation of the object, which should be reconstructed to
     * it's image before serialization.
     * @param {Function} [$asyncHandler] Specify handler function, which will be asynchronously called,
     * when the unserialization is finished.
     * @throws {Exception} Throws exception in case of, that has been found invalid or unrecognized data
     * in time of parsing.
     * @returns {any} Returns reconstructed object, if $asyncHandler has not been specified, otherwise null
     * and fully reconstructed object will be returned as $asyncHandler input argument.
     */
    public static Unserialize($input : string, $asyncHandler? : ($data : any) => void) : any {
        let classMap : string[] = [];
        const asyncManager : TimeoutManager = new TimeoutManager();
        const reflection : Reflection = new Reflection();

        const classProcessor : ($className : string, $value : any) => any = ($className : string, $value : any) : any => {
            $value[SyntaxConstants.CLASS_NAME] = () : string => {
                return $className;
            };
            let output : any;
            const instance : any = reflection.getClass($className);
            if (!ObjectValidator.IsEmptyOrNull(instance) && ObjectValidator.IsSet(instance.getInstance)) {
                output = instance.getInstance($value);
            } else {
                output = null;
            }

            if (output === null) {
                ExceptionsManager.Throw("Unserialize", "Object type of \"" + $className + "\" can not be unserialized.");
            }
            return output;
        };

        let unserializeAsync : ($input : string, $handler : ($data : any) => void) => void; // eslint-disable-line prefer-const
        const unserialize : ($input : string, $handler? : ($data : any) => void) => any =
            ($input : string, $handler? : ($data : any) => void) : any => {
                const async : boolean = ObjectValidator.IsSet($handler);
                let type : string;
                let value : string = "";
                let length : string = "";
                let offset : number = 0;
                let output : any;

                const parse : ($input : string) => string = ($input : string) : string => {
                    const type = StringUtils.ToLowerCase(StringUtils.Substring($input, 0, 1));
                    if (type === "i" || type === "d") {
                        value = StringUtils.Substring($input, 2, StringUtils.IndexOf($input, ";", true, 2));
                        length = StringUtils.Length(value).toString();
                    } else if (type === "s" || type === "f" || type === "a" || type === "o" || type === "m" || type === "c") {
                        length = StringUtils.Substring($input, 2, StringUtils.IndexOf($input, ":", true, 2));
                        offset = 3 + StringUtils.Length(length);
                        value = StringUtils.Substring($input, offset, offset + StringUtils.ToInteger(length));
                    } else {
                        value = StringUtils.Substring($input, 2, 3);
                        length = "1";
                    }
                    return type;
                };

                type = parse($input);
                switch (type) {
                case "u":
                    if (async) {
                        $handler(output);
                    }
                    break;
                case "n":
                    output = null;
                    if (async) {
                        $handler(output);
                    }
                    break;
                case "e":
                    output = "";
                    if (async) {
                        $handler(output);
                    }
                    break;
                case "b":
                    output = StringUtils.ToBoolean(value);
                    if (async) {
                        $handler(output);
                    }
                    break;
                case "i":
                    output = StringUtils.ToInteger(value);
                    if (async) {
                        $handler(output);
                    }
                    break;
                case "d":
                    output = StringUtils.ToDouble(value);
                    if (async) {
                        $handler(output);
                    }
                    break;
                case "s":
                    output = value;
                    if (async) {
                        $handler(output);
                    }
                    break;
                case "f":
                    output = new Function("return function (" + value + "};"); // eslint-disable-line @typescript-eslint/no-implied-eval
                    output = output();
                    if (async) {
                        $handler(output);
                    }
                    break;
                case "a":
                case "o":
                    if (type === "a") {
                        output = [];
                    } else {
                        output = {};
                    }
                    const outputParts : ArrayList<string> = new ArrayList<string>(); // eslint-disable-line
                    let objectData : string = value; // eslint-disable-line
                    while (StringUtils.Length(objectData) !== 0) {
                        type = parse(objectData);
                        offset = StringUtils.ToInteger(length) + 3;
                        if (type === "s") {
                            offset += StringUtils.Length(length);
                        }
                        const propertyName : any = unserialize(StringUtils.Substring(objectData, 0, offset));
                        objectData = StringUtils.Substring(objectData, offset, StringUtils.Length(objectData));

                        type = parse(objectData);
                        if (type === "c") {
                            length = StringUtils.Substring(objectData, 2, StringUtils.IndexOf(objectData, ":", true, 2));
                            offset = 3 + StringUtils.Length(length);
                            length = StringUtils.Substring(objectData, offset, StringUtils.IndexOf(objectData, ":", true, offset));
                            offset += StringUtils.Length(length) + StringUtils.ToInteger(length) + 1;
                        } else if (type === "s" || type === "a" || type === "o" || type === "f") {
                            offset = 3 + StringUtils.Length(length) + StringUtils.ToInteger(length);
                        } else if (type === "b") {
                            offset = 3;
                        } else if (type === "u" || type === "n" || type === "e") {
                            offset = 1;
                        } else {
                            offset = 3 + StringUtils.ToInteger(length);
                        }
                        outputParts.Add(StringUtils.Substring(objectData, 0, offset), propertyName);
                        objectData = StringUtils.Substring(objectData, offset, StringUtils.Length(objectData));
                    }

                    if (async) {
                        if (outputParts.IsEmpty()) {
                            $handler(output);
                        } else {
                            const partsLength : number = outputParts.Length();
                            let parserIndex : number = 0;
                            outputParts.foreach(($value : string, $name? : string) : void => {
                                unserializeAsync($value, ($data : any) : void => {
                                    output[$name] = $data;
                                    parserIndex++;
                                    if (parserIndex === partsLength) {
                                        $handler(output);
                                    }
                                });
                            });
                        }
                    } else {
                        outputParts.foreach(($value : string, $name? : string) : void => {
                            output[$name] = unserialize($value);
                        });
                    }
                    break;
                case "m":
                    classMap = unserialize(value);
                    if (async) {
                        unserializeAsync(StringUtils.Substring($input,
                            offset + StringUtils.ToInteger(length), StringUtils.Length($input)), $handler);
                    } else {
                        output = unserialize(StringUtils.Substring($input,
                            offset + StringUtils.ToInteger(length), StringUtils.Length($input)));
                    }
                    break;
                case "c":
                    // eslint-disable-next-line
                    const classIndex : number = StringUtils.ToInteger(StringUtils.Substring($input,
                        2, StringUtils.IndexOf($input, ":", true, 2)));
                    if (<number>classMap.length === 0) {
                        ExceptionsManager.Throw("Unserialize",
                            "Class map has not been found, so class can not be unserialized.");
                    }
                    const className : string = classMap[classIndex]; // eslint-disable-line
                    if (ObjectValidator.IsEmptyOrNull(className)) {
                        ExceptionsManager.Throw("Unserialize",
                            "Class name has not been found in class map, so the class can not be unserialized.");
                    }
                    offset = 3 + StringUtils.Length(classIndex.toString());
                    length = StringUtils.Substring($input, offset, StringUtils.IndexOf($input, ":", true, offset));
                    offset += StringUtils.Length(length) + 1;
                    value = StringUtils.Substring($input, offset, offset + StringUtils.ToInteger(length));
                    if (async) {
                        unserializeAsync(value, ($data : any) : void => {
                            output = classProcessor(className, $data);
                            $handler(output);
                        });
                    } else {
                        output = classProcessor(className, unserialize(value));
                    }
                    break;
                default :
                    ExceptionsManager.Throw("Unserialize", "Unrecognized data type \"" + type + "\".");
                    /* istanbul ignore next : unreachable code */
                    break;
                }

                if (!async) {
                    return output;
                }
            };
        unserializeAsync = ($input : string, $handler : ($data : any) => void) : void => {
            asyncManager.Add(() : void => {
                unserialize($input, $handler);
            });
        };

        if (ObjectValidator.IsSet($asyncHandler)) {
            let output : any;
            unserializeAsync($input, ($data : any) : void => {
                output = $data;
            });
            EventsManager.getInstanceSingleton().setEvent(asyncManager.getId(), EventType.ON_COMPLETE, () : void => {
                $asyncHandler(output);
            });
            asyncManager.Execute();
            return null;
        } else {
            return unserialize($input);
        }
    }

    /**
     * @param {string} $input String value, which should be decoded.
     * @returns {string} Returns unescaped string.
     */
    public static Url($input : string) : string {
        return decodeURIComponent($input);
    }

    /**
     * @param {string} $input String value, which should be decoded.
     * @returns {string} Returns string decoded from Uft8 format.
     */
    public static Utf8($input : string) : string {
        if (ObjectValidator.IsEmptyOrNull($input)) {
            return "";
        }

        $input += "";
        const buffer : string[] = [];
        let bufferSize : number = 0;
        let index : number = 0;
        let code : number = 0;
        let sequenceLength : number = 0;
        let sequenceIndex : number;

        while (index < StringUtils.Length($input)) {
            code = StringUtils.getCodeAt($input, index) & 0xFF;
            sequenceLength = 0;

            if (code <= 0xBF) {
                code = (code & 0x7F);
                sequenceLength = 1;
            } else if (code <= 0xDF) {
                code = (code & 0x1F);
                sequenceLength = 2;
            } else if (code <= 0xEF) {
                code = (code & 0x0F);
                sequenceLength = 3;
            } else {
                code = (code & 0x07);
                sequenceLength = 4;
            }

            for (sequenceIndex = 1; sequenceIndex < sequenceLength; ++sequenceIndex) {
                code = ((code << 0x06) | (StringUtils.getCodeAt($input, sequenceIndex + index) & 0x3F));
            }

            if (sequenceLength === 4) {
                code -= 0x10000;
                buffer[bufferSize++] = Convert.UnicodeToString(0xD800 | ((code >> 10) & 0x3FF));
                buffer[bufferSize++] = Convert.UnicodeToString(0xDC00 | (code & 0x3FF));
            } else {
                buffer[bufferSize] = Convert.UnicodeToString(code);
                bufferSize++;
            }
            index += sequenceLength;
        }

        return buffer.join("");
    }

    /**
     * @param {string} $input String value, which should be decoded.
     * Allowed is not encoded and also url safe base64 encoded string.
     * @param {boolean} [$normalize=true] Normalize $input value for decoding.
     * @returns {string} Returns decoded string, if input string has been recognized as base64 encoded.
     */
    public static Base64($input : string, $normalize : boolean = true) : string {
        if (ObjectValidator.IsEmptyOrNull($input)) {
            return "";
        }

        if ($normalize) {
            let urlUnsafe : string = StringUtils.Replace($input, "-", "+");
            urlUnsafe = StringUtils.Replace(urlUnsafe, "_", "/");
            urlUnsafe = StringUtils.Replace(urlUnsafe, ".", "=");

            try {
                const base64Matcher : RegExp =
                    new RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$");
                if (!base64Matcher.test(urlUnsafe)) {
                    return $input;
                }
            } catch (ex) {
                // regex exception should be considered as data in url unsafe format
            }

            $input = urlUnsafe;
        }
        if ($normalize) {
            $input = this.Url(ObjectEncoder.Url($input));
        }
        $input = ObjectDecoder.getBase64Decoder()($input);
        if ($normalize) {
            $input = this.Utf8($input);
        }
        return $input;
    }

    private static getBase64Decoder() : any {
        let decoder : any;
        if (ObjectValidator.IsSet(window.atob)) {
            decoder = window.atob;
        } else {
            decoder = ($input : string) : string => {
                const b64 : string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
                let o1 : number;
                let o2 : number;
                let o3 : number;
                let h1 : number;
                let h2 : number;
                let h3 : number;
                let h4 : number;
                let bits : number;
                let index : number = 0;
                let ac : number = 0;
                const tmp : string[] = [];

                do {
                    h1 = StringUtils.IndexOf(b64, StringUtils.getCharacterAt($input, index++));
                    h2 = StringUtils.IndexOf(b64, StringUtils.getCharacterAt($input, index++));
                    h3 = StringUtils.IndexOf(b64, StringUtils.getCharacterAt($input, index++));
                    h4 = StringUtils.IndexOf(b64, StringUtils.getCharacterAt($input, index++));

                    bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

                    o1 = bits >> 16 & 0xff;
                    o2 = bits >> 8 & 0xff;
                    o3 = bits & 0xff;
                   
                    if (h3 === 64) {
                        tmp[ac++] = Convert.UnicodeToString(o1);
                    } else if (h4 === 64) {
                        tmp[ac++] = Convert.UnicodeToString(o1, o2);
                    } else {
                        tmp[ac++] = Convert.UnicodeToString(o1, o2, o3);
                    }
                } while (index < ($input + "").length);
                return tmp.join("").replace(/\0+$/, "");
            };
        }
        ObjectDecoder.getBase64Decoder = () : any => {
            return decoder;
        };
        return decoder;
    }
}
