/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { SyntaxConstants } from "../Enums/SyntaxConstants.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { StringUtils } from "./StringUtils.js";

/**
 * ObjectValidator class provides static methods focused on object type validations.
 */
export class ObjectValidator extends BaseObject {

    /**
     * @param {any} $input Validate this object or property.
     * @returns {boolean} Returns true, if object or property is defined, otherwise false.
     */
    public static IsSet($input : any) : boolean {
        return SyntaxConstants.UNDEFINED.indexOf($input) === -1;
    }

    /**
     * @param {any} $input Validate this type of object.
     * @returns {boolean} Returns true, if object is empty or null, otherwise false.
     */
    public static IsEmptyOrNull($input : any) : boolean {
        if (!this.IsSet($input)) {
            return true;
        }
        if ($input === null || $input === "") {
            return true;
        }
        if (this.IsNativeArray($input)) {
            return $input.length === 0;
        }
        if (typeof $input[SyntaxConstants.IS_EMPTY] === SyntaxConstants.FUNCTION &&
            typeof $input[SyntaxConstants.LENGTH] === SyntaxConstants.FUNCTION) {
            return $input.IsEmpty();
        }
        return false;
    }

    /**
     * @param {any} $input Validate this type of object.
     * @returns {boolean} Returns true, if $input is object, otherwise false.
     */
    public static IsObject($input : any) : boolean {
        return $input !== null && typeof $input === SyntaxConstants.OBJECT && !this.IsNativeArray($input);
    }

    /**
     * @param {any} $input Validate this type of object.
     * @returns {boolean} Returns true, if $input is function, otherwise false.
     */
    public static IsFunction($input : any) : boolean {
        return this.IsSet($input) && typeof $input === SyntaxConstants.FUNCTION;
    }

    /**
     * @param {any} $input Validate this type of class instance.
     * @returns {boolean} Returns true, if $input is class instance, otherwise false.
     */
    public static IsClass($input : any) : boolean {
        return this.IsObject($input) && typeof $input[SyntaxConstants.CLASS_NAME] === SyntaxConstants.FUNCTION;
    }

    /**
     * @param {any} $input Validate this type of object.
     * @returns {boolean} Returns true, if object is type of boolean, otherwise false.
     */
    public static IsBoolean($input : any) : boolean {
        return typeof $input === SyntaxConstants.BOOLEAN;
    }

    /**
     * @param {any} $input Validate this type of object.
     * @returns {boolean} Returns true, if object is number, otherwise false.
     */
    public static IsDigit($input : any) : boolean {
        if (isNaN($input)) {
            return false;
        }

        if (typeof $input === SyntaxConstants.NUMBER) {
            return true;
        }

        if (!this.IsEmptyOrNull($input)) {
            if (this.IsString($input)) {
                return $input.match(/[a-z]/i) === null && !isNaN(parseInt($input, 10));
            }
        }
        return false;
    }

    /**
     * @param {any} $input Validate this type of object.
     * @returns {boolean} Returns true, if object is integer, otherwise false.
     */
    public static IsInteger($input : any) : boolean {
        return this.IsDigit($input) && parseFloat($input) === parseInt($input, 10) && !isNaN($input);
    }

    /**
     * @param {any} $input Validate this type of object.
     * @returns {boolean} Returns true, if object has floating point, otherwise false.
     */
    public static IsDouble($input : any) : boolean {
        return this.IsDigit($input) && !this.IsInteger($input);
    }

    /**
     * @param {any} $input Validate this type of object.
     * @returns {boolean} Returns true, if object is type of string, otherwise false.
     */
    public static IsString($input : any) : boolean {
        return typeof $input === SyntaxConstants.STRING || $input instanceof StringUtils;
    }

    /**
     * @param {any} $input Validate this type of object.
     * @returns {boolean} Returns true, if object is type of array, otherwise false.
     */
    public static IsNativeArray($input : any) : boolean {
        return Array.isArray($input);
    }

    /**
     * @param {any} $input Validate this type of object.
     * @returns {boolean} Returns true, if object is type of native array or instance of ArrayList, otherwise false.
     */
    public static IsArray($input : any) : boolean {
        return this.IsNativeArray($input) || $input instanceof ArrayList;
    }

    /**
     * @param {string} $input Validate if string is hex format.
     * @returns {boolean} Returns true, if string represents hexadecimal number, otherwise false.
     */
    public static IsHexadecimal($input : string) : boolean {
        if (!this.IsEmptyOrNull($input)) {
            if (($input + "").toLowerCase().indexOf("0x") === 0) {
                $input = ($input + "").slice(2);
            }
            if (($input + "").toLowerCase().indexOf("#") === 0) {
                $input = ($input + "").slice(1);
            }
        }
        return !this.IsEmptyOrNull($input) && !isNaN(parseInt($input, 16));
    }

    /**
     * @param {any} $input Validate this type of object.
     * @returns {boolean} Returns true, if $input is error, otherwise false.
     */
    public static IsError($input : any) : boolean {
        return this.IsObject($input) && ($input instanceof Error || this.toType($input) === SyntaxConstants.ERROR);
    }

    private static toType($input : any) : string {
        return Object.prototype.toString.call($input);
    }
}

globalThis.Io.Oidis.Commons.Utils.ObjectValidator = ObjectValidator;
