/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "../Primitives/BaseObject.js";
import { ObjectValidator } from "./ObjectValidator.js";

/**
 * Counters class provides static methods focused on procession of global counters.
 */
export class Counters extends BaseObject {

    private static register : any[] = [];

    /**
     * @param {string} $owner Specify counter's owner name.
     * @returns {number} Returns next increment of required counter
     */
    public static getNext($owner : string) : number {
        if (!Counters.register.hasOwnProperty($owner)) {
            Counters.register[$owner] = -1;
        }
        Counters.register[$owner] += 1;
        return Counters.register[$owner];
    }

    /**
     * Reset all registered counters or just one specified by its name.
     * @param {string} [$owner] Specify counter name, which should be reseated.
     * @returns {void}
     */
    public static Clear($owner? : string) : void {
        if (!ObjectValidator.IsEmptyOrNull($owner)) {
            if (Counters.register.hasOwnProperty($owner)) {
                Counters.register[$owner] = -1;
            }
        } else {
            let owner;
            for (owner in Counters.register) {
                if (Counters.register.hasOwnProperty(owner)) {
                    Counters.register[owner] = -1;
                }
            }
        }
    }
}
