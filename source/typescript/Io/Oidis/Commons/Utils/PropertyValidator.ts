/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IValidator } from "../Interfaces/IValidator.js";
import { BaseObject } from "../Primitives/BaseObject.js";

export class PropertyValidator extends BaseObject implements IValidator {
    public validate($value : any) : boolean {
        return true;
    }
}
