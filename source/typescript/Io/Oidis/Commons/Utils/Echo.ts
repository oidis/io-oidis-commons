/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IOHandlerType } from "../Enums/IOHandlerType.js";
import { NewLineType } from "../Enums/NewLineType.js";
import { IOHandler } from "../Interfaces/IOHandler.js";
import { IOHandlerFactory } from "../IOApi/IOHandlerFactory.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { Convert } from "./Convert.js";
import { ObjectValidator } from "./ObjectValidator.js";
import { StringUtils } from "./StringUtils.js";

/**
 * Echo class provides static API for handling of browser content.
 */
export class Echo extends BaseObject {
    private static stream : string;
    private static output : IOHandler;
    private static htmlEnabled : boolean;

    /**
     * Execute initialization manually, in case of that, it has not been initialized automatically.
     * @param {string|IOHandler} $targetIdOrHandler Set HTML element ID subscribed to echo or
     * specify IOHandler, which should be used for echo.
     * @param {boolean} $forceInit Reload output handler, if needed.
     * @returns {void}
     */
    public static Init($targetIdOrHandler? : string | IOHandler, $forceInit : boolean = false) : void {
        if (!ObjectValidator.IsSet(Echo.output) || $forceInit) {
            if (!ObjectValidator.IsSet($targetIdOrHandler)) {
                Echo.output = IOHandlerFactory.getHandler(IOHandlerType.HTML_ELEMENT, "Content");
            } else if (ObjectValidator.IsString($targetIdOrHandler)) {
                Echo.output = IOHandlerFactory.getHandler(IOHandlerType.HTML_ELEMENT, <string>$targetIdOrHandler);
            } else {
                Echo.output = <IOHandler>$targetIdOrHandler;
            }
            Echo.output.Init();
            Echo.htmlEnabled = Echo.getHandlerType() === IOHandlerType.HTML_ELEMENT;
        }
        if (!ObjectValidator.IsSet(Echo.stream) || $forceInit) {
            Echo.stream = "";
        }
    }

    /**
     * @returns {IOHandlerType} Returns currently used Output handler.
     */
    public static getHandlerType() : IOHandlerType {
        return IOHandlerFactory.getHandlerType(Echo.output);
    }

    /**
     * @param {string} $message Print message text to the screen without any modifications.
     * @param {boolean} [$clearBeforePrint=false] Clean up screen content before print if true,
     * otherwise append message to current content.
     * @returns {void}
     */
    public static Print($message : string, $clearBeforePrint : boolean = false) : void {
        if ($clearBeforePrint) {
            Echo.Clear();
        }
        Echo.save($message);
    }

    /**
     * @param {string|any} $messageOrObject Print text or any type of object to the screen.
     * String message has support of string formatting.
     * @param {...any} $parameters Open-ended args for search in message string, if arg is not typeof string it will
     * be converted to string.
     * @returns {void}
     */
    public static Printf($messageOrObject : string | any, ...$parameters : any[]) : void {
        if (ObjectValidator.IsSet($parameters) && ObjectValidator.IsString($messageOrObject)) {
            const args : any[] = [$messageOrObject];
            let index : number;
            for (index = 0; index < $parameters.length; index++) {
                args[index + 1] = Convert.ObjectToString($parameters[index], "", Echo.htmlEnabled);
            }
            Echo.Println(StringUtils.Format.apply(String, args));
        } else {
            Echo.Println(Convert.ObjectToString($messageOrObject, "", Echo.htmlEnabled));
        }
    }

    /**
     * @param {string} $message Print message to the screen at new line.
     * @returns {void}
     */
    public static Println($message : string) : void {
        Echo.Print((Echo.htmlEnabled ? NewLineType.HTML : NewLineType.LINUX) + $message);
    }

    /**
     * @param {string} $message Print message with escaped html to the screen at new line.
     * @returns {void}
     */
    public static PrintCode($message : string) : void {
        $message = StringUtils.Replace($message, "<", "&lt;");
        $message = StringUtils.Replace($message, ">", "&gt;");
        Echo.Println("<pre>" + $message + "</pre>");
    }

    /**
     * Clean up screen content.
     * @returns {void}
     */
    public static Clear() : void {
        Echo.Init();
        Echo.output.Clear();
    }

    /**
     * Clean up screen content and also content stream.
     * @returns {void}
     */
    public static ClearAll() : void {
        Echo.Clear();
        Echo.stream = "";
    }

    /**
     * @returns {string} Returns whole content stream printed by Echo, from last clean up.
     */
    public static getStream() : string {
        return Echo.stream;
    }

    /**
     * @param {Function} $handler Callback, which should be called in time on message print.
     * @returns {void}
     */
    public static setOnPrint($handler : ($message : string) => void) : void {
        Echo.Init();
        Echo.output.setOnPrint($handler);
    }

    private static save($message : string) : void {
        Echo.Init();
        Echo.stream += $message;
        Echo.output.Print($message);
    }
}
