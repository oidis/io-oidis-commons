/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { NewLineType } from "../Enums/NewLineType.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { ObjectValidator } from "./ObjectValidator.js";
import { StringUtils } from "./StringUtils.js";

/**
 * Property class provides static helper methods focused on correctly handled set and get of property.
 */
export class Property extends BaseObject {

    /**
     * @param {string} $property Handle this type of property.
     * @param {string} [$value] Set this type of value - null is not allowed.
     * @returns {string} Returns original or modified value.
     */
    public static String($property : string, $value? : string) : string {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            $property = $value;
        }
        if (!ObjectValidator.IsSet($property)) {
            $property = "";
        }
        return $property;
    }

    /**
     * @param {string} $property Handle this type of property.
     * @param {string} [$value] Set this type of value - null is allowed.
     * @returns {string} Returns original or modified value.
     */
    public static NullString($property : string, $value? : string) : string {
        if (ObjectValidator.IsSet($value)) {
            $property = $value;
        }
        if (!ObjectValidator.IsSet($property)) {
            $property = null;
        }
        return $property;
    }

    /**
     * @param {string} $property Handle this type of property.
     * @param {(string|boolean)} [$value] If is type of string, set this type of value - null is not allowed,
     * otherwise if is type of boolean choose type of output data.
     * @returns {string} If $value is boolean and is true returns raw data with visible eol characters, otherwise
     * returns original or modified value without visible eol characters.
     */
    public static EOLString($property : string, $value? : string | boolean) : string {
        if (!ObjectValidator.IsSet($property)) {
            $property = "";
        }
        if (!ObjectValidator.IsSet($value) || ObjectValidator.IsBoolean($value)) {
            if (ObjectValidator.IsSet($value) && $value) {
                return $property;
            } else {
                return StringUtils.Replace($property, NewLineType.DATABASE, NewLineType.WINDOWS);
            }
        }
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            $value = StringUtils.Replace(<string>$value, "\r\n", "\n");
            $value = StringUtils.Replace(<string>$value, "\n", NewLineType.DATABASE);
            $property = this.String($property, <string>$value);
        }
        return $property;
    }

    /**
     * @param {number} $property Handle this type of property.
     * @param {(string|number)} [$value] Set this type of value - null is not allowed. Float and string values are
     * converted to integer.
     * @param {number} [$min] Set minimum value to be set.
     * @param {number} [$max] Set maximum value to be set.
     * @returns {number} Returns original or modified value.
     */
    public static Integer($property : number, $value? : string | number, $min? : number, $max? : number) : number {
        if (!ObjectValidator.IsSet($property)) {
            $property = null;
        }
        if (ObjectValidator.IsString($value)) {
            $value = StringUtils.ToInteger(<string>$value);
        }
        if (ObjectValidator.IsDouble($value)) {
            $value = Math.floor(<number>$value);
        }
        if (ObjectValidator.IsInteger($value) &&
            (!ObjectValidator.IsSet($min) || $value >= $min) &&
            (!ObjectValidator.IsSet($max) || $value <= $max)) {
            $property = (<number>$value);
        }
        return $property;
    }

    /**
     * @param {number} $property Handle this type of property.
     * @param {(string|number)} [$value] Set this type of value - null is not allowed. Float and string values are
     * converted to integer. Allowed are only positive values.
     * @param {number} [$min] Set minimum value to be set. Allowed is limit bigger than or equal to 0.
     * @param {number} [$max] Set maximum value to be set.
     * @returns {number} Returns original or modified value.
     */
    public static PositiveInteger($property : number, $value? : string | number, $min? : number,
                                  $max? : number) : number {
        if (!ObjectValidator.IsSet($property)) {
            $property = null;
        }
        const value : number = this.Integer(null, $value, $min, $max);
        if (!ObjectValidator.IsEmptyOrNull(value) && value >= 0) {
            $property = value;
        }

        return $property;
    }

    /**
     * @param {boolean} $property Handle this type of property.
     * @param {(boolean|number|string)} [$value] Set this type of value. String, integer and double values are
     * converted to boolean.
     * @returns {boolean} Returns original or modified value.
     */
    public static Boolean($property : boolean, $value? : boolean | number | string) : boolean {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            let out : boolean = false;
            if (ObjectValidator.IsInteger($value) && $value === 1) {
                out = true;
            } else if (ObjectValidator.IsDouble($value) && Math.floor(<number>$value) === 1) {
                out = true;
            } else if (ObjectValidator.IsBoolean($value)) {
                out = (<boolean>$value);
            } else if (ObjectValidator.IsString($value)) {
                out = StringUtils.ToBoolean(<string>$value);
            }
            $property = out;
        }
        if (!ObjectValidator.IsSet($property)) {
            $property = null;
        }
        return $property;
    }

    /**
     * @param {number} $property Handle this type of property.
     * @param {(number|string)} [$value] Set this type of value. String values are converted to timestamp.
     * Allowed is also simplified relative format of time.
     * @returns {number} Returns original or modified value in timestamp format.
     */
    public static Time($property : number, $value? : number | string) : number {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            if (ObjectValidator.IsDigit($value) && $value !== 0) {
                $property = <number>$value;
            } else if (ObjectValidator.IsString($value)) {
                let time : number = new Date(<string>$value).getTime();

                if (!ObjectValidator.IsDigit(time)) {
                    $value = StringUtils.ToLowerCase(<string>$value);
                    $value = StringUtils.Remove(<string>$value, " ");
                    let plus : boolean = false;
                    let minus : boolean = false;
                    if (StringUtils.Contains(<string>$value, "+")) {
                        $value = StringUtils.Remove(<string>$value, "+");
                        plus = true;
                    }
                    if (StringUtils.Contains(<string>$value, "-")) {
                        $value = StringUtils.Remove(<string>$value, "-");
                        minus = true;
                    }
                    let value : number;
                    if (StringUtils.Contains(<string>$value, "min", "minute", "mins", "minutes")) {
                        value = StringUtils.ToInteger(StringUtils.Substring(<string>$value,
                            0, StringUtils.IndexOf(<string>$value, "min"))) * 60 * 1000;
                    } else if (StringUtils.Contains(<string>$value, "h", "hour", "hours")) {
                        value = StringUtils.ToInteger(StringUtils.Substring(<string>$value,
                            0, StringUtils.IndexOf(<string>$value, "h"))) * 60 * 60 * 1000;
                    } else if (StringUtils.Contains(<string>$value, "d", "day", "days")) {
                        value = StringUtils.ToInteger(StringUtils.Substring(<string>$value,
                            0, StringUtils.IndexOf(<string>$value, "d"))) * 24 * 60 * 60 * 1000;
                    } else if (StringUtils.Contains(<string>$value, "w", "week", "weeks")) {
                        value = StringUtils.ToInteger(StringUtils.Substring(<string>$value,
                            0, StringUtils.IndexOf(<string>$value, "w"))) * 7 * 24 * 60 * 60 * 1000;
                    } else if (StringUtils.Contains(<string>$value, "m", "month", "months")) {
                        value = StringUtils.ToInteger(StringUtils.Substring(<string>$value,
                            0, StringUtils.IndexOf(<string>$value, "m"))) * 31 * 24 * 60 * 60 * 1000;
                    } else if (StringUtils.Contains(<string>$value, "y", "year", "years")) {
                        value = StringUtils.ToInteger(StringUtils.Substring(<string>$value,
                            0, StringUtils.IndexOf(<string>$value, "y"))) * 365 * 24 * 60 * 60 * 1000;
                    } else if (StringUtils.Contains(<string>$value, "s", "sec", "second", "secs", "seconds")) {
                        value = StringUtils.ToInteger(StringUtils.Substring(<string>$value,
                            0, StringUtils.IndexOf(<string>$value, "s"))) * 1000;
                    }
                    const date : Date = new Date();
                    if (plus) {
                        date.setTime(date.getTime() + value);
                        time = date.getTime();
                    } else if (minus) {
                        date.setTime(date.getTime() - value);
                        time = date.getTime();
                    } else {
                        time = value;
                    }
                }

                if (!ObjectValidator.IsDigit(time)) {
                    throw new Error("\"" + $value + "\" is not supported format for Time property.");
                } else {
                    $property = time;
                }
            }
        }
        if (!ObjectValidator.IsSet($property)) {
            $property = null;
        }
        return $property;
    }

    /**
     * @param {number} $property Handle this type of property.
     * @param {(number|string)} [$value] Set this type of value. String values are converted to integer.
     * Allowed is string format with expected size unit.
     * @returns {number} Returns original or modified value as integer.
     */
    public static Size($property : number, $value? : number | string) : number {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            if (ObjectValidator.IsString($value)) {
                let value : number;
                if (StringUtils.ContainsIgnoreCase(<string>$value, "k")) {
                    value = StringUtils.ToInteger(StringUtils.Substring(<string>$value,
                        0, StringUtils.IndexOf(StringUtils.ToLowerCase(<string>$value), "k"))) * 1024;
                } else if (StringUtils.Contains(<string>$value, "M")) {
                    value = StringUtils.ToInteger(StringUtils.Substring(<string>$value,
                        0, StringUtils.IndexOf(<string>$value, "M"))) * 1024 * 1024;
                } else if (StringUtils.Contains(<string>$value, "G")) {
                    value = StringUtils.ToInteger(StringUtils.Substring(<string>$value,
                        0, StringUtils.IndexOf(<string>$value, "G"))) * 1024 * 1024 * 1024;
                } else if (StringUtils.Contains(<string>$value, "T")) {
                    value = StringUtils.ToInteger(StringUtils.Substring(<string>$value,
                        0, StringUtils.IndexOf(<string>$value, "T"))) * 1024 * 1024 * 1024 * 1024;
                } else if (StringUtils.ContainsIgnoreCase(<string>$value, "b")) {
                    value = StringUtils.ToInteger(StringUtils.Substring(<string>$value,
                        0, StringUtils.IndexOf(StringUtils.ToLowerCase(<string>$value), "b")));
                } else {
                    value = StringUtils.ToInteger(<string>$value);
                }
                $property = value >= 0 ? value : 0;
            } else {
                if (ObjectValidator.IsDigit($value) && $value < 0) {
                    $value = 0;
                }
                $property = this.Integer($property, <number>$value, 0);
            }
        }
        if (!ObjectValidator.IsSet($property)) {
            $property = 0;
        }
        return $property;
    }

    /**
     * @param {any} $property Handle this type of property.
     * @param {any} [$value] Set this type of value if value is member of property class name.
     * @param {BaseEnum} [$propertyClassName] Set this type of value - null is not allowed.
     * @param {any} [$defaultValue] Set this type of value if property has not been set.
     * @returns {any} Returns default or modified value.
     */
    public static EnumType($property : any, $value? : any, $propertyClassName? : any, $defaultValue? : any) : any {
        if (ObjectValidator.IsSet($value) && (!ObjectValidator.IsSet($propertyClassName) ||
            (ObjectValidator.IsSet($propertyClassName) && ObjectValidator.IsFunction($propertyClassName.Contains) &&
                $propertyClassName.Contains($value)))) {
            $property = $value;
            if (ObjectValidator.IsEmptyOrNull($property) && ObjectValidator.IsSet($defaultValue)) {
                $property = $defaultValue;
            }
        }
        if (!ObjectValidator.IsSet($property) && ObjectValidator.IsSet($defaultValue)) {
            $property = $defaultValue;
        }

        return $property;
    }
}
