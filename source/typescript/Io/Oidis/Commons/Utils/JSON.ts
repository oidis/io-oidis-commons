/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "../Primitives/BaseObject.js";
import { ObjectValidator } from "./ObjectValidator.js";
import { StringUtils } from "./StringUtils.js";

/**
 * JSONFallback class provides serialization and parsing of JavaScript objects to and from string in JSON format.
 * THIS class is used only in case of that standard JSON implementation is missing in runtime environment eg. IE7<.
 * WARNING: this class is not fully equivalent to standard JSON implementation - mainly parser is focused only on
 * needs of Oidis Framework.
 */
export class JSONFallback extends BaseObject {

    /**
     * @param {any} $input Input object for conversion.
     * @param {Function} [$replacer] Specify custom handler for object parameter.
     * @param {string | number} [$space] Specify white space into the output JSON string for readability purposes.
     * @returns {string} Returns input object converted to string in JSON format.
     */
    public static stringify($input : any, $replacer? : ($key : string, $value : any) => any, $space? : string | number) : string {
        const format : ($value : string) => string = ($value : string) : string => {
            if (ObjectValidator.IsSet($space)) {
                if (ObjectValidator.IsString($space)) {
                    $value = "\n" + $space + $value + " ";
                } else if (ObjectValidator.IsInteger($space)) {
                    if ($space > 10) {
                        $space = 10;
                    }
                    if ($space > 0) {
                        $value = "\n" + StringUtils.Space(<number>$space, false) + $value + " ";
                    }
                }
            }
            return $value;
        };
        const parser : ($input : any) => string = ($input : any) : string => {
            /* istanbul ignore else : default condition should not be met */
            if (ObjectValidator.IsNativeArray($input)) {
                let parsedArray : string = "";
                let itemIndex : number;
                for (itemIndex = 0; itemIndex < $input.length; itemIndex++) {
                    const arrayValue : string = parser($input[itemIndex]);
                    if (!ObjectValidator.IsEmptyOrNull(arrayValue) && !ObjectValidator.IsEmptyOrNull(parsedArray)) {
                        parsedArray += ",";
                    }
                    parsedArray += arrayValue;
                }
                return "[" + parsedArray + "]";
            } else if ($input instanceof Date) {
                return "\"" + (<Date>$input).toISOString() + "\"";
            } else if (ObjectValidator.IsObject($input)) {
                return JSONFallback.stringify($input);
            } else if (ObjectValidator.IsString($input)) {
                return "\"" + $input + "\"";
            } else if (ObjectValidator.IsBoolean($input)) {
                return $input === true ? "true" : "false";
            } else if (ObjectValidator.IsDigit($input)) {
                return $input.toString();
            } else if (ObjectValidator.IsFunction($input)) {
                return;
            } else if (ObjectValidator.IsEmptyOrNull($input)) {
                return "null";
            } else {
                return $input.toString();
            }
        };
        if (ObjectValidator.IsObject($input)) {
            let output : string = "";
            let key : string;
            for (key in $input) {
                /* istanbul ignore else : bulletproof condition */
                if ($input.hasOwnProperty(key)) {
                    if (!ObjectValidator.IsFunction($input[key])) {
                        let value : any = $input[key];
                        if (ObjectValidator.IsSet($replacer)) {
                            if (ObjectValidator.IsFunction($replacer)) {
                                value = $replacer(key, value);
                            }
                        }
                        if (ObjectValidator.IsSet(value)) {
                            if (!ObjectValidator.IsEmptyOrNull(output)) {
                                output += ",";
                            }
                            output += format("\"" + key + "\":") + parser(value);
                        }
                    }
                }
            }
            output = "{" + output;
            if (ObjectValidator.IsSet($space) &&
                (ObjectValidator.IsString($space) || ObjectValidator.IsInteger($space) && $space > 0)) {
                output += "\n";
            }
            return output + "}";
        }
        if (!ObjectValidator.IsSet($input)) {
            return;
        }
        return parser($input);
    }

    /**
     * @param {string} $input Input string in JSON format for conversion.
     * @returns {any} Returns JavaScript object converted from string in JSON format.
     */
    public static parse($input : string) : any {
        const parseValue : ($input : string) => any = ($input : string) : any => {
            let value : any = null;
            if (StringUtils.StartsWith($input, "\"")) {
                value = StringUtils.Substring($input, 1, StringUtils.Length($input) - 1);
            } else if ($input === "true" || $input === "false") {
                value = $input === "true";
            } else if (StringUtils.StartsWith($input, "[")) {
                value = [];
                const items : string[] = StringUtils.Split(StringUtils.Substring($input, 1, StringUtils.Length($input)), ",");
                let itemIndex : number;
                for (itemIndex = 0; itemIndex < items.length; itemIndex++) {
                    value[itemIndex] = parseValue(items[itemIndex]);
                }
            } else if (StringUtils.StartsWith($input, "{")) {
                value = JSONFallback.parse($input + "}");
            } else if (ObjectValidator.IsDouble($input)) {
                value = StringUtils.ToDouble($input);
            } else if (ObjectValidator.IsInteger($input)) {
                value = StringUtils.ToInteger($input);
            }
            return value;
        };
        const parser : ($input : string) => any = ($input : string) : any => {
            if (StringUtils.StartsWith($input, "{") && StringUtils.EndsWith($input, "}")) {
                const output : any = {};
                const regExp : RegExp = new RegExp("\"(.*?)\":(.*?)[,|}]", "gm");
                let regResult : RegExpExecArray = regExp.exec($input);
                while (regResult) {
                    const reindexValue : ($value : string, $startChar : string, $endChar : string) => string =
                        ($value : string, $startChar : string, $endChar : string) : string => {
                            if (StringUtils.StartsWith($value, $startChar)) {
                                const startIndex : number = regExp.lastIndex - StringUtils.Length($value);
                                let endIndex : number = StringUtils.IndexOf($input, $endChar + "}", true, startIndex);
                                /* istanbul ignore else : not fully implemented */
                                if (endIndex === -1) {
                                    endIndex = StringUtils.IndexOf($input, $endChar + ",\"", true, startIndex);
                                }
                                /* istanbul ignore else : not fully implemented */
                                if (endIndex !== -1) {
                                    $value = $startChar + StringUtils.Substring($input, startIndex, endIndex);
                                }
                            }
                            return $value;
                        };
                    output[regResult[1]] = parseValue(reindexValue(reindexValue(regResult[2], "[", "]"), "{", "}"));
                    regResult = regExp.exec($input);
                }
                return output;
            } else {
                throw new Error("Unexpected token in JSON");
            }
        };
        return parser($input);
    }
}

const __jsonClassName : string = "JSON";
/* istanbul ignore next : create global JSON object just in case of that standard implementation is missing */
if (!(__jsonClassName in window)) {
    window[__jsonClassName] = JSONFallback;
}
