/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { FileSystemItemType } from "../Enums/FileSystemItemType.js";
import { BaseEnum } from "../Primitives/BaseEnum.js";
import { ObjectValidator } from "./ObjectValidator.js";

export class FileSystemFilter extends BaseEnum {
    public static ALL : string = "*";
    public static FILESYSTEM_GROUPS : string = (<any>FileSystemFilter).Create(
        FileSystemItemType.MY_COMPUTER,
        FileSystemItemType.FAVORITES,
        FileSystemItemType.NETWORK,
        FileSystemItemType.RECENT,
        FileSystemItemType.PINNED
    );
    public static DIRECTORIES : string = (<any>FileSystemFilter).Create(
        FileSystemFilter.FILESYSTEM_GROUPS,
        FileSystemItemType.DRIVE,
        FileSystemItemType.DIRECTORY
    );
    public static HIDDEN_DIRECTORIES : string = (<any>FileSystemFilter).Create(
        FileSystemFilter.DIRECTORIES,
        FileSystemItemType.HIDDEN
    );
    public static HIDDEN_ALL : string = (<any>FileSystemFilter).Create(
        FileSystemFilter.HIDDEN_DIRECTORIES,
        FileSystemItemType.FILE,
        FileSystemItemType.LINK
    );

    /**
     * @param {...(string|FileSystemItemType|FileSystemFilter)} $options Specify filter options,
     * which should be used for filter creation
     * @returns {string} Returns filter in string format composed from filter options.
     */
    public static Create(...$options : Array<string | FileSystemItemType | FileSystemFilter>) : string {
        if (ObjectValidator.IsEmptyOrNull($options)) {
            return "";
        }
        return $options.join("|");
    }
}
