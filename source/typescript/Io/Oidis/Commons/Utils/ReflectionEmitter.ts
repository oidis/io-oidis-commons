/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export class ReflectionEmitter {
    private static classNamespace : string = "";
    private static className : string = "";

    public static ClassName() : string {
        return this.classNamespace + "." + this.className;
    }

    public static NamespaceName() : string {
        return this.classNamespace;
    }

    public static ClassNameWithoutNamespace() : string {
        return this.className;
    }

    public static async Register($namespace : string, $class? : any) : Promise<void> {
        const target : any = this.getTargetObject($namespace, $class);
        if (this.IsEmptyOrNull($class)) {
            try {
                const module : any = await import(target.name);
                Object.entries(module).forEach(([$key, $value]) => target.instance[$key] = $value);
            } catch (ex) {
                console.log(ex); // eslint-disable-line no-console
            }
        } else {
            target.instance[target.name] = $class;
        }
    }

    public static RegisterSync($namespace : string, $class : any) : boolean {
        if (!this.IsEmptyOrNull($class)) {
            const target : any = this.getTargetObject($namespace, $class);
            target.instance[target.name] = $class;
            return true;
        }
        return false;
    }

    private static getTargetObject($namespace : string, $class : any) : any {
        const parts : string[] = $namespace.split(".");
        const getObject : any = ($name : string, $parent : any) : any => {
            if (!$parent.hasOwnProperty($name)) {
                $parent[$name] = {};
            }
            return $parent[$name];
        };
        let instance : any = globalThis;
        for (let index : number = 0; index < parts.length - 1; index++) {
            instance = getObject(parts[index], instance);
        }
        let name : string;
        if (!this.IsEmptyOrNull($class)) {
            name = parts[parts.length - 1];
        } else {
            name = "./../../../../" + parts.join("/") + ".js";
        }
        return {
            instance,
            name
        };
    }

    private static IsEmptyOrNull($input : any) : boolean {
        if (["undefined", undefined].indexOf($input) !== -1) {
            return true;
        }
        return $input === null || $input === "";
    }
}

globalThis.RegisterClass = ($namespace : string, $class : any) : boolean => {
    return ReflectionEmitter.RegisterSync($namespace, $class);
};

if (globalThis.Io === undefined) {
    globalThis.Io = {
        Oidis: {
            Commons: {
                Events: {ThreadPool: {}, EventsManager: {}},
                Utils : {Reflection: {}, StringUtils: {}, ObjectValidator: {}, LogIt: {}}
            }
        }
    };
}
