/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { NewLineType } from "../Enums/NewLineType.js";
import { SpecialCharacters } from "../Enums/SpecialCharacters.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { Convert } from "./Convert.js";
import { ObjectValidator } from "./ObjectValidator.js";

/**
 * String class provides static methods focused on string validations and handling.
 */
export class StringUtils extends BaseObject {

    private static crcTable : number[] = [
        0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA, 0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3, 0x0EDB8832, 0x79DCB8A4,
        0xE0D5E91E, 0x97D2D988, 0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91, 0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
        0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7, 0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC, 0x14015C4F, 0x63066CD9,
        0xFA0F3D63, 0x8D080DF5, 0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172, 0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
        0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940, 0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59, 0x26D930AC, 0x51DE003A,
        0xC8D75180, 0xBFD06116, 0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F, 0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
        0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D, 0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A, 0x71B18589, 0x06B6B51F,
        0x9FBFE4A5, 0xE8B8D433, 0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818, 0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
        0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E, 0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457, 0x65B0D9C6, 0x12B7E950,
        0x8BBEB8EA, 0xFCB9887C, 0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65, 0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
        0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB, 0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0, 0x44042D73, 0x33031DE5,
        0xAA0A4C5F, 0xDD0D7CC9, 0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086, 0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
        0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4, 0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD, 0xEDB88320, 0x9ABFB3B6,
        0x03B6E20C, 0x74B1D29A, 0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683, 0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
        0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1, 0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE, 0xF762575D, 0x806567CB,
        0x196C3671, 0x6E6B06E7, 0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC, 0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
        0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252, 0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B, 0xD80D2BDA, 0xAF0A1B4C,
        0x36034AF6, 0x41047A60, 0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79, 0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
        0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F, 0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04, 0xC2D7FFA7, 0xB5D0CF31,
        0x2CD99E8B, 0x5BDEAE1D, 0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A, 0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
        0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38, 0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21, 0x86D3D2D4, 0xF1D4E242,
        0x68DDB3F8, 0x1FDA836E, 0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777, 0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
        0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45, 0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2, 0xA7672661, 0xD06016F7,
        0x4969474D, 0x3E6E77DB, 0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0, 0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
        0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6, 0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF, 0xB3667A2E, 0xC4614AB8,
        0x5D681B02, 0x2A6F2B94, 0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
    ];

    /**
     * @param {boolean} [$htmlTag=true] Choose, if new line will be in HTML format or in plain text.
     * @returns {string} Returns new line in selected format.
     */
    public static NewLine($htmlTag : boolean = true) : string {
        if ($htmlTag) {
            return NewLineType.HTML;
        } else {
            return NewLineType.WINDOWS;
        }
    }

    /**
     * @param {number} [$count=1] Set number or returned spaces.
     * @param {boolean} [$htmlTag=true] Choose, if space will be in HTML format or in plain text.
     * @returns {string} Returns escaped space char.
     */
    public static Space($count : number = 1, $htmlTag : boolean = true) : string {
        return StringUtils.repeatedAddChar($count, $htmlTag ? SpecialCharacters.WHITESPACE : " ");
    }

    /**
     * @param {number} [$count=1] Set number or returned tabs.
     * @param {boolean} [$htmlTag=true] Choose, if tab will be in HTML format or in plain text.
     * @returns {string} Returns escaped tab char.
     */
    public static Tab($count : number = 1, $htmlTag : boolean = true) : string {
        return StringUtils.repeatedAddChar($count, $htmlTag ? SpecialCharacters.TAB : "    ");
    }

    /**
     * @param {string} $input Input string for calculation of length.
     * @returns {number} Returns 0, if $input is empty or null, otherwise returns string length.
     */
    public static Length($input : string) : number {
        if ($input !== null) {
            return parseInt(($input + "").length.toString(), 10);
        } else {
            return 0;
        }
    }

    /**
     * @param {string} $input Input string for validation.
     * @returns {boolean} Returns 0, if $input is empty, null or undefined, otherwise returns true.
     */
    public static IsEmpty($input : string) : boolean {
        return $input === null || $input === "" || !ObjectValidator.IsSet($input);
    }

    /**
     * @param {string} $input Input string for conversion.
     * @returns {string} Returns input string in lower case format.
     */
    public static ToLowerCase($input : string) : string {
        if (!StringUtils.IsEmpty($input)) {
            return ($input + "").toLowerCase();
        } else {
            return "";
        }
    }

    /**
     * @param {string} $input Input string for conversion.
     * @returns {string} Returns input string in upper case format.
     */
    public static ToUpperCase($input : string) : string {
        if (!StringUtils.IsEmpty($input)) {
            return ($input + "").toUpperCase();
        } else {
            return "";
        }
    }

    /**
     * @param {string} $input Input string for validation.
     * @param {string} $searchString String or character witch will be searched.
     * @param {boolean} [$fromStartPosition=true] If true searching is done from start position,
     * if false searching is done from end to start position.
     * @param {number} [$offset=0] Set start position for search.
     * @returns {number} Returns index of searched string value in input string or -1 if value has not been found.
     * false.
     */
    public static IndexOf($input : string, $searchString : string, $fromStartPosition : boolean = true,
                          $offset : number = 0) : number {
        if ($fromStartPosition) {
            if (StringUtils.IsEmpty($searchString)) {
                return 0;
            } else {
                return ($input + "").indexOf($searchString, $offset);
            }
        } else {
            if (StringUtils.IsEmpty($searchString)) {
                return StringUtils.Length($input);
            } else {
                return ($input + "").lastIndexOf($searchString, StringUtils.Length($input) - $offset);
            }
        }
    }

    /**
     * @param {string} $input Input string for search.
     * @param {number} $index Index of desired character.
     * @returns {string} Returns desired character.
     */
    public static getCharacterAt($input : string, $index : number) : string {
        if (StringUtils.IsEmpty($input)) {
            return "";
        }
        return ($input + "").charAt($index);
    }

    /**
     * @param {string} $input Input string for search.
     * @param {number} $index Index of desired character.
     * @returns {number} Returns Unicode value of desired character, if index exist, otherwise null.
     */
    public static getCodeAt($input : string, $index : number) : number {
        if (StringUtils.IsEmpty($input)) {
            return null;
        }
        const code : number = ($input + "").charCodeAt($index);
        if (!ObjectValidator.IsDigit(code)) {
            return null;
        } else {
            return code;
        }
    }

    /**
     * @param {string} $input Input string for calculation.
     * @returns {number} Returns crc32 checksum useful for validation of string manipulation.
     */
    public static getCrc($input : string) : number {
        let output : number = 0;

        output = output ^ (-1);
        let index : number;
        const length : number = StringUtils.Length($input);
        for (index = 0; index < length; index++) {
            output = (output >>> 8) ^ StringUtils.crcTable[((output ^ (<any>$input).charCodeAt(index)) & 0xFF)];
        }
        output = output ^ (-1);

        return output;
    }

    /**
     * @param {string} $input String value for, which should be calculated hash value.
     * @returns {string} Returns irreversible hash of input string useful for e.g. password check.
     */
    public static getSha1($input : string) : string {
       
        const rotateLeft : any = ($input : number, $bitsCount : number) : any => {
            return ($input << $bitsCount) | ($input >>> (32 - $bitsCount));
        };

        const length : number = StringUtils.Length($input);
        const word : number[] = [80];
        const wordArray : number[] = [];
        let wordArraySize : number = 0;
        let wordIndex : number;
        let H0 : number = 0x67452301;
        let H1 : number = 0xEFCDAB89;
        let H2 : number = 0x98BADCFE;
        let H3 : number = 0x10325476;
        let H4 : number = 0xC3D2E1F0;
        let A : number;
        let B : number;
        let C : number;
        let D : number;
        let E : number;
        let temp : number;

        for (wordIndex = 0; wordIndex < length - 3; wordIndex += 4) {
            wordArray[wordArraySize] = StringUtils.getCodeAt($input, wordIndex) << 24 |
                StringUtils.getCodeAt($input, wordIndex + 1) << 16 |
                StringUtils.getCodeAt($input, wordIndex + 2) << 8 |
                StringUtils.getCodeAt($input, wordIndex + 3);
            wordArraySize++;
        }

        switch (length % 4) {
        case 0:
            wordIndex = 0x080000000;
            break;
        case 1:
            wordIndex = StringUtils.getCodeAt($input, length - 1) << 24 |
                0x0800000;
            break;
        case 2:
            wordIndex = StringUtils.getCodeAt($input, length - 2) << 24 |
                StringUtils.getCodeAt($input, length - 1) << 16 |
                0x08000;
            break;
        case 3:
            wordIndex = StringUtils.getCodeAt($input, length - 3) << 24 |
                StringUtils.getCodeAt($input, length - 2) << 16 |
                StringUtils.getCodeAt($input, length - 1) << 8 |
                0x80;
            break;
            /* istanbul ignore next: unreachable code, because length is integer and all cases are covered */
        default:
            break;
        }

        wordArray[wordArraySize] = wordIndex;
        wordArraySize++;

        while ((wordArraySize % 16) !== 14) {
            wordArray[wordArraySize] = 0;
            wordArraySize++;
        }

        wordArray[wordArraySize] = length >>> 29;
        wordArraySize++;
        wordArray[wordArraySize] = (length << 3) & 0x0ffffffff;
        wordArraySize++;

        let blockIndex : number;
        for (blockIndex = 0; blockIndex < wordArraySize; blockIndex += 16) {
            for (wordIndex = 0; wordIndex < 16; wordIndex++) {
                word[wordIndex] = wordArray[blockIndex + wordIndex];
            }
            for (wordIndex = 16; wordIndex <= 79; wordIndex++) {
                word[wordIndex] =
                    rotateLeft(word[wordIndex - 3] ^ word[wordIndex - 8] ^ word[wordIndex - 14] ^ word[wordIndex - 16], 1);
            }

            A = H0;
            B = H1;
            C = H2;
            D = H3;
            E = H4;

            for (wordIndex = 0; wordIndex <= 19; wordIndex++) {
                temp = (rotateLeft(A, 5) + ((B & C) | (~B & D)) + E + word[wordIndex] + 0x5A827999) & 0x0ffffffff;
                E = D;
                D = C;
                C = rotateLeft(B, 30);
                B = A;
                A = temp;
            }

            for (wordIndex = 20; wordIndex <= 39; wordIndex++) {
                temp = (rotateLeft(A, 5) + (B ^ C ^ D) + E + word[wordIndex] + 0x6ED9EBA1) & 0x0ffffffff;
                E = D;
                D = C;
                C = rotateLeft(B, 30);
                B = A;
                A = temp;
            }

            for (wordIndex = 40; wordIndex <= 59; wordIndex++) {
                temp = (rotateLeft(A, 5) + ((B & C) | (B & D) | (C & D)) + E + word[wordIndex] + 0x8F1BBCDC) & 0x0ffffffff;
                E = D;
                D = C;
                C = rotateLeft(B, 30);
                B = A;
                A = temp;
            }

            for (wordIndex = 60; wordIndex <= 79; wordIndex++) {
                temp = (rotateLeft(A, 5) + (B ^ C ^ D) + E + word[wordIndex] + 0xCA62C1D6) & 0x0ffffffff;
                E = D;
                D = C;
                C = rotateLeft(B, 30);
                B = A;
                A = temp;
            }

            H0 = (H0 + A) & 0x0ffffffff;
            H1 = (H1 + B) & 0x0ffffffff;
            H2 = (H2 + C) & 0x0ffffffff;
            H3 = (H3 + D) & 0x0ffffffff;
            H4 = (H4 + E) & 0x0ffffffff;
        }

        return (Convert.HexadecimalToString(H0) +
            Convert.HexadecimalToString(H1) +
            Convert.HexadecimalToString(H2) +
            Convert.HexadecimalToString(H3) +
            Convert.HexadecimalToString(H4)).toLowerCase();
    }

    /**
     * @param {string} $input Input string for validation.
     * @param {...string} $searchString Open-ended string args for search in input string.
     * @returns {boolean} Returns true, if one of searched strings has been found in input string, otherwise returns
     * false.
     */
    public static Contains($input : string, ...$searchString : string[]) : boolean {
        if (ObjectValidator.IsSet($input) && !StringUtils.IsEmpty($input)) {
            let index : number;
            for (index = 0; index < $searchString.length; index++) {
                if (!StringUtils.IsEmpty($searchString[index]) && ($input + "").length >= ($searchString[index] + "").length &&
                    ($input + "").indexOf($searchString[index]) !== -1) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param {string} $input Input string for validation.
     * @param {...string} $searchString Open-ended string args for search in input string.
     * @returns {boolean} Returns true, if one of searched strings has been found in input string, otherwise returns
     * false. Does not matter on case sensitive character of input or searched strings.
     */
    public static ContainsIgnoreCase($input : string, ...$searchString : string[]) : boolean {
        if (!StringUtils.IsEmpty($input)) {
            $input = StringUtils.ToLowerCase($input);
            let index : number;
            const length : number = $searchString.length;
            for (index = 0; index < length; index++) {
                if (!StringUtils.IsEmpty($searchString[index]) && ($input + "").length >= ($searchString[index] + "").length &&
                    ($input + "").indexOf(StringUtils.ToLowerCase($searchString[index])) !== -1) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param {string} $input Input string for validation.
     * @param {string} $searchFor String or character witch will be searched.
     * @returns {number} Returns count of searched value in input string.
     */
    public static OccurrenceCount($input : string, $searchFor : string) : number {
        $input += "";
        if (StringUtils.Length($searchFor) <= 0) {
            return 0;
        }

        let count : number = 0;
        let position : number = 0;
        const step : number = StringUtils.Length($searchFor);

        while (true) {
            position = StringUtils.IndexOf($input, $searchFor, true, position);
            if (position >= 0) {
                count++;
                position += step;
            } else {
                break;
            }
        }
        return count;
    }

    /**
     * @param {string} $input Input string for validation.
     * @param {string} $searchString String or character witch will be validated.
     * @returns {boolean} Returns true if input string starts with searched value otherwise false.
     */
    public static StartsWith($input : string, $searchString : string) : boolean {
        return StringUtils.Contains($input, $searchString) && StringUtils.IndexOf($input, $searchString) === 0;
    }

    /**
     * @param {string} $input Input string for validation.
     * @param {string} $searchString String or character witch will be validated.
     * @returns {boolean} Returns true if input string ends with searched value otherwise false.
     */
    public static EndsWith($input : string, $searchString : string) : boolean {
        return StringUtils.Contains($input, $searchString) &&
            StringUtils.IndexOf($input, $searchString, false) === (StringUtils.Length($input) - StringUtils.Length($searchString));
    }

    /**
     * @param {string|string[]} $pattern Validation pattern for value match with wildcard level of freedom
     * @param {string} $value Actual string value for match with pattern
     * @returns {boolean} Returns true, if pattern has been matched, otherwise returns false.
     */
    public static PatternMatched($pattern : string | string[], $value : string) : boolean {
        if ($value === $pattern) {
            return true;
        }

        const validate : any = ($pattern : string, $value : string) : boolean => {
            if ($value === $pattern) {
                return true;
            }

            if (StringUtils.Contains($pattern, "*")) {
                const parts : string[] = StringUtils.Split($pattern, "*");
                const partsLength : number = <number>parts.length;
                const lastPart : number = partsLength - 1;
                let partIndex : number;
                for (partIndex = 0; partIndex < partsLength; partIndex++) {
                    if (!StringUtils.IsEmpty(parts[partIndex])) {
                        if (partIndex === 0 && !StringUtils.StartsWith($value, parts[partIndex])) {
                            return false;
                        }

                        if (partIndex === lastPart && !StringUtils.EndsWith($value, parts[partIndex])) {
                            return false;
                        }

                        const searchIndex : number = StringUtils.IndexOf($value, parts[partIndex]);
                        if (searchIndex === -1) {
                            return false;
                        }

                        if (searchIndex === 0) {
                            if (partIndex === lastPart - 1 && StringUtils.IsEmpty(parts[partIndex + 1]) || partIndex < lastPart - 1) {
                                $value = StringUtils.Substring($value, searchIndex + StringUtils.Length(parts[partIndex]),
                                    StringUtils.Length($value)
                                );
                            }
                        } else {
                            /* istanbul ignore else : all uses cases are covered by previous conditions */
                            if (partIndex === 1 && StringUtils.IsEmpty(parts[0]) || partIndex > 1) {
                                $value = StringUtils.Substring($value, searchIndex + StringUtils.Length(parts[partIndex]),
                                    StringUtils.Length($value)
                                );
                            }
                        }
                    }
                }
            } else {
                return false;
            }

            return true;
        };
        if (ObjectValidator.IsString($pattern)) {
            return validate($pattern, $value);
        }

        let patterns : string[] = [];
        if (!ObjectValidator.IsEmptyOrNull($pattern)) {
            patterns = <string[]>$pattern;
        }
        let output : boolean = false;
        patterns.forEach(($pattern : string) : void => {
            if (!output && validate($pattern, $value)) {
                output = true;
            }
        });
        return output;
    }

    /**
     * @param {string} $input Input string for search in.
     * @param {number} $start Start index of substring.
     * @param {number} [$end] End index of substring, if is not equal to end of input string.
     * @returns {string} Returns substring, if start and end index has been found, otherwise null.
     */
    public static Substring($input : string, $start : number, $end? : number) : string {
        return ($input + "").substring($start, ObjectValidator.IsSet($end) ? $end : StringUtils.Length($input));
    }

    /**
     * @param {string} $input Input string for replace.
     * @param {string} $searchString String or character witch should be replaced.
     * @param {string} $replaceBy String or character witch will be used for replacement.
     * @returns {string} Returns input string with replaced values.
     */
    public static Replace($input : string, $searchString : string, $replaceBy : string) : string {
        return ($input + "").replace(
            // eslint-disable-next-line no-useless-escape
            new RegExp(($searchString + "").replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"), "g"),
            $replaceBy
        );
    }

    /**
     * @param {string} $input Input string for remove.
     * @param {...string[]} $searchString Open-ended string or character args witch should be removed.
     * @returns {string} Returns input string without searched string or character.
     */
    public static Remove($input : string, ...$searchString : string[]) : string {
        if ($searchString.length < 1) {
            return $input;
        }
        if ($searchString.length === 1) {
            return StringUtils.Replace($input, $searchString[0], "");
        }
        let output : string = $input;
        let searchIndex : number;
        for (searchIndex = 0; searchIndex < $searchString.length; searchIndex++) {
            output = StringUtils.Replace(output, $searchString[searchIndex], "");
        }
        return output;
    }

    /**
     * @param {string} $input Input string for split.
     * @param {...string} $splitBy Open-ended string args for search in input string.
     * @returns {string[]} Returns array of strings as result of split.
     */
    public static Split($input : string, ...$splitBy : string[]) : string[] {
        const output : string[] = [];

        if (<number>$splitBy.length !== 0) {
            let outputSize : number = 0;
            let buffer : string[][] = [];
            let bufferSize : number = 0;
            let tmp : string[] = [$input];

            buffer[bufferSize] = tmp;
            bufferSize++;

            let splitIndex : number;
            for (splitIndex = 0; splitIndex < $splitBy.length; splitIndex++) {
                const splitter : string = $splitBy[splitIndex];
                const lastData : string[][] = buffer;
                buffer = [];
                bufferSize = 0;

                let dataIndex : number;
                let dataPartIndex : number;
                const lastDataLength : number = lastData.length;
                for (dataIndex = 0; dataIndex < lastDataLength; dataIndex++) {
                    for (dataPartIndex = 0; dataPartIndex < lastData[dataIndex].length; dataPartIndex++) {
                        tmp = (lastData[dataIndex][dataPartIndex] + "").split(splitter);
                        buffer[bufferSize] = tmp;
                        bufferSize++;
                    }
                }
            }

            let bufferIndex : number;
            let partIndex : number;
            for (bufferIndex = 0; bufferIndex < bufferSize; bufferIndex++) {
                for (partIndex = 0; partIndex < buffer[bufferIndex].length; partIndex++) {
                    output[outputSize] = buffer[bufferIndex][partIndex];
                    outputSize++;
                }
            }
        } else {
            output[0] = $input;
        }

        return output;
    }

    /**
     * @param {string} $input Input string for formatting.
     * @param {...any} $parameters Open-ended args for search in input string, if arg is not typeof string it will
     * be converted to string.
     * @returns {string} Returns input string with replaced values {#} by parameters. Parameters index starts at 0.
     */
    public static Format($input : string, ...$parameters : any[]) : string {
        if (!StringUtils.IsEmpty($input)) {
            let index : number;
            for (index = 0; index < $parameters.length; index++) {
                if (StringUtils.Contains($input, "{" + index + "}")) {
                    if (ObjectValidator.IsEmptyOrNull($parameters[index])) {
                        $parameters[index] = "NULL";
                    } else if (ObjectValidator.IsBoolean($parameters[index])) {
                        $parameters[index] = Convert.BooleanToString($parameters[index]);
                    } else if (ObjectValidator.IsObject($parameters[index])) {
                        $parameters[index] = Convert.ObjectToString($parameters[index]);
                    }

                    $input = StringUtils.Replace($input, "{" + index + "}", $parameters[index]);
                }
            }
        }

        return $input;
    }

    /**
     * @param {string} $input Input string for conversion.
     * @returns {string} Returns a string with backslashes stripped off.
     * Double backslashes are made into a single backslash.
     */
    public static StripSlashes($input : string) : string {
        if (ObjectValidator.IsEmptyOrNull($input)) {
            return "";
        }
        $input = StringUtils.Remove($input, "\\");
        $input = StringUtils.Replace($input, "\0", "\u0000");
        return $input;
    }

    /**
     * @param {string} $input Input string for conversion.
     * @param {string} [$allowed] Skip strip of those tags.
     * @returns {string} Returns a string with HTML and JavaScript tags stripped off.
     */
    public static StripTags($input : string, $allowed? : string) : string {
        if (ObjectValidator.IsEmptyOrNull($input)) {
            return "";
        }

        $allowed = ((($allowed || "") + "")
            .toLowerCase()
            .match(/<[a-z][a-z0-9]*>/g) || [])
            .join("");

        const tags : RegExp = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
        const commentsAndJsTags : RegExp = /<!--[\s\S]*?-->|<script[\s\S]*?<\/script>/gi;

        while (StringUtils.Contains($input, "<script", "<!--")) {
            $input = ($input + "").replace(commentsAndJsTags, "");
        }
        return ($input + "").replace(tags, ($match : string, $val : string) : string => {
            return ($allowed + "").indexOf("<" + ($val + "").toLowerCase() + ">") > -1 ? $match : "";
        });
    }

    /**
     * @param {string} $input Input string for conversion.
     * @returns {string} Returns a string with comments stripped off.
     */
    public static StripComments($input : string) : string {
        if (ObjectValidator.IsEmptyOrNull($input)) {
            return "";
        }

        return ($input + "").replace(/\/\*([\s\S]*?)\*\/|\/\/.*(?=[\n\r])/g, "");
    }

    /**
     * @param {string} $input Input string for conversion.
     * @param {number} [$length] Set maximum length of each line.
     * @param {boolean} [$htmlTag] Specify, if output format should be HTML or plain text.
     * @returns {string} Returns a string separated to lines with defined maximum length regardless on spaces positions.
     */
    public static HardWrap($input : string, $length? : number, $htmlTag : boolean = true) : string {
        let output : string = "";
        const wrapSize : number = ObjectValidator.IsSet($length) ? $length : 300;
        const wrapsCount : number = Math.ceil(StringUtils.Length($input) / wrapSize);
        let index : number;
        for (index = 0; index < wrapsCount; index++) {
            let wrapPart : string = $input;
            wrapPart = StringUtils.Substring(wrapPart, index * wrapSize, (index + 1) * wrapSize);
            output += wrapPart;
            if (index < wrapsCount - 1) {
                output += StringUtils.NewLine($htmlTag);
            }
        }
        if (!$htmlTag) {
            output = StringUtils.StripTags(output);
        }
        return output;
    }

    /**
     * @param {string} $input Input string for conversion.
     * @returns {string[]} Returns array of characters witch input string contains.
     */
    public static ToArray($input : string) : string[] {
        return ($input + "").split("");
    }

    /**
     * @param {string} $input Input string for conversion.
     * @returns {number} Returns number representation of input string. Conversion is done in numeral system 10.
     */
    public static ToInteger($input : string) : number {
        return parseInt($input, 10);
    }

    /**
     * @param {string} $input Input string for conversion.
     * @returns {number} Returns number representation of input string. Conversion is done with floating point.
     */
    public static ToDouble($input : string) : number {
        return parseFloat($input);
    }

    /**
     * @param {string} $input Input string for conversion.
     * @returns {boolean} Returns boolean representation of input string.
     */
    public static ToBoolean($input : string) : boolean {
        return !StringUtils.IsEmpty($input) && ($input === "1" || $input === "true" || $input === "on");
    }

    /**
     * @param {string} $input Input string for conversion.
     * @returns {number} Returns hexadecimal representation of input string, if it is possible, otherwise null.
     */
    public static ToHexadecimal($input : string) : number {
        let output : any = "";
        let index : number;
        for (index = 0; index < StringUtils.Length($input); index++) {
            output += "" + StringUtils.getCodeAt($input, index).toString(16);
        }
        output = parseInt(output, 16);
        if (ObjectValidator.IsHexadecimal(output)) {
            return output;
        }
        return null;
    }

    /**
     * @param {string} $value Input string for validation.
     * @param {string} $patter Pattern, which should be used for validation condition.
     * @returns {boolean} Returns true, if version is lower than expected pattern, otherwise false.
     */
    public static VersionIsLower($value : string, $patter : string) : boolean {
        if (!ObjectValidator.IsString($value) || ObjectValidator.IsEmptyOrNull($value) ||
            !ObjectValidator.IsString($patter) || ObjectValidator.IsEmptyOrNull($patter) ||
            $value === $patter) {
            return false;
        }
        const valueParts : string[] = StringUtils.Split($value, ".");
        const patternParts : string[] = StringUtils.Split($patter, ".");
        let status : boolean = false;
        let index : number;
        const partsLength : number = patternParts.length;
        for (index = 0; index < partsLength; index++) {
            if (index < valueParts.length) {
                if (valueParts[index] === patternParts[index] || patternParts[index] === "x" || patternParts[index] === "*") {
                    continue;
                }
                if (StringUtils.ToInteger(valueParts[index]) > StringUtils.ToInteger(patternParts[index])) {
                    break;
                }
                if (StringUtils.ToInteger(valueParts[index]) < StringUtils.ToInteger(patternParts[index])) {
                    status = true;
                    break;
                }
            } else {
                break;
            }
        }
        return status;
    }

    /**
     * @param {string} $value Input string for conversion.
     * @returns {string} Returns converted input string with CamelCase format.
     */
    public static ToCamelCase($value : string) : string {
        if (StringUtils.IsEmpty($value)) {
            return "";
        }
        return (<any>$value).replace(/^([A-Z])|[\s-_](\w)/g, ($match : string, $before : any, $after : any) : string => {
            if ($after) {
                return $after.toUpperCase();
            }
            return $before.toLowerCase();
        });
    }

    /**
     * @param {number} $start Specify year for range start
     * @returns {string} Returns year range in format $start[-$currentYear]
     */
    public static YearFrom($start : number) : string {
        const currentYear : number = new Date().getFullYear();
        if ($start < currentYear) {
            return $start + "-" + currentYear;
        }
        return $start + "";
    }

    private static repeatedAddChar($count : number, $type : SpecialCharacters) : string {
        let output : string = "";
        let counter : number;
        for (counter = 0; counter < $count; counter++) {
            output += $type;
        }
        return output;
    }
}

globalThis.Io.Oidis.Commons.Utils.StringUtils = StringUtils;
