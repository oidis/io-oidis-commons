/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
import { BaseObject } from "../Primitives/BaseObject.js";
import { ObjectValidator } from "./ObjectValidator.js";

export class Stopwatch extends BaseObject {
    private start : Date;
    private stop : Date;

    public Start() : void {
        this.start = this.stop = new Date();
    }

    public Stop() : number {
        this.stop = new Date();
        return this.ElapsedTime();
    }

    public Restart() : number {
        const elapsed : number = this.Stop();
        this.Start();
        return elapsed;
    }

    public Reset() : void {
        this.start = this.stop = new Date(0);
    }

    public ElapsedTime() : number {
        return this.Elapsed().getTime();
    }

    public Elapsed() : Date {
        return new Date(this.stop.getTime() - this.start.getTime());
    }

    public Block($block : () => void) : number {
        this.Start();
        if (!ObjectValidator.IsFunction($block)) {
            $block();
        }
        return this.Stop();
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        return "Elapsed time: " + this.ElapsedTime() + " ms";
    }
}
