/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "../Primitives/BaseObject.js";
import { ObjectValidator } from "./ObjectValidator.js";

export class TraceBack extends BaseObject {

    public static CaptureEntry($offset : number | string = 3, $infinityTrace : boolean = false) : ITraceBackEntry {
        let error : any = {};
        const ErrorClass : any = Error;
        if (ObjectValidator.IsSet(ErrorClass.prepareStackTrace) && $infinityTrace) {
            const getStack : any = (belowFn? : any) : void => {
                const stackTraceLimit : number = ErrorClass.stackTraceLimit;
                ErrorClass.stackTraceLimit = Infinity;
                const dummyError : any = {};
                const prepareStackTrace : any = ErrorClass.prepareStackTrace;
                ErrorClass.prepareStackTrace = ($error : any, $v8StackTrace : any) : any => {
                    return $v8StackTrace;
                };
                ErrorClass.captureStackTrace(dummyError, belowFn || getStack);
                const stackTrace : any = dummyError.stack;
                ErrorClass.prepareStackTrace = prepareStackTrace;
                ErrorClass.stackTraceLimit = stackTraceLimit;
                return stackTrace;
            };
            error.stack = "Error: dummy trace\n" + getStack().map(($callSite : any) : string => {
                return "   at " + $callSite.toString() + "\n";
            });
        } else {
            error = new ErrorClass();
        }
        if (ObjectValidator.IsEmptyOrNull(error.stack)) {
            try {
                throw new Error();
            } catch (ex) {
                error = ex;
            }
        }

        const traceback : StackTraceCallSite[] = this.Parse(error);
        if (ObjectValidator.IsString($offset)) {
            traceback.forEach(($trace : StackTraceCallSite, $index : number) : void => {
                if ($trace.getTypeName() === $offset) {
                    $offset = $index + 1;
                }
            });
        }
        if (ObjectValidator.IsString($offset)) {
            $offset = 3;
        }
        const getTraceDescriptor : any = ($offset : number) : string => {
            let output : string = "undefined";
            if ($offset < traceback.length) {
                const callSite : StackTraceCallSite = traceback[$offset];
                output = callSite.getFunctionName();
                if (ObjectValidator.IsEmptyOrNull(output)) {
                    output = callSite.getTypeName();
                }
                if (ObjectValidator.IsEmptyOrNull(output)) {
                    output = "anonymous";
                } else if (!ObjectValidator.IsEmptyOrNull(callSite.getFileName())) {
                    output += " " + callSite.getFileName();
                    if (!ObjectValidator.IsEmptyOrNull(callSite.getLineNumber())) {
                        output += ":" + callSite.getLineNumber();
                    }
                    if (!ObjectValidator.IsEmptyOrNull(callSite.getColumnNumber())) {
                        output += ":" + callSite.getColumnNumber();
                    }
                }
            }
            return output;
        };
        const at : string = getTraceDescriptor($offset);
        let from : string = "undefined";
        if ($offset < traceback.length) {
            for (let index : number = (<number>$offset) + 1; index < traceback.length; index++) {
                from = getTraceDescriptor(index);
                if (at !== from) {
                    break;
                }
            }
        }

        return {
            at,
            from
        };
    }

    public static getTrace() : StackTraceCallSite[] {
        return this.Parse(new Error());
    }

    public static Parse($error : Error) : StackTraceCallSite[] {
        if (!$error.stack) {
            return [];
        }

        return $error.stack.split("\n").slice(1).map(($line : string) : StackTraceCallSite => {
            if ($line.match(/^\s*[-]{4,}$/)) {
                return new StackTraceCallSite({
                    columnNumber: null,
                    fileName    : $line,
                    functionName: null,
                    lineNumber  : null,
                    methodName  : null,
                    native      : null,
                    typeName    : null
                });
            }

            const lineMatch : any = $line.match(/at (?:(.+)\s+\()?(?:(.+?):(\d+)(?::(\d+))?|([^)]+))\)?/);
            if (!lineMatch) {
                return;
            }

            let object : string = null;
            let method : string = null;
            let functionName : string = null;
            let typeName : string = null;
            let methodName : string = null;
            const isNative : boolean = (lineMatch[5] === "native");

            if (lineMatch[1]) {
                functionName = lineMatch[1];
                let methodStart : number = functionName.lastIndexOf(".");
                if (functionName[methodStart - 1] === ".") {
                    methodStart--;
                }
                if (methodStart > 0) {
                    object = functionName.substr(0, methodStart);
                    method = functionName.substr(methodStart + 1);
                    const objectEnd : number = object.indexOf(".Module");
                    if (objectEnd > 0) {
                        functionName = functionName.substr(objectEnd + 1);
                        object = object.substr(0, objectEnd);
                    }
                }
                typeName = null;
            }

            if (method) {
                typeName = object;
                methodName = method;
            }

            if (method === "<anonymous>") {
                methodName = null;
                functionName = null;
            }

            return new StackTraceCallSite({
                columnNumber: parseInt(lineMatch[4], 10) || null,
                fileName    : lineMatch[2] || null,
                functionName,
                lineNumber  : parseInt(lineMatch[3], 10) || null,
                methodName,
                native      : isNative,
                typeName
            });
        }).filter(($callSite : StackTraceCallSite) : boolean => {
            return !!$callSite;
        });
    }
}

export class StackTraceCallSite extends BaseObject {
    private readonly properties : any;

    constructor($properties : any) {
        super();
        this.properties = {
            this        : null,
            typeName    : null,
            functionName: null,
            methodName  : null,
            fileName    : null,
            lineNumber  : null,
            columnNumber: null,
            function    : null,
            evalOrigin  : null,
            topLevel    : false,
            eval        : false,
            native      : false,
            constructor : false
        };
        for (const property in $properties) {
            if ($properties.hasOwnProperty(property) && this.properties.hasOwnProperty(property)) {
                this.properties[property] = $properties[property];
            }
        }
    }

    public getThis() : string {
        return this.properties.this;
    }

    public getTypeName() : string {
        return this.properties.typeName;
    }

    public getFunctionName() : string {
        return this.properties.functionName;
    }

    public getMethodName() : string {
        return this.properties.methodName;
    }

    public getFileName() : string {
        return this.properties.fileName;
    }

    public getLineNumber() : string {
        return this.properties.lineNumber;
    }

    public getColumnNumber() : string {
        return this.properties.columnNumber;
    }

    public getEvalOrigin() : string {
        return this.properties.evalOrigin;
    }

    public getFunction() : string {
        return this.properties.function;
    }

    public IsTopLevel() : boolean {
        return this.properties.topLevel;
    }

    public IsEval() : boolean {
        return this.properties.eval;
    }

    public IsNative() : boolean {
        return this.properties.native;
    }

    public IsConstructor() : boolean {
        return this.properties.constructor;
    }

    public toString() : string {
        return JSON.stringify(this.properties);
    }
}

export interface ITraceBackEntry {
    at : string;
    from : string;
}

// generated-code-start
export const ITraceBackEntry = globalThis.RegisterInterface(["at", "from"]);
// generated-code-end
