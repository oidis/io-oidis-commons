/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "../Enums/LogLevel.js";
import { LogSeverity } from "../Enums/LogSeverity.js";
import { Logger } from "../LogProcessor/Logger.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { ObjectValidator } from "./ObjectValidator.js";

/**
 * LogIt class provides static API for logging to selected output handler.
 */
export class LogIt extends BaseObject {
    private static logger : Logger;

    /**
     * Execute initialization manually, in case of that, it has not been initialized automatically.
     * @param {Logger|LogLevel} $logger Set processor for handling of log message or specify log level for default processor.
     * @returns {void}
     */
    public static Init($logger : Logger | LogLevel) : void {
        if (!ObjectValidator.IsEmptyOrNull($logger)) {
            if (ObjectValidator.IsInteger($logger)) {
                LogIt.getLogger().setLevels(<LogLevel>$logger, LogSeverity.HIGH);
            } else {
                LogIt.logger = <Logger>$logger;
                LogIt.logger.setTrackBackEntryPoint(LogIt);
            }
        }
    }

    /**
     * @param {LogLevel} $level Set maximal level, which will be allowed for output.
     * @param {LogSeverity} [$severity] Set minimal level, which will be allowed for Info log.
     * @returns {void}
     */
    public static setLevel($level : LogLevel, $severity? : LogSeverity) : void {
        LogIt.getLogger().setLevels($level, $severity);
    }

    /**
     * Turn off/on print of messages.
     * @returns {void}
     */
    public static MuteToggle() : void {
        LogIt.getLogger().MuteToggle();
    }

    /**
     * Log message at Info level.
     * @param {string} $message Message which should be logged.
     * @param {LogSeverity} [$severity = LogSeverity.HIGH] Specify severity level of info message.
     * @returns {void}
     */
    public static Info($message : string, $severity? : LogSeverity) : void;
    /**
     * Log message at Info level.
     * @param {string} $message Message which should be logged and contains format symbols.
     * @param {...any} [$parameters] Open-ended args for formatted message.
     * @returns {void}
     */
    public static Info($message : string, ...$parameters : any[]) : void;

    public static Info($message : string, ...$parameters : any[]) : void {
        LogIt.getLogger().Info.apply(LogIt.getLogger(), [$message].concat($parameters)); // eslint-disable-line
    }

    /**
     * Log message at Warning level.
     * @param {string} $message Message which should be logged and may contains format symbols.
     * @param {...any} [$parameters] Open-ended args for formatted message.
     * @returns {void}
     */
    public static Warning($message : string, ...$parameters : any[]) : void {
        LogIt.getLogger().Warning.apply(LogIt.getLogger(), [$message].concat($parameters)); // eslint-disable-line
    }

    /**
     * Log message at Error level.
     * @param {string} $message Message which should be logged.
     * @returns {void}
     */
    public static Error($message : string) : void;
    /**
     * Log message at Error level.
     * @param {Error} $exception Exception which should be logged.
     * @returns {void}
     */
    public static Error($exception : Error) : void;
    /**
     * Log message at Error level.
     * @param {string} $message Message which should be logged.
     * @param {Error} [$exception] Include exception stack trace, if is provided by environment.
     * @returns {void}
     */
    public static Error($message : string, $exception? : Error) : void;
    /**
     * Log message at Error level.
     * @param {string} $message Message which should be logged and contains format symbols.
     * @param {...any} [$parameters] Open-ended args for formatted message.
     * @returns {void}
     */
    public static Error($message : string, ...$parameters : any[]) : void;
   
    public static Error($message : string | Error, ...$parameters : any[]) : void {
        LogIt.getLogger().Error.apply(LogIt.getLogger(), [$message].concat($parameters)); // eslint-disable-line
    }

    /**
     * Log message at Debug level.
     * @param {string} $message Message, which should be logged.
     * Message, which is not type of string, is converted to the string.
     * @returns {void}
     */
    public static Debug($message : string) : void;
    /**
     * Log message at Debug level.
     * @param {any} $object Object, which should be converted to string and logged.
     * @returns {void}
     */
    public static Debug($object : any) : void;
    /**
     * Log message at Debug level.
     * @param {string} $message Message which should be logged and contains format symbols.
     * @param {...any} [$parameters] Open-ended args for formatted message.
     * @returns {void}
     */
    public static Debug($message : string, ...$parameters : any[]) : void;
   
    public static Debug($messageOrObject : string | any, ...$parameters : any[]) : void {
        LogIt.getLogger().Debug.apply(LogIt.getLogger(), [$messageOrObject].concat($parameters)); // eslint-disable-line
    }

    /**
     * Log message at Verbose level.
     * @param {string} $message Message which should be logged and contains format symbols.
     * @param {...any} [$parameters] Open-ended args for formatted message.
     * @returns {void}
     */
    public static Verbose($message : string, ...$parameters : any[]) : void {
        LogIt.getLogger().Verbose.apply(LogIt.getLogger(), [$message].concat($parameters)); // eslint-disable-line
    }

    public static getLogger() : Logger {
        if (ObjectValidator.IsEmptyOrNull(LogIt.logger)) {
            LogIt.logger = new Logger();
            LogIt.logger.setTrackBackEntryPoint(LogIt);
        }
        LogIt.getLogger = () : Logger => {
            return LogIt.logger;
        };
        return LogIt.logger;
    }
}

globalThis.Io.Oidis.Commons.Utils.LogIt = LogIt;
