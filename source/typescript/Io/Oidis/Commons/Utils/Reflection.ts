/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { SyntaxConstants } from "../Enums/SyntaxConstants.js";
import { IBaseObject } from "../Interfaces/IBaseObject.js";
import { IClassName, Interface } from "../Interfaces/Interface.js";
import { INamespaceMapping } from "../Interfaces/IProject.js";
import { IReflection } from "../Interfaces/IReflection.js";
import { IPartialDescriptor } from "../Primitives/Decorators.js";
import { Convert } from "./Convert.js";
import { ObjectValidator } from "./ObjectValidator.js";
import { StringUtils } from "./StringUtils.js";

/**
 * Reflection class provides OOP metadata for classes implemented at Io.Oidis namespace.
 */
export class Reflection implements IReflection {

    private static singletonNamespaces : string[] = ["Io.Oidis"];
    private static namespaceMappings : INamespaceMapping[] = [];
    private static singleton : Reflection;
    private static classNamespace : string = "";
    private static className : string = "";
    private classesList : string[] = [];
    private classesSize : number;
    private defaultProperties : string[];

    /**
     * @returns {string} Returns class name with namespace in string format.
     */
    public static ClassName() : string {
        return this.classNamespace + "." + this.className;
    }

    /**
     * @returns {string} Returns namespace of the class in string format.
     */
    public static NamespaceName() : string {
        return this.classNamespace;
    }

    /**
     * @returns {string} Returns only the class name without namespace in string format.
     */
    public static ClassNameWithoutNamespace() : string {
        return this.className;
    }

    /**
     * @param {...string[]} $values Specify namespaces, which should be reflected by singleton instance.
     * @returns {void}
     */
    public static setInstanceNamespaces(...$values : string[]) : void {
        if (!ObjectValidator.IsEmptyOrNull($values)) {
            this.singletonNamespaces = $values;
        }
    }

    /**
     * @param {...string[]} $values Specify namespaces, which are mapped onto other namespaces
     * @returns {void}
     */
    public static setNamespaceMappings(...$values : INamespaceMapping[]) : void {
        if (!ObjectValidator.IsEmptyOrNull($values)) {
            this.namespaceMappings = Reflection.flattenNamespaceMappings($values);
        }
    }

    /**
     * @returns {Reflection} Returns class instance singleton.
     */
    public static getInstance() : Reflection {
        if (!ObjectValidator.IsSet(this.singleton)) {
            this.singleton = new Reflection();
        }
        return this.singleton;
    }

    public static Refresh() : void {
        if (ObjectValidator.IsSet(this.singleton)) {
            delete this.singleton;
            Reflection.getInstance();
        }
    }

    /**
     * @param {string} $className Desired instance's class name, which should be created.
     * @param {object} [$dataObject] Data, which should be passed to the created instance.
     * @returns {any} Returns instance of desired class with defined data.
     */
    public static getInstanceOf($className : string, $dataObject? : object) : any {
        if (!ObjectValidator.IsSet($dataObject) ||
            ObjectValidator.IsFunction($dataObject[SyntaxConstants.CLASS_NAME]) &&
            $dataObject[SyntaxConstants.CLASS_NAME]() === $className) {

            const baseClass : any = function () : void {
                // default constructor with empty body
            };
            const thisClass : any = function () : void {
                // default constructor with empty body
            };

            /// TODO: validate if this is still possible in ESModule without usage of __proto__?
            baseClass.prototype = Reflection.getInstance().getClass($className).prototype;
            let propertyName : string;
            for (propertyName in baseClass.prototype) {
                /* istanbul ignore else : bulletproof condition */
                if (ObjectValidator.IsSet(baseClass.prototype[propertyName])) {
                    thisClass.prototype[propertyName] = baseClass.prototype[propertyName];
                }
            }
            thisClass.prototype.constructor = thisClass;
            thisClass.prototype = new baseClass();

            const output : any = new thisClass();
            if (ObjectValidator.IsSet($dataObject)) {
                for (propertyName in $dataObject) {
                    /* istanbul ignore else : bulletproof condition */
                    if (ObjectValidator.IsSet($dataObject[propertyName])) {
                        output[propertyName] = $dataObject[propertyName];
                    }
                }
            }
            return output;
        }
        return null;
    }

    /**
     * @param {string} [$className] Specify class name for which should be UID generated.
     * @returns {string} Returns unique identifier, based on class name and timestamp.
     */
    public static UID($className? : string) : string {
        if (ObjectValidator.IsEmptyOrNull($className)) {
            $className = this.ClassName();
        }
        return StringUtils.getSha1($className + new Date().getTime() + Math.random() + Math.random() + Math.random());
    }

    /**
     * @param {string} [$value] Specify namespace to normalize
     * @returns {string} Returns namespace normalized according to namespaceMapping inside package.conf.json or private.conf.json
     */
    public static NormalizeNamespace($value : string) : string {
        let returnVal : string = $value;
        Reflection.namespaceMappings.forEach(($fallback : INamespaceMapping) : void => {
            if (StringUtils.StartsWith($value, $fallback.namespace)) {
                returnVal = StringUtils.Replace(returnVal, $fallback.namespace, $fallback.mappedTo);
            }
        });
        return returnVal;
    }

    private static isMappedNamespace($namespace : string) : boolean {
        let returnVal : boolean = false;
        Reflection.namespaceMappings.forEach(($fallback : INamespaceMapping) : boolean => {
            return !(returnVal = StringUtils.StartsWith($namespace, $fallback.namespace));
        });
        return returnVal;
    }

    private static flattenNamespaceMappings($mappings : INamespaceMapping[]) : INamespaceMapping[] {
        const mappings : INamespaceMapping[] = [];
        $mappings.forEach(($mapping : INamespaceMapping) : void => {
            if (ObjectValidator.IsEmptyOrNull($mapping.childNamespaces)) {
                mappings.push({namespace: $mapping.namespace, mappedTo: $mapping.mappedTo});
            } else {
                $mapping.childNamespaces.forEach(($child : string) : void => {
                    mappings.push({namespace: $mapping.namespace + "." + $child, mappedTo: $mapping.mappedTo + "." + $child});
                });
            }
        });
        return mappings;
    }

    constructor() {
        this.classesSize = 0;
        let index : number;
        for (index = 0; index < Reflection.singletonNamespaces.length; index++) {
            let parentNamespace : string = Reflection.singletonNamespaces[index];
            let subNamespace : string = "";
            if ((<any>parentNamespace).indexOf(".") !== -1) {
                const namespaces : string[] = (<any>parentNamespace).split(".");
                parentNamespace = namespaces[0];
                subNamespace = namespaces[1];
            }
            if (ObjectValidator.IsSet(globalThis[parentNamespace])) {
                this.setNamespace(globalThis[parentNamespace], parentNamespace, subNamespace);
            }
        }

        this.defaultProperties = [
            "constructor", "__defineGetter__", "__defineSetter__", "hasOwnProperty", "__lookupGetter__", "__lookupSetter__",
            "isPrototypeOf", "propertyIsEnumerable", "valueOf", "__proto__", "toString", "toLocaleString",
            "length", "name", "prototype"
        ];

        this.resolvePartials();
    }

    /**
     * @returns {string} Returns unique identifier, based on class name and timestamp.
     */
    public getUID() : string {
        return Reflection.UID(this.getClassName());
    }

    /**
     * @param {IBaseObject} [$instance] Object instance for, which should be looked up class name.
     * @returns {string} Returns class name with namespace in string format for the this class instance
     * or for instance of Object, which has been passed as method argument.
     * If class has not been found in reflected namespace, it will returns null.
     */
    public getClassName($instance? : IBaseObject) : string {
        if (ObjectValidator.IsSet($instance)) {
            let output : string = null;
            let index : number;
            for (index = 1; index < this.classesSize; index++) {
                if ($instance instanceof this.getClass(this.classesList[index])) {
                    output = this.classesList[index];
                }
            }

            return output;
        } else {
            return this.getClassName(<any>this);
        }
    }

    /**
     * @param {IBaseObject} [$instance] Object instance for, which should be looked up class name.
     * @returns {string} Returns namespace of the class in string format for the this class instance
     * or for instance of Object, which has been passed as method argument.
     * If class has not been found in reflected namespace, it will returns null.
     */
    public getNamespaceName($instance? : IBaseObject) : string {
        if (!ObjectValidator.IsSet($instance)) {
            $instance = <any>this;
        }
        const fullName : any = this.getClassName($instance);
        return fullName.substring(0, fullName.lastIndexOf("."));
    }

    /**
     * @param {IBaseObject} [$instance] Object instance for, which should be looked up class name.
     * @returns {string} Returns only the class name without namespace in string format for the this class instance
     * or for instance of Object, which has been passed as method argument.
     * If class has not been found in reflected namespace, it will returns null.
     */
    public getClassNameWithoutNamespace($instance? : IBaseObject) : string {
        if (!ObjectValidator.IsSet($instance)) {
            $instance = <any>this;
        }
        const fullName : any = this.getClassName($instance);
        return fullName.substr((this.getNamespaceName($instance) + "").length + 1);
    }

    /**
     * @returns {string[]} Returns list of classes hierarchy for current instance.
     */
    public getClassHierarchy() : string[] {
        return this.getClassHierarchyOf(this);
    }

    /**
     * @returns {string[]} Returns list of namespaces hierarchy for current instance.
     */
    public getNamespaceHierarchy() : string[] {
        return this.getNamespaceHierarchyOf(this);
    }

    /**
     * @returns {string[]} Returns list of all mapped classes in looked up namespace.
     */
    public getAllClasses() : string[] {
        return this.classesList;
    }

    /**
     * @param {string} $className Class name in string format, which should be validated.
     * @returns {boolean} Returns true, if class has been mapped, otherwise false.
     */
    public Exists($className : string) : boolean {
        return this.classesList.indexOf($className) !== -1;
    }

    /**
     * @param {string} $className Class name in string format, which should be reflected.
     * @returns {any} Returns class object, if class name has been found in namespace, otherwise null.
     */
    public getClass($className : string) : any {
        const namespaces : string[] = (<any>$className).split(".");
        let object : any = null;
        if (namespaces.length > 1 && ObjectValidator.IsSet(globalThis[namespaces[0]])) {
            object = globalThis[namespaces[0]];
            let index : number;
            for (index = 1; index < namespaces.length; index++) {
                object = object[namespaces[index]];
            }
        }
        return object;
    }

    /**
     * @param {IBaseObject} $instance Object, which should be validated.
     * @param {string|IClassName} $className Class name in string format or as Object, which should be validated as instance type.
     * @returns {boolean} Returns true, if instance is type of $className object.
     */
    public IsInstanceOf($instance : IBaseObject, $className : string | IClassName) : boolean {
        if (!ObjectValidator.IsString($className) && ObjectValidator.IsSet((<any>$className).ClassName)) {
            $className = (<any>$className).ClassName();
        }
        if (ObjectValidator.IsSet($instance.getClassName)) {
            return $className === $instance.getClassName();
        }
        return false;
    }

    /**
     * @param {IBaseObject} $instance Object, which should be validated.
     * @param {string|IClassName} $className Class name in string format or as Object, which should be validated as instance type.
     * @returns {boolean} Returns true, if instance is type of $className object or it's child.
     */
    public IsMemberOf($instance : IBaseObject, $className? : string | IClassName) : boolean {
        /// TODO: IsMemeberOf should return false if namespace does not exits, but this.getClass can't simply return null
        //        > this needs deeper investigation based on unit tests
        let requiredClass : any = null;
        if (!ObjectValidator.IsSet($className)) {
            if (ObjectValidator.IsSet((<any>$instance).ClassName)) {
                $className = (<any>$instance).ClassName();
            } else if (ObjectValidator.IsString($instance)) {
                $className = $instance;
            } else {
                $className = null;
            }
            if ($className === "Io.Oidis.Commons.Primitives.BaseObject") {
                return true;
            }
            return this.IsMemberOf(<any>this, $className);
        } else if (ObjectValidator.IsString($className)) {
            requiredClass = this.getClass(<string>$className);
        } else {
            requiredClass = $className;
        }

        if (requiredClass === null) {
            return false;
        }
        return $instance instanceof requiredClass;
    }

    /**
     * @param {string | IClassName | IBaseObject} $instanceOrClass Object, which should be inspected.
     * @returns {string[]} Returns list of classes hierarchy for given instance.
     */
    public getClassHierarchyOf($instanceOrClass : string | IClassName | IBaseObject) : string[] {
        return this.getHierarchy($instanceOrClass).classes;
    }

    /**
     * @param {string | IClassName | IBaseObject} $instanceOrClass Object, which should be inspected.
     * @returns {string[]} Returns list of namespaces hierarchy for given instance.
     */
    public getNamespaceHierarchyOf($instanceOrClass : string | IClassName | IBaseObject) : string[] {
        return this.getHierarchy($instanceOrClass).namespaces;
    }

    /**
     * @param {string|IClassName} $className Class, which should be validated.
     * @param {string|Interface} $interface Interface, which should be reflected by the class.
     * @returns {boolean} Returns true, if class has required interface, otherwise false.
     */
    public ClassHasInterface($className : string | IClassName, $interface : string | Interface) : boolean {
        if (ObjectValidator.IsString($interface)) {
            $interface = this.getClass(<string>$interface);
        }
        if (!ObjectValidator.IsEmptyOrNull($interface) &&
            ObjectValidator.IsSet((<Interface>$interface).ClassName) &&
            ObjectValidator.IsSet((<Interface>$interface).NamespaceName) &&
            ObjectValidator.IsSet((<Interface>$interface).ClassNameWithoutNamespace)) {
            if (!<any>$interface.hasOwnProperty("getClassName")) {
                $interface = <Interface>(<any>$interface).prototype;
            }
            if (ObjectValidator.IsSet((<any>$interface).getClassName)) {
                let classDeclaration : any = $className;
                if (ObjectValidator.IsString($className)) {
                    classDeclaration = this.getClass(<string>$className);
                }
                const classProperties : string[] = [];
                const scanObject : any = ($object : any) : void => {
                    Object.getOwnPropertyNames($object).forEach(($classProperty : string) : void => {
                        if (this.defaultProperties.indexOf($classProperty) === -1 &&
                            classProperties.indexOf($classProperty) === -1) {
                            classProperties.push($classProperty);
                        }
                    });
                    if ($object.__proto__) {
                        scanObject($object.__proto__);
                    }
                };
                scanObject(classDeclaration.prototype);
                let classProperty : string;
                for (classProperty in classDeclaration.prototype) {
                    if (classProperty !== "" &&
                        this.defaultProperties.indexOf(classProperty) === -1 &&
                        classProperties.indexOf(classProperty) === -1) {
                        classProperties.push(classProperty);
                    }
                }
                const requiredProperties : string[] = this.getInterfaceProperties($interface);
                const size : number = requiredProperties.length;
                for (let index : number = 0; index < size; index++) {
                    if (classProperties.indexOf(requiredProperties[index]) === -1) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    /**
     * @param {IBaseObject|Interface|string} $instance Object, which should be validated.
     * @param {(Interface|string)} $className Interface class name in string format or as Object, which should be validated.
     * @returns {boolean} Returns true if the class instance implements $className interface, otherwise false.
     */
    public Implements($instance : IBaseObject | Interface | string, $className? : Interface | string) : boolean {
        if (!ObjectValidator.IsSet($className)) {
            if (ObjectValidator.IsString($instance)) {
                $className = this.getClass(<any>$instance);
            } else if (ObjectValidator.IsSet((<any>$instance).ClassName)) {
                $className = <any>$instance;
            }
            $instance = <any>this;
        } else if (ObjectValidator.IsString($className)) {
            $className = this.getClass(<string>$className);
        }

        if (!ObjectValidator.IsEmptyOrNull($className) &&
            ObjectValidator.IsSet((<Interface>$className).ClassName) &&
            ObjectValidator.IsSet((<Interface>$className).NamespaceName) &&
            ObjectValidator.IsSet((<Interface>$className).ClassNameWithoutNamespace)) {
            const requiredProperties : string[] = this.getInterfaceProperties($className);
            const size : number = requiredProperties.length;
            for (let index : number = 0; index < size; index++) {
                if (!ObjectValidator.IsSet($instance[requiredProperties[index]])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @returns {string[]} Returns string array of all properties in the class instance.
     */
    public getProperties() : string[] {
        return [];
    }

    /**
     * @returns {string[]} Returns string array of all methods in the class instance.
     */
    public getMethods() : string[] {
        const output : string[] = [];
        Object.getOwnPropertyNames(Object.getPrototypeOf(this)).forEach(($parameter : string) : void => {
            if (output.indexOf($parameter) === -1 && ObjectValidator.IsFunction(this[parameter])) {
                output.push($parameter);
            }
        });
        let parameter : any;
        for (parameter in this) {
            if (output.indexOf(parameter) === -1 && ObjectValidator.IsFunction(this[parameter])) {
                output.push(parameter);
            }
        }
        return output;
    }

    /**
     * @returns {number} Returns CRC calculated from data, which represents current object.
     */
    public getHash() : number {
        return StringUtils.getCrc(JSON.stringify(this.classesList));
    }

    /**
     * @param {string|IClassName} $className Class name for validation.
     * @returns {boolean} Returns true if the class instance is type of $className, otherwise false.
     */
    public IsTypeOf($className : string | IClassName) : boolean {
        return this.IsInstanceOf(<any>this, $className);
    }

    /**
     * @returns {object} Returns data suitable for object serialization.
     */
    public SerializationData() : object {
        return {classesList: this.classesList};
    }

    public ToString($prefix? : string, $htmlTag : boolean = true) : string {
        return Convert.ArrayToString(this.classesList, $prefix, $htmlTag);
    }

    public toString() : string {
        return this.ToString();
    }

    public ValidateProperties() : boolean {
        return true;
    }

    public ValidateDeep($property? : string, $verbose? : boolean) : string[] {
        return [];
    }

    private setNamespace($objectHolder : any, $parentNamespace : string, $name : string) : void {
        let currentNamespace : string = $parentNamespace;
        if ($name !== null && $name !== "") {
            $objectHolder = $objectHolder[$name];
            currentNamespace = $parentNamespace + "." + $name;
        }
        let parameter : any;
        for (parameter in $objectHolder) {
            if (typeof $objectHolder[parameter] === SyntaxConstants.OBJECT) {
                let child : any;
                let isNamespace : boolean = false;
                for (child in $objectHolder[parameter]) {
                    /* istanbul ignore else : bulletproof condition */
                    if (ObjectValidator.IsSet($objectHolder[parameter][child])) {
                        if (ObjectValidator.IsFunction($objectHolder[parameter][child])) {
                            const namespace : string = currentNamespace + "." + parameter.toString();
                            const classNameWithNamespace : string = namespace + "." + child.toString();
                            /* istanbul ignore else : other than singleton instance can not be created */
                            if (this.classesList.indexOf(classNameWithNamespace) === -1) {
                                this.classesList[this.classesSize] = classNameWithNamespace;
                                this.classesSize++;
                            }
                            if (!Reflection.isMappedNamespace(namespace)) {
                                if (ObjectValidator.IsSet($objectHolder[parameter][child].ClassName)) {
                                    $objectHolder[parameter][child].classNamespace = namespace;
                                    $objectHolder[parameter][child].className = child.toString();
                                    if (namespace + "." + child.toString() !== "Io.Oidis.Commons.Utils.Reflection") {
                                        $objectHolder[parameter][child].ClassName = function () : string {
                                            return this.classNamespace + "." + this.className;
                                        };
                                        $objectHolder[parameter][child].NamespaceName = function () : string {
                                            return this.classNamespace;
                                        };
                                        $objectHolder[parameter][child].ClassNameWithoutNamespace = function () : string {
                                            return this.className;
                                        };
                                    }
                                }
                                if (ObjectValidator.IsSet($objectHolder[parameter][child].prototype.getClassName) &&
                                    namespace !== "Io.Oidis.Commons.Environment") {
                                    $objectHolder[parameter][child].prototype.objectNamespace = namespace;
                                    const objectClassName : string = child.toString();
                                    $objectHolder[parameter][child].prototype.objectClassName = objectClassName;
                                    if (namespace + "." + child.toString() !== "Io.Oidis.Commons.Utils.Reflection") {
                                        $objectHolder[parameter][child].prototype.getClassName = () : string => {
                                            return namespace + "." + objectClassName;
                                        };
                                        $objectHolder[parameter][child].prototype.getNamespaceName = () : string => {
                                            return namespace;
                                        };
                                        $objectHolder[parameter][child].prototype.getClassNameWithoutNamespace = () : string => {
                                            return objectClassName;
                                        };
                                    }
                                }
                            }
                            isNamespace = true;
                        } else if (ObjectValidator.IsObject($objectHolder[parameter][child])) {
                            isNamespace = true;
                        }
                    }
                }
                if (isNamespace) {
                    this.setNamespace($objectHolder, currentNamespace, parameter);
                }
            } else if (ObjectValidator.IsFunction($objectHolder[parameter])) {
                if (this.classesList.indexOf(currentNamespace + "." + parameter.toString()) === -1) {
                    this.classesList[this.classesSize] = currentNamespace + "." + parameter.toString();
                    this.classesSize++;
                }
            }
        }
    }

    private getInterfaceProperties($instance : any) : string[] {
        const output : string[] = [];
        Object.getOwnPropertyNames($instance).forEach(($property : string) : void => {
            if (this.defaultProperties.indexOf($property) === -1 &&
                $property !== "classNamespace" &&
                $property !== "className" &&
                $property !== "ClassName" &&
                $property !== "NamespaceName" &&
                $property !== "ClassNameWithoutNamespace") {
                if (output.indexOf($property) === -1) {
                    output.push($property);
                }
            }
        });
        let property : any;
        for (property in ($instance)) {
            if (this.defaultProperties.indexOf(property) === -1 &&
                property !== "classNamespace" &&
                property !== "className" &&
                property !== "ClassName" &&
                property !== "NamespaceName" &&
                property !== "ClassNameWithoutNamespace") {
                if (output.indexOf(property) === -1) {
                    output.push(property);
                }
            }
        }
        return output;
    }

    private getHierarchy($objectOrClass : any) : any {
        const classes : string[] = [];
        const namespaces : string[] = [];
        const scanObject : any = ($object : any) : void => {
            try {
                if (ObjectValidator.IsSet($object.ClassName)) {
                    const className : string = $object.ClassName();
                    const namespaceName : string = $object.NamespaceName();
                    if (!classes.includes(className)) {
                        classes.push(className);
                        if (!namespaces.includes(namespaceName)) {
                            namespaces.push(namespaceName);
                        }
                    }
                }
                if ($object.__proto__) {
                    scanObject($object.__proto__);
                }
            } catch (ex) {
                console.error("failed to scan object tree: " + ex.stack); // eslint-disable-line no-console
            }
        };
        if (!ObjectValidator.IsEmptyOrNull($objectOrClass)) {
            let instance : any = $objectOrClass;
            if (ObjectValidator.IsString($objectOrClass)) {
                instance = this.getClass($objectOrClass);
            } else if (ObjectValidator.IsSet($objectOrClass.getClassName)) {
                instance = this.getClass($objectOrClass.getClassName());
            }
            scanObject(instance);
        }
        return {
            classes,
            namespaces
        };
    }

    private resolvePartials() : void {
        if (!globalThis.hasOwnProperty("oidisPartials")) {
            globalThis.oidisPartials = [];
        }
        globalThis.oidisPartials.forEach(($partial : IPartialDescriptor) : void => {
            if (!ObjectValidator.IsEmptyOrNull($partial.subclasses) && !(<any>$partial.instance).prototype.isInjected) {
                $partial.subclasses.forEach(($instance : any) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($instance.prototype.__partialTarget)) {
                        throw new Error(`Failed to inject partial class "${$instance.prototype.getClassName()}" to "${(<any>$partial.instance).prototype.getClassName()}" because it is already injected by "${$instance.prototype.__partialTarget.getClassName()}".`);
                    }
                    $instance.prototype.__partialTarget = (<any>$partial.instance).prototype;
                    Object.getOwnPropertyNames($instance.prototype).forEach(($method : string) : void => {
                        if (!(<any>$partial.instance).prototype.hasOwnProperty($method)) {
                            (<any>$partial.instance).prototype[$method] = $instance.prototype[$method];
                        }
                    });
                    Object.getOwnPropertyNames($instance).forEach(($method : string) : void => {
                        if (!$partial.instance.hasOwnProperty($method)) {
                            $partial.instance[$method] = $instance[$method];
                        }
                    });
                    (<any>$partial.instance).prototype.isInjected = true;
                });
            }
        });
    }
}

globalThis.Io.Oidis.Commons.Utils.Reflection = Reflection;
