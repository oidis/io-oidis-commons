/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IClassName } from "../Interfaces/Interface.js";
import { ArrayList } from "../Primitives/ArrayList.js";
import { BaseObject } from "../Primitives/BaseObject.js";
import { Convert } from "./Convert.js";
import { LogIt } from "./LogIt.js";
import { ObjectValidator } from "./ObjectValidator.js";
import { StringUtils } from "./StringUtils.js";

export class JsonUtils extends BaseObject {

    /**
     * @param {object} $parent Specify data object, which should be extended.
     * @param {object} $child Specify data object, which should be used for parent override and extend.
     * @returns {object} Returns extended data object.
     */
    public static Extend($parent : any, $child : any) : any {
        const extend : any = ($parent : any, $child : any) : void => {
            let property : any;
            let value : any;
            for (property in $child) {
                if ($child.hasOwnProperty(property)) {
                    value = $child[property];
                    if (!$parent.hasOwnProperty(property)) {
                        $parent[property] = value;
                    } else {
                        if (ObjectValidator.IsObject(value) && ObjectValidator.IsObject($parent[property])) {
                            extend($parent[property], value);
                        } else {
                            $parent[property] = value;
                        }
                    }
                }
            }
        };
        if (ObjectValidator.IsEmptyOrNull($parent)) {
            $parent = {};
        }
        if (ObjectValidator.IsEmptyOrNull($child)) {
            $child = {};
        }
        extend($parent, $child);
        return $parent;
    }

    /**
     * @param {object} $source Specify data object, which should be cloned.
     * @returns {object} Returns clone of the source data object based on JSON functionally.
     */
    public static Clone($source : any) : any {
        if (ObjectValidator.IsEmptyOrNull($source)) {
            return $source;
        }
        return JSON.parse(JSON.stringify($source));
    }

    /**
     * @param {object} $source Specify data object, which should be deeply cloned.
     * @param {boolean} [$withReferences=true] Specify if cloned object should respect properties references.
     * @param {boolean} [$withRefSymbols=true] Specify if referenced properties should be replaced by symbols.
     * @returns {object} Returns deep clone of the source data object based on recursion functions.
     */
    public static DeepClone($source : any, $withReferences : boolean = true, $withRefSymbols : boolean = true) : any {
        let cache : any[] = [];
        let refs : any[] = [];
        let clones : ArrayList<any> = new ArrayList<any>();
        const deepClone : any = ($source : any, $clone : any, $parentKey : string) : void => {
            let property : any;
            let value : any;
            for (property in $source) {
                if ($source.hasOwnProperty(property)) {
                    const key : string = $parentKey + "/" + property;
                    value = $source[property];
                    const cacheIndex : number = cache.indexOf(value);
                    if (cacheIndex === -1) {
                        if (ObjectValidator.IsNativeArray(value)) {
                            $clone[property] = [];
                            let index : number;
                            for (index = 0; index < value.length; index++) {
                                if (ObjectValidator.IsObject(value[index])) {
                                    $clone[property][index] = {};
                                    deepClone(value[index], $clone[property][index], key + "/" + index);
                                } else if (ObjectValidator.IsNativeArray(value[index])) {
                                    $clone[property][index] = [];
                                    deepClone(value[index], $clone[property][index], key + "/" + index);
                                } else {
                                    $clone[property][index] = value[index];
                                }
                            }
                        } else if (ObjectValidator.IsObject(value)) {
                            cache.push(value);
                            $clone[property] = {};
                            clones.Add({
                                key,
                                value: $clone[property]
                            }, cache.length - 1);
                            deepClone(value, $clone[property], key);
                        } else {
                            $clone[property] = value;
                        }
                    } else {
                        $clone[property] = {};
                        refs.push({
                            key,
                            ref  : cacheIndex,
                            value: $clone[property]
                        });
                    }
                }
            }
        };
        const clone : any = {};
        deepClone($source, clone, "#");
        let index : number;
        const refsLength : number = refs.length;
        for (index = 0; index < refsLength; index++) {
            let value : any;
            try {
                const clone : any = clones.getItem(refs[index].ref);
                if (!ObjectValidator.IsEmptyOrNull(clone)) {
                    if ($withReferences) {
                        value = clone.value;
                    } else if (!$withRefSymbols) {
                        value = JSON.parse(JSON.stringify(clone.value));
                    } else {
                        value = {
                            $ref: clone.key
                        };
                    }
                } else {
                    value = null;
                }
                this.Extend(refs[index].value, value);
            } catch (ex) {
                LogIt.Warning("Failed to process reference: " + refs[index].key);
            }
        }
        cache = null;
        refs = null;
        clones = null;
        return clone;
    }

    /**
     * @param {object} $source Specify data object, in which should be resolved properties referenced by symbols.
     * @returns {object} Returns resolved data object.
     */
    public static ParseRefSymbols($source : any) : any {
        const getRef : any = ($key : string) : any => {
            const keys : string[] = StringUtils.Split(StringUtils.Remove($key, "#/"), "/");
            let output : any = $source;
            let resolved : any = true;
            keys.forEach(($key : string) : void => {
                if (output.hasOwnProperty($key)) {
                    output = output[$key];
                } else {
                    resolved = false;
                }
            });
            if (resolved) {
                return output;
            }
            LogIt.Warning("Failed to resolve reference: " + $key);
            return {
                $ref: $key
            };
        };
        const traverse : any = ($source : any, $parentKey : string) : void => {
            let property : any;
            let value : any;
            for (property in $source) {
                if ($source.hasOwnProperty(property)) {
                    value = $source[property];
                    if (ObjectValidator.IsNativeArray(value)) {
                        let index : number;
                        for (index = 0; index < value.length; index++) {
                            if (ObjectValidator.IsObject(value[index]) || ObjectValidator.IsNativeArray(value[index])) {
                                traverse(value[index], $parentKey + "/" + property + "/" + index);
                            }
                        }
                    } else if (ObjectValidator.IsObject(value)) {
                        if (!ObjectValidator.IsEmptyOrNull(value.$ref)) {
                            $source[property] = getRef(value.$ref);
                        } else {
                            traverse(value, $parentKey + "/" + property);
                        }
                    }
                }
            }
        };
        traverse($source, "#");
        return $source;
    }

    /**
     * @param {object} $source Specify data object, which should be converted to JSONP format.
     * @param {IClassName} [$interface] Specify custom interface for which will be appended by static Data method.
     * @param {IClassName[]} [$imports] Specify array of classes, which will be used as imports for JSONP content.
     * @returns {string} Returns string in JSONP format.
     */
    public static ToJsonp($source : any, $interface? : IClassName, $imports? : IClassName[]) : string {
        let imports : string = "";
        if (!ObjectValidator.IsEmptyOrNull($imports)) {
            $imports.forEach(($import : any) : void => {
                imports += "var " + $import.ClassNameWithoutNamespace() + " = " + $import.ClassName() + ";\n";
            });
            imports += "\n";
        }
        let scope : string;
        if (ObjectValidator.IsEmptyOrNull($interface)) {
            scope = "JsonpData";
        } else {
            scope = (<any>$interface).ClassName() + ".Data";
        }
        const serialize : any = ($input : any, $prefix : string) : string => {
            let output : string = "";
            let property : string;
            for (property in $input) {
                if ($input.hasOwnProperty(property)) {
                    if (!ObjectValidator.IsEmptyOrNull(output)) {
                        output += ",\n";
                    }
                    if (ObjectValidator.IsObject($input[property])) {
                        output += $prefix + property + ": " + serialize($input[property], $prefix + "   ");
                    } else if (ObjectValidator.IsFunction($input[property])) {
                        output += $prefix + property + ": " + Convert.FunctionToString($input[property]);
                    } else {
                        output += $prefix + property + ": " + $input[property];
                    }
                }
            }
            return "{\n" + output + "\n}";
        };
        return imports + scope + "(" + serialize($source, "    ") + ");\n";
    }

    public static ToString($value : any, $prefix : string = "", $htmlTag : boolean = true) : string {
        return $prefix + JSON.stringify($value, null, 2);
    }

    /**
     * @param {object} $value Specify data object, which should be sorted by keys. Array items order is untouched only array object
     * internal properties will be sorted.
     * @returns {object} Returns sorted JSON object.
     */
    public static Sort($value : any) : any {
        let index : number = 0;
        if (ObjectValidator.IsEmptyOrNull($value)) {
            return $value;
        } else if (ObjectValidator.IsSet($value.forEach)) {
            for (index; index < $value.length; index++) {
                $value[index] = <any>this.Sort($value[index]);
            }
            return $value;
        } else if (typeof $value !== "object") {
            return $value;
        }
        let keys : string[] = Object.keys($value);
        if (keys.length === 0) {
            return $value;
        }
        keys = keys.sort();
        const newObject = {};
        for (index = 0; index < keys.length; index++) {
            newObject[keys[index]] = this.Sort($value[keys[index]]);
        }
        return newObject;
    }
}
