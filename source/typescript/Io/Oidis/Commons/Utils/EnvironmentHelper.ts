/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

const isBrowser : boolean = globalThis.isBrowser;
const buildTime : string = globalThis.buildTime;
const appExceptionHandler : ($message : string, $stack : string) => void = globalThis.appExceptionHandler;

export { isBrowser, buildTime, appExceptionHandler };
