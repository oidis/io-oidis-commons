/* ********************************************************************************************************* *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-<? @var build.year ?> Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
let isBrowser = true;
try {
    if (global) {
        isBrowser = false;
    }
} catch (ex) { // eslint-disable-line no-unused-vars
    // do nothing already detected as browser
}
try {
    if (window) {
        isBrowser = true;
    }
} catch (ex) { // eslint-disable-line no-unused-vars
    isBrowser = false;
}
((isBrowser) => {
    if (isBrowser) {
        let exception = "";
        let stackIndex = 0;
        globalThis.appExceptionHandler = function ($message, $stack) {
            if (exception === "") {
                console.log("Package \"<? @var clientConfig.packageName ?>\" has not been loaded.");
                exception =
                    "<div class=\"Exception\">" +
                    "   <h1>FATAL Error!</h1>" +
                    "   <div class=\"Message\">";
            }
            console.log($message);
            exception += $message;
            if ($stack !== undefined && $stack !== "" && $stack !== null) {
                console.error($stack);
                exception +=
                    "<br>" +
                    "       <span onclick=\"" +
                    "document.getElementById('exceptionItem_" + stackIndex + "').style.display=" +
                    "document.getElementById('exceptionItem_" + stackIndex + "').style.display===" +
                    "'block'?'none':'block';\" " +
                    "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana;\">" +
                    "Stack trace" +
                    "</span>" +
                    "       <div id=\"exceptionItem_" + stackIndex + "\" style=\"border: 0 solid black; display: none;\">" +
                    $stack +
                    "</div>";
                stackIndex++;
            }
            exception += "<br><br>";

            let content = document.getElementById("Content");
            try {
                if (!content) {
                    content = document.body;
                }
                content.innerHTML =
                    exception +
                    "   </div>" +
                    "</div>";
            } catch (ex) {
                console.log(ex.message);
                if (ex.stack) {
                    console.log(ex.stack);
                }
            }
        };
        window.onerror = function (message, filename, lineno, colno, error) {
            message =
                "       <b>" + message + "</b><br>" +
                "       file: " + filename + "<br>" +
                "       at line: " + lineno;
            let stack = null;
            if (error) {
                message += ":" + colno;
                stack = error.stack;
            }
            globalThis.appExceptionHandler(message, stack);
            return true;
        };
        globalThis.require = false;
        globalThis.module = {};
        globalThis.Buffer = {};
        globalThis.isBrowser = isBrowser;
        globalThis.JsonpData = function () {
        };
        globalThis.buildTime = "<? @var build.timestamp ?>";
        const bundleInject = document.createElement("script");
        bundleInject.src = "resource/javascript/<? @var clientConfig.packageName ?>.js?v=" + globalThis.buildTime;
        bundleInject.type = "text/javascript";
        bundleInject.nonce = globalThis.nonce;
        bundleInject.onerror = ($error) => {
            const error = new Error($error.message !== undefined ? $error.message : $error);
            globalThis.appExceptionHandler(error.message, error.stack);
        };
        bundleInject.onload = () => {
            try {
                let loader = globalThis;
                let loaderClass = "<? @var project.guiLoaderClass ?>";
                if (loaderClass.startsWith("<? @")) {
                    loaderClass = "<? @var clientConfig.loaderClass ?>";
                }
                loaderClass.split(".").forEach(($namespace) => {
                    if (loader.hasOwnProperty($namespace)) {
                        loader = loader[$namespace];
                    }
                });
                let clientConfig = JSON.parse("<? @var clientConfig ?>");
                if (globalThis.clientConfig !== undefined) {
                    clientConfig = globalThis.clientConfig;
                }
                loader.Load(clientConfig);
            } catch (ex) {
                globalThis.appExceptionHandler(ex.message, ex.stack);
            }
        };
        window.document.body.appendChild(bundleInject);
    } else {
        let beCodeMark = "beLoaderStart"; // mark start of BE code which should be stripped in FE version provided by BE resolver
        const modulesDir = "./../esmodules-<? @var build.timestamp ?>";
        let fs, path, url;
        import("node:fs").then(($imports) => {
            fs = $imports;
            return import("node:path");
        }).then(($imports) => {
            path = $imports;
            return import("node:url");
        }).then(($imports) => {
            url = $imports;
            return import("module");
        }).then(($imports) => {
            const {createRequire} = $imports;
            const require = createRequire(import.meta.url);

            const nodejsPath = path.normalize(path.dirname(process.execPath)).replace(/\\/g, "/");
            globalThis.appExceptionHandler = function ($message, $stack) {
                console.log($message);
                console.error($stack);
            };
            const processFatalError = (ex) => {
                globalThis.appExceptionHandler("Package \"<? @var clientConfig.packageName ?>\" has not been loaded.", ex.stack);
                fs.writeFileSync(nodejsPath + "/error.stack.log", ex.stack);
                process.exit(1);
            };
            try {
                if (process.env.NODE_PATH === undefined || process.env.NODE_PATH === "" || !fs.existsSync(process.env.NODE_PATH)) {
                    process.env.NODE_PATH = path.normalize(path.dirname(process.argv0) + "/build/node_modules").replace(/\\/g, "/");
                }
                if (!fs.existsSync(process.env.NODE_PATH)) {
                    console.error("Unable to find NODE_PATH at \"" + process.env.NODE_PATH + "\", please specify existing folder.");
                    process.exit(1);
                }
                const __filename = url.fileURLToPath(import.meta.url);
                const __dirname = url.fileURLToPath(new URL(".", import.meta.url));
                const nodeModulesPath = process.env.NODE_PATH;

                const sourceFile = path.join(__dirname + "/../esmodules-<? @var build.timestamp ?>", "reference.js");
                const sourceMap = sourceFile + ".map";
                if (!fs.existsSync(sourceMap)) {
                    if (process.argv.indexOf("--debug") !== -1) {
                        console.log("Map file \"" + sourceMap + "\" not found. Running without stacktrace remap support.");
                    }
                } else {
                    const contentMap = JSON.parse(fs.readFileSync(sourceMap).toString());
                    require(nodeModulesPath + "/source-map-support").install({
                        environment: "node",
                        handleUncaughtExceptions: true,
                        retrieveSourceMap: function ($source) {
                            if ($source === sourceFile || $source.indexOf("evalmachine") !== -1) {
                                return {
                                    map: contentMap,
                                    url: sourceMap
                                };
                            }
                            return null;
                        }
                    });
                }
                process.nodejsRoot = nodejsPath;
                require(nodeModulesPath + "/reflect-metadata");

                globalThis.window = {};
                globalThis.__filename = __filename;
                globalThis.__dirname = __dirname;
                globalThis.require = function ($id) {
                    try {
                        return require($id);
                    } catch (ex) { // eslint-disable-line no-unused-vars
                        return require(nodeModulesPath + "/" + $id);
                    }
                };
                globalThis.WebSocket = {};
                globalThis.nodejsRoot = nodejsPath;
                globalThis.document = {};
                globalThis.localStorage = {};
                globalThis.JsonpData = function () {
                };
                globalThis.Reflect = Reflect;
                globalThis.console = console;
                globalThis.console.clear = function () {
                };
                globalThis.ErrorEvent = Error;
                globalThis.buildTime = "<? @var build.timestamp ?>";
                globalThis.clientConfig = "<? @var clientConfig ?>";
                globalThis.embedFeatures = "<? @var project.target.embedFeatures ?>".split(",");
                ["underline", "red", "green", "yellow"].forEach(($name) => {
                    Object.defineProperty(String.prototype, $name, {configurable: true});
                });
                import(modulesDir + "/" + ("<? @var serverConfig.loaderClass ?>").replace(/\./gm, "/") + ".js")
                    .then(($imports) => {
                        const {Loader} = $imports;
                        Loader.Load(JSON.parse("<? @var serverConfig ?>"));
                    })
                    .catch((ex) => {
                        processFatalError(ex);
                    });
            } catch (ex) {
                processFatalError(ex);
            }
        }).catch((ex) => {
            console.log("Unable to import core modules.");
            console.error(ex.stack);
            process.exit(1);
        });

        // mark end of BE code which withstand also minification, do not modify!
        beCodeMark = "beLoaderEnd";
        if (beCodeMark !== "") {
            beCodeMark = undefined;
        }
    }
})(isBrowser);
