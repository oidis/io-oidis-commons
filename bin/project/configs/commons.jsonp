/* ********************************************************************************************************* *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
Io.Oidis.Builder.DAO.Resources.Data({
    $interface: "IProjectSolution",
    guiLoaderClass: "<? @var project.loaderClass ?>",
    dbConfigs: {
        dbName: "<? @var project.name ?>"
    },
    dependencies: {
        sentry: {
            location: {
                type: "cdi",
                files: [
                    {
                        src: "https://browser.sentry-cdn.com/7.19.0/bundle.min.js",
                        dest: "sentry.min.js"
                    }
                ]
            }
        }
    },
    target: {
        toolchainSettings: {
            version: "module",
            serverless: true
        },
        resources: {
            bootstrap: {
                input: "resource/css/BootstrapPatch.css",
                output: "resource/libs/Bootstrap/bootstrap.min.css",
                skipReplace: true
            },
            sentry: {
                input: "dependencies/sentry/*",
                output: "resource/libs/Sentry/*",
                skipReplace: true
            },
            packageLock: {
                input: "build_cache/package.lock.json",
                output: "package.lock.json"
            }
        },
        requestPatterns: {
            bootstrap: "/resource/libs/Bootstrap/*",
            sentry: "/resource/libs/Sentry/*"
        },
        embedFeatures: {
            commonsFeatures: [
                "project.dbConfigs.dbName",
                "project.sentry"
            ]
        }
    },
    noServer: true,
    sentry: {
        dsn: "",
        environment: "",
        tunnel: "",
        tracesSampleRate: 0,
        useStrictSSL: true,
        debug: false,
        tags: {},
        maxMessageLength: 10 * 1024,
        cspReportTunnel: false
    }
});
