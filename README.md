# io-oidis-commons

> Commons library for Oidis Framework TypeScript projects

## Requirements

This library does not have any special requirements but it depends on the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder). See the Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`oidis docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2022.2.0
Fixed parallel jsonp processing issue and some minor issues in URI parser.
### v2022.1.1
Fixed AsyncBasePageController resolver detection.
### v2022.1.0
Updated logs for WS. Enabled async resolver for HttpResolvers and enabled ErrorPage resolver on backend services.
### v2022.0.0
BugFixes for deprecated reload attribute and JsonUtils data mangling. Added full support for native error objects detection in LogIt.
### v2021.2.0
Updated support for native async promise. Refactored logger. Several minor bugfixes and enhancements.
### v2021.0.0
BugFix for reconnect counter called by external StartCommunication invocation.
### v2020.3.0
Bugfix for jsonp file reader error handling.
### v2020.1.0
Fixed issue in Convert.ToFixed() conversion method. Use template type for Array.Contains().
### v2020.0.0
Identity update. Change of configuration files format.
### v2019.3.0
Refactoring of namespaces. Migration to gitlab. Initial update of identity for project fork. 
Memory leak bug fixes and performance enhancements.
### v2019.1.0
Updated runtime tests environment. Enable to use external typedefs. Enable to detect EDGE browser. 
Added ability to serialize objects with circular structure.
### v2019.0.2
Added ability to use package configuration on front-end. Usage of EnvironmentArgs defined by app loader.
### v2019.0.1
Enable to detect WUI Localhost RE. Enable to use log severity for info messages. Removed Alpha and Beta checks. 
Fixes persistence based on session id. Enable to control websocket client reconnection. Use native chunks for websocket protocol.
### v2019.0.0
Added ability to consume general error object. Added ability to override jsonp source path. Added Contains API for native arrays.
### v2018.3.0
Updates and clean up required by usage in WUI Builder. Synchronization with new WUI Hub. Bug fix for HTTP globing patterns resolver. 
String class moved to StringUtils. Tests update.
### v2018.2.0
Added new conversion methods. Fixed resolving of http globing patterns. Fixed IsEmpty validation for instances with incorrect interface. 
Convert of docs to TypeDoc.
### v2018.1.3
Added more robust and CORS free online validation. Fixed appName cache.
### v2018.1.2
Fixed normalization of url double slashes. Enhancements for RuntimeTestRunner.
### v2018.1.1
Fixed tests for updated core. API clean up focused on better override of Loader.
### v2018.1.0
Refactoring of Commons core for ability to support multithreading architecture. 
### v2018.0.2
Added ToCamelCase method to String class. Update of RuntimeTests UI for ability to jump over failed assertions. 
Decrease of chunk size required by communication with WUI Connector.
### v2018.0.1
Added support for load of jsonp data from string. Added method filter for RuntimeTests. Fixed issue with persistence loading.
### v2018.0.0
RuntimeTestRunner supports emit of status events now. Updates in Webservice API required by SSL communication. 
Change version format.
### v2.2.0
Increase of code coverage and tests update required by new test environment. Update of SCR and change of history ordering.
### v2.1.1
BugFix for JSON fallback. Increase of code coverage with usage of FileAPI and based on single coverage results. Fixed integrity tests.
### v2.1.0
Added JxBrowser client. Usage of new UnitTestRunner with better sandboxing.
### v2.0.3
Usage of UnitTestRunner PromiseAPI. Tests bug fixes.
### v2.0.2
Increase of code coverage with support of jsdom. Bug fixes for JsonpFileReader. Added support for fully async runtime tests.
### v2.0.1
Increase of code coverage. Added more debugging options. Update of TypeScript syntax.
### v2.0.0
Namespaces refactoring.
### v1.1.0
Changed the licence from proprietary to BSD-3-Clause.
### v1.0.8
Usage of new version compiler features—mainly protected, lambda expression, and multi-type arguments. 
### v1.0.7
Usage of a stand-alone builder.
### v1.0.6
Package renamed from com-freescale-mvc-commons to com-freescale-wui-commons and minor bug fixes.
### v1.0.5
Updated the Grunt build tool and enhancements to support the extended GUI libraries.
### v1.0.4
Implemented the asynchronous processing and utils to support the GUI libraries.
### v1.0.3
Implemented the Reflection and utils to support the GUI libraries.
### v1.0.2
Implemented the advanced utils, Events, and Exceptions manager.
### v1.0.1
Implemented the Http resolver, Primitives, IOHandlers, and basic utils.
### v1.0.0
Initial release.

## License

This software is owned or controlled by Oidis. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Copyright 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright 2017-2019 [NXP](http://nxp.com/), 
Copyright 2019-2025 [Oidis](https://www.oidis.io/)
