/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseUnitTestRunner } from "./BaseUnitTestRunner.js";

import { EnvironmentArgs } from "../../../../../../source/typescript/Io/Oidis/Commons/EnvironmentArgs.js";
import { Loader } from "../../../../../../source/typescript/Io/Oidis/Commons/Loader.js";
import { ObjectDecoder } from "../../../../../../source/typescript/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectEncoder } from "../../../../../../source/typescript/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "../../../../../../source/typescript/Io/Oidis/Commons/Utils/ObjectValidator.js";

export class UnitTestEnvironmentArgs extends EnvironmentArgs {
    public Load($appConfig : any, $handler : () => void) : void {
        super.Load($appConfig, $handler);
    }

    public HtmlOutputAllowed($value? : boolean) : boolean {
        return false;
    }

    protected getConfigPaths() : string[] {
        return [];
    }
}

export class ClientUnitTestEnvironmentArgs extends UnitTestEnvironmentArgs {
    public HtmlOutputAllowed($value? : boolean) : boolean {
        return true;
    }
}

export class UnitTestLoader extends Loader {
    protected initEnvironment() : UnitTestEnvironmentArgs {
        return new UnitTestEnvironmentArgs();
    }
}

export class ClientUnitTestLoader extends UnitTestLoader {
    protected initEnvironment() : ClientUnitTestEnvironmentArgs {
        return new ClientUnitTestEnvironmentArgs();
    }
}

export class UnitTestRunner extends BaseUnitTestRunner {
    private static singletons : any;
    protected globalCache : any;

    constructor() {
        super();
        if (ObjectValidator.IsEmptyOrNull(UnitTestRunner.singletons)) {
            UnitTestRunner.singletons = {
                getBase64Decoder: (<any>ObjectDecoder).getBase64Decoder,
                getBase64Encoder: (<any>ObjectEncoder).getBase64Encoder
            };
        }
        this.globalCache = UnitTestRunner.singletons;
    }

    protected initLoader() : void {
        super.initLoader(UnitTestLoader);
    }
}
