/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseHttpResolver } from "../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import "../../../../../../source/typescript/Io/Oidis/Commons/Utils/ReflectionEmitter.js";

export interface IAssert {
    fail($actual : any, $expected : any, $message : string, $operator : string) : void;

    assert($value : any, $message : string) : void;

    ok($value : any, $message? : string) : void;

    equal($actual : any, $expected : any, $message? : string) : void;

    notEqual($actual : any, $expected : any, $message? : string) : void;

    deepEqual($actual : any, $expected : any, $message? : string) : void;

    notDeepEqual($actual : any, $expected : any, $message? : string) : void;

    strictEqual($actual : any, $expected : any, $message? : string) : void;

    notStrictEqual($actual : any, $expected : any, $message? : string) : void;

    throws($block : any, $error? : any, $message? : string) : void;

    doesNotThrow($block : any, $error? : any, $message? : string) : void;

    ifError($value : any) : void;

    onRedirect($setUp : () => void, $resolve : ($eventArgs : any) => void, $done : () => void, $urlBase? : string) : void;

    resolveEqual($className : any, $expected : string, $eventArgs? : any, $message? : string) : BaseHttpResolver;

    patternEqual($actual : any, $expected : any, $message? : string) : void;

    doesHandleException($block : any, $error? : any, $message? : string) : void;

    onGuiComplete($instance : any, $resolve : () => void, $done : () => void, $owner? : any) : void;
}

export interface IVBoxSession {
    Copy($sourcePath : string, $destinationPath : string) : IVBoxPromise;

    Execute($filePath : string) : IVBoxPromise;
}

export interface IVBoxPromise {
    Then($callback : ($status : boolean, $session? : IVBoxSession) => void) : void;
}

export interface IVBoxManager {
    Start($machineNameOrId : string, $user? : string, $pass? : string) : IVBoxPromise;
}

export const __unitTestRunner : any = globalThis.__unitTestRunner;

export const assert : IAssert = globalThis.assert;
export const isBrowserRun : boolean = globalThis.isBrowserRun;
export const selenium : any = globalThis.selenium;
export const chromeBuilder : any = globalThis.chromeBuilder;
export const jsdom : any = globalThis.jsdom;
export const vbox : IVBoxManager = globalThis.vbox;
export const builder : any = globalThis.builder;
export const nodeVM : any = globalThis.nodeVM;

export const NodeFile : any = globalThis.NodeFile;

export interface ILocation {
    headers : string;
    protocol : string;
    port : string;
}

export const ColorType : any = globalThis.ColorType;

// generated-code-start
/* eslint-disable */
export const IAssert = globalThis.RegisterInterface(["fail", "assert", "ok", "equal", "notEqual", "deepEqual", "notDeepEqual", "strictEqual", "notStrictEqual", "throws", "doesNotThrow", "ifError", "onRedirect", "resolveEqual", "patternEqual", "doesHandleException", "onGuiComplete"]);
export const IVBoxSession = globalThis.RegisterInterface(["Copy", "Execute"]);
export const IVBoxPromise = globalThis.RegisterInterface(["Then"]);
export const IVBoxManager = globalThis.RegisterInterface(["Start"]);
export const ILocation = globalThis.RegisterInterface(["headers", "protocol", "port"]);
/* eslint-enable */
// generated-code-end
