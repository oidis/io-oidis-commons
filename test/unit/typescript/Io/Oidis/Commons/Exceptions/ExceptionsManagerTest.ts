/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestLoader, UnitTestRunner } from "../UnitTestRunner.js";

import { LogLevel } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/LogLevel.js";
import { ExceptionsManager } from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { ErrorPageException } from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/Type/ErrorPageException.js";
import { Exception } from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/Type/Exception.js";
import { FatalError } from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/Type/FatalError.js";
import {
    IllegalArgumentException
} from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/Type/IllegalArgumentException.js";
import { NullPointerException } from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/Type/NullPointerException.js";
import { OutOfRangeException } from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/Type/OutOfRangeException.js";
import { ResolverFatalException } from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/Type/ResolverFatalException.js";
import { ArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { LogIt } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/LogIt.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";

export class ExceptionsManagerTest extends UnitTestRunner {

    @Test()
    public ThrowMessage() : void {
        try {
            ExceptionsManager.Throw("test", "string message");
        } catch (ex) {
            assert.equal(ExceptionsManager.getAll().ToString("", false),
                "Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
                "[ 0 ]    string message\r\n");
        }
    }

    @Test()
    public ThrowError() : void {
        try {
            ExceptionsManager.Throw("test", new Error("error message"));
        } catch (ex) {
            assert.patternEqual(
                ExceptionsManager.getAll().ToString("", false),
                "Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
                "[ 0 ]    error message, stack: \r\n" +
                "Error: error message\r\n" +
                "    at ExceptionsManagerTest.ThrowError (*:*:*)\r\n" +
                "    at *:*:*\r\n");
        }
    }

    @Test()
    public ThrowFatal() : void {
        try {
            ExceptionsManager.Throw("test", new ResolverFatalException());
        } catch (ex) {
            assert.equal(ExceptionsManager.getAll().ToString("", false),
                "Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
                "[ 0 ]    ResolverFatalException - RESOLVER_EXCEPTION (6)\r\n");
        }
    }

    @Test()
    public ThrowSelferror() : void {
        try {
            ExceptionsManager.Throw("test", new ErrorPageException());
        } catch (ex) {
            assert.equal(ExceptionsManager.getAll().ToString("", false),
                "Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
                "[ 0 ]    ErrorPageException - ERROR_PAGE_EXCEPTION (5)\r\n");
        }
    }

    @Test()
    public ThrowExit() : void {
        try {
            assert.ok(true, "This line should be reachable");
            ExceptionsManager.ThrowExit();
            assert.ok(false, "This line should be unreachable");
        } catch (ex) {
            assert.ok(ExceptionsManager.getAll().IsEmpty());
        }
    }

    @Test()
    public IsNativeException() : void {
        assert.ok(ExceptionsManager.IsNativeException(new Error()));
        assert.ok(ExceptionsManager.IsNativeException("message"));
        try {
            ExceptionsManager.ThrowExit();
        } catch (ex) {
            assert.ok(!ExceptionsManager.IsNativeException(ex));
        }
        try {
            ExceptionsManager.Throw("test", "message");
        } catch (ex) {
            assert.ok(!ExceptionsManager.IsNativeException(ex));
        }
    }

    @Test()
    public getAll() : void {
        try {
            ExceptionsManager.Throw("test", "message1");
        } catch (ex) {
            // register exception
        }
        try {
            ExceptionsManager.Throw("test", "message2");
        } catch (ex) {
            // register exception
        }
        assert.equal(ExceptionsManager.getAll().Length(), 2);
        assert.equal(ExceptionsManager.getAll(), (<any>ExceptionsManager).exceptionList);

        const list : ArrayList<any> = (<any>ExceptionsManager).exceptionList;
        const propertyName : string = "exceptionList";
        delete ExceptionsManager[propertyName];
        assert.equal(ExceptionsManager.getAll().Length(), 0);
        (<any>ExceptionsManager).exceptionList = list;
    }

    @Test()
    public getLast() : void {
        try {
            ExceptionsManager.Throw("test", "message1");
        } catch (ex) {
            // register exception
        }
        try {
            ExceptionsManager.Throw("test", "message2");
        } catch (ex) {
            // register exception
        }
        assert.equal(ExceptionsManager.getAll().Length(), 2);
        assert.equal(ExceptionsManager.getLast(), (<any>ExceptionsManager).exceptionList.getLast());

        const list : ArrayList<any> = (<any>ExceptionsManager).exceptionList;
        const propertyName : string = "exceptionList";
        delete ExceptionsManager[propertyName];
        assert.equal(ExceptionsManager.getLast(), null);
        (<any>ExceptionsManager).exceptionList = list;
    }

    @Test()
    public HandleExceptionMessage() : void {
        LogIt.setLevel(LogLevel.ERROR);
        UnitTestLoader.Load(<any>{
            build: {time: new Date().toTimeString(), type: "prod"}, name: "com-wui-framework-commons", version: "1.0.0"
        });

        try {
            ExceptionsManager.Throw("test", "message");
        } catch (ex) {
            assert.doesNotThrow(() : void => {
                ExceptionsManager.HandleException(ex);
            });
        }
        this.initSendBox();
    }

    @Test()
    public HandleExceptionWithMoreInfo() : void {
        try {
            ExceptionsManager.Throw("test", "message", 58, "ExceptionManagerTest", 120);
        } catch (ex) {
            assert.doesNotThrow(() : void => {
                ExceptionsManager.HandleException(ex);
            });
        }
    }

    @Test(true)
    public HandleExceptionNative() : void {
        try {
            throw new Error("error message exception");
        } catch (ex) {
            assert.doesNotThrow(() : void => {
                ExceptionsManager.HandleException(ex);
            });
        }
    }

    @Test()
    public HandleExceptionExit() : void {
        try {
            ExceptionsManager.ThrowExit();
        } catch (ex) {
            assert.doesNotThrow(() : void => {
                ExceptionsManager.HandleException(ex);
            });
        }
    }

    @Test()
    public ToString1() : void {
        try {
            ExceptionsManager.Throw("test", new Error("message"));
        } catch (ex) {
            // register exception
        }

        assert.patternEqual(ExceptionsManager.ToString(),
            "<h1>Oops, something went wrong...</h1>" +
            "thrown by: <b>test</b>: <br/>" +
            "message<br/>" +
            "<span onclick=\"" +
            "document.getElementById('ContentBlock_0').style.display=" +
            "document.getElementById('ContentBlock_0').style.display==='block'?'none':'block';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Stack trace</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">Error: message<br/>" +
            "    at ExceptionsManagerTest.ToString1 (*:*:*)<br/>" +
            "    at *<br/>" +
            "*</span><br/>");
        this.resetCounters();
        assert.patternEqual(ExceptionsManager.ToString("", false),
            "Oops, something went wrong...\r\n" +
            "thrown by: test\r\n" +
            "message, stack: \r\n" +
            "Error: message\r\n" +
            "    at ExceptionsManagerTest.ToString1 (*:*:*)\r\n" +
            "    at *\r\n");
    }

    @Test()
    public ToStringWithFatal() : void {
        try {
            ExceptionsManager.Throw("test", new FatalError("message"));
        } catch (ex) {
            // register exception
        }
        assert.equal(ExceptionsManager.ToString(),
            "<h1>FATAL Error!</h1>" +
            "thrown by: <b>test</b>: <br/>" +
            "message<br/><br/>");
        this.resetCounters();
        assert.equal(ExceptionsManager.ToString("", false),
            "FATAL Error!\r\n" +
            "thrown by: test\r\n" +
            "message\r\n");
    }

    @Test()
    public ToString2() : void {
        try {
            ExceptionsManager.Throw("some owner", null, null);
        } catch (ex) {
            // register exception
        }
        assert.equal(ExceptionsManager.ToString(""), "");
    }

    @Test()
    public ToString3() : void {
        this.resetCounters();
        try {
            ExceptionsManager.Throw(null, "string message");
        } catch (ex) {
            assert.deepEqual((<any>ExceptionsManager).exceptionList.ToString(),
                "<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;string message<br/><br/></span>");
        }
    }

    @Test()
    public ToStringNullExeptionList() : void {
        this.resetCounters();
        const propertyName : string = "exceptionList";
        delete ExceptionsManager[propertyName];
        assert.equal(ExceptionsManager.ToString(""), "");
    }

    @Test()
    public ToString4() : void {
        const exception : Exception = new Exception();
        const exception1 : Exception = new Exception();
        const exception2 : ErrorPageException = new ErrorPageException();
        const exception3 : IllegalArgumentException = new IllegalArgumentException();
        const exception4 : NullPointerException = new NullPointerException();
        const exception5 : ResolverFatalException = new ResolverFatalException();

        const exceptionList : ArrayList<Exception> = new ArrayList<Exception>();
        exceptionList.Add(exception, 0);
        exceptionList.Add(exception1, 1);
        exceptionList.Add(exception2, 2);
        exceptionList.Add(exception3, 3);
        exceptionList.Add(exception4, 4);
        exceptionList.Add(exception5, 5);

        const storage : any = (<any>ExceptionsManager).exceptionList;
        (<any>ExceptionsManager).exceptionList = exceptionList;
        this.resetCounters();
        assert.equal((<any>ExceptionsManager).exceptionList.ToString(),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i>" +
            " <span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 4 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 5 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/></span>");
        (<any>ExceptionsManager).exceptionList = storage;
    }

    @Test()
    public HandleException() : void {
        const error : Error = new Error("this is my message");
        const exception : Exception = new Exception();
        const exception1 : Exception = new Exception();
        const exception2 : ErrorPageException = new ErrorPageException();
        const exception3 : IllegalArgumentException = new IllegalArgumentException();
        const exception4 : NullPointerException = new NullPointerException();
        const exception5 : ResolverFatalException = new ResolverFatalException();
        const exception6 : ErrorPageException = new ErrorPageException();
        const exception7 : IllegalArgumentException = new IllegalArgumentException();
        const exception8 : NullPointerException = new NullPointerException();
        const exception9 : OutOfRangeException = new OutOfRangeException();
        const exception10 : ResolverFatalException = new ResolverFatalException();
        const exception11 : Exception = new Exception("Try again");
        const exception12 : Exception = new Exception("Exception");
        const exception13 : Exception = new Exception("Wrong arguments");
        const exception14 : OutOfRangeException = new OutOfRangeException();
        const exception15 : NullPointerException = new NullPointerException();

        const exceptionList : ArrayList<Exception> = new ArrayList<Exception>();
        exceptionList.Add(exception, 0);
        exceptionList.Add(exception1, 1);
        exceptionList.Add(exception2, 2);
        exceptionList.Add(exception3, 3);
        exceptionList.Add(exception4, 4);
        exceptionList.Add(exception5, 5);
        exceptionList.Add(exception6, 6);
        exceptionList.Add(exception7, 7);
        exceptionList.Add(exception8, 8);
        exceptionList.Add(exception9, 9);
        exceptionList.Add(exception10, 10);
        exceptionList.Add(exception11, 11);
        exceptionList.Add(exception12, 12);
        exceptionList.Add(exception13, 13);
        exceptionList.Add(exception14, 14);
        exceptionList.Add(exception15, 15);

        (<any>ExceptionsManager).exceptionList = exceptionList;
        assert.equal((<any>ExceptionsManager).exceptionList.ToString(),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i>" +
            " <span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 4 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 5 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 6 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 7 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 8 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 9 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 10 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 11 ]&nbsp;&nbsp;&nbsp;&nbsp;Try again<br/><br/>" +
            "[ 12 ]&nbsp;&nbsp;&nbsp;&nbsp;Exception<br/><br/>" +
            "[ 13 ]&nbsp;&nbsp;&nbsp;&nbsp;Wrong arguments<br/><br/>" +
            "[ 14 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
            "[ 15 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/></span>");
    }

    @Test(true)
    public ToString20() : void {
        const error : Error = new Error("this is my message");
        const exception : Exception = new Exception();
        const exception1 : Exception = new Exception();
        const exception2 : ErrorPageException = new ErrorPageException();
        const exception3 : IllegalArgumentException = new IllegalArgumentException();
        const exception4 : NullPointerException = new NullPointerException();
        const exception5 : IllegalArgumentException = new IllegalArgumentException();
        const exception6 : ErrorPageException = new ErrorPageException();
        const exception7 : IllegalArgumentException = new IllegalArgumentException();
        const exception8 : NullPointerException = new NullPointerException();
        const exception9 : OutOfRangeException = new OutOfRangeException();
        const exception10 : NullPointerException = new NullPointerException();
        const exception11 : Exception = new Exception();
        const exception12 : Exception = new Exception();
        const exception13 : Exception = new Exception();
        const exception14 : OutOfRangeException = new OutOfRangeException();
        const exception15 : NullPointerException = new NullPointerException();

        const exceptionList : ArrayList<Exception> = new ArrayList<Exception>();
        exceptionList.Add(exception, 0);
        exceptionList.Add(exception1, 1);
        exceptionList.Add(exception2, 2);
        exceptionList.Add(exception3, 3);
        exceptionList.Add(exception4, 4);
        exceptionList.Add(exception5, 5);
        exceptionList.Add(exception6, 6);
        exceptionList.Add(exception7, 7);
        exceptionList.Add(exception8, 8);
        exceptionList.Add(exception9, 9);
        exceptionList.Add(exception10, 10);
        exceptionList.Add(exception11, 11);
        exceptionList.Add(exception12, 12);
        exceptionList.Add(exception13, 13);
        exceptionList.Add(exception14, 14);
        exceptionList.Add(exception15, 15);

        const storage : any = (<any>ExceptionsManager).exceptionList;
        (<any>ExceptionsManager).exceptionList = exceptionList;
        try {
            throw new Error("error message exception");
        } catch (ex) {
            assert.doesNotThrow(() : void => {
                ExceptionsManager.HandleException(ex);
            });
            this.resetCounters();
            assert.equal(ExceptionsManager.ToString(), "<h1>Oops, something went wrong...</h1>thrown by: <b></b>: <br/>" +
                "<br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/>" +
                "<br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/>" +
                "<br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/>" +
                "<br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/>" +
                "<br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/><br/><br/><br/>" +
                "... and more 2 exceptions not printed");
            this.resetCounters();
            assert.patternEqual(
                (<any>ExceptionsManager).exceptionList.ToString(),
                "<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 4 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 5 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 6 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 7 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 8 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 9 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 10 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 11 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 12 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 13 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 14 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 15 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 16 ]&nbsp;&nbsp;&nbsp;&nbsp;error message exception<br/>" +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_1\').style.display=" +
                "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Stack trace</span>" +
                "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">" +
                "Error: error message exception<br/>    " +
                "at ExceptionsManagerTest.testToString20 (*)<br/>" +
                "    at ExceptionsManagerTest.UnitTestRunner.handleWuiExceptions (*)<br/>" +
                "    at Test.test (*)<br/>" +
                "    at *<br/>" +
                "    at *<br/>" +
                "    at runCallbacks (*)<br/>" +
                "    at *<br/>" +
                "    at run (*)<br/>" +
                "    at *<br/>" +
                "    at _combinedTickCallback (internal/process/next_tick.js:*)</span><br/></span>");
            (<any>ExceptionsManager).exceptionList = storage;
        }
    }

    @Test(true)
    public ToString30() : void {
        const exceptionList : ArrayList<Exception> = new ArrayList<Exception>();
        const storage : any = (<any>ExceptionsManager).exceptionList;
        (<any>ExceptionsManager).exceptionList = exceptionList;

        assert.equal(ExceptionsManager.ToString(), "");
        assert.equal((<any>ExceptionsManager).exceptionList.ToString(),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i>" +
            " <span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "Data object <b>EMPTY</b></span>");
        (<any>ExceptionsManager).exceptionList = storage;
    }

    @Test(true)
    public HandleException10() : void {
        const error : Error = new Error("this is my message");
        const data : ArrayList<any> = new ArrayList<any>();
        data.Add("test", "key");
        this.getHttpManager().ReloadTo("/some/other/link", data, true);
        ExceptionsManager.HandleException(error);
        this.resetCounters();
        assert.patternEqual(ExceptionsManager.ToString(""), "");
    }

    protected setUp() : void {
        ExceptionsManager.Clear();
        this.resetCounters();
    }

    protected tearDown() : void {
        ExceptionsManager.Clear();
    }
}
