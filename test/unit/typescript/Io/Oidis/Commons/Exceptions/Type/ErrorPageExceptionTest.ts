/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionCode } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Enums/ExceptionCode.js";
import { ErrorPageException } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/Type/ErrorPageException.js";
import { Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class ErrorPageExceptionTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        assert.doesNotThrow(() : void => {
            const instance : ErrorPageException = new ErrorPageException();
        });
        const exception : ErrorPageException = new ErrorPageException("message");
        assert.equal(exception.Message(), "message");
    }

    @Test()
    public Code() : void {
        const exception : ErrorPageException = new ErrorPageException();
        assert.equal(exception.Code(), ExceptionCode.ERROR_PAGE_EXCEPTION);
    }
}
