/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionCode } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Enums/ExceptionCode.js";
import {
    ResolverFatalException
} from "../../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/Type/ResolverFatalException.js";
import { Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class ResolverFatalExceptionTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        assert.doesNotThrow(() : void => {
            const instance : ResolverFatalException = new ResolverFatalException();
        });
        const exception : ResolverFatalException = new ResolverFatalException("message");
        assert.equal(exception.Message(), "message");
    }

    @Test()
    public Code() : void {
        const exception : ResolverFatalException = new ResolverFatalException();
        assert.equal(exception.Code(), ExceptionCode.RESOLVER_EXCEPTION);
    }
}
