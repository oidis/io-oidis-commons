/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionCode } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Enums/ExceptionCode.js";
import { Exception } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/Type/Exception.js";
import { BaseObject } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

class MockBaseObject extends BaseObject {
}

export class ExceptionTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        assert.doesNotThrow(() : void => {
            const instance : Exception = new Exception();
        });
        const exception : Exception = new Exception("message");
        assert.equal(exception.Message(), "message");
    }

    @Test()
    public Code() : void {
        const exception : Exception = new Exception();
        assert.equal(exception.Code(), ExceptionCode.GENERAL);
        assert.equal(exception.Code(ExceptionCode.EXIT), ExceptionCode.EXIT);
    }

    @Test()
    public Message() : void {
        const exception : Exception = new Exception();
        assert.equal(exception.Message("test of exception"), "test of exception");
    }

    @Test()
    public Stack() : void {
        const exception : Exception = new Exception();
        assert.equal(exception.Stack("stack"), "stack");
    }

    @Test()
    public Owner() : void {
        const exception : Exception = new Exception();
        assert.equal(exception.Owner(), "");
        exception.Owner("testOwner");
        assert.equal(exception.Owner(), "testOwner");
        const owner : BaseObject = new MockBaseObject();
        exception.Owner(owner);
        assert.equal(exception.Owner(), owner.getClassName());
    }

    @Test()
    public Line() : void {
        const exception : Exception = new Exception();
        assert.equal(exception.Line(25), 25);
    }

    @Test()
    public ToString_test() : void {
        const exception : Exception = new Exception();
        exception.File("exception.txt");
        exception.Line(5);
        assert.equal(exception.ToString("", true), "<br/>file: exception.txt<br/>at line: 5<br/>");

        exception.File("testing.txt");
        exception.Line(6);
        assert.equal(exception.ToString("", false), ", file: testing.txt, at line: 6");
    }

    @Test()
    public toString_test() : void {
        const exception : Exception = new Exception();
        assert.equal(exception.toString(), "<br/>");
    }
}
