/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpResolversCollection } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/HttpResolversCollection.js";
import { ArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

class MockBaseObject extends BaseObject {
}

export class ArrayListTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const array : ArrayList<string> = new ArrayList<string>("value");
        assert.deepEqual(array.getAll(), ["value"]);
    }

    @Test()
    public Add() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test1");
        array.Add("test2", "key2");
        array.Add("test3", 2);
        assert.deepEqual(array.getAll(), ["test1", "test2", "test3"]);

        const array2 : ArrayList<string> = new ArrayList<string>();
        array2.Add("test1", "key1");
        array2.Add("test2", "key2");
        array2.Add("test3", "key3");
        array2.Add("test4");
        assert.deepEqual(array2.getKeys(), ["key1", "key2", "key3", 0]);

        const array3 : ArrayList<string | number> = new ArrayList<string | number>();
        array3.Add("test1", "key1");
        array3.Add("test2", "key2");
        array3.Add("test3", "key3");
        assert.deepEqual(array3.getKeys(), ["key1", "key2", "key3"]);
        assert.deepEqual(array3.Length(), 3);

        const array4 : ArrayList<string | number> = new ArrayList<string | number>();
        array4.Add("test1");
        array4.Add("test2");
        array4.Add("test3");
        array4.Add("test4", "key");
        array4.Add("test5");
        assert.equal(array4.IndexOf("test5"), 4);
        assert.equal(array4.Length(), 5);

        const array5 : ArrayList<string | number> = new ArrayList<string | number>();
        array5.Add("test1", 2);
        array5.Add("test2", 0);
        array5.Add("test3", "key1");
        array5.Add("test4", "key2");
        array5.Add("test5");
        assert.equal(array5.getLast(), "test5");
        assert.equal(array5.getKey(array5.getLast()), 1);
        assert.equal(array5.getKeys().indexOf(array5.getKey(array5.getLast())), 4);
    }

    @Test()
    public EmptyArray() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        assert.deepEqual(array.Length(), 0);
    }

    @Test()
    public getKeys() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test1");
        array.Add("test2", "key2");
        array.Add("test3", 2);
        assert.deepEqual(array.getKeys(), [0, "key2", 2]);
    }

    @Test()
    public getKey() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test1");
        array.Add("test2", "key2");
        array.Add("test3", 2);
        assert.equal(array.getKey("test1"), 0);
        assert.equal(array.getKey("test2"), "key2");
        assert.equal(array.getKey("test3"), 2);
        assert.equal(array.getKey("test5"), null);
    }

    @Test()
    public KeyExists() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test1");
        array.Add("test2", "key2");
        array.Add("test3", 2);
        assert.equal(array.KeyExists(0), true);
        assert.equal(array.KeyExists(5), false);
        assert.equal(array.KeyExists("key2"), true);
        assert.equal(array.KeyExists("key5"), false);

        const array2 : ArrayList<string> = new ArrayList<string>();
        array2.Add("test1");
        array2.Add("test2", "key2");
        array2.Add("test3", 2);
        assert.equal(array2.KeyExists(null), false);
        assert.equal(array2.KeyExists(""), false);

        const array3 : ArrayList<string> = new ArrayList<string>();
        array3.Add("test1");
        array3.Add("test2", "key2");
        array3.Add("test3", 2);
        assert.throws(() : void => {
            array3.KeyExists(<any>true);
        }, /Only string or number type of key is allowed for 'Io.Oidis.Commons.Primitives.ArrayList'/);
    }

    @Test()
    public Contains() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test1");
        array.Add("test2", "key2");
        array.Add("test3", 2);
        assert.equal(array.Contains("test1"), true);
        assert.equal(array.Contains("test4"), false);
    }

    @Test()
    public IndexOf() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test1");
        array.Add("test2", "key2");
        array.Add("test3", 3);
        assert.equal(array.IndexOf("test1"), 0);
        assert.equal(array.IndexOf("test2"), 1);
        assert.equal(array.IndexOf("test3"), 2);
        assert.equal(array.IndexOf("test4"), -1);
    }

    @Test()
    public RemoveAt() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test");
        array.Add("test2");
        array.Add("test3");
        array.RemoveAt(array.IndexOf("test2"));
        array.Add("test4");
        array.RemoveAt(0);
        array.Add("test5");
        array.RemoveAt(2);
        assert.deepEqual(array.getAll(), ["test3", "test4"]);
        assert.deepEqual(array.getKeys(), [2, 3]);
        array.Reindex();
        assert.deepEqual(array.getAll(), ["test3", "test4"]);
        assert.deepEqual(array.getKeys(), [0, 1]);

        const array2 : ArrayList<string> = new ArrayList<string>();
        array2.Add("test");
        array2.Add("test2");
        array2.Add("test3");
        array2.RemoveAt(5);
        array2.RemoveAt(-1);
        assert.deepEqual(array2.getAll(), ["test", "test2", "test3"]);
        assert.deepEqual(array2.getKeys(), [0, 1, 2]);

        const array3 : ArrayList<string> = new ArrayList<string>();
        array3.Add("test");
        array3.Add("test2");
        array3.Add("test3");
        array3.Add("test4");
        array3.Add("test5");
        array3.RemoveAt(array3.IndexOf("test2"));
        assert.deepEqual(array3.getAll(), ["test", "test3", "test4", "test5"]);
        assert.deepEqual(array3.getKeys(), [0, 2, 3, 4]);
        array3.RemoveAt(0);
        assert.deepEqual(array3.getAll(), ["test3", "test4", "test5"]);
        assert.deepEqual(array3.getKeys(), [2, 3, 4]);
        array3.RemoveAt(2);
        assert.deepEqual(array3.getAll(), ["test3", "test4"]);
        assert.deepEqual(array3.getKeys(), [2, 3]);
    }

    @Test()
    public Reindex() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Reindex();
        assert.equal(array.IsEmpty(), true);

        const array2 : ArrayList<string> = new ArrayList<string>();
        array2.Add("test1");
        array2.Add("test2", "key");
        array2.Add("test3", 4);
        assert.deepEqual(array2.getAll(), ["test1", "test2", "test3"]);
        assert.deepEqual(array2.getKeys(), [0, "key", 4]);
        array2.Reindex();
        assert.deepEqual(array2.getAll(), ["test1", "test2", "test3"]);
        assert.deepEqual(array2.getKeys(), [0, "key", 1]);
    }

    @Test()
    public getAll() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test1");
        array.Add("test2", "key2");
        array.Add("test3", 2);
        assert.deepEqual(array.getAll(), ["test1", "test2", "test3"]);
    }

    @Test()
    public getItem() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test");
        array.Add("test2");
        array.Add("test3", 5);
        array.Add("test4", "key6");
        array.Add("test5", 5);
        array.Add("test6", "key6");
        assert.equal(array.getItem(0), "test");
        assert.equal(array.getItem(5), "test5");
        assert.equal(array.getItem("key6"), "test6");

        const array2 : ArrayList<string> = new ArrayList<string>();
        array2.Add("test");
        array2.Add("test2", "key2");
        array2.Add("test3", "key3");
        array2.Add("test4", "key4");
        array2.Add("test5", "key5");
        assert.equal(array2.getItem("6"), null);
    }

    @Test()
    public getFirst() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test");
        array.Add("test2");
        array.Add("test3");
        assert.equal(array.getFirst(), "test");

        const array2 : ArrayList<string> = new ArrayList<string>();
        assert.equal(array2.getFirst(), null);
    }

    @Test()
    public getLast() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test");
        array.Add("test2");
        array.Add("test3");
        assert.equal(array.getLast(), "test3");

        const array2 : ArrayList<string> = new ArrayList<string>();
        assert.equal(array2.getLast(), null);
    }

    @Test()
    public Equal() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test1");
        array.Add("test2", "key2");
        array.Add("test3", 3);
        const array2 : ArrayList<string> = new ArrayList<string>();
        array2.Add("test1");
        array2.Add("test2", "key2");
        array2.Add("test5", 3);
        assert.equal(array.Equal(array2), false);

        const array3 : ArrayList<number> = new ArrayList<number>();
        array3.Add(1);
        array3.Add(2, "key2");
        array3.Add(3, 3);
        assert.equal(array3.Equal(array2), false);

        const array4 : ArrayList<string> = new ArrayList<string>();
        array4.Add("test1");
        array4.Add("test2", "abc");
        array4.Add("test3", 3);
        assert.equal(array4.Equal(array2), false);

        const array5 : ArrayList<string> = new ArrayList<string>();
        array2.Add("test1");
        array2.Add("test2", "key2");
        array2.Add("test3", 3);
        assert.equal(array.Equal(array5), true);
    }

    @Test()
    public ToArray() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test1");
        array.Add("test2", "key2");
        array.Add("test3", "key3");
        array.Add("test4", 1);
        assert.deepEqual(array.ToArray(), ["test1", "test2", "test3", "test4"]);
    }

    @Test()
    public Length() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test");
        array.Add("test2");
        array.Add("test3");
        assert.equal(array.Length(), 3);
    }

    @Test()
    public IsEmpty() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        assert.equal(array.IsEmpty(), true);
        array.Add("test");
        assert.equal(array.IsEmpty(), false);
    }

    @Test()
    public Clear() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test");
        array.Add("test2");
        array.Add("test3");
        assert.equal(array.Length(), 3);
        array.Clear();
        assert.equal(array.Length(), 0);
    }

    @Test()
    public Copy() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test");
        array.Add("test2");
        array.Add("test3");

        const array2 : ArrayList<string> = new ArrayList<string>();
        array2.Copy(array);
        assert.equal(array2.Equal(array), true);

        const array3 : ArrayList<string> = new ArrayList<string>();
        array3.Copy(array, 2);
        assert.equal(array3.Length(), 2);

        array3.Copy(array, 1, true);
        assert.equal(array3.Length(), 1);
    }

    @Test()
    public RemoveLast() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test");
        array.Add("test2");
        array.Add("test3");
        array.Add("test4");
        array.Add("test5");
        array.RemoveLast();
        assert.deepEqual(array.getAll(), ["test", "test2", "test3", "test4"]);
        array.RemoveLast();
        assert.deepEqual(array.getAll(), ["test", "test2", "test3"]);
    }

    @Test()
    public SortByKeyUp() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test");
        array.Add("test2", 3);
        array.Add("test3", 2);
        array.Add("test4", "key");
        array.Add("test5");
        array.SortByKeyUp();
        assert.deepEqual(array.getAll(), ["test", "test3", "test2", "test5", "test4"]);
    }

    @Test()
    public SortByKeyDown() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test");
        array.Add("test2", 3);
        array.Add("test3", 2);
        array.Add("test4", "key");
        array.Add("test5");
        array.SortByKeyDown();
        assert.deepEqual(array.getAll(), ["test4", "test5", "test2", "test3", "test"]);
    }

    @Test()
    public foreach() : void {
        const array : ArrayList<string> = new ArrayList<string>();
        array.Add("test");
        array.Add("test2", 3);
        array.Add("test3", 2);
        array.Add("test4", "key");
        array.Add("test5");

        let output : string = "out to global var: \n";
        array.foreach(($value : string) : void => {
            output += "value: " + $value + "\n";
        });
        assert.equal(output,
            "out to global var: \n" +
            "value: test\n" +
            "value: test2\n" +
            "value: test3\n" +
            "value: test4\n" +
            "value: test5\n");

        output = "";
        array.foreach(($value : string, $key? : any) : void => {
            output += "[" + $key + "] " + $value + "\n";
        });
        assert.equal(output,
            "[0] test\n" +
            "[3] test2\n" +
            "[2] test3\n" +
            "[key] test4\n" +
            "[4] test5\n");

        output = "";
        array.foreach(($value : string, $key? : any) : boolean => {
            if ($key === 3) {
                output += "break test at [" + $key + "] " + $value;
                return false;
            }
        });
        assert.equal(output, "break test at [3] test2");
    }

    @Test()
    public ToString1() : void {
        const array : ArrayList<any> = new ArrayList<any>();
        array.Add("abc");
        array.Add("cde", 3);
        array.Add("fgh", 2);
        array.Add(new ArrayList<any>());
        array.Add(["", "key", "efd"]);
        array.Add(true);
        array.Add({name: "value"});
        array.Add(125);
        array.Add(new MockBaseObject());
        array.Add(new HttpResolversCollection(""));
        array.Add("test", "key");
        array.Add(null);
        array.Add("", "key2");
        array.Add("");

        this.resetCounters();
        assert.equal(array.ToString("", false),
            "Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
            "[ 0 ]    abc\r\n" +
            "[ 3 ]    cde\r\n" +
            "[ 2 ]    fgh\r\n" +
            "[ 4 ]    Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
            "        Data object EMPTY" +
            "[ 5 ]    Array object\r\n" +
            "        [ 0 ]    EMPTY\r\n" +
            "        [ 1 ]    key\r\n" +
            "        [ 2 ]    efd\r\n" +
            "[ 6 ]    true\r\n" +
            "[ 7 ]    {\"name\":\"value\"}\r\n" +
            "[ 8 ]    Object type: number. Return value: 125\r\n" +
            "[ 9 ]    object type of \'Io.Oidis.Commons.Primitives.BaseObject\'\r\n" +
            "[ 10 ]    \"\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ \"key\" ]    test\r\n" +
            "[ 11 ]    NULL\r\n" +
            "[ \"key2\" ]    EMPTY\r\n" +
            "[ 12 ]    EMPTY\r\n",
            "return plain text without prefix");

        this.resetCounters();
        assert.equal(array.ToString("___", false),
            "Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
            "___[ 0 ]    abc\r\n" +
            "___[ 3 ]    cde\r\n" +
            "___[ 2 ]    fgh\r\n" +
            "___[ 4 ]    Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
            "___        Data object EMPTY" +
            "___[ 5 ]    Array object\r\n" +
            "___        [ 0 ]    EMPTY\r\n" +
            "___        [ 1 ]    key\r\n" +
            "___        [ 2 ]    efd\r\n" +
            "___[ 6 ]    true\r\n" +
            "___[ 7 ]    {\"name\":\"value\"}\r\n" +
            "___[ 8 ]    Object type: number. Return value: 125\r\n" +
            "___[ 9 ]    object type of \'Io.Oidis.Commons.Primitives.BaseObject\'\r\n" +
            "___[ 10 ]    \"\" => resolver not assigned   \r\n" +
            "___        ignore case: true\r\n" +
            "___        count of levels: 0\r\n" +
            "___        parameter name: NULL\r\n" +
            "___        child list: EMPTY\r\n" +
            "___[ \"key\" ]    test\r\n" +
            "___[ 11 ]    NULL\r\n" +
            "___[ \"key2\" ]    EMPTY\r\n" +
            "___[ 12 ]    EMPTY\r\n",
            "return plain text with prefix");

        this.resetCounters();
        assert.equal(array.ToString(),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_4\').style.display=" +
            "document.getElementById(\'ContentBlock_4\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_4\" style=\"border: 0 solid black; display: none;\">" +
            "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;abc<br/>" +
            "[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;cde<br/>" +
            "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;fgh<br/>" +
            "[ 4 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data object <b>EMPTY</b></span>" +
            "[ 5 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Array object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_1\').style.display=" +
            "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;<b>EMPTY</b><br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;key<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;efd<br/></span>" +
            "[ 6 ]&nbsp;&nbsp;&nbsp;&nbsp;true<br/>" +
            "[ 7 ]&nbsp;&nbsp;&nbsp;&nbsp;{<br/>" +
            "&nbsp;&nbsp;&nbsp;\"name\": \"value\"<br/>}<br/>" +
            "[ 8 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Object type:</i> number. <i>Return value:</i> 125<br/>" +
            "[ 9 ]&nbsp;&nbsp;&nbsp;&nbsp;object type of \'Io.Oidis.Commons.Primitives.BaseObject\'<br/>" +
            "[ 10 ]&nbsp;&nbsp;&nbsp;&nbsp;\"\" => resolver not assigned&nbsp;&nbsp;&nbsp;" +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_3\').style.display=" +
            "document.getElementById(\'ContentBlock_3\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_3\" style=\"border: 0 solid black; display: none;\">" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>ignore case:</i> true<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>count of levels:</i> 0<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>parameter name:</i> NULL<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>child list:</i> <i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_2\').style.display=" +
            "document.getElementById(\'ContentBlock_2\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_2\" style=\"border: 0 solid black; display: none;\">" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data object <b>EMPTY</b></span></span>" +
            "[ \"key\" ]&nbsp;&nbsp;&nbsp;&nbsp;test<br/>[ 11 ]&nbsp;&nbsp;&nbsp;&nbsp;NULL<br/>" +
            "[ \"key2\" ]&nbsp;&nbsp;&nbsp;&nbsp;<b>EMPTY</b><br/>" +
            "[ 12 ]&nbsp;&nbsp;&nbsp;&nbsp;<b>EMPTY</b><br/>" +
            "</span>",
            "return HTML-based output without prefix");

        this.resetCounters();
        assert.equal(array.ToString("____"),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_4\').style.display=" +
            "document.getElementById(\'ContentBlock_4\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_4\" style=\"border: 0 solid black; display: none;\">" +
            "____[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;abc<br/>" +
            "____[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;cde<br/>" +
            "____[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;fgh<br/>" +
            "____[ 4 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "____&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data object <b>EMPTY</b></span>" +
            "____[ 5 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Array object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_1\').style.display=" +
            "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">" +
            "____&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;<b>EMPTY</b><br/>" +
            "____&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;key<br/>" +
            "____&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;efd<br/></span>" +
            "____[ 6 ]&nbsp;&nbsp;&nbsp;&nbsp;true<br/>" +
            "____[ 7 ]&nbsp;&nbsp;&nbsp;&nbsp;{<br/>&nbsp;&nbsp;&nbsp;\"name\": \"value\"<br/>}<br/>" +
            "____[ 8 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Object type:</i> number. <i>Return value:</i> 125<br/>" +
            "____[ 9 ]&nbsp;&nbsp;&nbsp;&nbsp;object type of \'Io.Oidis.Commons.Primitives.BaseObject\'<br/>" +
            "____[ 10 ]&nbsp;&nbsp;&nbsp;&nbsp;\"\" => resolver not assigned&nbsp;&nbsp;&nbsp;" +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_3\').style.display=" +
            "document.getElementById(\'ContentBlock_3\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_3\" style=\"border: 0 solid black; display: none;\">" +
            "____&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>ignore case:</i> true<br/>" +
            "____&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>count of levels:</i> 0<br/>" +
            "____&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>parameter name:</i> NULL<br/>" +
            "____&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>child list:</i> <i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_2\').style.display=" +
            "document.getElementById(\'ContentBlock_2\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_2\" style=\"border: 0 solid black; display: none;\">" +
            "____&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data object <b>EMPTY</b></span></span>" +
            "____[ \"key\" ]&nbsp;&nbsp;&nbsp;&nbsp;test<br/>" +
            "____[ 11 ]&nbsp;&nbsp;&nbsp;&nbsp;NULL<br/>" +
            "____[ \"key2\" ]&nbsp;&nbsp;&nbsp;&nbsp;<b>EMPTY</b><br/>" +
            "____[ 12 ]&nbsp;&nbsp;&nbsp;&nbsp;<b>EMPTY</b><br/>" +
            "</span>",
            "return HTML-based output with prefix");
    }

    @Test()
    public ToArrayList() : void {
        assert.equal(ArrayList.ToArrayList(["value1", "value2", ["test1"], new ArrayList<string>()]).ToString("", false),
            "Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
            "[ 0 ]    value1\r\n" +
            "[ 1 ]    value2\r\n" +
            "[ 2 ]    Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
            "        [ 0 ]    test1\r\n" +
            "[ 3 ]    Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
            "        Data object EMPTY",
            "convert array to arrayList");

        const array : ArrayList<string> = ArrayList.ToArrayList(["value1", "value2", "value3"]);
        assert.deepEqual(array.getAll(), ["value1", "value2", "value3"]);
        assert.deepEqual(array.getFirst(), "value1");
        assert.deepEqual(array.Length(), 3);
        assert.deepEqual(array.getKeys(), [0, 1, 2]);

        const array2 : ArrayList<string> = ArrayList.ToArrayList(["value1", "value2", "value3"]);
        array2.Add("value4");
        assert.deepEqual(array2.getAll(), ["value1", "value2", "value3", "value4"]);
        assert.deepEqual(array2.getKeys(), [0, 1, 2, 3]);

        const array3 : ArrayList<string> = ArrayList.ToArrayList("value1");
        array3.Add("value4");
        assert.deepEqual(array3.KeyExists(0), true);
        assert.deepEqual(array3.KeyExists(1), true);
        assert.deepEqual(array3.getKeys(), [0, 1]);

        const list : ArrayList<any> = new ArrayList<any>();
        list.Add("test1");
        list.Add("test1");
        list.Add(["value1", "value2", "value3"]);
        const array4 : ArrayList<string> = ArrayList.ToArrayList(list);
        array4.Add("value4");
        assert.deepEqual(array4.getKeys(), [0, 1, 2, 3]);
        assert.equal(array4.Length(), 4);

        const array5 : ArrayList<string> = ArrayList.ToArrayList(["test1", "test2", "test3"]);
        array5.Add("value1", "key1");
        array5.Add("value2", "key2");
        array5.Add("value3", "key3");
        assert.deepEqual(array5.getAll(), ["test1", "test2", "test3", "value1", "value2", "value3"]);
        assert.deepEqual(array5.getKeys(), [0, 1, 2, "key1", "key2", "key3"]);

        const array6 : ArrayList<string> = ArrayList.ToArrayList([]);
        array6.Add("value1");
        array6.Add("value2");
        array6.Add("value3");
        assert.deepEqual(array6.getAll(), ["value1", "value2", "value3"]);
    }

    @Test()
    public SerializationData1() : void {
        const object : ArrayList<number> = new ArrayList<number>();
        object.Add(1);
        object.Add(3);
        assert.deepEqual(object.SerializationData(), {size: 2, keys: [0, 1], data: [1, 3]});
    }

    @Test()
    public toString1() : void {
        const object : ArrayList<any> = new ArrayList<any>();
        object.Add(true, 27);
        this.resetCounters();
        assert.equal(object.toString(),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "[ 27 ]&nbsp;&nbsp;&nbsp;&nbsp;true<br/>" +
            "</span>");
    }
}
