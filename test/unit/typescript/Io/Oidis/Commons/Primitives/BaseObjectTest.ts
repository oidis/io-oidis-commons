/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { HttpRequestEventArgs } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/HttpRequestEventArgs.js";
import { IArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/IArrayList.js";
import { IBaseObject } from "../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { Interface } from "../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/BaseObject.js";
import { StringUtils } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/StringUtils.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

class MockBaseObject extends BaseObject {
}

export class BaseObjectTest extends UnitTestRunner {

    @Test()
    public ClassName() : void {
        assert.equal(BaseObject.ClassName(), "Io.Oidis.Commons.Primitives.BaseObject");
    }

    @Test()
    public ClassNameWithoutNamespace() : void {
        assert.equal(BaseObject.ClassNameWithoutNamespace(), "BaseObject");
    }

    @Test()
    public NamespaceName() : void {
        assert.equal(BaseObject.NamespaceName(), "Io.Oidis.Commons.Primitives");
    }

    @Test()
    public UID() : void {
        const object : BaseObject = new MockBaseObject();
        assert.ok(object.getUID(), "298349f41a3db65d34bee42365dea9a115c4420e");
        assert.notEqual(object.getUID(), object.getUID());
        assert.notEqual(BaseObject.UID(), BaseObject.UID());
    }

    @Test()
    public getInstance() : void {
        const object : BaseObject = new MockBaseObject();
        const object1 : BaseObject = MockBaseObject.getInstance();
        assert.ok(new MockBaseObject(), MockBaseObject.getInstance());
    }

    @Test()
    public getClassName1() : void {
        const object : BaseObject = new MockBaseObject();
        assert.equal(object.getClassName(), "Io.Oidis.Commons.Primitives.BaseObject");
    }

    @Test()
    public getNamespaceName1() : void {
        const object : BaseObject = new MockBaseObject();
        assert.equal(object.getNamespaceName(), "Io.Oidis.Commons.Primitives");
    }

    @Test()
    public getClassNameWithoutNamespace1() : void {
        const object : BaseObject = new MockBaseObject();
        assert.equal(object.getClassNameWithoutNamespace(), "BaseObject");
    }

    @Test()
    public getUID1() : void {
        const object1 : BaseObject = new MockBaseObject();
        const object2 : BaseObject = new MockBaseObject();
        assert.notEqual(object1.getUID(), object2.getUID());
    }

    @Test()
    public IsTypeOf1() : void {
        const object : BaseObject = new MockBaseObject();
        assert.equal(object.IsTypeOf(MockBaseObject), true);
        assert.equal(object.IsTypeOf(StringUtils), false);
    }

    @Test()
    public IsMemberOf1() : void {
        const object : BaseObject = new MockBaseObject();
        assert.equal(object.IsMemberOf(BaseObject), true);
        assert.equal(object.IsMemberOf(StringUtils), false);
    }

    @Test()
    public Implements1() : void {
        const object : BaseObject = new MockBaseObject();
        assert.equal(object.Implements((<Interface>IBaseObject).ClassName()), true);
        assert.equal(object.Implements(IBaseObject), true);
        assert.equal(object.IsMemberOf(IArrayList), false);
    }

    @Test(true)
    public SerializationData1() : void {
        const object : BaseObject = new MockBaseObject();
        assert.deepEqual(object.SerializationData(), {},
            "validate object data for serialization");

        const object2 : EventArgs = new EventArgs();
        assert.deepEqual(object2.SerializationData(), {owner: object2},
            "validate object data for serialization with recursive reference");
        object2.Type("test");
        assert.deepEqual(object2.SerializationData(), {owner: object2, type: "test"},
            "validate object data for serialization");

        const nativeArgs : any = {name: "test"};
        object2.NativeEventArgs(nativeArgs);
        assert.deepEqual(object2.SerializationData(), {nativeEventArgs: nativeArgs, owner: object2, type: "test"},
            "validate object data for serialization with native args");
    }

    @Test(true)
    public getMethods1() : void {
        const object : BaseObject = new MockBaseObject();
        assert.deepEqual(object.getMethods(), [
            "getUID",
            "getClassName",
            "getNamespaceName",
            "getClassNameWithoutNamespace",
            "getProperties",
            "getMethods",
            "IsTypeOf",
            "IsMemberOf",
            "Implements",
            "SerializationData",
            "ToString",
            "toString",
            "getHash",
            "excludeSerializationData",
            "excludeIdentityHashData"
        ]);
    }

    @Test(true)
    public getHash1() : void {
        const object : BaseObject = new MockBaseObject();
        assert.equal(object.getHash(), 0);
        const object2 : HttpRequestEventArgs = new HttpRequestEventArgs("test", new ArrayList("test"));
        assert.equal(object2.getHash(), 1184522040);
        (<any>object2).test = () : void => {

        };
        assert.equal(object2.getHash(), 1184522040);
    }

    @Test()
    public ToString1() : void {
        const object : BaseObject = new MockBaseObject();
        assert.equal(object.ToString(), "object type of 'Io.Oidis.Commons.Primitives.BaseObject'",
            "return HTML without attributes");
        assert.equal(object.ToString("___", true), "___object type of 'Io.Oidis.Commons.Primitives.BaseObject'",
            "return HTML with prefix");
        assert.equal(object.ToString("", true), "object type of 'Io.Oidis.Commons.Primitives.BaseObject'",
            "return HTML without prefix");
        assert.equal(object.ToString("___", false),
            "___object type of 'Io.Oidis.Commons.Primitives.BaseObject'",
            "return PlainText");
    }
}
