/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/Events/EventType.js";
import { ExceptionCode } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/ExceptionCode.js";
import { BaseEnum } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class BaseEnumTest extends UnitTestRunner {

    @Test()
    public ClassName() : void {
        assert.equal(BaseEnum.ClassName(), "Io.Oidis.Commons.Primitives.BaseEnum");
        assert.equal(EventType.ClassName(), "Io.Oidis.Commons.Enums.Events.EventType");
    }

    @Test()
    public ClassNameWithoutNamespace() : void {
        assert.equal(BaseEnum.ClassNameWithoutNamespace(), "BaseEnum");
    }

    @Test()
    public NamespaceName() : void {
        assert.equal(BaseEnum.NamespaceName(), "Io.Oidis.Commons.Primitives");
    }

    @Test()
    public Contains() : void {
        assert.equal(BaseEnum.Contains("onstart"), false);
        assert.equal(EventType.Contains(EventType.ON_CHANGE), true);
        assert.equal(EventType.Contains(ExceptionCode.EXIT), false);
    }

    @Test()
    public getProperties1() : void {
        assert.deepEqual(EventType.getProperties(), [
            "ON_START",
            "ON_CHANGE",
            "ON_COMPLETE",
            "BEFORE_LOAD",
            "ON_LOAD",
            "BEFORE_REFRESH",
            "ON_HTTP_REQUEST",
            "ON_ASYNC_REQUEST",
            "ON_SUCCESS",
            "ON_ERROR",
            "ON_MESSAGE"
        ]);
        assert.notDeepEqual(EventType.getProperties(), [
            "ON_START",
            "ON_CHANGE",
            "ON_COMPLETE"
        ]);
    }

    @Test()
    public getKey() : void {
        assert.equal(BaseEnum.getKey("onstart"), null);
        assert.equal(EventType.getKey(EventType.ON_CHANGE), "ON_CHANGE");
        assert.equal(BaseEnum.getKey(ExceptionCode.EXIT), null);
    }
}
