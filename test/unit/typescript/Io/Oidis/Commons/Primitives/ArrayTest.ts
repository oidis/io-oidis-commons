/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class ArrayTest extends UnitTestRunner {

    @Test()
    public indexOf() : void {
        const object : string[] = ["test1", "test2", "test3"];
        assert.equal(object.indexOf("test1"), 0);
        assert.equal(object.indexOf("test2"), 1);
        assert.equal(object.indexOf("test3"), 2);
        assert.equal(object.indexOf("test4"), -1);
    }

    @Test()
    public forEach() : void {
        const object : string[] = ["test1", "test2", "test3"];
        let index : number = 0;
        object.forEach(($item : string) : void => {
            assert.equal(object.indexOf("test1"), 0);
            switch (index) {
            case 0:
                assert.equal($item, "test1");
                break;
            case 1:
                assert.equal($item, "test2");
                break;
            case 2:
                assert.equal($item, "test3");
                break;
            default:
                assert.equal($item, "unknow");
                break;
            }
            index++;
        });
    }

    @Test(true)
    public Contains() : void {
        const object : string[] = ["test1", "test2", "test3"];
        assert.ok((<any>object).Contains("test1"));
        assert.ok(!(<any>object).Contains("test4"));
        assert.ok((<any>object).Contains("test1", "test2"));
        assert.ok(!(<any>object).Contains("test1", "test4"));
    }
}
