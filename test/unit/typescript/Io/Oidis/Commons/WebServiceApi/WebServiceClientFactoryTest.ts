/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { WebServiceClientType } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/WebServiceClientType.js";
import { EventsManager } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/EventsManager.js";
import { IWebServiceClient } from "../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/IWebServiceClient.js";
import { WebServiceClientFactory } from "../../../../../../../source/typescript/Io/Oidis/Commons/WebServiceApi/WebServiceClientFactory.js";
import { WebServiceConfiguration } from "../../../../../../../source/typescript/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";

export class WebServiceClientFactoryTest extends UnitTestRunner {

    @Test()
    public getClient() : void {
        const clientManager1 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.CEF_QUERY, "config");
        const clientManager2 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.POST_MESSAGE, "service");

        const configuration : WebServiceConfiguration = new WebServiceConfiguration(
            "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
        const clientManager3 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.AJAX, configuration);
        const clientManager4 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.AJAX_SYNCHRONOUS, null);
        const clientManager6 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.IFRAME, "iframeservice");
        const clientManager7 : IWebServiceClient = WebServiceClientFactory.getClient(undefined);

        const config : WebServiceConfiguration = new WebServiceConfiguration(
            "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
        this.setUrl("http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader");
        this.getHttpManager().RefreshWithoutReload();
        this.getHttpManager().getRequest().IsSameOrigin("http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader");
        const clientManager5 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.AJAX_SYNCHRONOUS, config);
        assert.equal(WebServiceClientFactory.getClient(WebServiceClientType.AJAX_SYNCHRONOUS, config), clientManager5);

        assert.equal(WebServiceClientFactory.getClient(WebServiceClientType.CEF_QUERY, "config"), clientManager1);
        assert.equal(WebServiceClientFactory.getClient(WebServiceClientType.POST_MESSAGE, "service"), clientManager2);
        assert.equal(WebServiceClientFactory.getClient(WebServiceClientType.AJAX, configuration), clientManager3);
        assert.equal(WebServiceClientFactory.getClient(WebServiceClientType.AJAX_SYNCHRONOUS), clientManager4);
        assert.equal(WebServiceClientFactory.getClient(WebServiceClientType.IFRAME, "iframeservice"), clientManager6);
        assert.equal(WebServiceClientFactory.getClient(undefined), null);

        EventsManager.getInstanceSingleton().Clear(clientManager2.getId().toString());
        EventsManager.getInstanceSingleton().Clear(clientManager6.getId().toString());
    }

    @Test()
    public IsSupported() : void {
        const clientManager1 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.IFRAME, "configuration");
        const clientManager2 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.HTTP, "httpservice");
        const clientManager3 : IWebServiceClient = WebServiceClientFactory.getClient(null, null);

        const service : WebServiceConfiguration = new WebServiceConfiguration(
            "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
        const clientManager5 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.CEF_QUERY, service);
        assert.equal(WebServiceClientFactory.IsSupported(WebServiceClientType.CEF_QUERY), false);

        const config : WebServiceConfiguration = new WebServiceConfiguration(
            "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
        this.setUrl("http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader");
        this.getHttpManager().RefreshWithoutReload();
        this.getHttpManager().getRequest().IsSameOrigin(config.getServerUrl());
        const clientManager6 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.AJAX_SYNCHRONOUS, config);
        assert.equal(WebServiceClientFactory.IsSupported(WebServiceClientType.AJAX_SYNCHRONOUS), true);

        assert.equal(WebServiceClientFactory.IsSupported(WebServiceClientType.IFRAME), true);
        assert.equal(WebServiceClientFactory.IsSupported(WebServiceClientType.HTTP), true);
        assert.equal(WebServiceClientFactory.IsSupported(null), false);

        EventsManager.getInstanceSingleton().Clear(clientManager1.getId().toString());
        EventsManager.getInstanceSingleton().Clear(clientManager2.getId().toString());
    }

    @Test()
    public Exists() : void {
        const clientManager : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.POST_MESSAGE, "conf");
        assert.equal(WebServiceClientFactory.Exists(clientManager.getId()), true);
        assert.equal(WebServiceClientFactory.Exists(0), false);
        EventsManager.getInstanceSingleton().Clear(clientManager.getId().toString());
    }

    @Test()
    public getClientById() : void {
        assert.equal(WebServiceClientFactory.getClientById(null), null);
    }

    @Test()
    public CloseConnection() : void {
        const configuration : WebServiceConfiguration = new WebServiceConfiguration(
            "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
        const clientManager : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.AJAX, configuration);
        WebServiceClientFactory.CloseAllConnections();
        assert.equal(configuration.TimeoutLimit(0), 0);
    }
}
