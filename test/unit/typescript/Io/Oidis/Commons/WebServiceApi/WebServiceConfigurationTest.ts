/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceConfiguration } from "../../../../../../../source/typescript/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { IUnitTestRunnerPromise, Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class WebServiceConfigurationTest extends UnitTestRunner {

    @Test()
    public getId() : void {
        const service : WebServiceConfiguration = new WebServiceConfiguration();
        assert.ok(service.getId(), "1878964429");
    }

    @Test()
    public getSource() : void {
        const service : WebServiceConfiguration = new WebServiceConfiguration(
            "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
        assert.equal(service.getSource(), "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
    }

    @Test()
    public ServerLocation() : void {
        const service : WebServiceConfiguration = new WebServiceConfiguration();
        assert.equal(service.ServerLocation("www.wuiframevork.dev"), "www.wuiframevork.dev");
    }

    @Test()
    public ServerProtocol() : void {
        const service : WebServiceConfiguration = new WebServiceConfiguration();
        assert.equal(service.ServerProtocol(), "http");
        assert.equal(service.ServerProtocol("https"), "https");
    }

    @Test()
    public ServerAddress() : void {
        const service : WebServiceConfiguration = new WebServiceConfiguration();
        assert.equal(service.ServerAddress(), "127.0.0.1");
    }

    @Test()
    public ServerBase() : void {
        const service : WebServiceConfiguration = new WebServiceConfiguration();
        assert.equal(service.ServerBase("server-base-log"), "server-base-log");
    }

    @Test()
    public ServerPort() : void {
        const service : WebServiceConfiguration = new WebServiceConfiguration();
        assert.equal(service.ServerPort(), 80);
        assert.equal(service.ServerPort(100), 100);
    }

    @Test()
    public getServerUrl() : void {
        const service : WebServiceConfiguration = new WebServiceConfiguration();
        assert.equal(service.getServerUrl(), "http://127.0.0.1/");

        service.ServerPort(100);
        service.ServerBase("home");
        service.ServerAddress("port");
        assert.equal(service.getServerUrl(), "http://port:100/home/");

        service.ServerBase("/home/");
        assert.equal(service.getServerUrl(), "http://port:100/home/");
    }

    @Test()
    public ResponseUrl() : void {
        const service : WebServiceConfiguration = new WebServiceConfiguration();
        assert.equal(service.ResponseUrl("http://myObjectUrl"), "http://myObjectUrl");
    }

    @Test()
    public TimeoutLimit() : void {
        const service : WebServiceConfiguration = new WebServiceConfiguration();
        assert.equal(service.TimeoutLimit(40), 40);
    }

    @Test(true)
    public testLoad() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const service : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
            service.Load(
                () => {
                    assert.equal(service.ServerAddress(), "192.168.1.111");
                    $done();
                },
                () => {
                    assert.equal(service.ServerAddress(), "127.0.0.1");
                    assert.ok(false, "Unable to load WebServiceConfiguration");
                    $done();
                });
        };
    }
}
