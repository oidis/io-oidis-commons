/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { CefQueryClient } from "../../../../../../../../source/typescript/Io/Oidis/Commons/WebServiceApi/Clients/CefQueryClient.js";
import {
    WebServiceConfiguration
} from "../../../../../../../../source/typescript/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class CefQueryClientTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const cefclient : CefQueryClient = new CefQueryClient();
        assert.notEqual(cefclient.getId(), 425631);
    }

    @Test()
    public StartCommunication() : void {
        const cefclient : CefQueryClient = new CefQueryClient();
        cefclient.StartCommunication();

        const cefclient2 : CefQueryClient = new CefQueryClient();
        assert.throws(() : void => {
            cefclient.StartCommunication();
            throw new Error("Not communication");
        }, /Not communication/);
    }

    @Test()
    public StopCommunication() : void {
        const cefclient : CefQueryClient = new CefQueryClient();
        cefclient.StartCommunication();
        assert.equal(cefclient.CommunicationIsRunning(), true);
        cefclient.StopCommunication();
    }

    @Test(true)
    public StartCommunicationSecond() : void {
        const service : WebServiceConfiguration = new WebServiceConfiguration();
        const cefclient : CefQueryClient = new CefQueryClient();
        cefclient.Send("hello", () : void => {
            service.ServerAddress("192.168.1.111");
        });
        cefclient.StartCommunication();
        service.TimeoutLimit(2000);
        assert.equal(cefclient.CommunicationIsRunning(), true);
        cefclient.StopCommunication();
        assert.equal(cefclient.CommunicationIsRunning(), false);
    }
}
