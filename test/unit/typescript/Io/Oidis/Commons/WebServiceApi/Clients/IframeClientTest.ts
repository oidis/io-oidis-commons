/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Events/EventsManager.js";
import { StringUtils } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Utils/StringUtils.js";
import { IframeClient } from "../../../../../../../../source/typescript/Io/Oidis/Commons/WebServiceApi/Clients/IframeClient.js";
import {
    WebServiceConfiguration
} from "../../../../../../../../source/typescript/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { IUnitTestRunnerPromise, Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";

export class IframeClientTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const config : WebServiceConfiguration = new WebServiceConfiguration();
        const client : IframeClient = new IframeClient(config);
        assert.ok(StringUtils.PatternMatched("WebServiceClient(*)", client.toString()));
        client.StartCommunication();
        client.getEvents().OnTimeout(() : void => {
            client.StopCommunication();
        });
        EventsManager.getInstanceSingleton().Clear(client.getId().toString());
    }

    @Test()
    public CommunicationIsRunning() : void {
        const config : WebServiceConfiguration = new WebServiceConfiguration(
            "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
        const client : IframeClient = new IframeClient(config);
        const iframeelement : HTMLIFrameElement = document.createElement("iframe");
        iframeelement.onload = () : void => {
            client.CommunicationIsRunning();
        };
        EventsManager.getInstanceSingleton().Clear(client.getId().toString());
    }

    @Test(true)
    public Load() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const service : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
            service.Load(
                () => {
                    const iframeclient : IframeClient = new IframeClient(service);
                    iframeclient.StartCommunication();
                    const client : HTMLIFrameElement = (<any>iframeclient).clientInstance(document.createElement("iframe"));
                    (<any>client).CommunicationIsRunning();
                    $done();
                },
                () => {
                    assert.equal(service.ServerAddress(), "127.0.0.1");
                    assert.ok(true, "Unable to load client configuration from: \" + this.getConfiguration().getSource()");
                    $done();
                });
        };
    }

    @Test(true)
    public StartCommunication() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const config : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
            config.ServerAddress("10.171.89.149");
            const iframeclient : IframeClient = new IframeClient(config);
            iframeclient.Send("TestingOfTheSocket", () : void => {
                iframeclient.StartCommunication();
                config.TimeoutLimit(2000);
                assert.equal(config.ServerAddress(), "127.0.0.1");
                assert.equal(iframeclient.CommunicationIsRunning(), true);
                $done();
            });
            assert.ok(true, "Unable to load client configuration from: \" + this.getConfiguration().getSource()");
            iframeclient.StopCommunication();
            assert.equal(iframeclient.CommunicationIsRunning(), false);
        };
    }

    @Test()
    public StartCommunicationSecond() : void {
        const service : WebServiceConfiguration = new WebServiceConfiguration();
        const client : IframeClient = new IframeClient(service);
        client.Send("hello", () : void => {
            service.ServerAddress("192.168.1.111");
        });
        client.StartCommunication();
        service.TimeoutLimit(2000);
        assert.equal(client.CommunicationIsRunning(), false);
        client.StopCommunication();
        assert.equal(client.CommunicationIsRunning(), false);
        EventsManager.getInstanceSingleton().Clear(client.getId().toString());
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
