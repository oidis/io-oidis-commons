/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { WebSocketsClient } from "../../../../../../../../source/typescript/Io/Oidis/Commons/WebServiceApi/Clients/WebSocketsClient.js";
import {
    WebServiceConfiguration
} from "../../../../../../../../source/typescript/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { IUnitTestRunnerPromise, Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";

export class WebSocketsClientTest extends UnitTestRunner {

    @Test()
    public getServerUrl() : void {
        const config : WebServiceConfiguration = new WebServiceConfiguration(
            "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp"
        );
        const socket : WebSocketsClient = new WebSocketsClient(config);
        assert.equal(socket.getServerUrl(), "ws://127.0.0.1/");
    }

    @Test()
    public getServerBase() : void {
        const config : WebServiceConfiguration = new WebServiceConfiguration(
            "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
        const socket : WebSocketsClient = new WebSocketsClient(config);
        assert.equal(socket.getServerBase(), "");
    }

    @Test(true)
    public StartCommunication() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const config : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
            config.ServerAddress("10.171.89.149");
            const socket : WebSocketsClient = new WebSocketsClient(config);
            socket.Send("TestingOfTheSocket", () : void => {
                socket.StartCommunication();
                config.TimeoutLimit(2000);
                assert.equal(config.ServerAddress(), "127.0.0.1");
                assert.equal(socket.CommunicationIsRunning(), true);
                $done();
            });
            assert.ok(true, "Unable to load client configuration from: \" + this.getConfiguration().getSource()");
            socket.StopCommunication();
            assert.equal(socket.CommunicationIsRunning(), false);
        };
    }

    @Test(true)
    public StartCommunicationSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const config3 : WebServiceConfiguration = new WebServiceConfiguration();
            const socket3 : WebSocketsClient = new WebSocketsClient(config3);
            socket3.Send("hello", () : void => {
                socket3.StartCommunication();
                config3.TimeoutLimit(2000);
                assert.equal(socket3.CommunicationIsRunning(), false);
                socket3.StopCommunication();
                assert.equal(socket3.CommunicationIsRunning(), false);
                $done();
            });
            assert.ok(true, "Unable to load client configuration from: \" + this.getConfiguration().getSource()");
        };
    }
}
