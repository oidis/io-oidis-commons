/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Enums/Events/EventType.js";
import { GeneralEventOwner } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Enums/Events/GeneralEventOwner.js";
import { MessageEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/MessageEventArgs.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Events/EventsManager.js";
import { StringUtils } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Utils/StringUtils.js";
import { PostMessageClient } from "../../../../../../../../source/typescript/Io/Oidis/Commons/WebServiceApi/Clients/PostMessageClient.js";
import {
    WebServiceConfiguration
} from "../../../../../../../../source/typescript/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { IUnitTestRunnerPromise, Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class PostMessageClientTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const config : WebServiceConfiguration = new WebServiceConfiguration();
        const message : PostMessageClient = new PostMessageClient(config);
        assert.ok(StringUtils.PatternMatched("WebServiceClient(*)", message.toString()));

        message.Send("hello", () : void => {
            config.ServerAddress("192.168.1.111");
        });
        message.StartCommunication();
        message.getEvents().OnTimeout(() : void => {
            message.StopCommunication();
        });
        EventsManager.getInstanceSingleton().Clear(message.getId().toString());
        EventsManager.getInstanceSingleton().Clear("OnTimeout");
        EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.WINDOW, EventType.ON_MESSAGE);
    }

    @Test()
    public StartCommunication() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const config : WebServiceConfiguration = new WebServiceConfiguration();
            const message : PostMessageClient = new PostMessageClient(config);
            const client : HTMLIFrameElement = document.createElement("iframe");
            message.StartCommunication();

            const event : MessageEventArgs = new MessageEventArgs();
            const messageEvent : any = {origin: "test"};
            event.NativeEventArgs(messageEvent);
            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_MESSAGE, messageEvent);
            (<any>PostMessageClient).onResponse = messageEvent;
            message.StopCommunication();
            EventsManager.getInstanceSingleton().Clear(message.getId().toString());
            EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.WINDOW, EventType.ON_MESSAGE);
            $done();
        };
    }

    @Test()
    public StartCommunicationError() : void {
        const config : WebServiceConfiguration = new WebServiceConfiguration(
            "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
        assert.equal(config.getSource(), "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
        const message : PostMessageClient = new PostMessageClient(config);
        const client : HTMLIFrameElement = document.createElement("iframe");
        const messageEvent : any = {origin: "test"};
        assert.throws(() : void => {
            client.src = config.getServerUrl();
            (<any>PostMessageClient).isLoaded = false;
            throw new Error("Unable to load client configuration from: " + config.getSource());
        }, /Unable to load client configuration from:/);
        EventsManager.getInstanceSingleton().Clear(message.getId().toString());
    }

    @Test(true)
    public StartCommunicationSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const service : WebServiceConfiguration = new WebServiceConfiguration();
            const message : PostMessageClient = new PostMessageClient(service);
            const event : MessageEventArgs = new MessageEventArgs();
            const messageEvent : any = {origin: "test"};
            event.NativeEventArgs(messageEvent);
            service.ServerAddress("192.168.1.111");
            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_MESSAGE, messageEvent);
            message.Send("hello", () : void => {
                assert.equal(event.NativeEventArgs(), messageEvent);
                message.StartCommunication();
                service.TimeoutLimit(2000);
                assert.equal(message.CommunicationIsRunning(), true);
                $done();
            });
            assert.ok(true, "Unable to load client configuration from: \" + this.getConfiguration().getSource()");
            message.StopCommunication();
            assert.equal(message.CommunicationIsRunning(), false);
        };
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
