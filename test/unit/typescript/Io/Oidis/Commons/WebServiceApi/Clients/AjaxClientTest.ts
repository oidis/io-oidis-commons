/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { StringUtils } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Utils/StringUtils.js";
import { AjaxClient } from "../../../../../../../../source/typescript/Io/Oidis/Commons/WebServiceApi/Clients/AjaxClient.js";
import {
    WebServiceConfiguration
} from "../../../../../../../../source/typescript/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { IUnitTestRunnerPromise, Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";

export class AjaxClientTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const config : WebServiceConfiguration = new WebServiceConfiguration();
        const ajaxclient : AjaxClient = new AjaxClient(config, true);
        assert.ok(StringUtils.PatternMatched("WebServiceClient(*)", ajaxclient.toString()));
    }

    @Test(true)
    public Communication() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const config : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
            const ajaxclient : AjaxClient = new AjaxClient(config, true);
            ajaxclient.getEvents().OnStart(() : void => {
                assert.equal(ajaxclient.getServerUrl(), "http://192.168.1.111:56047/builder/");
                $done();
            });
            ajaxclient.StartCommunication();
        };
    }

    @Test(true)
    public ComunicationSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const config : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
            const ajaxclient : AjaxClient = new AjaxClient(config, false);
            const activeXObject = {s: "Msxml2.XMLHTTP"};
            (<any>ajaxclient).clientInstance(activeXObject);
            ajaxclient.getEvents().OnStart(() : void => {
                config.TimeoutLimit(2000);
            });
            ajaxclient.Send("hello", () : void => {
                assert.equal(ajaxclient.getServerUrl(), "http://127.0.0.1/");
                ajaxclient.StopCommunication();
                $done();
            });
        };
    }

    @Test(true)
    public Load() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const service : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Io/Oidis/Commons/WebServiceConfiguration.jsonp");
            const activeXObject = {s: "Msxml2.XMLHTTP"};
            const ajaxclient : AjaxClient = new AjaxClient(service, true);
            (<any>ajaxclient).clientInstance(activeXObject);
            service.Load(
                () => {
                    $done();
                },
                () => {
                    assert.equal(service.ServerAddress(), "127.0.0.1");
                    assert.ok(true, "Unable to load client configuration from: \" + this.getConfiguration().getSource()");
                    $done();
                });
            ajaxclient.StartCommunication();
        };
    }

    @Test()
    public StartCommunication() : void {
        const service4 : WebServiceConfiguration = new WebServiceConfiguration();
        const ajaxclient4 : AjaxClient = new AjaxClient(service4, true);
        ajaxclient4.Send("hello", () : void => {
            service4.ServerAddress("192.168.1.111");
        });
        ajaxclient4.StartCommunication();
        service4.TimeoutLimit(2000);
        assert.equal(ajaxclient4.CommunicationIsRunning(), true);
        ajaxclient4.StopCommunication();
        assert.equal(ajaxclient4.CommunicationIsRunning(), false);
    }

    @Test(true)
    public StartCommunicationSecond() : void {
        const service5 : WebServiceConfiguration = new WebServiceConfiguration();
        const ajaxclient5 : AjaxClient = new AjaxClient(service5, false);
        ajaxclient5.Send("hello", () : void => {
            service5.ServerAddress("192.168.1.111");
        });
        ajaxclient5.StartCommunication();
        service5.TimeoutLimit(2000);
        assert.equal(ajaxclient5.CommunicationIsRunning(), true);
        ajaxclient5.StopCommunication();
        assert.equal(ajaxclient5.CommunicationIsRunning(), false);
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
