/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { Exception } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/Type/Exception.js";
import { Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class ErrorEventArgsTest extends UnitTestRunner {

    @Test()
    public ConstructorWithString() : void {
        const error : ErrorEventArgs = new ErrorEventArgs("Error message");
        assert.equal(error.ToString("", false),
            "Io.Oidis.Commons.Events.Args.ErrorEventArgs\r\n" +
            "[\"Message\"] Error message\r\n" +
            "[\"Owner\"] Io.Oidis.Commons.Events.Args.ErrorEventArgs\r\n");
    }

    @Test()
    public ConstructorWithException() : void {
        const error : ErrorEventArgs = new ErrorEventArgs(new Exception());
        assert.equal(error.ToString("", false),
            "Io.Oidis.Commons.Events.Args.ErrorEventArgs\r\n" +
            "[\"Message\"] \r\n" +
            "[\"Owner\"] Io.Oidis.Commons.Events.Args.ErrorEventArgs\r\n");
    }

    @Test()
    public Message() : void {
        const error : ErrorEventArgs = new ErrorEventArgs("Error message");
        assert.equal(error.Message(), "Error message");
        error.Message("new error message");
        assert.equal(error.Message(), "new error message");
    }

    @Test()
    public ExceptionFromString() : void {
        const error : ErrorEventArgs = new ErrorEventArgs("test message");
        assert.equal(error.Exception().getClassName(), Exception.ClassName());
        assert.equal(error.Exception().Message(), "test message");
        assert.equal(error.Exception().Line(10), 10);
    }

    @Test()
    public ExceptionFromWuiException() : void {
        const error : ErrorEventArgs = new ErrorEventArgs(new Exception("test message"));
        assert.equal(error.Exception().getClassName(), Exception.ClassName());
        assert.equal(error.Exception().Message(), "test message");
        assert.equal(error.Exception().Stack("info message"), "info message");
    }

    @Test()
    public ExceptionFromNativeError() : void {
        const error : ErrorEventArgs = new ErrorEventArgs(new Error("native test message"));
        assert.equal(error.Exception().getClassName(), Exception.ClassName());
        assert.equal(error.Exception().Message(), "native test message");
        assert.patternEqual(
            error.Exception().ToString("", false),
            "native test message, stack: \r\n" +
            "Error: native test message\r\n" +
            "    at ErrorEventArgsTest.ExceptionFromNativeError (*:*:*)\r\n" +
            "    at *");
    }

    @Test()
    public ExceptionFromNativeErrorWithoutStack() : void {
        const nativeError : Error = new Error("native test message");
        const property : string = "stack";
        delete nativeError[property];
        const error : ErrorEventArgs = new ErrorEventArgs(nativeError);
        assert.equal(error.Exception().getClassName(), Exception.ClassName());
        assert.equal(error.Exception().Message(), "native test message");
        assert.equal(error.Exception().ToString("", false), "native test message");
    }

    @Test()
    public ExceptionFromNull() : void {
        const error : ErrorEventArgs = new ErrorEventArgs(null);
        assert.equal(error.Exception().getClassName(), Exception.ClassName());
        assert.equal(error.Exception().Message(), "");
        assert.equal(error.Exception().Owner("owner"), "owner");
    }

    @Test()
    public ToStringPlainText() : void {
        const error : ErrorEventArgs = new ErrorEventArgs("Error Message");
        assert.equal(error.ToString("__", false),
            "__Io.Oidis.Commons.Events.Args.ErrorEventArgs\r\n" +
            "__[\"Message\"] Error Message\r\n" +
            "__[\"Owner\"] Io.Oidis.Commons.Events.Args.ErrorEventArgs\r\n");
    }

    @Test()
    public ToStringHTML() : void {
        const error : ErrorEventArgs = new ErrorEventArgs("Error Message");
        error.Owner("test");
        assert.equal(error.ToString(),
            "Io.Oidis.Commons.Events.Args.ErrorEventArgs<br/>" +
            "[\"Message\"] Error Message<br/>" +
            "[\"Owner\"] test<br/>");
    }
}
