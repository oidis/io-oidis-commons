/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Enums/Events/EventType.js";
import { EventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { BaseObject } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class EventArgsTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const args : EventArgs = new EventArgs();
        assert.equal(args.Owner(BaseObject), BaseObject);
    }

    @Test()
    public Owner() : void {
        const args : EventArgs = new EventArgs();
        assert.equal(args.Owner("owner"), "owner");
    }

    @Test()
    public Type() : void {
        const args : EventArgs = new EventArgs();
        assert.equal(args.Type(), "");
        args.Type(EventType.BEFORE_LOAD);
        assert.equal(args.Type(), EventType.BEFORE_LOAD);
    }

    @Test()
    public NativeEventArgs() : void {
        const args : EventArgs = new EventArgs();
        const nativeEventArgs : any = {type: "MouseEvent"};
        assert.equal(args.NativeEventArgs(nativeEventArgs), nativeEventArgs);
    }

    @Test()
    public PreventDefaultOnUndefinedNativeArgs() : void {
        const args : EventArgs = new EventArgs();
        assert.doesNotThrow(() : void => {
            assert.equal(args.NativeEventArgs(), undefined);
            args.PreventDefault();
        });
    }

    @Test()
    public PreventDefaultWithoutRequiredAPI() : void {
        const args : EventArgs = new EventArgs();
        const nativeEventArgs : any = {
            type: "MouseEvent"
        };
        args.NativeEventArgs(nativeEventArgs);
        assert.equal(args.NativeEventArgs(), nativeEventArgs);
        assert.doesNotThrow(() : void => {
            args.PreventDefault();
        });
        assert.equal(nativeEventArgs.returnValue, false);
    }

    @Test()
    public PreventDefaultWithRequiredAPI() : void {
        const args : EventArgs = new EventArgs();
        const nativeEventArgs : any = {
            preventDefault() : void {
                assert.equal(this.type, "MouseEvent");
            },
            type: "MouseEvent"
        };
        args.NativeEventArgs(nativeEventArgs);
        assert.equal(args.NativeEventArgs(), nativeEventArgs);
        assert.doesNotThrow(() : void => {
            args.PreventDefault();
        });
    }

    @Test(true)
    public ToStringHTML() : void {
        const args : EventArgs = new EventArgs();
        this.resetCounters();
        assert.equal(this.stripInstrumentation(args.ToString()),
            "[Type] EMPTY<br/>" +
            "[NativeEventArgs] NOT DEFINED<br/>" +
            "[PreventDefault] NOT DEFINED<br/>" +
            "[StopAllPropagation] NOT DEFINED<br/>" +
            "[getClassName] Io.Oidis.Commons.Events.Args.EventArgs<br/>" +
            "[getNamespaceName] Io.Oidis.Commons.Events.Args<br/>" +
            "[getClassNameWithoutNamespace] EventArgs<br/>" +
            "[IsMemberOf] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
            "[Implements] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
            "[getHash] <i>Object type:</i> number. <i>Return value:</i> -684756430<br/>" +
            "[excludeSerializationData] <i>Object type:</i> array. <i>Return values:</i><br/>" +
            "<i>Array object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;objectNamespace<br/>" +
            "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;objectClassName<br/></span><br/>" +
            "[excludeIdentityHashData] <i>Object type:</i> array. <i>Return values:</i><br/>" +
            "<i>Array object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_1\').style.display=" +
            "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span><br/>");
    }

    @Test(true)
    public ToStringPlainText() : void {
        const args : EventArgs = new EventArgs();
        this.resetCounters();
        assert.equal(this.stripInstrumentation(args.ToString("", false)),
            "[Type] EMPTY\r\n" +
            "[NativeEventArgs] NOT DEFINED\r\n" +
            "[PreventDefault] NOT DEFINED\r\n" +
            "[StopAllPropagation] NOT DEFINED\r\n" +
            "[getClassName] Io.Oidis.Commons.Events.Args.EventArgs\r\n" +
            "[getNamespaceName] Io.Oidis.Commons.Events.Args\r\n" +
            "[getClassNameWithoutNamespace] EventArgs\r\n" +
            "[IsMemberOf] Object type: boolean. Return value: false\r\n" +
            "[Implements] Object type: boolean. Return value: false\r\n" +
            "[getHash] Object type: number. Return value: -684756430\r\n" +
            "[excludeSerializationData] Object type: array. Return values: \r\n" +
            "Array object\r\n" +
            "[ 0 ]    objectNamespace\r\n" +
            "[ 1 ]    objectClassName\r\n\r\n" +
            "[excludeIdentityHashData] Object type: array. Return values: \r\nArray object\r\nData object EMPTY\r\n");
    }

    @Test()
    public toString_test() : void {
        const args : EventArgs = new EventArgs();
        this.resetCounters();
        const value : string = this.stripInstrumentation(args.ToString());
        this.resetCounters();
        assert.equal(this.stripInstrumentation(args.toString()), value);
    }
}
