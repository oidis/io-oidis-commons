/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ProgressEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/ProgressEventArgs.js";
import { Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class ProgressEventArgsTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const event : ProgressEventArgs = new ProgressEventArgs();
        assert.equal(event.getClassName(), "Io.Oidis.Commons.Events.Args.ProgressEventArgs");
    }

    @Test()
    public RangeStart() : void {
        const event : ProgressEventArgs = new ProgressEventArgs();
        assert.equal(event.RangeStart(6), 6);
    }

    @Test()
    public RangeEnd() : void {
        const event : ProgressEventArgs = new ProgressEventArgs();
        assert.equal(event.RangeEnd(100), 100);
    }

    @Test()
    public CurrentValue() : void {
        const event : ProgressEventArgs = new ProgressEventArgs();
        assert.equal(event.CurrentValue(6), 6);
    }
}
