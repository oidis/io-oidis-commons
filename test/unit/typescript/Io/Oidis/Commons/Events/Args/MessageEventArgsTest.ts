/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { MessageEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/MessageEventArgs.js";
import { Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class MessageEventArgsTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const event : MessageEventArgs = new MessageEventArgs();
        assert.equal(event.getClassName(), "Io.Oidis.Commons.Events.Args.MessageEventArgs");
    }

    @Test()
    public NativeEventArgs() : void {
        const event : MessageEventArgs = new MessageEventArgs();
        const messageEvent : any = {origin: "test"};
        event.NativeEventArgs(messageEvent);
        assert.equal(event.NativeEventArgs(), messageEvent);
    }
}
