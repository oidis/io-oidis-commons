/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { TimeoutManager } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/TimeoutManager.js";
import { ExceptionsManager } from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { IUnitTestRunnerPromise, Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";

export class TimeoutManagerTest extends UnitTestRunner {
    private timeout : TimeoutManager;

    @Test()
    public getId() : void {
        const timeout : TimeoutManager = new TimeoutManager();
        assert.notEqual(this.timeout.getId(), timeout.getId());
    }

    @Test()
    public Length() : void {
        assert.equal(this.timeout.Length(), 0);
    }

    @Test()
    public Execute() : void {
        assert.ok(this.timeout.Execute(), this.timeout.getId());
    }

    @Test(true)
    public ExecuteWithException() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            assert.onRedirect(
                () : void => {
                    this.timeout.Add(() : void => {
                        throw new Error("test async exception");
                    });
                    this.timeout.Execute();
                },
                () : void => {
                    assert.equal(ExceptionsManager.getLast().Message(), "test async exception");
                },
                $done);
        };
    }

    protected before() : void {
        this.timeout = new TimeoutManager();
    }
}
