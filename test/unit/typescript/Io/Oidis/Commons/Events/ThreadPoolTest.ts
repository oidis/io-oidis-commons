/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { GeneralEventOwner } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/Events/GeneralEventOwner.js";
import { EventArgs } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ThreadPool } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/ThreadPool.js";
import { HttpManager } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/HttpManager.js";
import { HttpRequestParser } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/HttpRequestParser.js";
import { IUnitTestRunnerPromise, Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";

export class ThreadPoolTest extends UnitTestRunner {
    private tmpHref : string;

    @Test()
    public AddThread() : void {
        assert.equal(ThreadPool.IsRunning("owner1", "type1"), false);
        ThreadPool.AddThread("owner1", "type1", () : void => {
            ThreadPool.RemoveThread("owner1", "type1");
        });
        assert.equal(ThreadPool.IsRunning("owner1", "type1"), true);
    }

    @Test()
    public RemoveThread() : void {
        assert.equal(ThreadPool.IsRunning("owner2", "type2"), false);
        ThreadPool.AddThread("owner2", "type2", () : void => {
            // mock thread callback
        });
        assert.equal(ThreadPool.IsRunning("owner2", "type2"), true);
        ThreadPool.RemoveThread("owner2", "type2");
        assert.equal(ThreadPool.IsRunning("owner2", "type2"), false);
    }

    @Test()
    public IsRunning() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            assert.equal(ThreadPool.IsRunning("owner3", "type3"), false);
            ThreadPool.AddThread("owner3", "type3", () : void => {
                assert.equal(ThreadPool.IsRunning("owner3", "type3"), true);
                this.initSendBox();
                $done();
            });
            ThreadPool.Execute();
        };
    }

    @Test()
    public IsNotRunning() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            assert.equal(ThreadPool.IsRunning(GeneralEventOwner.WINDOW, "myEventType1"), false);
            assert.equal(ThreadPool.IsRunning(GeneralEventOwner.WINDOW, "myEventType2"), false);
            ThreadPool.AddThread(GeneralEventOwner.WINDOW, "myEventType2", () : void => {
                assert.equal(ThreadPool.IsRunning(GeneralEventOwner.WINDOW, "myEventType1"), false);
                assert.equal(ThreadPool.IsRunning(GeneralEventOwner.WINDOW, "myEventType2"), true);
                ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, "myEventType2");
                this.initSendBox();
                $done();
            });
            ThreadPool.Execute();
        };
    }

    @Test(true)
    public setThreadArgs() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const args : EventArgs = new EventArgs();
            ThreadPool.AddThread(GeneralEventOwner.WINDOW, "myEventType3", ($eventArgs : EventArgs) : void => {
                ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, "myEventType3");
                assert.deepEqual($eventArgs, args);
                $done();
            });
            ThreadPool.setThreadArgs(GeneralEventOwner.WINDOW, "myEventType3", args);
            ThreadPool.Execute();
        };
    }

    @Test()
    public Clear() : void {
        assert.doesNotThrow(() : void => {
            ThreadPool.Clear();
        });
    }

    protected before() : void {
        this.tmpHref = window.location.href;
        this.setUrl("http://localhost.wuiframework.com/index.html#/project-name/unit/ThreadPoolTest");
        const manager : HttpManager = new HttpManager(new HttpRequestParser("project-name"));
    }

    protected after() : void {
        ThreadPool.RemoveThread("owner", "type1");
        ThreadPool.RemoveThread("owner", "type2");
        ThreadPool.RemoveThread("owner", "type3");
        ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, "myEventType1");
        ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, "myEventType2");
        ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, "myEventType3");

        this.setUrl(this.tmpHref);
        const manager : HttpManager = new HttpManager(new HttpRequestParser());
    }
}
