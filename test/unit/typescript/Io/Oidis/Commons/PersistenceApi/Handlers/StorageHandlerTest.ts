/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

import { ExceptionsManager } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import {
    BasePersistenceHandler
} from "../../../../../../../../source/typescript/Io/Oidis/Commons/PersistenceApi/Handlers/BasePersistenceHandler.js";
import { StorageHandler } from "../../../../../../../../source/typescript/Io/Oidis/Commons/PersistenceApi/Handlers/StorageHandler.js";
import { ArrayList } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { IUnitTestRunnerPromise, Test } from "../../BaseUnitTestRunner.js";

export class StorageHandlerTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const handler1 : StorageHandler = new StorageHandler();
        const handler2 : StorageHandler = new StorageHandler();
        assert.notEqual(handler1.getSessionId(), handler2.getSessionId());

        const handler3 : StorageHandler = new StorageHandler("sessionId");
        assert.equal(handler3.getSessionId(), "sessionId");
    }

    @Test()
    public getLoadTime() : void {
        const handler : StorageHandler = new StorageHandler();
        assert.equal(handler.getLoadTime(), 0);
    }

    @Test()
    public Exists() : void {
        const handler : StorageHandler = new StorageHandler();
        assert.ok(!handler.Exists("key"));
        handler.Variable("key", "value");
        assert.ok(handler.Exists("key"));
    }

    @Test()
    public getSize() : void {
        const handler1 : StorageHandler = new StorageHandler("storage4");
        const variablesList : ArrayList<any> = new ArrayList<any>();
        variablesList.Add("value", "key1");
        variablesList.Add("value2", "key2");
        handler1.Variable(variablesList);
        assert.equal(handler1.getSize(), 0);

        const handler2 : StorageHandler = new StorageHandler("storage4");
        assert.equal(handler2.Variable("key2"), "value2");
        assert.equal(handler2.getSize(), 258);
    }

    @Test()
    public Variable() : IUnitTestRunnerPromise {
        const handler : StorageHandler = new StorageHandler("storage2");
        assert.ok(!handler.Exists("key"));
        handler.Variable("key", "value");
        assert.ok(handler.Exists("key"));

        assert.ok(!handler.Exists("key1"));
        assert.ok(!handler.Exists("key2"));
        const variablesList : ArrayList<any> = new ArrayList<any>();
        variablesList.Add("value", "key1");
        variablesList.Add("value", "key2");
        handler.Variable(variablesList);
        assert.ok(handler.Exists("key"));
        assert.ok(handler.Exists("key1"));
        assert.ok(handler.Exists("key2"));
        const asyncHandler : any = () : void => {
            const data : any = "data";
        };

        const storage : StorageHandler = new StorageHandler("storage1");
        (<any>BasePersistenceHandler).crcCheckEnabled = true;
        assert.throws(() : void => {
            storage.Variable(null, asyncHandler);
        }, /Persistence name is supposed to be type of string or integer/);

        return ($done : () => void) : void => {
            handler.Variable("key2", "value", () : void => {
                assert.ok(handler.Exists("key2"));
                ExceptionsManager.Clear();
            });
            this.initSendBox();
            $done();
        };
    }

    @Test()
    public LoadPersistenceAsynchronously() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const handler : StorageHandler = new StorageHandler();
            handler.LoadPersistenceAsynchronously(() : void => {
                assert.equal(handler.Variable("key"), null);
            });
            ExceptionsManager.Clear();
            this.initSendBox();
            $done();
        };
    }

    @Test(true)
    public LoadPersistenceAsynchronously2() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const handler : StorageHandler = new StorageHandler();
            handler.LoadPersistenceAsynchronously(
                () : void => {
                    assert.equal(handler.Variable("key"), "value");
                    $done();
                }, "file://" + this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Commons/StoragePersistence.jsonp");
        };
    }

    @Test(true)
    public LoadPersistenceAsynchronously3() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const handler : StorageHandler = new StorageHandler("storage2");
            handler.LoadPersistenceAsynchronously(() : void => {
                assert.equal(handler.Variable("loading"), null);
                $done();
            }, "file://" + this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Commons/ValidData2.jsonp");

        };
    }

    @Test()
    public LoadPersistenceAsynchronously4() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const handler : StorageHandler = new StorageHandler("storage2");
            handler.LoadPersistenceAsynchronously(() : void => {
                assert.equal(handler.Variable(null), null);
                ExceptionsManager.Clear();

            }, "file://" + this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Commons/EmptyObject.jsonp");
            this.initSendBox();
            $done();
        };
    }

    @Test()
    public LoadPersistenceAsynchronously5() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const handler : StorageHandler = new StorageHandler();
            const list : ArrayList<any> = new ArrayList<any>();
            list.Add("test", 0);
            list.Add("test1", 1);
            (<any>StorageHandler).session = list;
            handler.LoadPersistenceAsynchronously(() : void => {
                $done();
            });
            ExceptionsManager.Clear();
        };
    }

    @Test()
    public Destroy() : void {
        const handler : StorageHandler = new StorageHandler();
        handler.Variable("key", "value");
        handler.Variable("key1", "value");
        handler.Variable("key2", "value");
        handler.Variable("key3", "value");

        assert.ok(handler.Exists("key"));
        assert.ok(handler.Exists("key1"));
        assert.ok(handler.Exists("key2"));
        assert.ok(handler.Exists("key3"));

        handler.Destroy("key");
        const variablesList : ArrayList<string> = new ArrayList<string>();
        variablesList.Add("key1");
        variablesList.Add("key2");
        variablesList.Add("key5");
        handler.Destroy(variablesList);

        assert.ok(!handler.Exists("key"));
        assert.ok(!handler.Exists("key1"));
        assert.ok(!handler.Exists("key2"));
        assert.ok(handler.Exists("key3"));

        const handler3 : StorageHandler = new StorageHandler("storage3");
        handler3.Destroy(null);
        assert.ok(!handler3.Exists("key3"));
        ExceptionsManager.Clear();
    }

    @Test()
    public Clear() : void {
        const handler : StorageHandler = new StorageHandler();
        handler.Variable("key", "value");
        handler.Variable("key1", "value");
        handler.Variable("key2", "value");

        assert.ok(handler.Exists("key"));
        assert.ok(handler.Exists("key1"));
        assert.ok(handler.Exists("key2"));

        handler.Clear();

        assert.ok(!handler.Exists("key"));
        assert.ok(!handler.Exists("key1"));
        assert.ok(!handler.Exists("key2"));
        ExceptionsManager.Clear();
    }

    @Test()
    public ToString1() : void {
        const handler : StorageHandler = new StorageHandler();
        handler.Variable("key", "value");
        this.resetCounters();
        assert.equal(handler.ToString(),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "[ \"key\" ]&nbsp;&nbsp;&nbsp;&nbsp;value<br/>" +
            "</span>");
        ExceptionsManager.Clear();
    }

    @Test()
    public getRawData() : void {
        const handler : StorageHandler = new StorageHandler();
        handler.Variable("key", "value");
        assert.equal(handler.getRawData(),
            "m:51:a:46:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayListc:0:168:o:162:s:4:sizei:2;s:4:keysa:28:i:0;" +
            "s:9:variablesi:1;s:3:crcs:4:dataa:96:i:0;c:0:67:o:62:s:4:sizei:1;" +
            "s:4:keysa:11:i:0;s:3:keys:4:dataa:13:i:0;s:5:valuei:1;i:-1799600031;");

        const handler2 : StorageHandler = new StorageHandler("storage1");
        const asyncHandler : any = () : void => {
            const data : string = "data";
        };
        assert.deepEqual(handler2.getRawData(asyncHandler), "");

        handler.Variable("key1", "value1");
        const crcstring : string = handler.getRawData();
        assert.equal(handler.getRawData(), crcstring);
        this.initSendBox();
    }

    @Test()
    public toString1() : void {
        const handler3 : StorageHandler = new StorageHandler("storage3");
        this.resetCounters();
        assert.equal(handler3.toString(),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span>");
        ExceptionsManager.Clear();
    }

    public testIsSupported() : void {
        const storage : any = localStorage;
        assert.equal(StorageHandler.IsSupported(), true);
        localStorage = null;
        assert.equal(StorageHandler.IsSupported(), false);
        localStorage = storage;
        ExceptionsManager.Clear();
    }
}
