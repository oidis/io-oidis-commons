/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

import {
    BasePersistenceHandler
} from "../../../../../../../../source/typescript/Io/Oidis/Commons/PersistenceApi/Handlers/BasePersistenceHandler.js";
import { CookiesHandler } from "../../../../../../../../source/typescript/Io/Oidis/Commons/PersistenceApi/Handlers/CookiesHandler.js";
import { ArrayList } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Convert } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Convert.js";
import { Property } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Utils/StringUtils.js";
import { IUnitTestRunnerPromise, Test } from "../../BaseUnitTestRunner.js";

export class CookiesHandlerTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const handler1 : CookiesHandler = new CookiesHandler();
        const handler2 : CookiesHandler = new CookiesHandler();
        assert.notEqual(handler1.getSessionId(), handler2.getSessionId());
        const handler3 : CookiesHandler = new CookiesHandler("sessionId");
        assert.equal(handler3.getSessionId(), "sessionId");
    }

    @Test()
    public getLoadTime() : void {
        const handler : CookiesHandler = new CookiesHandler();
        assert.equal(handler.getLoadTime(), 0);
    }

    @Test()
    public Exists() : void {
        this.setUrl("http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader");
        (<any>window.navigator).__defineGetter__("cookieEnabled", () : boolean => {
            return true;
        });
        const handler : CookiesHandler = new CookiesHandler();
        assert.ok(!handler.Exists("key"));
        handler.Variable("key", "value");
        assert.ok(handler.Exists("key"));

        (<any>window.navigator).__defineGetter__("cookieEnabled", () : boolean => {
            return false;
        });
    }

    @Test(true)
    public Variable() : IUnitTestRunnerPromise {
        const handler : CookiesHandler = new CookiesHandler();
        assert.ok(!handler.Exists("key"));
        handler.Variable("key", "value");
        assert.ok(handler.Exists("key"));
        assert.ok(!handler.Exists("key1"));
        assert.ok(!handler.Exists("key2"));
        const variablesList : ArrayList<any> = new ArrayList<any>();
        variablesList.Add("value", "key1");
        variablesList.Add("value", "key2");
        handler.Variable(variablesList);
        assert.ok(handler.Exists("key"));
        assert.ok(handler.Exists("key1"));
        assert.ok(handler.Exists("key2"));

        const handler3 : CookiesHandler = new CookiesHandler();
        const list : ArrayList<any> = new ArrayList<any>();
        list.Add("test", "key");
        list.Add("test1", "key1");
        handler3.Variable(list);
        list.Add("test2", "key2");
        handler3.Variable(list);
        list.Add("test3", "key3");

        const handler4 : CookiesHandler = new CookiesHandler();
        const array = ["key", "key1", "key2"];
        handler4.Variable("array");
        this.resetCounters();
        assert.equal(handler4.toString(),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span>");

        const handler2 : CookiesHandler = new CookiesHandler("cookies1");
        const asyncHandler : any = () : void => {
            const data : any = "data";
        };
        (<any>BasePersistenceHandler).crcCheckEnabled = true;
        assert.throws(() : void => {
            handler2.Variable(null, asyncHandler);
        }, /Persistence name is supposed to be type of string or integer/);

        return ($done : () => void) : void => {
            handler.Variable("key2", "value", () : void => {
                assert.ok(handler.Exists("key2"));
                $done();
            });
        };
    }

    @Test(true)
    public LoadPersistenceAsynchronously() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const handler : CookiesHandler = new CookiesHandler();
            handler.LoadPersistenceAsynchronously(() : void => {
                assert.equal(handler.Variable("key"), null);
                $done();
            });
        };
    }

    @Test(true)
    public LoadPersistenceAsynchronously2() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const handler : CookiesHandler = new CookiesHandler();
            handler.LoadPersistenceAsynchronously(
                () : void => {
                    assert.equal(handler.Variable("key"), null);
                    $done();
                },
                "file://" + this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Commons/CookiesPersistence.jsonp");
        };
    }

    @Test(true)
    public LoadPersistenceAsynchronously3() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const handler3 : CookiesHandler = new CookiesHandler("cookie2");
            const list : ArrayList<any> = new ArrayList<any>();
            list.Add("test", 0);
            list.Add("test1", 1);
            (<any>CookiesHandler).session = list;
            handler3.LoadPersistenceAsynchronously(() : void => {
                $done();
            });
        };
    }

    @Test(true)
    public LoadPersistenceAsynchronouslyExceptions() : IUnitTestRunnerPromise {
        const handler1 : CookiesHandler = new CookiesHandler("cookie2");
        handler1.LoadPersistenceAsynchronously(() : void => {
                // async load handler
            },
            "file://" + this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Commons/ValidData2.jsonp");

        const handler2 : CookiesHandler = new CookiesHandler("cookie2");
        handler2.LoadPersistenceAsynchronously(() : void => {
                // async load handler
            },
            "file://" + this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Commons/EmptyObject.jsonp");

        return ($done : () => void) : void => {
            setTimeout(() : void => {
                $done();
            }, 250);
        };
    }

    @Test(true)
    public getSize() : void {
        const handler8 : CookiesHandler = new CookiesHandler("cookie4");
        const variablesList : ArrayList<any> = new ArrayList<any>();
        variablesList.Add("value", "key1");
        variablesList.Add("value", "key2");
        handler8.Variable(variablesList);
        assert.equal(handler8.getSize(), 0);
    }

    @Test(true)
    public Destroy() : void {
        const handler : CookiesHandler = new CookiesHandler();
        handler.Variable("key", "value");
        handler.Variable("key1", "value");
        handler.Variable("key2", "value");
        handler.Variable("key3", "value");

        assert.ok(handler.Exists("key"));
        assert.ok(handler.Exists("key1"));
        assert.ok(handler.Exists("key2"));
        assert.ok(handler.Exists("key3"));

        handler.Destroy("key");
        const variablesList : ArrayList<string> = new ArrayList<string>();
        variablesList.Add("key1");
        variablesList.Add("key2");
        variablesList.Add("key5");
        handler.Destroy(variablesList);

        assert.ok(!handler.Exists("key"));
        assert.ok(!handler.Exists("key1"));
        assert.ok(!handler.Exists("key2"));
        assert.ok(handler.Exists("key3"));

        const handler3 : CookiesHandler = new CookiesHandler("storage3");
        handler3.Destroy(null);
        assert.ok(!handler3.Exists("key3"));
    }

    @Test(true)
    public Clear() : void {
        const handler : CookiesHandler = new CookiesHandler();
        handler.Variable("key", "value");
        handler.Variable("key1", "value");
        handler.Variable("key2", "value");

        assert.ok(handler.Exists("key"));
        assert.ok(handler.Exists("key1"));
        assert.ok(handler.Exists("key2"));

        handler.Clear();

        assert.ok(!handler.Exists("key"));
        assert.ok(!handler.Exists("key1"));
        assert.ok(!handler.Exists("key2"));
    }

    @Test(true)
    public ToString1() : void {
        const handler : CookiesHandler = new CookiesHandler();
        handler.Variable("key", "value");
        this.resetCounters();
        assert.equal(handler.ToString(),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "[ \"key\" ]&nbsp;&nbsp;&nbsp;&nbsp;value<br/>" +
            "</span>");
    }

    @Test(true)
    public getRawData() : void {
        const handler : CookiesHandler = new CookiesHandler();
        handler.Variable("key", "value");
        assert.equal(handler.getRawData(),
            "m:60:a:55:i:0;s:46:Io.Oidis.Commons.Primitives.ArrayListc:0:166:o:160:s:4:sizei:2;s:4:keysa:28:i:0;" +
            "s:9:variablesi:1;s:3:crcs:4:dataa:94:i:0;c:0:67:o:62:s:4:sizei:1;" +
            "s:4:keysa:11:i:0;s:3:keys:4:dataa:13:i:0;s:5:valuei:1;i:438593955;");

        const handler2 : CookiesHandler = new CookiesHandler("cookie1");
        const asyncHandler : any = () : void => {
            const data : string = "data";
        };
        assert.deepEqual(handler2.getRawData(asyncHandler), "");
    }

    @Test(true)
    public toString1() : void {
        const handler3 : CookiesHandler = new CookiesHandler("storage3");
        this.resetCounters();
        assert.equal(handler3.toString(),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span>");
    }

    protected before() : void {
        this.clearAll();
        window.location.reload = (forcedReload? : boolean) : void => {
            // mock implementation for missing native API
        };
    }

    protected after() : void {
        this.clearAll();
        window.location.reload = null;
    }

    private clearAll() : void {
        StringUtils.Split(document.cookie, ";").forEach(($value : any, $key? : any) : void => {
            document.cookie = $key + "=; expires=" + Convert.TimeToGMTformat(Property.Time(null, "-1 year"));
        });
        localStorage.clear();
    }
}
