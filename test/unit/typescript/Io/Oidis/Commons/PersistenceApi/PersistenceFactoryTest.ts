/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { PersistenceHandlerType } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/PersistenceHandlerType.js";
import { PersistenceType } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/PersistenceType.js";
import { ExceptionsManager } from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { IPersistenceHandler } from "../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { CookiesHandler } from "../../../../../../../source/typescript/Io/Oidis/Commons/PersistenceApi/Handlers/CookiesHandler.js";
import { StorageHandler } from "../../../../../../../source/typescript/Io/Oidis/Commons/PersistenceApi/Handlers/StorageHandler.js";
import { PersistenceFactory } from "../../../../../../../source/typescript/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { BaseObject } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Convert } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Convert.js";
import { Property } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/StringUtils.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";

class MockBaseObject extends BaseObject {
}

export class PersistenceFactoryTest extends UnitTestRunner {

    @Test()
    public getPersistence() : void {
        const persistenceManager1 : IPersistenceHandler = PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, "config");
        const persistenceManager2 : IPersistenceHandler = PersistenceFactory.getPersistence(PersistenceType.BROWSER);
        const persistenceManager3 : IPersistenceHandler = PersistenceFactory.getPersistence("GuiAutocomplete", "user1");

        assert.equal(PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, "config"), persistenceManager1);
        assert.equal(PersistenceFactory.getPersistence(PersistenceType.BROWSER), persistenceManager2);
        assert.equal(PersistenceFactory.getPersistence("GuiAutocomplete", "user1"), persistenceManager3);

        this.setUserAgent(window.navigator.userAgent + " com-wui-framework-jre");
        this.getHttpManager().RefreshWithoutReload();

        const persistence : IPersistenceHandler = PersistenceFactory.getPersistence(PersistenceType.BROWSER);
        assert.notEqual(persistence, null);
        assert.ok(persistence.Implements(IPersistenceHandler));

        const persistence2 : IPersistenceHandler = PersistenceFactory.getPersistence(PersistenceType.BROWSER);
        assert.deepEqual(persistence2, persistence);
        ExceptionsManager.Clear();
    }

    @Test()
    public getPersistenceById() : void {
        PersistenceFactory.setPersistenceHandler(<any>"testType");
        assert.equal(PersistenceFactory.getPersistenceById("test"), null);
        const member : string = "persistenceHandlerType";
        delete PersistenceFactory[member];

        assert.equal(PersistenceFactory.getPersistenceById(null), null);

        PersistenceFactory.setPersistenceHandler(PersistenceHandlerType.COOKIES);
        const cookiesHandler : IPersistenceHandler = PersistenceFactory.getPersistenceById("cookies");
        assert.deepEqual(PersistenceFactory.getPersistenceById("cookies"), cookiesHandler);

        PersistenceFactory.setPersistenceHandler(PersistenceHandlerType.STORAGE);
        const storage : IPersistenceHandler = PersistenceFactory.getPersistenceById("storage");
        assert.deepEqual(PersistenceFactory.getPersistenceById("storage"), storage);
        ExceptionsManager.Clear();
    }

    @Test()
    public getHttpSessionId() : void {
        this.setUrl("http://localhost:8888/UnitTestEnvironment.js?sessionid=5");
        this.getHttpManager().RefreshWithoutReload();
        assert.equal(PersistenceFactory.getHttpSessionId(), "5");

        this.setUrl("http://localhost:8888/UnitTestEnvironment.js?sessionid=6");
        this.setUserAgent(window.navigator.userAgent + " com-wui-framework-jre");
        this.getHttpManager().RefreshWithoutReload();
        assert.equal(PersistenceFactory.getHttpSessionId(), PersistenceFactory.getHttpSessionId());

        this.setUrl("http://localhost:8888/UnitTestEnvironment.js");
        this.setUserAgent(window.navigator.userAgent);
        this.getHttpManager().RefreshWithoutReload();
        StringUtils.Split(document.cookie, ";").forEach(($value : any, $key? : any) : void => {
            document.cookie = $key + "=; expires=" + Convert.TimeToGMTformat(Property.Time(null, "-1 year"));
        });
        assert.notEqual(PersistenceFactory.getHttpSessionId(), PersistenceFactory.getHttpSessionId());
        ExceptionsManager.Clear();
    }

    @Test()
    public getPersistenceType() : void {
        assert.equal(PersistenceFactory.getPersistenceType(new CookiesHandler()), PersistenceHandlerType.COOKIES);
        assert.equal(PersistenceFactory.getPersistenceType(new StorageHandler()), PersistenceHandlerType.STORAGE);
        assert.equal(PersistenceFactory.getPersistenceType(<any>(new MockBaseObject())), null);
        ExceptionsManager.Clear();
    }

    @Test()
    public DestroyAll() : void {
        (<any>window.navigator).__defineGetter__("cookieEnabled", () : boolean => {
            return true;
        });
        this.getHttpManager().RefreshWithoutReload();
        const persistence1 : IPersistenceHandler = PersistenceFactory.getPersistence("GuiAutocomplete", "user1");
        persistence1.ExpireTime("10 sec");
        const persistence2 : IPersistenceHandler = PersistenceFactory.getPersistence("GuiAutocomplete", "user2");
        PersistenceFactory.DestroyAll();
        assert.equal(persistence2.Exists("GuiAutocomplete"), false);

        const member : string = "persistencesList";
        delete PersistenceFactory[member];
        assert.doesNotThrow(() : void => {
            PersistenceFactory.DestroyAll();
        });
        ExceptionsManager.Clear();
    }
}
