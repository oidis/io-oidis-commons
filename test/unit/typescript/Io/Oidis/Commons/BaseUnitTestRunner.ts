/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import {
    __unitTestRunner, assert,
    builder,
    chromeBuilder,
    ColorType,
    isBrowserRun,
    IVBoxManager,
    jsdom,
    selenium,
    vbox
} from "./UnitTestEnvironment.js";

import { BrowserType } from "../../../../../../source/typescript/Io/Oidis/Commons/Enums/BrowserType.js";
import { EventType } from "../../../../../../source/typescript/Io/Oidis/Commons/Enums/Events/EventType.js";
import { GeneralEventOwner } from "../../../../../../source/typescript/Io/Oidis/Commons/Enums/Events/GeneralEventOwner.js";
import { LogLevel } from "../../../../../../source/typescript/Io/Oidis/Commons/Enums/LogLevel.js";
import { LogSeverity } from "../../../../../../source/typescript/Io/Oidis/Commons/Enums/LogSeverity.js";
import { EnvironmentArgs } from "../../../../../../source/typescript/Io/Oidis/Commons/EnvironmentArgs.js";
import { AsyncRequestEventArgs } from "../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { HttpRequestEventArgs } from "../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/HttpRequestEventArgs.js";
import { EventsManager } from "../../../../../../source/typescript/Io/Oidis/Commons/Events/EventsManager.js";
import { ThreadPool } from "../../../../../../source/typescript/Io/Oidis/Commons/Events/ThreadPool.js";
import { ExceptionsManager } from "../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { HttpManager } from "../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/HttpManager.js";
import { HttpRequestParser } from "../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/HttpRequestParser.js";
import { HttpResolver } from "../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/HttpResolver.js";
import { BaseHttpResolver } from "../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { IEventsManager } from "../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/IEventsManager.js";
import { BaseOutputHandler } from "../../../../../../source/typescript/Io/Oidis/Commons/IOApi/Handlers/BaseOutputHandler.js";
import { ConsoleAdapter } from "../../../../../../source/typescript/Io/Oidis/Commons/LogProcessor/Adapters/ConsoleAdapter.js";
import { Logger } from "../../../../../../source/typescript/Io/Oidis/Commons/LogProcessor/Logger.js";
import { BaseObject } from "../../../../../../source/typescript/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Counters } from "../../../../../../source/typescript/Io/Oidis/Commons/Utils/Counters.js";
import { Echo } from "../../../../../../source/typescript/Io/Oidis/Commons/Utils/Echo.js";
import { JsonUtils } from "../../../../../../source/typescript/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "../../../../../../source/typescript/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "../../../../../../source/typescript/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "../../../../../../source/typescript/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "../../../../../../source/typescript/Io/Oidis/Commons/Utils/StringUtils.js";

declare const process : any;
declare const global : any;

/**
 * Decorate suite methods as test methods.
 * @param {boolean} [$ignored=false] Specify if test method should be ignored.
 * @returns {Function} Returns decorator which enable to generate interface expected by unit test runner process.
 */
export function Test($ignored : boolean = false) : any {

    return function ($target : any, $propertyKey : string, $descriptor : PropertyDescriptor) : any {
        if (ObjectValidator.IsEmptyOrNull($target._testMethods)) {
            $target._testMethods = [];
        } else {
            // copy of array causes that actual property will not be written into base
            // and thus also not into different child of the same base class
            $target._testMethods = $target._testMethods.slice();
        }
        const index = $target._testMethods.findIndex(($item : any) : boolean => {
            return $item.name === $propertyKey;
        });
        if (index < 0) {
            $target._testMethods.push({
                name    : $propertyKey,
                testName: $propertyKey,
                ignored : $ignored
            });
        } else {
            throw new Error("Multiple definitions of the same test method '" + $propertyKey +
                "' has been detected in '" + $target.getClassName() + " suite.");
        }
    };
}

export abstract class BaseUnitTestRunner extends BaseObject {
    private static windowCache : Window;
    private static loader : any;
    protected driver : any;
    protected by : any;
    protected vbox : IVBoxManager;
    protected defaultTimeout : number;
    private readonly unitRootPath : string;
    private readonly mockServerLocation : string;
    private methodFilter : string[];
    private readonly environment : any;
    private passingCount : number;
    private failingCount : number;
    private withSolutionConfig : boolean;
    private withEchoPrint : boolean;
    private currentTest : ITestContext;

    constructor() {
        super();
        this.defaultTimeout = 10000;

        if (isBrowserRun) {
            this.driver = chromeBuilder.build();
            this.driver.manage().window().maximize();
            this.by = selenium.By;
        }
        this.vbox = vbox;

        this.methodFilter = [];
        this.withSolutionConfig = true;
        this.withEchoPrint = false;

        this.environment = {
            name      : "io-oidis-builder",
            version   : "2022.3.0",
            build     : {
                time     : new Date().toTimeString(),
                timestamp: new Date().getTime(),
                type     : "dev"
            },
            namespaces: __unitTestRunner.namespaces,
            target    : {
                name   : "UnitTest",
                metrics: {
                    enabled : false,
                    location: ""
                }
            },
            domain    : {
                location: "localhost"
            }
        };
        this.unitRootPath = __unitTestRunner.unitRootPath;
        this.mockServerLocation = __unitTestRunner.mockServerLocation;
        this.initLoader();
        this.passingCount = 0;
        this.failingCount = 0;
    }

    public async Process() : Promise<void> {
        return this.resolver();
    }

    protected timeoutLimit($value? : number) : number {
        if (!ObjectValidator.IsEmptyOrNull(this.currentTest)) {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.currentTest.timeoutLimit = $value;
            }
            return this.currentTest.timeoutLimit;
        }
        return -1;
    }

    protected before() : void | IUnitTestRunnerPromise | Promise<void> {
        // override this method for ability to execute code BEFORE run of ALL tests in current UnitTest
        return;
    }

    protected setUp() : void | IUnitTestRunnerPromise | Promise<void> {
        // override this method for ability to execute code BEFORE EACH test in current UnitTest
        return;
    }

    protected tearDown() : void | IUnitTestRunnerPromise | Promise<void> {
        // override this method for ability to execute code AFTER EACH test in current UnitTest
        return;
    }

    protected after() : void | IUnitTestRunnerPromise | Promise<void> {
        // override this method for ability to execute code AFTER run of ALL tests in current UnitTest
        return;
    }

    protected setMethodFilter(...$names : string[]) : void {
        $names.forEach(($name : string) : void => {
            this.methodFilter.push(StringUtils.ToLowerCase($name));
        });
    }

    protected stripInstrumentation($input : string) : string {
        return ($input + "")
            .replace(/(cov_.*\+\+;)/gm, "")
            .replace(/\(cov_.*?\+\+, (.*?)(\))?\)/g, ($match : string, $value : string, $postfix : string) : string => {
                return $value + (!ObjectValidator.IsEmptyOrNull($postfix) ? $postfix : "");
            });
    }

    protected resetCounters() : void {
        Counters.Clear();
        (<any>BaseOutputHandler).handlerIterator = 0;
    }

    protected registerElement($id : string, $tagName? : string) : void {
        if (!ObjectValidator.IsSet($tagName)) {
            $tagName = "div";
        }
        const element = document.createElement($tagName);
        element.id = $id;
        document.body.appendChild(element);
    }

    protected reload() : void {
        if (!ObjectValidator.IsEmptyOrNull(BaseUnitTestRunner.loader.getInstance())) {
            this.getHttpManager().RefreshWithoutReload();
        }
    }

    protected setUrl($value : string) : void {
        window.location.href = $value;
        jsdom.reconfigure({url: $value});
        this.reload();
    }

    protected setUserAgent($browserTypeOrValue : BrowserType | string, $modern : boolean = true) : void {
        let value : string = BaseUnitTestRunner.windowCache.navigator.userAgent;
        switch ($browserTypeOrValue) {
        case BrowserType.INTERNET_EXPLORER:
            if ($modern) {
                value =
                    "Mozilla/4.0 (.NET CLR 3.5.30729; .NET CLR 3.0.30729; " +
                    "Media Center PC 6.0; MS-RTC LM 8; .NET4.0C; .NET4.0E; rv:11.0)";
            } else {
                value =
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; " +
                    "Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; " +
                    "Media Center PC 6.0; MS-RTC LM 8; .NET4.0C; .NET4.0E)";
            }
            break;
        case BrowserType.FIREFOX:
            value = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0";
            break;
        case BrowserType.GOOGLE_CHROME:
            value =
                "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) " +
                "Chrome/52.0.2743.116 Safari/537.36";
            break;
        case BrowserType.OPERA:
            value =
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) " +
                "Chrome/52.0.2743.116 Safari/537.36 OPR/39.0.2256.71";
            break;
        case BrowserType.SAFARI:
            value = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2";
            break;
        default:
            if (!ObjectValidator.IsEmptyOrNull($browserTypeOrValue)) {
                value = <string>$browserTypeOrValue;
            }
            break;
        }

        (<any>window.navigator).__defineGetter__("userAgent", function () {
            return value;
        });

        this.reload();
    }

    protected setCookieItem($key : string, $value : string) : void {
        document.cookie = $key + "=" + $value + ";";
        this.reload();
    }

    protected setStorageItem($key : string, $value : string) : void {
        localStorage.setItem($key, $value);
        this.reload();
    }

    protected getAbsoluteRoot() : string {
        return this.unitRootPath;
    }

    protected getMockServerLocation() : string {
        return this.mockServerLocation;
    }

    protected enableSolutionConfig($value : boolean = true) : void {
        this.withSolutionConfig = $value;
    }

    protected enableEchoPrint($value : boolean = true) : void {
        this.withEchoPrint = $value;
    }

    protected initLoader($className? : any) : void {
        if (ObjectValidator.IsEmptyOrNull($className)) {
            if (ObjectValidator.IsEmptyOrNull(BaseUnitTestRunner.loader)) {
                let loaderClassName = __unitTestRunner.loaderClass;
                if (ObjectValidator.IsEmptyOrNull(loaderClassName)) {
                    loaderClassName = "Io.Oidis.Commons.Loader";
                }
                let classObject = globalThis;
                loaderClassName.split(".").forEach(($part : string) : void => {
                    if (!ObjectValidator.IsEmptyOrNull(classObject[$part])) {
                        classObject = classObject[$part];
                    }
                });
                BaseUnitTestRunner.loader = classObject;
            }
        } else {
            BaseUnitTestRunner.loader = $className;
        }
    }

    protected initSendBox() : void {
        this.setUrl(BaseUnitTestRunner.windowCache.location.href);
        (<any>window).location.headers = (<any>BaseUnitTestRunner).windowCache.location.headers;
        this.setUserAgent(BrowserType.FIREFOX);

        this.registerElement("Content");
        let env : any = JsonUtils.Clone(this.environment);
        if (this.withSolutionConfig) {
            env = JsonUtils.Extend(builder.getProjectConfig(), env);
        }
        BaseUnitTestRunner.loader.Load(env);
        ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST);
        if (globalThis.Io.Oidis.Gui) {
            globalThis.Io.Oidis.Gui.Utils.StaticPageContentManager.Clear(true);
        }
        document.cookie = "";
        localStorage.clear();
        document.documentElement.innerHTML = "";
        this.registerElement("Content");
        Echo.Init("Content", true);
        if (this.withEchoPrint) {
            Echo.setOnPrint(($message : string) : void => {
                console.log($message); // eslint-disable-line no-console
            });
        }
        (<any>ThreadPool).getEvents = () : IEventsManager => {
            const instance : IEventsManager = globalThis.Io.Oidis.Gui ?
                new globalThis.Io.Oidis.Gui.Events.EventsManager() :
                new globalThis.Io.Oidis.Commons.Events.EventsManager();
            (<any>ThreadPool).getEvents = () : IEventsManager => {
                return instance;
            };
            return instance;
        };
        const logger : Logger = new Logger();
        logger.setLevels(LogLevel.ALL, LogSeverity.LOW);
        logger.setModes(true, false);
        const adapter : ConsoleAdapter = new ConsoleAdapter(true);
        adapter.ColorsEnabled(false);
        logger.setAdapters([adapter]);
        LogIt.Init(logger);

        /* eslint-disable no-console */
        console.log = ($message : string) : void => {
            if (!ObjectValidator.IsEmptyOrNull($message)) {
                console.info($message);
            }
            if (StringUtils.StartsWith($message, "ERROR:")) {
                throw new Error(this.getSuiteName() + "\n" + $message);
            }
        };
        /* eslint-enable */

        if (globalThis.Io.Oidis.Localhost) {
            process.env.NODE_PATH = process.cwd() + "/dependencies/nodejs/build/node_modules";
        }
    }

    protected integrityCheck() : string {
        let guiObjectManagerCRC : string = "";
        let bodyContentCRC : string = "";
        if (ObjectValidator.IsSet((<any>this.getHttpResolver()).getGuiRegister)) {
            guiObjectManagerCRC = "    GuiObjectManager CRC: " +
                StringUtils.getCrc((<any>this.getHttpResolver()).getGuiRegister().ToString("", false)) + "\n";
        }
        if (!globalThis.Io.Oidis.Localhost) {
            bodyContentCRC =
                "    Body content CRC: " + StringUtils.getCrc(document.documentElement.innerHTML) + "\n";
        }
        return "\n" +
            "    EventsManager CRC: " + StringUtils.getCrc(this.getEventsManager().ToString("", false)) + "\n" +
            "    ExceptionsManager CRC: " + StringUtils.getCrc(ExceptionsManager.getAll().ToString("", false)) + "\n" +
            "    HttpManager CRC: " + StringUtils.getCrc(this.getHttpManager().getResolversCollection().ToString("", false)) + "\n" +
            "    Environment CRC: " + StringUtils.getCrc(this.getEnvironmentArgs().ToString("", false)) + "\n" +
            bodyContentCRC +
            guiObjectManagerCRC;
    }

    protected async resolver() : Promise<void> {
        const tests : ITestMethod[] = ObjectValidator.IsEmptyOrNull((<any>this)._testMethods) ? [] : (<any>this)._testMethods;

        this.prepareAssert();

        if (!ObjectValidator.IsSet(BaseUnitTestRunner.windowCache)) {
            BaseUnitTestRunner.windowCache = <any>{
                location : {
                    hash    : window.location.hash,
                    headers : (<any>window).location.headers,
                    host    : window.location.host,
                    hostname: window.location.hostname,
                    href    : window.location.href,
                    origin  : window.location.origin,
                    pathname: window.location.pathname,
                    port    : window.location.port,
                    protocol: window.location.protocol,
                    search  : window.location.search
                },
                navigator: {
                    cookieEnabled: window.navigator.cookieEnabled,
                    language     : window.navigator.language,
                    onLine       : window.navigator.onLine,
                    platform     : window.navigator.platform,
                    userAgent    : window.navigator.userAgent
                }
            };
            this.initSendBox();
        }

        await this.handlePromiseCall(this.before);

        const methods : string[] = this.getMethods();
        for (const method of methods) {
            if (StringUtils.Contains(StringUtils.ToLowerCase(method), "test")) {
                const testName : string = StringUtils.Remove(method, "test", "__Ignore");
                if (tests.findIndex(($test) => $test.name === method) === -1) {
                    tests.push({
                        ignored: StringUtils.StartsWith(method, "__Ignore"),
                        name   : method,
                        testName
                    });
                } else {
                    // TODO(mkelnar) this error can not be thrown because it will fail also for "@Test() testMethod", TBD
                    // throw new Error("Only one definition from decorated '" + testName + "' or '" + methodName + "' method is allowed.");
                }
            }
        }

        if (tests.length > 0) {
            __unitTestRunner.events.onSuiteStart();
            const suiteContext : ISuiteContext = {
                assertErrors : [],
                integrityTest: "",
                isRunning    : true,
                name         : this.getSuiteName(),
                started      : new Date(),
                tests
            };
            for await(const test of tests) {
                await this.processTest(suiteContext, test);
            }
            if (suiteContext.assertErrors.length > 0) {
                this.reportAssertError(suiteContext);
            }
            await this.handlePromiseCall(this.after);
            if (!ObjectValidator.IsEmptyOrNull(suiteContext.integrityTest)) {
                console.error(suiteContext.integrityTest); // eslint-disable-line no-console
            }
            __unitTestRunner.events.onSuiteEnd(this.passingCount, this.failingCount);
        } else {
            console.log("Suite \"" + this.getClassName() + "\" skipped: no test methods found"); // eslint-disable-line no-console
        }
    }

    protected getEnvironmentArgs() : EnvironmentArgs {
        return BaseUnitTestRunner.loader.getInstance().getEnvironmentArgs();
    }

    protected getHttpResolver() : HttpResolver {
        return BaseUnitTestRunner.loader.getInstance().getHttpResolver();
    }

    protected getHttpManager() : HttpManager {
        return BaseUnitTestRunner.loader.getInstance().getHttpManager();
    }

    protected getRequest() : HttpRequestParser {
        return BaseUnitTestRunner.loader.getInstance().getHttpManager().getRequest();
    }

    protected getEventsManager() : EventsManager {
        return BaseUnitTestRunner.loader.getInstance().getHttpResolver().getEvents();
    }

    private getSuiteName() : string {
        return "Suite " + this.getClassName();
    }

    private reportAssertError($context : IEntityContext) : void {
        $context.assertErrors.forEach(($ex : Error) : void => {
            __unitTestRunner.report.assert($context.name, $ex);
        });
        $context.assertErrors = [];
        __unitTestRunner.report.fail($context.name, (new Date()).getTime() - $context.started.getTime());
    }

    private handlePromiseCall($promise : any, $context : IEntityContext = null) : Promise<void> {
        return new Promise<void>(($resolve : () => void, $reject : ($error : Error) => void) => {
            const update : any = ($status : boolean) => {
                if (!ObjectValidator.IsEmptyOrNull($context)) {
                    $context.isRunning = $status;
                }
            };
            const resolve : any = () => {
                update(false);
                $resolve();
            };
            const reject : any = ($error : Error) => {
                update(false);
                $reject($error);
            };
            try {
                update(true);
                const promiseInterface : any = $promise.apply(this, []);
                if (!ObjectValidator.IsEmptyOrNull(promiseInterface)) {
                    if (promiseInterface instanceof Promise || promiseInterface.constructor.name === "Promise") {
                        promiseInterface.then(resolve).catch(reject);
                    } else if (ObjectValidator.IsFunction(promiseInterface)) {
                        promiseInterface.apply(this, [resolve]);
                    } else {
                        reject(new Error("IUnitTestRunnerPromise or standard Promise interface is required " +
                            "for asynchronous tests."));
                    }
                } else {
                    resolve();
                }
            } catch (ex) {
                reject(ex);
            }
        });
    }

    private async processTest($suite : ISuiteContext, $method : ITestMethod) : Promise<void> {
        const fullTestName : string = this.getClassName() + " - " + StringUtils.Remove($method.name, "test", "__Ignore");

        if (__unitTestRunner.ignoreList !== null) {
            this.methodFilter = [];
            const fullTestMethodName : string = this.getClassName() + "." + $method.testName;
            __unitTestRunner.ignoreList.forEach(($pattern : string) : void => {
                if (StringUtils.PatternMatched($pattern, fullTestMethodName)) {
                    $method.ignored = true;
                }
            });
        }
        if (this.methodFilter.length === 0 && $method.ignored ||
            this.methodFilter.length !== 0 && (
                this.methodFilter.indexOf(StringUtils.ToLowerCase($method.name)) === -1 &&
                this.methodFilter.indexOf(StringUtils.ToLowerCase("test" + $method.name)) === -1 &&
                this.methodFilter.indexOf(StringUtils.ToLowerCase("test" + $method.testName)) === -1)) {
            __unitTestRunner.report.skip(fullTestName);
        } else {
            this.currentTest = {
                assertErrors  : [],
                integrityTest : "",
                isDisposed    : false,
                isFailing     : false,
                isRunning     : false,
                name          : fullTestName,
                started       : new Date(),
                timeout       : 0,
                timeoutExpired: false,
                timeoutLimit  : this.defaultTimeout
            };
            try {
                this.currentTest.integrityTest = this.integrityCheck();
                this.initSendBox();
                await this.handlePromiseCall(this.setUp);
                __unitTestRunner.events.onTestCaseStart(this.currentTest.name);

                try {
                    await Promise.race([
                        this.handlePromiseCall(this[$method.name], this.currentTest),
                        new Promise<void>(($resolve : () => void) : void => {
                            __unitTestRunner.processExist = $resolve;
                        }),
                        new Promise<void>(($resolve : () => void, $reject : ($error : Error) => void) : void => {
                            // save handle for timeout handler to prevent ref override by another test
                            // first timeout 100ms is just for this.timeoutLimit() at the beginning of test method to be applied
                            const testContextHandle : ITestContext = this.currentTest;
                            testContextHandle.timeout = <any>setTimeout(() : void => {
                                if (process.env.hasOwnProperty("OIDIS_NODEJS_DEBUGGER_ATTACHED")) {
                                    __unitTestRunner.console.log(ColorType.setColor(">>", ColorType.YELLOW) + " WARNING: " +
                                        "executing test with attached debugger sets timeout limit to -1 " +
                                        "- this can lead to test execution block");
                                } else if (this.currentTest.timeoutLimit > -1) {
                                    testContextHandle.timeout = <any>setTimeout(() : void => {
                                        if (!testContextHandle.isDisposed) {
                                            testContextHandle.timeoutExpired = true;
                                            const error : Error = new Error("Test case/suite has " +
                                                "reached timeout (" + testContextHandle.timeoutLimit + " ms)");
                                            __unitTestRunner.report.fail(testContextHandle.name, testContextHandle.timeoutLimit, error);
                                            $reject(error);
                                        }
                                    }, testContextHandle.timeoutLimit);
                                } else {
                                    __unitTestRunner.console.log(ColorType.setColor(">>", ColorType.YELLOW) + " WARNING: " +
                                        "executing test with infinite timeout [see this.timeoutLimit(-1)] " +
                                        "- it can lead to test execution block");
                                }
                            }, 100);
                        })
                    ]);
                } catch (ex) {
                    this.currentTest.isRunning = false;
                    this.currentTest.isFailing = true;
                    if (this.currentTest.timeoutExpired || !StringUtils.Contains(ex.stack, "AssertionError: ")) {
                        __unitTestRunner.report.error(this.currentTest.name, ex);
                    }
                } finally {
                    if (!ObjectValidator.IsEmptyOrNull(this.currentTest.timeout)) {
                        clearTimeout(this.currentTest.timeout);
                        delete this.currentTest.timeout;
                    }
                    __unitTestRunner.clearTimeouts();
                }

                if (!this.currentTest.isFailing && !this.currentTest.timeoutExpired) {
                    this.passingCount++;
                    __unitTestRunner.report.pass(this.currentTest.name, new Date().getTime() - this.currentTest.started.getTime());
                } else {
                    this.failingCount++;
                    if (!this.currentTest.timeoutExpired) {
                        this.reportAssertError(this.currentTest);
                    }
                }

                await this.handlePromiseCall(this.tearDown);

                __unitTestRunner.events.onTestCaseEnd(this.currentTest.name);
                const afterTest : string = this.integrityCheck();
                if (this.currentTest.integrityTest !== afterTest) {
                    $suite.integrityTest +=
                        "\x1b[31m" +
                        "WARNING: Integrity test failure at: " + this.currentTest.name + "\n" +
                        "Expected: " + this.currentTest.integrityTest + "\n" +
                        "Actual: " + afterTest +
                        "\x1b[0m";
                }
            } catch (ex) {
                if (!this.currentTest.isFailing) {
                    this.currentTest.isFailing = true;
                    __unitTestRunner.report.error(this.currentTest.name, ex);
                }
            }
            this.currentTest.isDisposed = true;
        }
    }

    private prepareAssert() : void {
        const resolveAssert : any = ($validator : () => void) : void => {
            try {
                $validator();
                if (!ObjectValidator.IsEmptyOrNull(this.currentTest)) {
                    this.currentTest.isFailing = false;
                    this.currentTest.assertErrors = [];
                }
                __unitTestRunner.events.onAssert(true);
            } catch (ex) {
                if (!ObjectValidator.IsEmptyOrNull(this.currentTest)) {
                    this.currentTest.isFailing = true;
                    this.currentTest.assertErrors.push(ex);
                }
                __unitTestRunner.events.onAssert(false);
                throw ex;
            }
        };

        assert.ok = ($value : any, $message? : string) : void => {
            resolveAssert(() : void => {
                __unitTestRunner.assert.ok($value, $message);
            });
        };

        assert.equal = ($actual : any, $expected : any, $message? : string) : void => {
            resolveAssert(() : void => {
                if (!ObjectValidator.IsEmptyOrNull($message)) {
                    $message += ": ";
                } else {
                    $message = "";
                }
                try {
                    __unitTestRunner.assert.equal($actual, $expected, $message);
                } catch (ex) {
                    throw new Error(
                        "AssertionError: " + $message + "\n" + JSON.stringify($actual) + "\nexpected to be\n" +
                        JSON.stringify($expected) + "\n"
                    );
                }
            });
        };

        assert.notEqual = ($actual : any, $expected : any, $message? : string) : void => {
            resolveAssert(() : void => {
                if (!ObjectValidator.IsEmptyOrNull($message)) {
                    $message += ": ";
                } else {
                    $message = "";
                }
                try {
                    __unitTestRunner.assert.notEqual($actual, $expected, $message);
                } catch (ex) {
                    throw new Error(
                        "AssertionError: " + $message + "\n" + JSON.stringify($actual) + "\nexpected to be NOT\n" +
                        JSON.stringify($expected) + "\n"
                    );
                }
            });
        };

        assert.deepEqual = ($actual : any, $expected : any, $message? : string) : void => {
            resolveAssert(() : void => {
                if (!ObjectValidator.IsEmptyOrNull($message)) {
                    $message += ": ";
                } else {
                    $message = "";
                }
                try {
                    __unitTestRunner.assert.deepEqual($actual, $expected, $message);
                } catch (ex) {
                    throw new Error(
                        "AssertionError: " + $message + "\n" + JSON.stringify($actual) + "\nexpected to be\n" +
                        JSON.stringify($expected) + "\n"
                    );
                }
            });
        };

        assert.notDeepEqual = ($actual : any, $expected : any, $message? : string) : void => {
            resolveAssert(() : void => {
                if (!ObjectValidator.IsEmptyOrNull($message)) {
                    $message += ": ";
                } else {
                    $message = "";
                }
                try {
                    __unitTestRunner.assert.notDeepEqual($actual, $expected, $message);
                } catch (ex) {
                    throw new Error(
                        "AssertionError: " + $message + "\n" + JSON.stringify($actual) + "\nexpected to be NOT\n" +
                        JSON.stringify($expected) + "\n"
                    );
                }
            });
        };

        assert.throws = ($block : any, $error? : any, $message? : string) : void => {
            resolveAssert(() : void => {
                let passed : boolean = true;
                ExceptionsManager.Clear();
                try {
                    $block();
                } catch (ex) {
                    passed = false;
                    if (!ExceptionsManager.IsNativeException(ex) && StringUtils.Contains(ex.message, ".ExRethrow")) {
                        ex.message = ExceptionsManager.getLast().Message();
                        ExceptionsManager.Clear();
                    }
                    if (ObjectValidator.IsString($error)) {
                        $error = new RegExp($error);
                    }
                    if (ObjectValidator.IsSet($error) && $error.exec(ex.message) === null) {
                        if (!ObjectValidator.IsEmptyOrNull($message)) {
                            $message += ": ";
                        } else {
                            $message = "";
                        }
                        throw Error("AssertionError: " + $message + "\"" + ex.message + "\" " +
                            "expected to be in match with regEx: \"" + $error.toString() + "\"");
                    }
                }
                if (passed) {
                    throw Error("AssertionError: Assertion expected throwing an error.");
                }
            });
        };

        assert.doesHandleException = ($block : any, $error? : any, $message? : string) : void => {
            resolveAssert(() : void => {
                let passed : boolean = true;
                let error : string = "";
                const consoleError : any = console.error; // eslint-disable-line no-console
                console.error = ($data : string) : void => { // eslint-disable-line no-console
                    if (StringUtils.Contains($data, "][ERROR] ")) {
                        error = ExceptionsManager.getLast().Message();
                        ExceptionsManager.Clear();
                    }
                };
                ExceptionsManager.Clear();
                try {
                    $block();
                } catch (ex) {
                    passed = false;
                }
                console.error = consoleError; // eslint-disable-line no-console
                if (passed) {
                    if (ObjectValidator.IsString($error)) {
                        $error = new RegExp($error);
                    }
                    if (ObjectValidator.IsSet($error) && $error.exec(error) === null) {
                        if (!ObjectValidator.IsEmptyOrNull($message)) {
                            $message += ": ";
                        } else {
                            $message = "";
                        }
                        throw Error("AssertionError: " + $message + "\"" + error + "\" " +
                            "expected to be in match with regEx: \"" + $error.toString() + "\"");
                    }
                } else {
                    throw Error("AssertionError: Assertion expected handling an error.");
                }
            });
        };

        assert.onRedirect = ($setUp : () => void, $resolve : ($eventArgs : AsyncRequestEventArgs) => void, $done : () => void,
                             $urlBase? : string) : void => {
            resolveAssert(() : void => {
                /* eslint-disable no-console */
                console.log = ($message : string) : void => {
                    console.info($message);
                };
                /* eslint-enable */
                this.getHttpResolver().CreateRequest($urlBase);
                this.reload();
                this.getEventsManager().Clear(GeneralEventOwner.WINDOW, EventType.ON_ASYNC_REQUEST);
                this.getEventsManager().setEvent(GeneralEventOwner.WINDOW, EventType.ON_ASYNC_REQUEST,
                    ($eventArgs : AsyncRequestEventArgs) : void => {
                        // eslint-disable-next-line no-useless-catch
                        try {
                            $resolve($eventArgs);
                        } catch (ex) {
                            throw ex;
                        }
                        $done();
                    });
                $setUp();
            });
        };

        assert.doesNotThrow = ($block : any, $error? : any, $message? : string) : void => {
            resolveAssert(() : void => {
                ExceptionsManager.Clear();
                try {
                    $block();
                } catch (ex) {
                    if (!ExceptionsManager.IsNativeException(ex) && StringUtils.Contains(ex.message, ".ExRethrow")) {
                        ex.message = ExceptionsManager.getLast().Message();
                        ExceptionsManager.Clear();
                    }
                    if (ObjectValidator.IsString($error)) {
                        $error = new RegExp($error);
                    }
                    if (!ObjectValidator.IsSet($error) || ObjectValidator.IsSet($error) && $error.exec(ex.message) !== null) {
                        if (!ObjectValidator.IsEmptyOrNull($message)) {
                            $message += ": ";
                        } else {
                            $message = "";
                        }
                        $message = "AssertionError: Assertion expected not throwing an error. " + $message + ex.message;
                        throw Error($message);
                    }
                }
            });
        };

        __unitTestRunner.assert.patternEqual = ($actual : any, $expected : any, $message? : string) : void => {
            if (!ObjectValidator.IsEmptyOrNull($message)) {
                $message += ": ";
            } else {
                $message = "";
            }
            try {
                __unitTestRunner.assert.ok(StringUtils.PatternMatched($expected, $actual));
            } catch (ex) {
                throw new Error(
                    "AssertionError: " + $message + "\n" + JSON.stringify($actual, null, 2) + "\nexpected to be\n" +
                    JSON.stringify($expected, null, 2) + "\n"
                );
            }
        };

        assert.patternEqual = ($actual : any, $expected : any, $message? : string) : void => {
            resolveAssert(() : void => {
                __unitTestRunner.assert.patternEqual($actual, $expected, $message);
            });
        };

        assert.resolveEqual = ($className : any, $expected : string, $eventArgs? : HttpRequestEventArgs | AsyncRequestEventArgs,
                               $message? : string) : BaseHttpResolver => {
            if (ObjectValidator.IsEmptyOrNull($eventArgs)) {
                $eventArgs = new HttpRequestEventArgs(this.getRequest().getScriptPath());
            }
            const resolver : BaseHttpResolver = new $className();
            resolver.RequestArgs($eventArgs);
            resolveAssert(() : void => {
                this.setUserAgent(BrowserType.FIREFOX);
                Echo.ClearAll();
                resolver.Process();
                let actual : string = document.documentElement.innerHTML;
                if (ObjectValidator.IsEmptyOrNull(actual) ||
                    actual === "<head></head><body></body>" ||
                    actual === "<head></head><body><div id=\"Content\"></div></body>") {
                    actual = Echo.getStream();
                }

                this.initSendBox();
                if (StringUtils.Contains($expected, "*")) {
                    __unitTestRunner.assert.patternEqual(actual, $expected, $message);
                } else {
                    LogIt.Debug(actual);
                    __unitTestRunner.assert.equal(actual, $expected, $message);
                }
            });
            return resolver;
        };

        assert.onGuiComplete = ($instance : any, $resolve : () => void, $done : () => void, $owner? : any) : void => {
            resolveAssert(() : void => {
                if (ObjectValidator.IsSet($owner)) {
                    $instance.InstanceOwner($owner);
                } else {
                    $instance.InstanceOwner({});
                }
                if (globalThis.Io.Oidis.Gui) {
                    globalThis.Io.Oidis.Gui.Utils.StaticPageContentManager.BodyAppend($instance.Draw());
                    globalThis.Io.Oidis.Gui.Utils.StaticPageContentManager.Draw();
                }

                $instance.getEvents().setOnComplete(() : void => {
                    $resolve();
                    this.initSendBox();
                    $done();
                });
                $instance.Visible(true);
            });
        };

        let validatorName : string;
        for (validatorName in __unitTestRunner.assert) {
            if (__unitTestRunner.assert.hasOwnProperty(validatorName) && !assert.hasOwnProperty(validatorName)) {
                assert[validatorName] = (...$args : any[]) : void => {
                    resolveAssert(() : void => {
                        __unitTestRunner.assert.apply(this, $args);
                    });
                };
            }
        }
    }
}

export type IUnitTestRunnerPromise = ($done : () => void) => void;

export abstract class IEntityContext {
    public name : string;
    public started : Date;
    public integrityTest : string;
    /**
     * isRunning means that test method process is running and has no connection to pass/fail classification
     */
    public isRunning : boolean;
    public assertErrors : Error[];
}

export abstract class ITestContext extends IEntityContext {
    public timeout : number;
    public timeoutLimit : number;
    public timeoutExpired : boolean;
    public isFailing : boolean;
    public isDisposed : boolean;
}

export interface ITestMethod {
    name : string;
    testName : string;
    ignored : boolean;
}

export abstract class ISuiteContext extends IEntityContext {
    public tests : ITestMethod[];
}

if (ObjectValidator.IsEmptyOrNull(__unitTestRunner.process)) {
    __unitTestRunner.process = async () : Promise<void> => {
        Reflection.setInstanceNamespaces.apply(Reflection, __unitTestRunner.namespaces); // eslint-disable-line
        const reflection : Reflection = Reflection.getInstance();
        const instances : any[] = [];
        reflection.getAllClasses().forEach(($class : string) : void => {
            if (StringUtils.EndsWith($class, "Test")) {
                const classObject : any = reflection.getClass($class);
                if (classObject.prototype.prepareAssert !== undefined) {
                    const instance : any = new classObject();
                    if (reflection.IsMemberOf(instance, BaseUnitTestRunner)) {
                        instances.push(instance);
                    }
                }
            }
        });
        for await (const instance of instances) {
            await instance.Process();
        }
    };
    global.console = __unitTestRunner.console;
    global.setTimeout = __unitTestRunner.setTimeout;
    globalThis.bootstrap = {Tooltip: BaseObject};
}

// generated-code-start
export const ITestMethod = globalThis.RegisterInterface(["name", "testName", "ignored"]);
// generated-code-end
