/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
// import IMockTestInterface = Io.Oidis.Commons.Interfaces.Test.IMockTestInterface;
// import IMockTestInterface2 = Io.Oidis.Commons.Interfaces.Test.IMockTestInterface2;
import { SyntaxConstants } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/SyntaxConstants.js";
import { IBaseObject } from "../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { Interface } from "../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/Interface.js";
import { ObjectValidator } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Reflection.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

// namespace Io.Oidis.Commons.Interfaces.Test {
//     "use strict";
//     export let IMockTestInterface : Interface =
//         function () : Interface {
//             return Interface.getInstance([
//                 "OnTest",
//                 "OnClose"
//             ]);
//         }();
// }
//
// namespace Io.Oidis.Commons.Interfaces.Test {
//     "use strict";
//     export let IMockTestInterface2 : Interface =
//         function () : Interface {
//             return Interface.getInstance([
//                 "OnTest",
//                 "Add",
//                 "GetAll",
//                 "Contains"
//             ], IMockTestInterface);
//         }();
// }

export class InterfaceTest extends UnitTestRunner {

    @Test()
    public getInstance() : void {
        // if (!ObjectValidator.IsSet((<any>Reflection).singleton)) {
        //     assert.equal(IMockTestInterface.ClassName(), ".");
        //     assert.equal(IMockTestInterface.NamespaceName(), "");
        //     assert.equal(IMockTestInterface.ClassNameWithoutNamespace(), "");
        //     assert.equal(IMockTestInterface.ClassNameWithoutNamespace(), "");
        // }
        //
        // delete (<any>Reflection).singleton;
        // Reflection.getInstance();
        //
        // assert.equal(IMockTestInterface.ClassName(), "Io.Oidis.Commons.Interfaces.Test.IMockTestInterface");
        // assert.equal(IMockTestInterface.NamespaceName(), "Io.Oidis.Commons.Interfaces.Test");
        // assert.equal(IMockTestInterface.ClassNameWithoutNamespace(), "IMockTestInterface");
        //
        // const output : string[] = [];
        // let parameterIndex : number = 0;
        // let parameter : any;
        // for (parameter in IMockTestInterface2) {
        //     if (typeof IMockTestInterface2[parameter] !== SyntaxConstants.FUNCTION) {
        //         output[parameterIndex++] = parameter;
        //     }
        // }
        //
        // assert.deepEqual(output, [
        //     "classNamespace",
        //     "className",
        //     "OnTest",
        //     "OnClose",
        //     "Add",
        //     "GetAll",
        //     "Contains"
        // ]);
    }

    @Test()
    public ClassName() : void {
        assert.deepEqual(Interface.getInstance([], IBaseObject).ClassName(), "Io.Oidis.Commons.Interfaces.IBaseObject");

        const inter : Interface = new Interface();
        assert.equal(inter.ClassName(), null);
    }

    @Test()
    public NamespaceName() : void {
        assert.deepEqual(Interface.getInstance([], IBaseObject).NamespaceName(), "Io.Oidis.Commons.Interfaces");

        const inter : Interface = new Interface();
        assert.equal(inter.NamespaceName(), null);
    }

    @Test()
    public ClassNameWithoutNamespace() : void {
        assert.deepEqual(Interface.getInstance([], IBaseObject).ClassNameWithoutNamespace(), "IBaseObject");

        const inter : Interface = new Interface();
        assert.equal(inter.ClassNameWithoutNamespace(), null);
    }

    @Test()
    public ToString1() : void {
        const inter : Interface = new Interface();
        assert.equal(inter.ToString(), null);
    }

    @Test()
    public toString1() : void {
        const inter : Interface = new Interface();
        assert.equal(inter.toString(), null);
    }
}
