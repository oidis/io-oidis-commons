/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

import { ExceptionCode } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/ExceptionCode.js";
import { HttpRequestConstants } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/HttpRequestConstants.js";
import { ExceptionErrorPage } from "../../../../../../../source/typescript/Io/Oidis/Commons/ErrorPages/ExceptionErrorPage.js";
import { AsyncRequestEventArgs } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { Exception } from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/Type/Exception.js";
import { ArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Test } from "../BaseUnitTestRunner.js";

class MockExceptionCorrupted extends Exception {
    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        throw Error("test MockExceptionCorrupted");
    }
}

class MockException extends ExceptionErrorPage {
    protected getPageBody() : any {
        throw new Error("test body exception");
    }
}

export class ExceptionErrorPageTest extends UnitTestRunner {

    @Test()
    public getPageBody() : void {
        assert.resolveEqual(ExceptionErrorPage, "" +
            "<head></head>" +
            "<body><div id=\"Content\"><span guitype=\"HtmlAppender\">" +
            "<br>" +
            "<h1>Oops, something went wrong...</h1>" +
            "<span onclick=\"" +
            "document.getElementById('exceptionEcho').style.display=" +
            "document.getElementById('exceptionEcho').style.display==='block'?'none':'block';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
            "Echo output before exception</span>\n" +
            "<div id=\"exceptionEcho\" style=\"border: 0 solid black; display: none;\">\n" +
            "Nothing has been printed by Echo yet.\n" +
            "</div>" +
            "<br>" +
            "<a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; text-decoration: none;\"" +
            " href=\"#/io-oidis-builder/about/Cache\">Cache info</a></span></div>" +
            "</body>");
    }

    @Test()
    public getExceptionsList() : void {
        const data : ArrayList<any> = new ArrayList<any>();
        const exceptionList : ArrayList<Exception> = new ArrayList<Exception>();
        exceptionList.Add(new Exception());
        exceptionList.Add(new MockExceptionCorrupted());
        data.Add(exceptionList, HttpRequestConstants.EXCEPTIONS_LIST);
        const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(
            this.getRequest().getScriptPath(), data);

        assert.equal(args.POST().getItem(HttpRequestConstants.EXCEPTIONS_LIST), exceptionList);

        const goldenData : string =
            "<head></head>" +
            "<body>" +
            "<div id=\"Content\"><span guitype=\"HtmlAppender\">" +
            "<br>" +
            "<h1>Oops, something went wrong...</h1>" +
            "thrown by: <b></b>:<br>" +
            "<br>" +
            "<br>" +
            "thrown by: <b></b>:<br>" +
            "test MockExceptionCorrupted<br>" +
            "<span onclick=\"" +
            "document.getElementById(\'exceptionEcho\').style.display=" +
            "document.getElementById(\'exceptionEcho\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
            "Echo output before exception</span>\n" +
            "<div id=\"exceptionEcho\" style=\"border: 0 solid black; display: none;\">\n" +
            "Nothing has been printed by Echo yet.\n" +
            "</div><br>" +
            "<a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; text-decoration: none;\" " +
            "href=\"#/io-oidis-builder/about/Cache\">Cache info</a></span></div>" +
            "</body>";

        assert.resolveEqual(ExceptionErrorPage, goldenData, args).Process();
        assert.equal(document.documentElement.innerHTML, goldenData);
        this.initSendBox();
    }

    @Test()
    public isFatalError() : void {
        const data : ArrayList<any> = new ArrayList<any>();
        data.Add(ExceptionCode.FATAL_ERROR, HttpRequestConstants.EXCEPTION_TYPE);

        const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(
            this.getRequest().getScriptPath(), data);
        assert.equal(args.GET(data).getItem(HttpRequestConstants.EXCEPTION_TYPE), ExceptionCode.FATAL_ERROR);

        const goldenData : string =
            "<head></head>" +
            "<body><div id=\"Content\"><span guitype=\"HtmlAppender\">" +
            "<br><h1>FATAL Error!</h1>" +
            "<span onclick=\"" +
            "document.getElementById(\'exceptionEcho\').style.display=" +
            "document.getElementById(\'exceptionEcho\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
            "Echo output before exception</span>\n" +
            "<div id=\"exceptionEcho\" style=\"border: 0 solid black; display: none;\">\n" +
            "Nothing has been printed by Echo yet.\n" +
            "</div><br>" +
            "<a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; text-decoration: none;\" " +
            "href=\"#/io-oidis-builder/about/Cache\">Cache info</a></span>" +
            "</div>" +
            "</body>";

        assert.resolveEqual(ExceptionErrorPage, goldenData, args).Process();
        assert.equal(document.documentElement.innerHTML, goldenData);
        this.initSendBox();
    }

    @Test(true)
    public SelfException() : void {
        assert.doesHandleException(() : void => {
            assert.resolveEqual(MockException, "");
        }, "Io.Oidis.Commons.ErrorPages.ExceptionErrorPage self error.");
    }
}
