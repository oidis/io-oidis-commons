/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

import { Http403ForbiddenPage } from "../../../../../../../source/typescript/Io/Oidis/Commons/ErrorPages/Http403ForbiddenPage.js";
import { Test } from "../BaseUnitTestRunner.js";

export class Http403ForbiddenPageTest extends UnitTestRunner {

    @Test()
    public getPageBody() : void {
        assert.resolveEqual(Http403ForbiddenPage, "" +
            "<head></head>" +
            "<body>" +
            "<div id=\"Content\"><span guitype=\"HtmlAppender\"><br>" +
            "<h1>HTTP status 403:</h1>\n" +
            "<h2>Access denied</h2>" +
            "</span></div>" +
            "</body>");
    }
}
