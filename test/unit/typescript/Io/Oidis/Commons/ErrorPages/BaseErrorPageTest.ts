/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

import { BaseErrorPage } from "../../../../../../../source/typescript/Io/Oidis/Commons/ErrorPages/BaseErrorPage.js";
import { AsyncRequestEventArgs } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { Echo } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Echo.js";
import { Test } from "../BaseUnitTestRunner.js";

class MockBaseErrorPage extends BaseErrorPage {
    public testEchoOutput() : string {
        return this.getEchoOutput();
    }
}

export class BaseErrorPageTest extends UnitTestRunner {

    @Test()
    public getEchoOutput() : void {
        const baseerror : MockBaseErrorPage = new MockBaseErrorPage();
        assert.equal(baseerror.testEchoOutput(), "Nothing has been printed by Echo yet.");
        Echo.Printf("test print");
        assert.equal(baseerror.testEchoOutput(), "<br/>test print");
        this.initSendBox();
    }

    @Test()
    public getPageBody() : void {
        assert.resolveEqual(BaseErrorPage, "" +
            "<head></head>" +
            "<body><div id=\"Content\"><span guitype=\"HtmlAppender\"><br>" +
            "<h1>Something has went wrong</h1></span>" +
            "</div>" +
            "</body>", new AsyncRequestEventArgs(""));
    }
}
