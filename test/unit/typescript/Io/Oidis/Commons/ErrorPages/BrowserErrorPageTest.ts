/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

import { BrowserErrorPage } from "../../../../../../../source/typescript/Io/Oidis/Commons/ErrorPages/BrowserErrorPage.js";
import { Test } from "../BaseUnitTestRunner.js";

export class BrowserErrorPageTest extends UnitTestRunner {

    @Test()
    public getPageBody() : void {
        assert.resolveEqual(BrowserErrorPage, "" +
            "<head></head>" +
            "<body><div id=\"Content\"><span guitype=\"HtmlAppender\">" +
            "<br>" +
            "You are using unsupported type or version of the browser. Please, choose one of the supported browser " +
            "from the list below:<br>" +
            "<a href=\"https://windows.microsoft.com/en-us/internet-explorer/download-ie\" target=\"_blank\">Internet Explorer 5+</a>" +
            "<br>" +
            "<a href=\"https://www.mozilla.org/en-US/firefox/new/\" target=\"_blank\">Firefox</a><br>" +
            "<a href=\"https://www.google.com/chrome/\" target=\"_blank\">Google Chrome</a><br>" +
            "<a href=\"https://www.opera.com/\" target=\"_blank\">Opera</a><br>" +
            "<a href=\"https://www.apple.com/safari/\" target=\"_blank\">Safari</a></span>" +
            "</div>" +
            "</body>");
    }
}
