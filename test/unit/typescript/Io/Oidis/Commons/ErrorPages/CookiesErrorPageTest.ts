/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

import { CookiesErrorPage } from "../../../../../../../source/typescript/Io/Oidis/Commons/ErrorPages/CookiesErrorPage.js";
import { Test } from "../BaseUnitTestRunner.js";

export class CookiesErrorPageTest extends UnitTestRunner {

    @Test()
    public getPageBody() : void {
        assert.resolveEqual(CookiesErrorPage, "" +
            "<head></head>" +
            "<body><div id=\"Content\"><span guitype=\"HtmlAppender\">" +
            "<br>This library requires enabled Cookies in the browser for ability to store persistence. " +
            "See link below for more information:<br>" +
            "<a href=\"https://www.wikihow.com/Enable-Cookies-in-Your-Internet-Web-Browser\" target=\"_blank\">" +
            "How to enable Cookies?</a></span>" +
            "</div>" +
            "</body>");
    }
}
