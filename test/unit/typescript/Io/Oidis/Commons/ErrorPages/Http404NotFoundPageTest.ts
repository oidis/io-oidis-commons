/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

import { HttpRequestConstants } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/HttpRequestConstants.js";
import { Http404NotFoundPage } from "../../../../../../../source/typescript/Io/Oidis/Commons/ErrorPages/Http404NotFoundPage.js";
import { AsyncRequestEventArgs } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { ArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Test } from "../BaseUnitTestRunner.js";

export class Http404NotFoundPageTest extends UnitTestRunner {

    @Test()
    public getPageBody() : void {
        assert.resolveEqual(Http404NotFoundPage, "" +
            "<head></head>" +
            "<body>" +
            "<div id=\"Content\"><span guitype=\"HtmlAppender\"><br>" +
            "<h1>HTTP status 404:</h1>\n" +
            "<h2>File has not been found</h2>" +
            "</span></div>" +
            "</body>");
    }

    @Test()
    public getPageBody2() : void {
        const data : ArrayList<any> = new ArrayList<any>();
        data.Add("newFilePath/404_Redirect", HttpRequestConstants.HTTP404_FILE_PATH);
        const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(
            this.getRequest().getScriptPath(), data);

        assert.equal(args.POST().getItem(HttpRequestConstants.HTTP404_FILE_PATH), "newFilePath/404_Redirect");
        assert.resolveEqual(Http404NotFoundPage, "" +
            "<head></head>" +
            "<body><div id=\"Content\"><span guitype=\"HtmlAppender\"><br>" +
            "<h1>HTTP status 404:</h1>\n" +
            "<h2>File has not been found. Required file path is:</h2>\n" +
            "<a href=\"newFilePath/404_Redirect\">newFilePath/404_Redirect</a>" +
            "</span></div>" +
            "</body>", args);
    }
}
