/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

import { HttpRequestConstants } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/HttpRequestConstants.js";
import { Http301MovedPage } from "../../../../../../../source/typescript/Io/Oidis/Commons/ErrorPages/Http301MovedPage.js";
import { AsyncRequestEventArgs } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { ArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Test } from "../BaseUnitTestRunner.js";

export class Http301MovedPageTest extends UnitTestRunner {

    @Test()
    public getPageBody() : void {
        assert.resolveEqual(Http301MovedPage, "" +
            "<head></head>" +
            "<body>" +
            "<div id=\"Content\"><span guitype=\"HtmlAppender\">" +
            "<br><h1>HTTP status 301:</h1>\n" +
            "<h2>File has been moved. New address is:</h2>\n" +
            "<a href=\"#\">http://localhost:8888/UnitTestEnvironment.js#</a>" +
            "</span></div>" +
            "</body>");
        this.initSendBox();
    }

    @Test()
    public getPageBody2() : void {
        const data : ArrayList<any> = new ArrayList<any>();
        data.Add("fileMoved/301_newLink", HttpRequestConstants.HTTP301_LINK);
        const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(
            this.getRequest().getScriptPath(), data);

        assert.equal(args.POST().getItem(HttpRequestConstants.HTTP301_LINK), "fileMoved/301_newLink");
        assert.resolveEqual(Http301MovedPage, "" +
            "<head></head>" +
            "<body>" +
            "<div id=\"Content\"><span guitype=\"HtmlAppender\">" +
            "<br><h1>HTTP status 301:</h1>\n" +
            "<h2>File has been moved. New address is:</h2>\n" +
            "<a href=\"#fileMoved/301_newLink\">http://localhost:8888/UnitTestEnvironment.js#fileMoved/301_newLink</a>" +
            "</span></div>" +
            "</body>", args);
        this.initSendBox();
    }

    protected setUp() : void {
        this.setUrl("http://localhost:8888/UnitTestEnvironment.js");
    }
}
