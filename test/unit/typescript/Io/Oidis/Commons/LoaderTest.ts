/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestLoader, UnitTestRunner } from "./UnitTestRunner.js";

import { LogLevel } from "../../../../../../source/typescript/Io/Oidis/Commons/Enums/LogLevel.js";
import { HttpResolver } from "../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/HttpResolver.js";
import { LogIt } from "../../../../../../source/typescript/Io/Oidis/Commons/Utils/LogIt.js";
import { Test } from "./BaseUnitTestRunner.js";
import { assert } from "./UnitTestEnvironment.js";

class MockErrorLoader extends UnitTestLoader {
    protected initResolver() : HttpResolver {
        throw new Error("test");
    }
}

export class LoaderTest extends UnitTestRunner {

    @Test()
    public InfoLevelInProduction() : void {
        LogIt.setLevel(LogLevel.INFO);
        UnitTestLoader.Load(<any>{
            build: {time: new Date().toTimeString(), type: "prod"}, name: "com-wui-framework-commons", version: "1.0.0"
        });
        assert.equal((<any>LogIt.getLogger()).level, LogLevel.INFO);
    }

    @Test()
    public DebugLevel() : void {
        LogIt.setLevel(LogLevel.DEBUG);
        UnitTestLoader.Load(<any>{
            build: {time: new Date().toTimeString(), type: "dev"}, name: "com-wui-framework-commons", version: "1.0.0"
        });
        assert.equal((<any>LogIt.getLogger()).level, LogLevel.DEBUG);
    }

    @Test()
    public ErrorLevelForProd() : void {
        LogIt.setLevel(LogLevel.DEBUG);
        UnitTestLoader.Load(<any>{
            build: {time: new Date().toTimeString(), type: "prod"}, name: "com-wui-framework-commons", version: "1.0.0"
        });
        assert.equal((<any>LogIt.getLogger()).level, LogLevel.DEBUG);
    }

    @Test()
    public LoadWithWarningLevel() : void {
        LogIt.setLevel(LogLevel.WARNING);
        UnitTestLoader.Load(<any>{
            build: {time: new Date().toTimeString(), type: "dev"}, name: "com-wui-framework-commons", version: "1.0.0"
        });
        assert.equal((<any>LogIt.getLogger()).level, LogLevel.WARNING);
    }

    @Test(true)
    public LoadException() : void {
        /// TODO: promise handler catch error before assertion, so even that test is passing it is reported as failing
        assert.doesNotThrow(() : void => {
            MockErrorLoader.Load(<any>{
                build: {time: new Date().toTimeString(), type: "dev"}, name: "com-wui-framework-commons", version: "1.0.0"
            });
        });
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
