/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { EventType } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/Events/EventType.js";
import { GeneralEventOwner } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/Events/GeneralEventOwner.js";
import { HttpRequestConstants } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/HttpRequestConstants.js";
import { AsyncRequestEventArgs } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { HttpRequestEventArgs } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/HttpRequestEventArgs.js";
import { EventsManager } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/EventsManager.js";
import { ExceptionsManager } from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { HttpManager } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/HttpManager.js";
import { HttpRequestParser } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/HttpRequestParser.js";
import { BaseHttpResolver } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { ArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { IUnitTestRunnerPromise, Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";

export class HttpManagerTest extends UnitTestRunner {

    constructor() {
        super();

        // this.setMethodFilter("testGetResolverClassName");
    }

    @Test()
    public getProcessTime() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            setTimeout(() : void => {
                assert.ok(this.getHttpManager().getProcessTime() > 0);
                $done();
            }, 100);
        };
    }

    @Test()
    public CreateLink() : void {
        this.setUrl("http://localhost.wuiframework.com/index.html#/project-name/unit/HttpManagerTest");
        const manager : HttpManager = new HttpManager(new HttpRequestParser("project-name"));
        assert.equal(manager.CreateLink("/test"), "/project-name/test", "absolute link '/test'");
        assert.equal(manager.CreateLink("/project-name/test"), "/project-name/test", "absolute link '/project-name/test'");
        assert.equal(manager.CreateLink("project-name/test"), "/project-name/test", "relative link 'project-name/test'");
        assert.equal(manager.CreateLink("test"), "/project-name/unit/test", "relative link 'test'");
        assert.equal(manager.CreateLink(""), manager.getRequest().getUrl(), "current link by empty");
        assert.equal(manager.CreateLink(null), manager.getRequest().getUrl(), "current link by null");
        assert.equal(manager.CreateLink("http://www.wuiframework.com"), "http://www.wuiframework.com", "foreign link");
        assert.equal(manager.CreateLink("https://www.wuiframework.com"), "https://www.wuiframework.com", "foreign https link");
        assert.equal(manager.CreateLink("www.wuiframework.com"), "http://www.wuiframework.com", "foreign www link");
        assert.equal(manager.CreateLink(manager.CreateLink("/test")), "/project-name/test", "recursive call test");
        assert.equal(manager.CreateLink("//test"), "/project-name/test", "double slash '//test'");
        assert.equal(manager.CreateLink("mailto:wui@nxp.com"), "mailto:wui@nxp.com", "mailto protocol");
        this.setUrl("http://localhost.wuiframework.com/index.html");
        const manager2 : HttpManager = new HttpManager(new HttpRequestParser());
        assert.equal(manager2.CreateLink("http://localhost.wuiframework.com/index.html/unit/test"), "/unit/test",
            "current http link with '/test'");
        assert.equal(manager2.CreateLink(""), "", "");
    }

    @Test()
    public Reload() : void {
        window.location.reload = (forcedReload? : boolean) : void => {
            // mock implementation for missing native API
        };
        assert.doesNotThrow(() : void => {
            this.getHttpManager().Reload();
        });
        window.location.reload = null;
    }

    @Test()
    public ReloadTo() : void {
        assert.doesNotThrow(() : void => {
            assert.equal(window.location.href, "file:///" + this.getAbsoluteRoot() + "/index.html#UnitTestLoader");
            this.getHttpManager().ReloadTo();
            assert.equal(window.location.href, "UnitTestLoader");
        });
    }

    @Test()
    public ReloadTo2() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const list : ArrayList<any> = new ArrayList<any>();
            assert.onRedirect(
                () : void => {
                    this.getHttpManager().ReloadTo("#/test", list, true);
                },
                ($eventArgs : AsyncRequestEventArgs) : void => {
                    assert.equal($eventArgs.Url(), "/test");
                },
                () : void => {
                    this.initSendBox();
                    $done();
                });
        };
    }

    @Test()
    public ReloadToWithData() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("test", "key");
            assert.onRedirect(
                () : void => {
                    this.getHttpManager().ReloadTo("/some/other/link", data, true);
                },
                ($eventArgs : AsyncRequestEventArgs) : void => {
                    assert.equal($eventArgs.POST().Length(), 1);
                },
                () : void => {
                    this.initSendBox();
                    $done();
                });
        };
    }

    @Test(true)
    public ReloadToWithoutArguments() : IUnitTestRunnerPromise {
        EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST);
        this.setUrl("http://localhost.wuiframework.com/index.html#/project-name/unit/HttpManagerTest");
        const manager : HttpManager = new HttpManager(new HttpRequestParser("project-name"));
        return ($done : () => void) : void => {
            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST,
                ($eventArgs : HttpRequestEventArgs) : void => {
                    assert.equal($eventArgs.Url(), "/project-name/unit/HttpManagerTest");
                    assert.equal($eventArgs.POST().Length(), 0);
                    this.initSendBox();
                    $done();
                });
            manager.ReloadTo();
        };
    }

    @Test()
    public ReloadToAsyncWithoutData() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            assert.onRedirect(
                () : void => {
                    this.getHttpManager().ReloadTo("/some/other/link2", null, true);
                },
                ($eventArgs : AsyncRequestEventArgs) : void => {
                    assert.equal($eventArgs.Url(), "/io-oidis-builder/some/other/link2");
                    assert.equal($eventArgs.POST().Length(), 0);
                },
                () : void => {
                    this.initSendBox();
                    $done();
                });
        };
    }

    @Test()
    public ReloadToNewWindow() : void {
        window.open = (url? : string, target? : string, features? : string, replace? : boolean) : Window => {
            assert.equal(url, "/io-oidis-builder/some/other/link3");
            assert.equal(target, "_blank");
            window.open = null;
            return;
        };
        this.getHttpManager().ReloadTo("/some/other/link3", true);
    }

    @Test()
    public Return301Moved() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            assert.onRedirect(
                () : void => {
                    assert.doesNotThrow(() : void => {
                        this.getHttpManager().Return301Moved("http://localhost.wuiframework.com/newLocation");
                    });
                },
                ($eventArgs : AsyncRequestEventArgs) : void => {
                    assert.equal($eventArgs.Url(), "/unit/ServerError/Http/Moved");
                    assert.ok($eventArgs.POST().KeyExists(HttpRequestConstants.HTTP301_LINK));
                    assert.equal($eventArgs.POST().getItem(HttpRequestConstants.HTTP301_LINK),
                        "http://localhost.wuiframework.com/newLocation");
                },
                () : void => {
                    this.initSendBox();
                    $done();
                },
                "unit");
        };
    }

    @Test()
    public Return403Forbidden() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            assert.onRedirect(
                () : void => {
                    assert.doesNotThrow(() : void => {
                        this.getHttpManager().Return403Forbidden();
                    });
                },
                ($eventArgs : AsyncRequestEventArgs) : void => {
                    assert.equal($eventArgs.Url(), "/unit/ServerError/Http/Forbidden");
                },
                () : void => {
                    this.initSendBox();
                    $done();
                },
                "unit");
        };
    }

    @Test()
    public Return404NotFound() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            assert.onRedirect(
                () : void => {
                    assert.doesNotThrow(() : void => {
                        this.getHttpManager().Return404NotFound("http://localhost:8888/required/path/location");
                    });
                },
                ($eventArgs : AsyncRequestEventArgs) : void => {
                    assert.equal($eventArgs.Url(), "/unit/ServerError/Http/NotFound");
                    assert.ok($eventArgs.POST().KeyExists(HttpRequestConstants.HTTP404_FILE_PATH));
                    assert.equal($eventArgs.POST().getItem(HttpRequestConstants.HTTP404_FILE_PATH),
                        "http://localhost:8888/required/path/location");
                },
                () : void => {
                    this.initSendBox();
                    $done();
                },
                "unit");
        };
    }

    @Test()
    public Return404NotFoundWithoutRequiredLink() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            assert.onRedirect(
                () : void => {
                    assert.doesNotThrow(() : void => {
                        this.getHttpManager().Return404NotFound();
                    });
                },
                ($eventArgs : AsyncRequestEventArgs) : void => {
                    assert.equal($eventArgs.Url(), "/unit/ServerError/Http/NotFound");
                    assert.equal($eventArgs.POST().KeyExists(HttpRequestConstants.HTTP404_FILE_PATH), false);
                },
                () : void => {
                    this.initSendBox();
                    $done();
                },
                "unit");
        };
    }

    @Test()
    public getRequest1() : void {
        const parser : HttpRequestParser = new HttpRequestParser("test");
        const manager : HttpManager = new HttpManager(parser);
        assert.deepEqual(manager.getRequest(), parser);
        assert.notDeepEqual(manager.getRequest(), this.getHttpManager().getRequest());
    }

    @Test()
    public Refresh() : void {
        assert.throws(() : void => {
            this.getHttpManager().Refresh();
        }, /window.location.reload is not a function/);

        window.location.reload = (forcedReload? : boolean) : void => {
            // mock implementation for missing native API
        };
        assert.doesNotThrow(() : void => {
            this.getHttpManager().Reload();
        });
        window.location.reload = null;
    }

    @Test()
    public ReloadBack() : void {
        assert.doesNotThrow(() : void => {
            this.getHttpManager().ReloadBack();
        });
        ExceptionsManager.Clear();
    }

    @Test()
    public ReloadBackException() : void {
        window.history.back = () : void => {
            throw new Error("Test Back exception");
        };
        assert.throws(() : void => {
            this.getHttpManager().ReloadBack();
        }, "Test Back exception");

        window.history.back = () : void => {
            // empty function
        };
        assert.doesNotThrow(() : void => {
            this.getHttpManager().ReloadBack();
        });
        ExceptionsManager.Clear();
    }

    @Test(true)
    public IsOnline() : IUnitTestRunnerPromise {
        (<any>window.navigator).__defineGetter__("onLine", () : boolean => {
            return true;
        });
        this.initSendBox();
        return ($done : () => void) : void => {
            this.getHttpManager().IsOnline(($status : boolean) : void => {
                assert.equal($status, true);
                this.initSendBox();
                $done();
            });
        };
    }

    @Test(true)
    public IsOnline2() : IUnitTestRunnerPromise {
        (<any>window.navigator).__defineGetter__("onLine", () : boolean => {
            return false;
        });
        this.initSendBox();
        return ($done : () => void) : void => {
            this.getHttpManager().IsOnline(($status : boolean) : void => {
                assert.equal($status, false);
                (<any>window.navigator).__defineGetter__("onLine", () : boolean => {
                    return true;
                });
                this.initSendBox();
                $done();
            });
        };
    }

    @Test()
    public RefreshWithoutReload() : void {
        this.getHttpManager().RefreshWithoutReload();
        assert.ok(this.getHttpManager().getProcessTime() < 5);
        assert.deepEqual(this.getHttpManager().getRequest(), this.getHttpResolver().CreateRequest());
    }

    @Test()
    public RegisterResolverEmpty() : void {
        const manager : HttpManager = new HttpManager(new HttpRequestParser());
        manager.RegisterResolver("/empty/test", "");
        assert.equal(manager.HttpPatternExists("/empty/test"), false);
    }

    @Test()
    public RegisterResolverIgnoreCase() : void {
        const manager : HttpManager = new HttpManager(new HttpRequestParser());
        manager.RegisterResolver("/test", "resolver", false);
        assert.equal(manager.getResolversCollection().getChild("test").IgnoreCase(), false);
    }

    @Test()
    public RegisterResolverCanNotBeOverridden() : void {
        const manager : HttpManager = new HttpManager(new HttpRequestParser());
        manager.RegisterResolver("/test2", "resolver");
        assert.equal(manager.getResolversCollection().getChild("test2").ResolverClassName(), "resolver");
        manager.RegisterResolver("/test2", "resolver2");
        assert.equal(manager.getResolversCollection().getChild("test2").ResolverClassName(), "resolver");
    }

    @Test()
    public RegisterResolverFlexibleLocation() : void {
        const manager : HttpManager = new HttpManager(new HttpRequestParser());
        manager.RegisterResolver("/**/test3", "dynamicResolver");
        assert.equal(manager.getResolversCollection().getChild("**").getChild("test3").ResolverClassName(), "dynamicResolver");

        manager.RegisterResolver("/**/test3/**/test4", "dynamicResolver2");
        assert.equal(manager.getResolversCollection().getChild("**").getChild("test3")
            .getChild("**").getChild("test4").ResolverClassName(), "dynamicResolver2");
    }

    @Test()
    public RegisterResolverMultiplyFlexibleLocation() : void {
        const manager : HttpManager = new HttpManager(new HttpRequestParser());
        manager.RegisterResolver("/**/**/test4/*/te**st*", "dynamicResolver3");
        assert.equal(manager.getResolversCollection().getChild("**").getChild("test4").getChild("*")
            .getChild("te*st*").ResolverClassName(), "dynamicResolver3");
    }

    @Test()
    public OverrideResolverEmpty() : void {
        const manager : HttpManager = new HttpManager(new HttpRequestParser());
        manager.RegisterResolver("/test", "resolver");
        assert.equal(manager.getResolversCollection().getChild("test").ResolverClassName(), "resolver");
        manager.OverrideResolver("/test", "");
        assert.equal(manager.getResolversCollection().getChild("test").ResolverClassName(), "resolver");
    }

    @Test()
    public OverrideResolverIgnoreCase() : void {
        const manager : HttpManager = new HttpManager(new HttpRequestParser());
        manager.RegisterResolver("/test", "resolver");
        assert.equal(manager.getResolversCollection().getChild("test").ResolverClassName(), "resolver");
        assert.equal(manager.getResolversCollection().getChild("test").IgnoreCase(), true);
        manager.OverrideResolver("/test", "resolver2", false);
        assert.equal(manager.getResolversCollection().getChild("test").ResolverClassName(), "resolver2");
        assert.equal(manager.getResolversCollection().getChild("test").IgnoreCase(), false);
    }

    @Test()
    public OverrideResolverWhichDoesNotExist() : void {
        const manager : HttpManager = new HttpManager(new HttpRequestParser());
        manager.RegisterResolver("/test", "resolver");
        manager.OverrideResolver("/someOtherTest", "resolver");
        assert.equal(manager.getResolversCollection().getChild("test").ResolverClassName(), "resolver");
        assert.equal(manager.HttpPatternExists("someOtherTest"), false);
    }

    @Test()
    public HttpPatternExists() : void {
        const manager : HttpManager = new HttpManager(new HttpRequestParser());
        assert.equal(manager.HttpPatternExists("https://www.google.com/"), false);
        manager.RegisterResolver("test", "http://localhost.wuiframework.com");
        assert.equal(manager.HttpPatternExists("test"), true);
    }

    @Test()
    public GetResolverClassName() : void {
        this.setUrl("http://localhost.wuiframework.com/index.html?appName=test#///");
        const manager : HttpManager = new HttpManager(new HttpRequestParser("com-wui-framework-commons"));
        manager.RegisterResolver("/test", "result1");
        manager.RegisterResolver("/*", "result3");

        manager.RegisterResolver("/*t*e*st*", "result10");

        manager.RegisterResolver("/*tes*t*", "result12");
        manager.RegisterResolver("/*te*st*", "result4");
        manager.RegisterResolver("/*t*est*", "result11");

        manager.RegisterResolver("/*test*", "result5");

        manager.RegisterResolver("/test*", "result7");
        manager.RegisterResolver("/tes*t", "result9");
        manager.RegisterResolver("/te*st", "result8");
        manager.RegisterResolver("/*test", "result6");

        assert.equal(manager.getResolverClassName("/test"), "result1");
        assert.equal(manager.getResolverClassName("/web"), "result3");
        assert.equal(manager.getResolverClassName("/2test"), "result6");
        assert.equal(manager.getResolverClassName("/tes3t"), "result9");
        assert.equal(manager.getResolverClassName("/4test4"), "result5");
        assert.equal(manager.getResolverClassName("/te55st"), "result8");
        assert.equal(manager.getResolverClassName("/6te6st6"), "result4");
        assert.equal(manager.getResolverClassName("/7t77est7"), "result11");
    }

    @Test()
    public getResolverClassName2() : void {
        this.setUrl("http://localhost.wuiframework.com/index.html#/project-name/unit/HttpManagerTest");
        const manager : HttpManager = new HttpManager(new HttpRequestParser("web"));
        assert.deepEqual(manager.getResolverClassName("/ServerError/Http/DefaultPage"), BaseHttpResolver);
        assert.equal(manager.getResolverClassName("/some/other"), null);

        manager.RegisterResolver("/web/test/test/test", "result1", false);
        manager.RegisterResolver("/*/*/test", "result2");
        manager.RegisterResolver("/web/*/test", "result3");
        manager.RegisterResolver("/**/test", "result4");
        manager.RegisterResolver("/**/*/test", "result5");
        manager.RegisterResolver("/**/*/test/*", "result7");
        manager.RegisterResolver("/web/{param1}/test/{param2}", "result6");
        assert.equal(manager.getResolverClassName("/web/test/test"), "result4");
        assert.equal(manager.getResolverClassName("/testtest1/testtest2/testtest3/testtest4/test"), "result5");
        assert.equal(manager.getResolverClassName("/web/test/test/test"), "result1");
        assert.equal(manager.getResolverClassName("/web/testparam1/test/testparam2"), "result6");
    }

    @Test()
    public getResolverClassName3() : void {
        this.setUrl("http://localhost.wuiframework.com/index.html#/project-name/unit/HttpManagerTest");
        const manager : HttpManager = new HttpManager(new HttpRequestParser("/"));
        manager.RegisterResolver("/web/test/test/test", "result1", false);
        manager.RegisterResolver("/*/*/test", "result2");
        manager.RegisterResolver("/web/*/test", "result3");
        manager.RegisterResolver("/**/test", "result4");
        manager.RegisterResolver("/**/*/test", "result5");
        manager.RegisterResolver("/**/*/test/*", "result7");
        manager.RegisterResolver("/web/{param1}/test/{param2}", "result6");
        assert.equal(manager.getResolverClassName("/web/test/test"), "result3");
        assert.equal(manager.getResolverClassName("/testtest1/testtest2/testtest3/testtest4/test"), "result5");
        assert.equal(manager.getResolverClassName("/web/test/test/test"), "result1");
        assert.equal(manager.getResolverClassName("/web/testparam1/test/testparam2"), "result6");
    }

    @Test()
    public getResolverClassName4() : void {
        this.setUrl("http://localhost.wuiframework.com/index.html#/project-name/unit/HttpManagerTest");
        const manager : HttpManager = new HttpManager(new HttpRequestParser(""));
        manager.RegisterResolver("/web/test/test/test", "result1", false);
        manager.RegisterResolver("/*/*/test", "result2");
        manager.RegisterResolver("/web/*/test", "result3");
        manager.RegisterResolver("/**/test", "result4");
        manager.RegisterResolver("/**/*/test", "result5");
        manager.RegisterResolver("/**/*/test/*", "result7");
        manager.RegisterResolver("/web/{param1}/test/{param2}", "result6");
        manager.RegisterResolver("", "result8");
        assert.equal(manager.getResolverClassName("/web/test/test"), "result3");
        assert.equal(manager.getResolverClassName("/testtest1/testtest2/testtest3/testtest4/test"), "result5");
        assert.equal(manager.getResolverClassName("/web/test/test/test"), "result1");
        assert.equal(manager.getResolverClassName("/web/testparam1/test/testparam2"), "result6");
        assert.equal(manager.getResolverClassName(""), "result8");
    }

    @Test()
    public getResolverClassName5() : void {
        this.setUrl("file://localhost.wuiframework.com#/resource/javascript/loader.min.js?AppName=test&AppPid=1234#///");
        const manager : HttpManager = new HttpManager(new HttpRequestParser(""));
        manager.RegisterResolver("*.min.js", "result1");
        manager.RegisterResolver("*.js", "result1");
        assert.equal(manager.getResolverClassName("/resource/javascript/loader.min.js"), "result1");
    }

    @Test()
    public getResolverParameters() : void {
        const manager : HttpManager = new HttpManager(new HttpRequestParser("web"));
        manager.RegisterResolver("/test/with/{parameter}/at/path", "testClass");

        assert.ok(manager.getResolverParameters().IsEmpty());
        manager.getResolverClassName("/test/other/path");
        assert.ok(manager.getResolverParameters().IsEmpty());

        manager.getResolverClassName("/test/with/TestValue/at/path");
        assert.equal(manager.getResolverParameters().Length(), 1);
        assert.equal(manager.getResolverParameters().getItem("parameter"), "TestValue");

        manager.getResolverClassName("/test/other/path2");
        assert.ok(manager.getResolverParameters().IsEmpty());
    }

    @Test()
    public getResolversCollection() : void {
        const manager : HttpManager = new HttpManager(new HttpRequestParser("web"));
        assert.equal(manager.getResolversCollection().Contains("pattern"), false);
        const manager2 : HttpManager = new HttpManager(new HttpRequestParser("web"));
        manager2.RegisterResolver("web", "result1", true);
        assert.equal(manager2.getResolversCollection().getChild("web").ResolverClassName(), null);
    }

    @Test()
    public ToString1() : void {
        const request : HttpRequestParser = new HttpRequestParser();
        const manager : HttpManager = new HttpManager(request);
        this.resetCounters();
        const value : string = request.ToString();
        const resolver : string = manager.getResolversCollection().ToString();
        this.resetCounters();
        assert.equal(manager.ToString(), value + "<br/>" + resolver);
    }

    @Test()
    public toString1() : void {
        const manager : HttpManager = new HttpManager(new HttpRequestParser());
        this.resetCounters();
        const value : string = manager.ToString();
        this.resetCounters();
        assert.equal(manager.toString(), value);
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
