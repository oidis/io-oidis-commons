/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IUnitTestRunnerPromise, Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

import { EventType } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Enums/Events/EventType.js";
import { GeneralEventOwner } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Enums/Events/GeneralEventOwner.js";
import { AsyncRequestEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Events/EventsManager.js";
import { AsyncHttpResolver } from "../../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/Resolvers/AsyncHttpResolver.js";
import { ArrayList } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";

class MockAsyncHttpResolver extends AsyncHttpResolver {
}

export class AsyncHttpResolverTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const async : AsyncHttpResolver = new MockAsyncHttpResolver();
        assert.equal(async.getClassName(), "Io.Oidis.Commons.HttpProcessor.Resolvers.AsyncHttpResolver");
    }

    @Test(true)
    public Process1() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("value1", "key");
            data.Add("value2", "key2");
            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs("http://localhost:8888/UnitTestEnvironment.js/", data);
            EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.ASYNC_REQUEST);
            let started : boolean = false;
            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_START,
                ($eventArgs : AsyncRequestEventArgs) : void => {
                    if (!started) {
                        started = true;
                        EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.ASYNC_REQUEST);
                        assert.deepEqual($eventArgs.Owner(), $eventArgs);
                        assert.equal($eventArgs.Type(), EventType.ON_START);
                        $done();
                    }
                });
            assert.resolveEqual(AsyncHttpResolver, "", args);
        };
    }

    @Test(true)
    public Process2() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const data : ArrayList<string> = new ArrayList<string>();
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            data.Add("value4", "key4");

            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.ASYNC_REQUEST);
            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_SUCCESS,
                () : void => {
                    EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.ASYNC_REQUEST);
                    assert.ok(false, "Test should be failing!");
                    $done();
                });
            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_ERROR,
                ($eventArgs : AsyncRequestEventArgs) : void => {
                    EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.ASYNC_REQUEST);
                    assert.equal($eventArgs.Result(),
                        "<h1>Missing async call subscribe data for resolver " +
                        "\"Io.Oidis.Commons.HttpProcessor.Resolvers.AsyncHttpResolver\"!</h1>");
                    $done();
                });
            assert.resolveEqual(AsyncHttpResolver, "", args);
        };
    }
}
