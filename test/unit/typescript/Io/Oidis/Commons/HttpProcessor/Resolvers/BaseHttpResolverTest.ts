/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

import { BaseHttpResolver } from "../../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { Echo } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Echo.js";
import { Test } from "../../BaseUnitTestRunner.js";

class MockResolveExit extends BaseHttpResolver {
    protected resolver() : void {
        Echo.Print("MockResolveExit reachable");
        assert.throws(() : void => {
            this.exit();
            Echo.Print("MockResolveExit not reachable");
        }, /Io.Oidis.Commons.Exceptions.ExceptionsManager.Exit/);
    }
}

class MockRegisterResolver extends BaseHttpResolver {
    protected resolver() : void {
        Echo.Print("MockRegisterResolver test");
        this.registerResolver("test3", "dynamicResolver", false);
        assert.equal(this.getHttpManager().getResolversCollection().getChild("test3").ToString("", false),
            "\"test3\" => \"dynamicResolver\"   \r\n" +
            "        ignore case: false\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n");
    }
}

class MockOverrideResolver extends BaseHttpResolver {
    protected resolver() : void {
        Echo.Print("MockOverrideResolver test");
        this.registerResolver("test", "resolver");
        assert.patternEqual(this.getHttpManager().getResolversCollection().getChild("test").ToString("", false),
            "\"test\" => \"resolver\"   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n");

        this.overrideResolver("test", "resolver2");
        assert.patternEqual(this.getHttpManager().getResolversCollection().getChild("test").ToString("", false),
            "\"test\" => \"resolver2\"   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n");
    }
}

class MockBaseHttpResolverClass extends BaseHttpResolver {
}

export class BaseHttpResolverTest extends UnitTestRunner {

    @Test(true)
    public Constructor() : void {
        assert.resolveEqual(MockBaseHttpResolverClass, "" +
            "<head></head>" +
            "<body>" +
            "<div id=\"Content\"><span guitype=\"HtmlAppender\">" +
            "<br>This is abstract BaseHttpResolver, which has been executed at: UnitTestLoader" +
            "</span>" +
            "</div>" +
            "</body>");
        this.initSendBox();
    }

    @Test(true)
    public toString1() : void {
        const async : BaseHttpResolver = new MockBaseHttpResolverClass();
        assert.equal(async.toString(), "object type of \'Io.Oidis.Commons.HttpProcessor.Resolvers.BaseHttpResolver\'");
    }

    @Test(true)
    public exit() : void {
        assert.resolveEqual(MockResolveExit, "" +
            "<head></head><body><div id=\"Content\"><span guitype=\"HtmlAppender\">MockResolveExit reachable</span></div></body>");
    }

    @Test(true)
    public registerResolver() : void {
        assert.resolveEqual(MockRegisterResolver, "" +
            "<head></head><body><div id=\"Content\"><span guitype=\"HtmlAppender\">MockRegisterResolver test</span></div></body>");
    }

    @Test(true)
    public overrideResolver() : void {
        assert.resolveEqual(MockOverrideResolver, "" +
            "<head></head>" +
            "<body>" +
            "<div id=\"Content\">" +
            "<span guitype=\"HtmlAppender\">MockOverrideResolver test</span>" +
            "</div>" +
            "</body>");
    }
}
