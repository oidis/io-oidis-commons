/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

import { BuilderConnector } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Connectors/BuilderConnector.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Events/EventsManager.js";
import { ExceptionsManager } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import {
    IRuntimeTestPromise,
    RuntimeTestRunner
} from "../../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { IPersistenceHandler } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { PersistenceFactory } from "../../../../../../../../source/typescript/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { ArrayList } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Echo.js";
import { Reflection } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Reflection.js";
import { IUnitTestRunnerPromise, Test } from "../../BaseUnitTestRunner.js";

class MockRuntimeTestUpandDownResolver extends RuntimeTestRunner {

    @Test()
    public Init() : void {
        Echo.Printf("MockRuntimeTestUpandDownResolver initialization");
    }

    @Test()
    public Case02() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            $done();
        };
    }

    protected setUp() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            Echo.Printf("set up");
            $done();
        };
    }

    protected tearDown() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            $done();
        };
    }
}

class MockRuntimeTestRunnerPromiseResolve extends RuntimeTestRunner {

    @Test()
    public Init() : void {
        Echo.Printf("MockRuntimeTestRunnerPromiseResolve initialization");
    }

    protected before() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            Echo.Printf("before");
            $done();
        };
    }

    protected after() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            Echo.Printf("after");
            $done();
        };
    }
}

class MockRuntimeTestRunner extends RuntimeTestRunner {
}

class MockRuntimeTestRunnerResolve extends RuntimeTestRunner {

    @Test()
    public Data() : void {
        this.assertEquals("hello", "hello");
    }

    @Test(true)
    public TestData() : void {
        // mock implementation
    }

    protected setUp() : void {
        // mock implementation
    }

    protected tearDown() : void {
        // mock implementation
    }
}

class MockRuntimeTestRunneProtectedTest extends RuntimeTestRunner {

    @Test()
    public AssertDeepEqual() : void {
        const listfailures : ArrayList<any> = new ArrayList<any>();
        const listexpected : ArrayList<string> = new ArrayList<string>();
        listexpected.Add("value2", "test");
        listexpected.Add("value3", "test3");
        listexpected.Add("value4", "test4");
        const listactual : ArrayList<string> = new ArrayList<string>();
        listactual.Add("value2", "test");
        listactual.Add("value3", "test3");
        listactual.Add("value4", "test4");
        this.assertDeepEqual(listexpected, {value2: "test", value3: "test3", value4: "test4"});
        this.assertDeepEqual(listactual, listexpected);
        this.assertDeepEqual(listactual, listfailures);
        this.assertDeepEqual({value2: "test", value3: "test3", value4: "test4"}, []);
        const listAny : ArrayList<any> = new ArrayList<any>();
        listAny.Add(listactual, "0");
        listAny.Add("test", "1");
        this.assertDeepEqual(listfailures, listexpected);
    }

    @Test()
    public DeepgetArgs() : void {
        const args : string[] = ["one", "two", "tree", "four"];
        const field : string[] = ["one", "two", "tree", "four"];
        this.assertEquals(args, field);
        this.assertDeepEqual(args, ["one", "two", "tree", "four"]);
    }

    @Test()
    public assertEquals1() : void {
        const persistenceManager4 : IPersistenceHandler = PersistenceFactory.getPersistence(
            "GuiAutocomplete", "user2");
        const vars : ArrayList<string> = new ArrayList<string>();
        vars.Add("value2", "test");
        vars.Add("value3", "test2");
        persistenceManager4.Variable(vars);
        this.assertEquals(persistenceManager4.Exists("test"), true, "test val exist before destroy");
        let value : string = "test for long assert message";
        while (value.length < 1100) {
            value += value;
        }
        this.assertEquals(value, "test");
        assert.patternEqual(Echo.getStream(), "" +
            "<h2>Runtime UNIT Test<h2>" +
            "<h3>Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner - AssertDeepEqual</h3>" +
            "<h4 id=\"RuntimeTestRunner_Assert_1\">" +
            "<i id=\"RuntimeTestRunner_Assert_Fail_1\" style=\"color: red;\">assert #1 has failed</i></h4><br/>" +
            "<u>actual:</u><br/>{*}<br/><u>expected:</u><br/>{*}" +
            "<h4 id=\"RuntimeTestRunner_Assert_2\">" +
            "<i id=\"RuntimeTestRunner_Assert_Pass_1\" style=\"color: green;\">assert #2 has passed</i></h4>" +
            "<h4 id=\"RuntimeTestRunner_Assert_3\">" +
            "<i id=\"RuntimeTestRunner_Assert_Pass_2\" style=\"color: green;\">assert #3 has passed</i></h4>" +
            "<h4 id=\"RuntimeTestRunner_Assert_4\">" +
            "<i id=\"RuntimeTestRunner_Assert_Fail_2\" style=\"color: red;\">assert #4 has failed</i></h4><br/>" +
            "<u>actual:</u><br/>{*}<br/><u>expected:</u><br/><i>Object type:</i> array. <i>Return values:</i><br/>" +
            "<i>Array object</i> <span onclick=\"" +
            "document.getElementById('ContentBlock_0').style.display=" +
            "document.getElementById('ContentBlock_0').style.display==='block'?'none':'block';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span>" +
            "<h4 id=\"RuntimeTestRunner_Assert_5\">" +
            "<i id=\"RuntimeTestRunner_Assert_Fail_3\" style=\"color: red;\">assert #5 has failed</i></h4><br/>" +
            "<u>actual:</u><br/>{*}<br/><u>expected:</u><br/>{*}<hr>" +
            "<h3>Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner - DeepgetArgs</h3>" +
            "<h4 id=\"RuntimeTestRunner_Assert_6\">" +
            "<i id=\"RuntimeTestRunner_Assert_Fail_4\" style=\"color: red;\">assert #6 has failed</i></h4><br/>" +
            "<u>actual:</u><br/><i>Object type:</i> array. <i>Return values:</i><br/>" +
            "<i>Array object</i> <span onclick=\"" +
            "document.getElementById('ContentBlock_1').style.display=" +
            "document.getElementById('ContentBlock_1').style.display==='block'?'none':'block';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">" +
            "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;one<br/>" +
            "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;two<br/>" +
            "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;tree<br/>" +
            "[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;four<br/></span><br/>" +
            "<u>expected:</u><br/><i>Object type:</i> array. <i>Return values:</i><br/>" +
            "<i>Array object</i> <span onclick=\"document.getElementById('ContentBlock_2').style.display=" +
            "document.getElementById('ContentBlock_2').style.display==='block'?'none':'block';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_2\" style=\"border: 0 solid black; display: none;\">" +
            "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;one<br/>" +
            "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;two<br/>" +
            "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;tree<br/>" +
            "[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;four<br/></span>" +
            "<h4 id=\"RuntimeTestRunner_Assert_7\">" +
            "<i id=\"RuntimeTestRunner_Assert_Pass_3\" style=\"color: green;\">assert #7 has passed</i></h4><hr>" +
            "<h3>Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner - assertEquals</h3>" +
            "<h4 id=\"RuntimeTestRunner_Assert_8\"> - test val exist before destroy:<br/>" +
            "<i id=\"RuntimeTestRunner_Assert_Pass_4\" style=\"color: green;\">assert #8 has passed</i></h4>" +
            "<h4 id=\"RuntimeTestRunner_Assert_9\">" +
            "<i id=\"RuntimeTestRunner_Assert_Fail_5\" style=\"color: red;\">assert #9 has failed</i></h4><br/>" +
            "<u>actual:</u><br/>" +
            "test for long assert messagetest for long assert* ...<br/><u>expected:</u><br/>" +
            "test");
    }
}

class MockAddButtonTest extends RuntimeTestRunner {

    @Test()
    public AddButton() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const test : RuntimeTestRunner = new MockRuntimeTestRunneProtectedTest();
            (<any>test).addButton("generate", () : void => {
                assert.ok(true, "test executed");
                ExceptionsManager.Clear();
                $done();
            });
            // setTimeout(() : void => {
            //     const button : HTMLElement = document.getElementById("TestButton0");
            //     button.click();
            // }, 150);
        };
    }

    @Test()
    public AddButtonException() : void {
        assert.doesHandleException(() : void => {
            this.addButton("exception", () : void => {
                // do nothing function
            });
        }, "this.exception.Message is not a function");
    }

    @Test()
    public assertEqualsSecond() : void {
        const reflect : Reflection = Reflection.getInstance();
        this.assertEquals(reflect.Exists("Io.Oidis.Commons.Primitives.BaseObject"), true);
        this.assertEquals(reflect.Exists(null), false);
    }
}

export class RuntimeTestRunnerTest extends UnitTestRunner {

    @Test(true)
    public GenerateCoverage() : void {
        const async : RuntimeTestRunner = new MockRuntimeTestRunneProtectedTest();
        RuntimeTestRunner.GenerateCoverage();
        assert.equal(async.ToString(),
            "object type of \'Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner\'");
        EventsManager.getInstanceSingleton().Clear(BuilderConnector.Connect().getId() + "");
        this.initSendBox();
    }

    @Test(true)
    public GenerateCoverageSecond() : void {
        const async : RuntimeTestRunner = new MockRuntimeTestRunneProtectedTest();
        (<any>RuntimeTestRunner).connector = BuilderConnector.Connect();
        assert.equal(RuntimeTestRunner.GenerateCoverage(), undefined);
        EventsManager.getInstanceSingleton().Clear(BuilderConnector.Connect().getId() + "");
        this.initSendBox();
    }

    @Test()
    public Constructor() : void {
        const async : RuntimeTestRunner = new MockRuntimeTestRunneProtectedTest();
        assert.equal(async.getClassName(), "Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner");
        assert.ok(async.getMethods());
    }

    @Test()
    public toString1() : void {
        const async : RuntimeTestRunner = new MockRuntimeTestRunneProtectedTest();
        assert.equal(async.toString(),
            "object type of \'Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner\'");
    }

    @Test(true)
    public AddButton() : void {
        assert.resolveEqual(MockAddButtonTest,
            "<h2>Runtime UNIT Test<h2><h3>Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner - AddButton" +
            "</h3><br/><div style=\"border: 1px solid red; cursor: pointer; width: 250px; overflow-x: hidden; overflow-y: hidden;" +
            " text-align: center; font-size: 16px; font-family: Consolas; color: red;\" id=\"TestButton0\">generate</div>");
        this.initSendBox();
        ExceptionsManager.Clear();
    }

    @Test()
    public ButtonExceptionSecond() : void {
        const test : RuntimeTestRunner = new MockRuntimeTestRunneProtectedTest();
        assert.doesNotThrow(() : void => {
            throw new Error("test button exception");

            //  assert.equal(ExceptionsManager.getLast().Message(), "test button exception");
        }, "Can not read propery 'click' of null");
    }

    @Test(true)
    public Resolver() : void {
        /// TODO: RuntimeTestRunner is using internally async body load, so Process() is not able to get innerHTML as sync output
        assert.resolveEqual(MockRuntimeTestRunnerResolve, "" +
            "<head></head>" +
            "<body><div id=\"Content\">" +
            "<span guitype=\"HtmlAppender\"><h2>Runtime UNIT Test</h2><h2></h2></span>" +
            "<span guitype=\"HtmlAppender\"><h3>Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner - Data</h3>" +
            "</span><span guitype=\"HtmlAppender\">" +
            "<h4 id=\"RuntimeTestRunner_Assert_1\">" +
            "<i id=\"RuntimeTestRunner_Assert_Pass_1\" style=\"color: green;\">assert #1 has passed</i></h4></span>" +
            "<span guitype=\"HtmlAppender\"><hr></span>" +
            "<span guitype=\"HtmlAppender\"><h3>Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner - TestData</h3>" +
            "</span><span guitype=\"HtmlAppender\"><h4><i style=\"color: orange;\">skipped</i></h4><hr></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<span class=\"Result\">SUCCESS<br></span>" +
            "Skipped tests: 1<br>Tests: 2, Assertions: 1, Failures: 0.</span>" +
            "<span guitype=\"HtmlAppender\"><br><br>Page was generated in * seconds.</span>" +
            "</div></body>"
        );
        ExceptionsManager.Clear();
    }

    @Test(true)
    public PromiseResolver() : void {
        assert.resolveEqual(MockRuntimeTestRunnerPromiseResolve, "" +
            "<head></head>" +
            "<body><div id=\"Content\"><span guitype=\"HtmlAppender\">" +
            "<h2>Runtime UNIT Test</h2>" +
            "<h2></h2></span>" +
            "<span guitype=\"HtmlAppender\">" +
            "<br>before</span><span guitype=\"HtmlAppender\">" +
            "<h3>Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner - Init</h3></span>" +
            "<span guitype=\"HtmlAppender\"><br>MockRuntimeTestRunnerPromiseResolve initialization</span>" +
            "<span guitype=\"HtmlAppender\"><hr></span>" +
            "<span guitype=\"HtmlAppender\"><br>after</span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<span class=\"Result\">SUCCESS<br></span>" +
            "Tests: 1, Assertions: 0, Failures: 0.</span>" +
            "<span guitype=\"HtmlAppender\"><br><br>" +
            "Page was generated in * seconds.</span>" +
            "</div>" +
            "</body>");
        ExceptionsManager.Clear();
    }

    @Test(true)
    public PromiseUpAndDown() : void {
        assert.resolveEqual(MockRuntimeTestUpandDownResolver, "" +
            "<head></head>" +
            "<body>" +
            "<div id=\"Content\"><span guitype=\"HtmlAppender\">" +
            "<h2>Runtime UNIT Test</h2>" +
            "<h2></h2></span><span guitype=\"HtmlAppender\">" +
            "<h3>Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner - Init</h3></span>" +
            "<span guitype=\"HtmlAppender\"><br>set up</span><span guitype=\"HtmlAppender\"><br>" +
            "MockRuntimeTestUpandDownResolver initialization</span>" +
            "<span guitype=\"HtmlAppender\"><hr></span><span guitype=\"HtmlAppender\">" +
            "<h3>Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner - Case02</h3></span>" +
            "<span guitype=\"HtmlAppender\"><br>set up</span>" +
            "<span guitype=\"HtmlAppender\"><hr></span><span guitype=\"HtmlAppender\"><br>" +
            "<span class=\"Result\">SUCCESS<br>" +
            "</span>Tests: 2, Assertions: 0, Failures: 0.</span>" +
            "<span guitype=\"HtmlAppender\"><br><br>" +
            "Page was generated in * seconds.</span>" +
            "</div>" +
            "</body>");
        ExceptionsManager.Clear();
    }

    @Test(true)
    public ProtectedMethods() : IUnitTestRunnerPromise {
        assert.resolveEqual(MockRuntimeTestRunneProtectedTest, "" +
            "<head></head>" +
            "<body>" +
            "<div id=\"Content\"><span guitype=\"HtmlAppender\"><h2>Runtime UNIT Test</h2><h2></h2></span>" +
            "<span guitype=\"HtmlAppender\">" +
            "<h3>Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner - AssertDeepEqual</h3></span>" +
            "<span guitype=\"HtmlAppender\"><h4 id=\"RuntimeTestRunner_Assert_1\">" +
            "<i id=\"RuntimeTestRunner_Assert_Fail_1\" style=\"color: red;\">assert #1 has failed</i></h4></span>" +
            "<span guitype=\"HtmlAppender\"><br><u>actual:</u><br>{*}</span>" +
            "<span guitype=\"HtmlAppender\"><br><u>expected:</u><br>{*}</span>" +
            "<span guitype=\"HtmlAppender\"><h4 id=\"RuntimeTestRunner_Assert_2\">" +
            "<i id=\"RuntimeTestRunner_Assert_Pass_1\" style=\"color: green;\">assert #2 has passed</i></h4></span>" +
            "<span guitype=\"HtmlAppender\"><h4 id=\"RuntimeTestRunner_Assert_3\">" +
            "<i id=\"RuntimeTestRunner_Assert_Pass_2\" style=\"color: green;\">assert #3 has passed</i></h4></span>" +
            "<span guitype=\"HtmlAppender\"><h4 id=\"RuntimeTestRunner_Assert_4\">" +
            "<i id=\"RuntimeTestRunner_Assert_Fail_2\" style=\"color: red;\">assert #4 has failed</i></h4></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<u>actual:</u><br>{*}</span><span guitype=\"HtmlAppender\"><br>" +
            "<u>expected:</u><br><i>Object type:</i> array. <i>Return values:</i><br>" +
            "<i>Array object</i> <span onclick=\"" +
            "document.getElementById('ContentBlock_0').style.display=" +
            "document.getElementById('ContentBlock_0').style.display==='block'?'none':'block';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span></span>" +
            "<span guitype=\"HtmlAppender\"><h4 id=\"RuntimeTestRunner_Assert_5\">" +
            "<i id=\"RuntimeTestRunner_Assert_Fail_3\" style=\"color: red;\">assert #5 has failed</i></h4></span>" +
            "<span guitype=\"HtmlAppender\"><br><u>actual:</u><br>{*}</span>" +
            "<span guitype=\"HtmlAppender\"><br><u>expected:</u><br>{*}</span>" +
            "<span guitype=\"HtmlAppender\"><hr></span><span guitype=\"HtmlAppender\">" +
            "<h3>Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner - DeepgetArgs</h3></span>" +
            "<span guitype=\"HtmlAppender\"><h4 id=\"RuntimeTestRunner_Assert_6\">" +
            "<i id=\"RuntimeTestRunner_Assert_Fail_4\" style=\"color: red;\">assert #6 has failed</i></h4></span>" +
            "<span guitype=\"HtmlAppender\"><br><u>actual:</u><br><i>Object type:</i> array. <i>Return values:</i><br>" +
            "<i>Array object</i> <span onclick=\"" +
            "document.getElementById('ContentBlock_1').style.display=" +
            "document.getElementById('ContentBlock_1').style.display==='block'?'none':'block';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br>" +
            "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">" +
            "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;one<br>" +
            "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;two<br>" +
            "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;tree<br>" +
            "[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;four<br></span></span>" +
            "<span guitype=\"HtmlAppender\"><br><u>expected:</u><br><i>Object type:</i> array. <i>Return values:</i><br>" +
            "<i>Array object</i> <span onclick=\"" +
            "document.getElementById('ContentBlock_2').style.display=" +
            "document.getElementById('ContentBlock_2').style.display==='block'?'none':'block';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br>" +
            "<span id=\"ContentBlock_2\" style=\"border: 0 solid black; display: none;\">" +
            "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;one<br>" +
            "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;two<br>" +
            "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;tree<br>" +
            "[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;four<br></span></span>" +
            "<span guitype=\"HtmlAppender\"><h4 id=\"RuntimeTestRunner_Assert_7\">" +
            "<i id=\"RuntimeTestRunner_Assert_Pass_3\" style=\"color: green;\">assert #7 has passed</i></h4></span>" +
            "<span guitype=\"HtmlAppender\"><hr></span><span guitype=\"HtmlAppender\">" +
            "<h3>Io.Oidis.Commons.HttpProcessor.Resolvers.RuntimeTestRunner - assertEquals</h3></span>" +
            "<span guitype=\"HtmlAppender\"><h4 id=\"RuntimeTestRunner_Assert_8\"> - test val exist before destroy:<br>" +
            "<i id=\"RuntimeTestRunner_Assert_Pass_4\" style=\"color: green;\">assert #8 has passed</i></h4></span>" +
            "<span guitype=\"HtmlAppender\"><h4 id=\"RuntimeTestRunner_Assert_9\">" +
            "<i id=\"RuntimeTestRunner_Assert_Fail_5\" style=\"color: red;\">assert #9 has failed</i></h4></span>" +
            "<span guitype=\"HtmlAppender\"><br><u>actual:</u><br>test for long assert messagetest for long *...</span>" +
            "<span guitype=\"HtmlAppender\"><br><u>expected:</u><br>" +
            "test</span><span guitype=\"HtmlAppender\"><hr></span>" +
            "<span guitype=\"HtmlAppender\"><br><span class=\"Result\">FAILURES!<br>" +
            "</span>Tests: 3, Assertions: 9, Failures: 5.</span><span guitype=\"HtmlAppender\"><br><br>" +
            "Page was generated in * seconds.</span>" +
            "<span guitype=\"HtmlAppender\">" +
            "<div id=\"RuntimeTestRunner_GoToFirstFailing\" class=\"GoToFirstFailing\">Go to first failing assert</div></span>" +
            "<span guitype=\"HtmlAppender\">" +
            "<div id=\"RuntimeTestRunner_GoToNextFailing\" class=\"GoToNextFailing\">Go to next failing assert</div></span>" +
            "<span guitype=\"HtmlAppender\">" +
            "<div id=\"RuntimeTestRunner_GoToLastFailing\" class=\"GoToLastFailing\">Go to last failing assert</div></span>" +
            "<span guitype=\"HtmlAppender\"><select id=\"RuntimeTestRunner_GoToSpecific\" class=\"GoToSpecific\"></select>" +
            "</span>" +
            "</div>" +
            "</body>");
        this.registerElement("RuntimeTestRunner_GoToFirstFailing");
        this.registerElement("RuntimeTestRunner_GoToNextFailing");
        this.registerElement("RuntimeTestRunner_GoToLastFailing");
        this.registerElement("RuntimeTestRunner_GoToSpecific", "select");

        return ($done : any) : void => {
            setTimeout(() : void => {
                this.initSendBox();
                $done();
            }, 200);
        };
    }
}
