/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ClientUnitTestLoader, UnitTestRunner } from "../UnitTestRunner.js";

import { EventType } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/Events/EventType.js";
import { GeneralEventOwner } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/Events/GeneralEventOwner.js";
import { HttpRequestConstants } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/HttpRequestConstants.js";
import { ExceptionErrorPage } from "../../../../../../../source/typescript/Io/Oidis/Commons/ErrorPages/ExceptionErrorPage.js";
import { AsyncRequestEventArgs } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { HttpRequestEventArgs } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/HttpRequestEventArgs.js";
import { EventsManager } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/EventsManager.js";
import { ExceptionsManager } from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { Exception } from "../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/Type/Exception.js";
import { HttpManager } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/HttpManager.js";
import { HttpResolver } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/HttpResolver.js";
import { HttpResolversCollection } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/HttpResolversCollection.js";
import { BaseHttpResolver } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { ArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";

class MockHttpResolver extends BaseHttpResolver {
    public Process() : void {
        throw new Error("test resolver exception");
    }
}

class MockExceptionResolver extends BaseHttpResolver {
    public static asyncProcessor : () => void;

    public Process() : void {
        if (this.RequestArgs().POST().KeyExists(HttpRequestConstants.EXCEPTIONS_LIST)) {
            const exception : Exception = this.RequestArgs().POST().getItem(HttpRequestConstants.EXCEPTIONS_LIST).getLast();
            this.getHttpManager().OverrideResolver(
                "/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}", MockExceptionResolver);
            assert.equal(exception.Message(), "test resolver exception");
            MockExceptionResolver.asyncProcessor();
        }
        // override process for ability to discard default behavior
    }
}

class MockBaseHttpResolver extends BaseHttpResolver {
    public Process() : void {
        assert.equal(this.RequestArgs().GET().Length(), 1);
        assert.equal(this.RequestArgs().GET().getItem("testKey"), "testValue");
        super.Process();
    }
}

class MockResolver extends HttpResolver {
    protected async getStartupResolvers() : Promise<any> {
        const resolvers : any = await super.getStartupResolvers({isBrowser: false, isProd: false});
        resolvers["/empty"] = "";
        resolvers["/ServerError/Http/DefaultPage"] = MockHttpResolver;
        resolvers["/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}"] = MockExceptionResolver;
        return resolvers;
    }
}

class MockException extends ExceptionErrorPage {
    protected getPageBody() : any {
        assert.throws(() : void => {
            this.getClassName();
        }, /Exceptions/);
    }
}

export class HttpResolverTest extends UnitTestRunner {

    @Test()
    public ConstructorWithArgs() : void {
        ClientUnitTestLoader.Load(<any>{
            build: {time: new Date().toTimeString()}, name: "com-wui-framework-commons", version: "1.0.0"
        });
        window.location.hash = "";
        const instance : HttpResolver = new HttpResolver("projectName/basePath");
        assert.equal(window.location.hash, "/projectName/basePath/");
        assert.equal(instance.getManager().getClassName(), HttpManager.ClassName());
        window.location.hash = "";
    }

    @Test()
    public ResolverRequestAsync() : void {
        const data : ArrayList<any> = new ArrayList<any>();
        const resolver : HttpResolver = new HttpResolver();
        data.Add("http://localhost:8888/baseAddress/UnitTestEnvironment.js#/projectName/pageName/task",
            HttpRequestConstants.EXCEPTIONS_LIST);
        this.setUserAgent(window.navigator.userAgent + " com-wui-framework-jre");
        const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
        resolver.ResolveRequest(args);

        EventsManager.getInstanceSingleton().FireEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST, false);
        this.initSendBox();
    }

    @Test()
    public ResolverRequestWithoutArgs() : void {
        assert.doesHandleException(() : void => {
            const resolver : HttpResolver = new HttpResolver();
            resolver.ResolveRequest(null);
        }, "");
        this.initSendBox();
    }

    @Test()
    public ResolverRequestSameUrlTwice() : void {
        assert.doesNotThrow(() : void => {
            const resolver : HttpResolver = new HttpResolver();
            this.setUserAgent(window.navigator.userAgent + " com-wui-framework-jre");
            resolver.ResolveRequest(new HttpRequestEventArgs(this.getRequest().getUrl()));
            this.initSendBox();
        });
    }

    @Test()
    public ResolverRequestCreateDefaultResolver() : void {
        const resolver : HttpResolver = new HttpResolver();
        const child : HttpResolversCollection = resolver.getManager().getResolversCollection().getChildrenList().getFirst()
            .getChild("ServerError").getChild("Http").getChild("DefaultPage");
        const defaultResolverClass : string = child.ResolverClassName();
        resolver.ResolveRequest(new HttpRequestEventArgs("/test/test1"));
        child.ResolverClassName("");
        resolver.ResolveRequest(new HttpRequestEventArgs("/test/test2"));
        child.ResolverClassName(defaultResolverClass);
        this.initSendBox();
    }

    @Test()
    public requestResolver() : void {
        assert.doesNotThrow(() : void => {
            const resolver : HttpResolver = new HttpResolver("http://localhost:8888/baseAddress/UnitTestEnvironment.js");
            this.setUserAgent(window.navigator.userAgent + " com-wui-framework-jre");
            resolver.getManager().RegisterResolver("/test/MockBaseHttpResolver", MockBaseHttpResolver);
            assert.equal(resolver.getManager().getResolverClassName("/test/MockBaseHttpResolver"), MockBaseHttpResolver);
            const requestArgs : HttpRequestEventArgs = new HttpRequestEventArgs("/test/MockBaseHttpResolver");
            requestArgs.GET().Add("testValue", "testKey");
            resolver.ResolveRequest(requestArgs);
            this.initSendBox();
        });
    }

    @Test()
    public toString2() : void {
        const resolver : HttpResolver = new HttpResolver();
        assert.equal(resolver.toString(), "object type of \'Io.Oidis.Commons.HttpProcessor.HttpResolver\'");
    }

    protected tearDown() : void {
        this.initSendBox();
        ExceptionsManager.Clear();
    }
}
