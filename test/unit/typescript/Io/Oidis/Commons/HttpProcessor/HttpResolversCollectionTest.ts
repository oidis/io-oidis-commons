/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

import { HttpResolversCollection } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/HttpResolversCollection.js";
import { BaseHttpResolver } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { Test } from "../BaseUnitTestRunner.js";

export class HttpResolversCollectionTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("parent", "parentClass");
        assert.equal(collection.Pattern(), "parent");
        assert.equal(collection.ResolverClassName(), "parentClass");
        assert.equal(collection.ParameterName(), null);
        assert.equal(collection.IgnoreCase(), true);
        assert.equal(collection.LevelsCount(), "0");
        assert.ok(collection.getChildrenList().IsEmpty());

        assert.throws(() : void => {
            const instance : HttpResolversCollection = new HttpResolversCollection(null, null);
        }, /HttpKeyPattern can not be null./);
    }

    @Test()
    public Pattern() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        assert.equal(collection.Pattern(), "pattern");
    }

    @Test()
    public Contains() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        collection.AddChild(new HttpResolversCollection("child"));
        assert.ok(collection.Contains("child"));
        assert.equal(collection.Contains("other"), false);
    }

    @Test()
    public ResolverClassName() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern", "className");
        assert.equal(collection.ResolverClassName(), "className");
    }

    @Test()
    public ParameterName() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        collection.ParameterName("parameter");
        assert.equal(collection.ParameterName(), "parameter");
    }

    @Test()
    public IgnoreCase() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        collection.IgnoreCase(true);
        assert.equal(collection.IgnoreCase(), true);
    }

    @Test()
    public LevelsCountFlexible() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        collection.LevelsCount(254, true);
        assert.equal(collection.LevelsCount(), "254+");
    }

    @Test()
    public LevelsCountNonFlexible() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        collection.LevelsCount(254, false);
        assert.equal(collection.LevelsCount(), "254");
    }

    @Test()
    public LevelsCountLastNumber() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        collection.LevelsCount("253+", true);
        collection.LevelsCount("254+", true);
        assert.equal(collection.LevelsCount(), "254+");
    }

    @Test()
    public LevelsCountFromString() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        collection.LevelsCount("fifty", true);
        assert.equal(collection.LevelsCount(), "0+");
    }

    @Test()
    public LevelsCountFromNullNumber() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        collection.LevelsCount(0, true);
        assert.equal(collection.LevelsCount(), "0+");
    }

    @Test()
    public LevelsCountFromNullString() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        collection.LevelsCount("", true);
        assert.equal(collection.LevelsCount(), "0");
    }

    @Test()
    public LevelsCountFromNonNumberFlexible() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        collection.LevelsCount("456test", true);
        assert.equal(collection.LevelsCount(), "456+");
    }

    @Test()
    public LevelsCountFromNonNumber() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        collection.LevelsCount("456test", false);
        assert.equal(collection.LevelsCount(), "456");
    }

    @Test()
    public AddChild() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        assert.equal(collection.getChildrenList().IsEmpty(), true);
        collection.AddChild(new HttpResolversCollection("child"));
        assert.equal(collection.getChildrenList().Length(), 1);
        collection.AddChild(null);
        assert.equal(collection.getChildrenList().Length(), 1);
    }

    @Test()
    public getChildrenList() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        const child : HttpResolversCollection = new HttpResolversCollection("child");
        collection.AddChild(child);
        assert.equal(collection.getChildrenList().Length(), 1);
        assert.deepEqual(collection.getChildrenList().getFirst(), child);
    }

    @Test()
    public getChild() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
        const child : HttpResolversCollection = new HttpResolversCollection("child");
        collection.AddChild(child);
        assert.deepEqual(collection.getChild("child"), child);
        assert.equal(collection.getChild("other"), null);
    }

    @Test()
    public Collection() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("web");
        const collection2 : HttpResolversCollection = new HttpResolversCollection("test");
        collection2.AddChild(new HttpResolversCollection("*te*st*"));
        collection.AddChild(collection2);
        collection.AddChild(new HttpResolversCollection("/7t77est7"));
        collection.AddChild(new HttpResolversCollection("6te6st6"));
        collection.AddChild(new HttpResolversCollection("**"));
        collection.AddChild(new HttpResolversCollection("t*est"));
        collection.AddChild(new HttpResolversCollection("te*st"));
        collection.AddChild(new HttpResolversCollection("tes*t"));
        collection.AddChild(new HttpResolversCollection("test*"));
        collection.AddChild(new HttpResolversCollection("*"));
        collection.getChild("6te6st6").AddChild(new HttpResolversCollection("8t88est8"));
        collection.getChild("6te6st6").AddChild(new HttpResolversCollection("7te7st7"));
        collection.getChild("6te6st6").AddChild(new HttpResolversCollection("test"));
        collection.getChild("te*st").AddChild(new HttpResolversCollection("va*lue"));
        collection.getChild("te*st").AddChild(new HttpResolversCollection("valu*e120"));
        collection.getChild("te*st").AddChild(new HttpResolversCollection("value*2556"));
        collection.getChild("6te6st6").getChild("8t88est8").AddChild(new HttpResolversCollection("5klm6455"));
        collection.getChild("6te6st6").getChild("8t88est8").AddChild(new HttpResolversCollection("ctest"));
        collection.getChild("6te6st6").getChild("8t88est8").AddChild(new HttpResolversCollection("atest"));
        collection.getChild("6te6st6").getChild("8t88est8").AddChild(new HttpResolversCollection("btest"));
        collection.getChild("6te6st6").getChild("8t88est8").AddChild(new HttpResolversCollection("test"));
        collection.getChild("te*st").getChild("va*lue").AddChild(new HttpResolversCollection("golden5gate7"));
        collection.getChild("te*st").getChild("va*lue").AddChild(new HttpResolversCollection("black6hacker6"));
        collection.getChild("te*st").getChild("va*lue").AddChild(new HttpResolversCollection("black6hacker8"));
        collection.OrderChildrenCollection();
        collection.getChild("te*st").OrderChildrenCollection();
        collection.getChild("te*st").getChild("va*lue").OrderChildrenCollection();
        collection.getChild("6te6st6").getChild("8t88est8").OrderChildrenCollection();
    }

    @Test()
    public OrderChildrenCollection() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("web");
        const collection2 : HttpResolversCollection = new HttpResolversCollection("test");
        collection2.AddChild(new HttpResolversCollection("*te*st*"));
        collection.AddChild(collection2);
        collection.AddChild(new HttpResolversCollection("/7t77est7"));
        collection.AddChild(new HttpResolversCollection("6te6st6"));
        collection.AddChild(new HttpResolversCollection("**"));
        collection.AddChild(new HttpResolversCollection("t*est"));
        collection.AddChild(new HttpResolversCollection("te*st"));
        collection.AddChild(new HttpResolversCollection("tes*t"));
        collection.AddChild(new HttpResolversCollection("test*"));
        collection.AddChild(new HttpResolversCollection("*"));

        assert.deepEqual(collection.getChildrenList().ToString("", false),
            "Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
            "[ 0 ]    \"test\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
            "        [ 0 ]    \"*te*st*\" => resolver not assigned   \r\n" +
            "                ignore case: true\r\n" +
            "                count of levels: 0\r\n" +
            "                parameter name: NULL\r\n" +
            "                child list: EMPTY\r\n" +
            "[ 1 ]    \"/7t77est7\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 2 ]    \"6te6st6\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 3 ]    \"**\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 4 ]    \"t*est\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 5 ]    \"te*st\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 6 ]    \"tes*t\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 7 ]    \"test*\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 8 ]    \"*\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n");
        collection.OrderChildrenCollection();
        assert.deepEqual(collection.getChildrenList().ToString("", false),
            "Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
            "[ 0 ]    \"/7t77est7\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 1 ]    \"6te6st6\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 2 ]    \"test\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
            "        [ 0 ]    \"*te*st*\" => resolver not assigned   \r\n" +
            "                ignore case: true\r\n" +
            "                count of levels: 0\r\n" +
            "                parameter name: NULL\r\n" +
            "                child list: EMPTY\r\n" +
            "[ 3 ]    \"test*\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 4 ]    \"tes*t\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 5 ]    \"te*st\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 6 ]    \"t*est\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 7 ]    \"*\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 8 ]    \"**\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n");

        (<any>HttpResolversCollection).isOrdered = true;
        collection.OrderChildrenCollection();
        assert.deepEqual(collection.getChildrenList().ToString("", false),
            "Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
            "[ 0 ]    \"/7t77est7\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 1 ]    \"6te6st6\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 2 ]    \"test\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: Io.Oidis.Commons.Primitives.ArrayList object\r\n        " +
            "[ 0 ]    \"*te*st*\" => resolver not assigned   \r\n" +
            "                ignore case: true\r\n" +
            "                count of levels: 0\r\n" +
            "                parameter name: NULL\r\n" +
            "                child list: EMPTY\r\n" +
            "[ 3 ]    \"test*\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 4 ]    \"tes*t\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 5 ]    \"te*st\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 6 ]    \"t*est\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 7 ]    \"*\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n" +
            "[ 8 ]    \"**\" => resolver not assigned   \r\n" +
            "        ignore case: true\r\n" +
            "        count of levels: 0\r\n" +
            "        parameter name: NULL\r\n" +
            "        child list: EMPTY\r\n");

        const collection3 : HttpResolversCollection = new HttpResolversCollection("web");
        collection3.OrderChildrenCollection();
        assert.deepEqual(collection3.getChildrenList().ToString("", false),
            "Io.Oidis.Commons.Primitives.ArrayList object\r\nData object EMPTY");

        const collection5 : HttpResolversCollection = new HttpResolversCollection("way");
        collection.getChild("test").AddChild(collection5);
        collection.getChild("test").AddChild(collection5);
        collection.OrderChildrenCollection();
        assert.equal(collection.getChild("test"), collection2);
        assert.equal(collection.getChild("test"), collection2);
        collection.OrderChildrenCollection();
        assert.equal(collection.getChild("test").getChild("way"), collection5);
    }

    @Test()
    public ToString1() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("child", BaseHttpResolver);
        this.resetCounters();
        assert.equal(collection.ToString("", true),
            "\"child\" => \"Io.Oidis.Commons.HttpProcessor.Resolvers.BaseHttpResolver\"&nbsp;&nbsp;&nbsp;" +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_1\').style.display=" +
            "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>ignore case:</i> true<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>count of levels:</i> 0<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>parameter name:</i> NULL<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>child list:</i> <i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data object <b>EMPTY</b></span></span>");
    }

    @Test()
    public toString1() : void {
        const collection : HttpResolversCollection = new HttpResolversCollection("pattern", BaseHttpResolver.ClassName());
        collection.AddChild(new HttpResolversCollection("child"));
        this.resetCounters();
        const value : string = collection.ToString();
        this.resetCounters();
        assert.equal(collection.toString(), value);
    }
}
