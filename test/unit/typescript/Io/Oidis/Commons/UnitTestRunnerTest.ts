/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "./UnitTestRunner.js";

import { ExceptionsManager } from "../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { LogIt } from "../../../../../../source/typescript/Io/Oidis/Commons/Utils/LogIt.js";
import { IUnitTestRunnerPromise, Test } from "./BaseUnitTestRunner.js";
import { assert } from "./UnitTestEnvironment.js";

export class UnitTestRunnerTest extends UnitTestRunner {

    constructor() {
        super();
        // this.setMethodFilter("testAsyncTimeout", "testTimeout", "testTimeout2");
    }

    @Test()
    public Sync1() : void {
        LogIt.Info("sync 1");
    }

    @Test()
    public Promise1() : IUnitTestRunnerPromise {
        LogIt.Info("before promise 1");
        return ($done : () => void) : void => {
            LogIt.Info("on promise 1");
            setTimeout(() : void => {
                assert.ok(true);
                LogIt.Info("after promise 1");
                $done();
            }, 300);
        };
    }

    @Test()
    public async Async1() : Promise<void> {
        this.timeoutLimit(1000);
        LogIt.Info("before async 1");
        return new Promise<void>(($resolve : () => void) : void => {
            LogIt.Info("on async 1");
            setTimeout(() : void => {
                assert.ok(true);
                LogIt.Info("after async 1");
                $resolve();
            }, 300);
        });
    }

    @Test()
    public Sync2() : void {
        LogIt.Info("sync 2");
        ExceptionsManager.Clear();
    }

    @Test()
    public Async2() : IUnitTestRunnerPromise {
        LogIt.Info("before async 2");
        return ($done : () => void) : void => {
            LogIt.Info("on async 2");
            setTimeout(() : void => {
                assert.ok(true);
                LogIt.Info("after async 2");
                $done();
            }, 300);
        };
    }

    @Test()
    public Sync3() : void {
        LogIt.Info("sync 3");
        assert.ok(true);
    }

    @Test()
    public Error() : void {
        assert.throws(() : void => {
            throw new Error("Test1");
        }, "Test1");
    }

    @Test(true)
    public testError2() : void {
        assert.doesHandleException(() : void => {
            try {
                throw new Error("Test2");
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        }, "Test2");
    }

    @Test(true)
    public testError3() : void {
        // This test should fail and assert should be correctly reported even that it is in try catch
        try {
            assert.doesHandleException(() : void => {
                throw new Error("Test3");
            }, "Test 3");
        } catch (ex) {
            LogIt.Warning(ex.message);
        }
    }

    @Test()
    public MockServer() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const http : any = globalThis.require("http");
            const httpRequest : any = http.request(this.getMockServerLocation() + "/com-wui-framework-commons/index",
                ($httpResponse : any) : void => {
                    if ($httpResponse.statusCode === 200) {
                        let body : string = "";
                        $httpResponse.on("data", ($chunk : string) : void => {
                            body += $chunk;
                        });
                        $httpResponse.on("end", () : void => {
                            assert.equal(body, "hello");
                            $done();
                        });
                    }
                });
            httpRequest.end();
        };
    }

    @Test()
    public NativePromise() : IUnitTestRunnerPromise {
        LogIt.Info("before native promise");
        return ($done : () => void) : void => {
            const promise : any = new Promise(($resolve : any, $reject : any) : void => {
                setTimeout(() : void => {
                    assert.ok(true);
                    $resolve();
                }, 300);
            });
            promise.then(() : void => {
                $done();
            });
        };
    }

    @Test(true)
    public async AsyncTimeout() : Promise<void> {
        this.timeoutLimit(5000);
        await new Promise<void>(($resolve, $reject) : void => {
            setTimeout(() : void => {
                assert.ok(true);
                $resolve();
            }, 7000);
            setTimeout(() : void => {
                assert.ok(false, "false from previous test AsyncTimeout");
                $reject(new Error("post timeout error"));
            }, 10000);
        });
    }

    @Test(true)
    public Timeout() : IUnitTestRunnerPromise {
        this.timeoutLimit(5000);
        return ($done : () => void) : void => {
            setTimeout(() : void => {
                assert.ok(true);
                $done();
            }, 10000);
        };
    }

    @Test(true)
    public Timeout2() : IUnitTestRunnerPromise {
        this.timeoutLimit(5000);
        return ($done : () => void) : void => {
            setTimeout(() : void => {
                assert.ok(false);
                $done();
            }, 10000);
        };
    }

    protected before() : IUnitTestRunnerPromise {
        this.timeoutLimit(180000);
        return ($done : () => void) : void => {
            setTimeout(() : void => {
                LogIt.Info("before");
                $done();
            }, 1000);
        };
    }

    protected after() : void {
        LogIt.Info("after");
    }

    protected setUp() : void {
        LogIt.Info("setUp");
    }

    protected tearDown() : void {
        LogIt.Info("tearDown");
    }
}
