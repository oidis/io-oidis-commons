/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Test } from "./BaseUnitTestRunner.js";
import { assert } from "./UnitTestEnvironment.js";
import { ClientUnitTestEnvironmentArgs, UnitTestEnvironmentArgs, UnitTestRunner } from "./UnitTestRunner.js";

export class EnvironmentArgsTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const environArgs : ClientUnitTestEnvironmentArgs = new ClientUnitTestEnvironmentArgs();
        environArgs.Load({
            build: {time: "235620"}, name: "envir", version: "333xxx"
        }, () : void => {
            assert.equal(environArgs.getBuildTime(), "235620");
            assert.equal(environArgs.HtmlOutputAllowed(), true);
        });
    }

    @Test()
    public getProjectName() : void {
        const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
        environArgs.Load({
            build: {time: "235620"}, name: "envir", version: "333xxx"
        }, () : void => {
            assert.equal(environArgs.getProjectName(), "envir");
        });
    }

    @Test()
    public getProjectVersion() : void {
        const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
        environArgs.Load({
            build: {time: "235620"}, name: "envir", version: "333xxx"
        }, () : void => {
            assert.equal(environArgs.getProjectVersion(), "333xxx");
        });
    }

    @Test()
    public getProcessID() : void {
        const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
        environArgs.Load({
            build: {time: "235620"}, name: "envir", version: "333xxx"
        }, () : void => {
            const tmpLocation : string = window.location.href;
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?AppPid=123456");
            this.getHttpResolver().CreateRequest();
            assert.equal(environArgs.getProcessID(), 123456);

            this.setUrl(tmpLocation);
            this.getHttpResolver().CreateRequest();

            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?AppPid=");
            this.getHttpResolver().CreateRequest();
            assert.equal(environArgs.getProcessID(), 123456);

            this.setUrl(tmpLocation);
            this.getHttpResolver().CreateRequest();
        });
    }

    @Test()
    public getAppName() : void {
        this.setUrl("http://localhost:8888/UnitTestEnvironment.js?AppName=app");
        this.getHttpResolver().CreateRequest();
        const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
        environArgs.Load({
            build: {time: "235620"}, name: "envir", version: "333xxx"
        }, () : void => {
            assert.equal(environArgs.getAppName(), "app");
            assert.equal(environArgs.getAppName(), "app");
        });
    }

    @Test()
    public getAppNameSecond() : void {
        this.setUrl("http://localhost:8888/UnitTestEnvironment.js");
        this.getHttpResolver().CreateRequest();
        const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
        environArgs.Load({
            build: {time: "235620"}, name: "envir", version: "333xxx"
        }, () : void => {
            assert.equal(environArgs.getAppName(), undefined);
        });
    }

    @Test()
    public getBuildTime() : void {
        const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
        environArgs.Load({
            build: {time: "235620"}, name: "envir", version: "333xxx"
        }, () : void => {
            assert.equal(environArgs.getBuildTime(), "235620");
        });
    }

    @Test()
    public getReleaseName() : void {
        this.setUrl("http://localhost:8888/UnitTestEnvironment.js?ReleaseName=release");
        this.getHttpResolver().CreateRequest();
        const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
        environArgs.Load({
            build: {time: "235620"}, name: "envir", version: "333xxx"
        }, () : void => {
            assert.equal(environArgs.getReleaseName(), "release");
            assert.equal(environArgs.getReleaseName(), "release");
        });
    }

    @Test()
    public getReleaseNameSecond() : void {
        this.setUrl("http://localhost:8888/UnitTestEnvironment.js");
        this.getHttpResolver().CreateRequest();
        const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
        environArgs.Load({
            build: {time: "235620"}, name: "envir", version: "333xxx"
        }, () : void => {
            assert.equal(environArgs.getReleaseName(), "null");
        });
    }

    @Test()
    public getPlatform() : void {
        this.setUrl("http://localhost:8888/UnitTestEnvironment.js?AppName=app");
        this.getHttpResolver().CreateRequest();
        const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
        environArgs.Load({
            build: {time: "235620"}, name: "envir", version: "333xxx"
        }, () : void => {
            assert.equal(environArgs.getPlatform(), "null");
            assert.equal(environArgs.getPlatform(), "null");
        });
    }

    @Test()
    public ToString1() : void {
        const environArgs : ClientUnitTestEnvironmentArgs = new ClientUnitTestEnvironmentArgs();
        environArgs.Load({
            build: {time: "235620", type: "prod"}, name: "envir", version: "333xxx"
        }, () : void => {
            this.resetCounters();
            assert.equal(environArgs.ToString(),
                "[\"getProjectName\"]&nbsp;&nbsp;&nbsp;envir<br/>" +
                "[\"getProjectVersion\"]&nbsp;&nbsp;&nbsp;333xxx<br/>" +
                "[\"getBuildTime\"]&nbsp;&nbsp;&nbsp;235620<br/>" +
                "[\"IsProductionMode\"]&nbsp;&nbsp;&nbsp;<i>Object type:</i> boolean. <i>Return value:</i> true<br/>" +
                "[\"HtmlOutputAllowed\"]&nbsp;&nbsp;&nbsp;<i>Object type:</i> boolean. <i>Return value:</i> true<br/>");
            assert.equal(environArgs.ToString("", false),
                "[\"getProjectName\"]    envir\r\n" +
                "[\"getProjectVersion\"]    333xxx\r\n" +
                "[\"getBuildTime\"]    235620\r\n" +
                "[\"IsProductionMode\"]    Object type: boolean. Return value: true\r\n" +
                "[\"HtmlOutputAllowed\"]    Object type: boolean. Return value: true\r\n");
        });
    }
}
