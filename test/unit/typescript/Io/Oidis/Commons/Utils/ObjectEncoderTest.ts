/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { ArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectEncoder } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { IUnitTestRunnerPromise, Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";

class MockBaseObject extends BaseObject {
}

export class ObjectEncoderTest extends UnitTestRunner {

    @Test(true)
    public Serialize() : IUnitTestRunnerPromise {
        let unsetProperty : any;
        assert.equal(ObjectEncoder.Serialize(unsetProperty), "u");
        ObjectEncoder.Serialize(unsetProperty, ($data : string) : void => {
            assert.equal($data, "u");
        });

        assert.equal(ObjectEncoder.Serialize(null), "n");
        ObjectEncoder.Serialize(null, ($data : string) : void => {
            assert.equal($data, "n");
        });

        assert.equal(ObjectEncoder.Serialize(""), "e");
        ObjectEncoder.Serialize("", ($data : string) : void => {
            assert.equal($data, "e");
        });

        assert.equal(ObjectEncoder.Serialize(false), "b:0");
        assert.equal(ObjectEncoder.Serialize(true), "b:1");
        ObjectEncoder.Serialize(false, ($data : string) : void => {
            assert.equal($data, "b:0");
        });

        assert.equal(ObjectEncoder.Serialize(1), "i:1;");
        assert.equal(ObjectEncoder.Serialize(100), "i:100;");
        ObjectEncoder.Serialize(100, ($data : string) : void => {
            assert.equal($data, "i:100;");
        });

        assert.equal(ObjectEncoder.Serialize(1.10), "d:1.1;");
        assert.equal(ObjectEncoder.Serialize(-2.10), "d:-2.1;");
        ObjectEncoder.Serialize(-2.10, ($data : string) : void => {
            assert.equal($data, "d:-2.1;");
        });

        assert.equal(ObjectEncoder.Serialize("string"), "s:6:string");
        assert.equal(ObjectEncoder.Serialize("\nstr\ring"), "s:8:\nstr\ring");
        assert.equal(ObjectEncoder.Serialize("按钮的工具提示文本"), "s:9:按钮的工具提示文本");
        assert.equal(ObjectEncoder.Serialize("012345"), "s:6:012345");

        assert.equal(ObjectEncoder.Serialize([]),
            "a:0:");
        ObjectEncoder.Serialize([], ($data : string) : void => {
            assert.equal($data, "a:0:");
        });

        assert.equal(ObjectEncoder.Serialize([1, 2, 3, "test"]),
            "a:36:i:0;i:1;i:1;i:2;i:2;i:3;i:3;s:4:test");
        ObjectEncoder.Serialize([1, 2, 3, "test"], ($data : string) : void => {
            assert.equal($data, "a:36:i:0;i:1;i:1;i:2;i:2;i:3;i:3;s:4:test");
        });

        assert.equal(ObjectEncoder.Serialize(["test", true, ["value", 1.12, false, "test"], 150]),
            "a:81:i:0;s:4:testi:1;b:1i:2;a:43:i:0;s:5:valuei:1;d:1.12;i:2;b:0i:3;s:4:testi:3;i:150;");

        assert.equal(ObjectEncoder.Serialize({}),
            "o:0:");
        ObjectEncoder.Serialize({}, ($data : string) : void => {
            assert.equal($data, "o:0:");
        });

        assert.equal(ObjectEncoder.Serialize({name: "value", test: 125}),
            "o:31:s:4:names:5:values:4:testi:125;");
        ObjectEncoder.Serialize({name: "value", test: 125}, ($data : string) : void => {
            assert.equal($data, "o:31:s:4:names:5:values:4:testi:125;");
        });

        const result : string = "f:97:$arg) {var property = \"test\\r\\n2\";" +
            "var property2 = \"test2=\\\"test\\\"\";" +
            "var property3 = 1;return $arg;";

        const object : ($arg : string) => string = ($arg : string) : string => {
            const property : string = "test\r\n2";
            const property2 : string = "test2=\"test\"";
            const property3 : number = 1;
            return $arg;
        };

        const actual : string = ObjectEncoder.Serialize(object);
        const hasCoverage : boolean = actual.includes("cov_");
        if (!hasCoverage) {
            assert.equal(ObjectEncoder.Serialize(object), result);
        }
        ObjectEncoder.Serialize(object, ($data : string) : void => {
            if (!hasCoverage) {
                assert.equal($data, result);
            }
        });

        const result2 : string = "a:125:i:0;s:4:testi:1;f:97:$arg) {var property = \"test\\r\\n2\";" +
            "var property2 = \"test2=\\\"test\\\"\";var property3 = 1;return $arg;i:2;b:1";
        const actual2 : string = ObjectEncoder.Serialize([
            "test",
            ($arg : string) : string => {
                const property : string = "test\r\n2";
                const property2 : string = "test2=\"test\"";
                const property3 : number = 1;
                return $arg;
            },
            true
        ]);
        if (!hasCoverage) {
            assert.equal(actual2, result2);
        }

        const result3 : string = "o:157:s:9:objstrings:4:tests:11:objFunctionf:97:$arg) {var property = \"test\\r\\n2\";" +
            "var property2 = \"test2=\\\"test\\\"\";var property3 = 1;return $arg;s:10:objbooleanb:1";
        const actual3 : string = ObjectEncoder.Serialize({
            objstring : "test",
            objFunction($arg : string) : string {
                const property : string = "test\r\n2";
                const property2 : string = "test2=\"test\"";
                const property3 : number = 1;
                return $arg;
            },
            objboolean: true
        });
        if (!hasCoverage) {
            assert.equal(actual3, result3);
        }

        const arrayListProperty : ArrayList<string> = new ArrayList<string>();
        arrayListProperty.Add("test");
        arrayListProperty.Add("test2", 2);
        arrayListProperty.Add("test3", "key");
        assert.equal(ObjectEncoder.Serialize(arrayListProperty),
            "m:51:a:46:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayListc:0:109:o:103:" +
            "s:4:sizei:3;s:4:keysa:27:i:0;i:0;i:1;i:2;i:2;s:3:key" +
            "s:4:dataa:38:i:0;s:4:testi:1;s:5:test2i:2;s:5:test3");
        ObjectEncoder.Serialize(arrayListProperty, ($data : string) : void => {
            assert.equal($data,
                "m:51:a:46:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayListc:0:109:o:103:" +
                "s:4:sizei:3;s:4:keysa:27:i:0;i:0;i:1;i:2;i:2;s:3:key" +
                "s:4:dataa:38:i:0;s:4:testi:1;s:5:test2i:2;s:5:test3");
        });

        const arrayListProperty2 : ArrayList<any> = new ArrayList<any>();
        arrayListProperty2.Add("test");
        arrayListProperty2.Add(1.11, 2);
        arrayListProperty2.Add([1, 2, 3, 4], "array");
        arrayListProperty2.Add(true, "bool val key");

        assert.equal(ObjectEncoder.Serialize([1, 2, arrayListProperty2, 3]),
            "m:51:a:46:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayList" +
            "a:201:i:0;i:1;i:1;i:2;i:2;c:0:165:o:159:s:4:sizei:4;" +
            "s:4:keysa:50:i:0;i:0;i:1;i:2;i:2;s:5:arrayi:3;s:12:bool val key" +
            "s:4:dataa:71:i:0;s:4:testi:1;d:1.11;i:2;a:32:i:0;i:1;i:1;i:2;i:2;i:3;i:3;i:4;i:3;b:1i:3;i:3;");

        const arrayListProperty3 : ArrayList<any> = new ArrayList<any>();
        arrayListProperty3.Add("test");
        arrayListProperty3.Add(1, 2);
        arrayListProperty3.Add(true, "bool val key");
        arrayListProperty3.Add(new ArrayList<string>(), "array");
        arrayListProperty3.getItem("array").Add("value", "key");

        assert.equal(ObjectEncoder.Serialize(arrayListProperty3),
            "m:51:a:46:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayListc:0:200:o:194:s:4:" +
            "sizei:4;s:4:keysa:50:i:0;i:0;i:1;i:2;i:2;s:12:bool val keyi:3;s:5:array" +
            "s:4:dataa:105:i:0;s:4:testi:1;i:1;i:2;b:1i:3;c:0:67:o:62:s:4:" +
            "sizei:1;s:4:keysa:11:i:0;s:3:keys:4:dataa:13:i:0;s:5:value");

        assert.equal(ObjectEncoder.Serialize(1400483705187), "i:1400483705187;");
        assert.equal(ObjectEncoder.Serialize(14004837051871400483705187), "d:1.40048370518714e+25;");
        assert.equal(ObjectEncoder.Serialize([1400483705187]),
            "a:20:i:0;i:1400483705187;");
        assert.equal(ObjectEncoder.Serialize([1400483705187, 1400483705187, 1400483705187]),
            "a:60:i:0;i:1400483705187;i:1;i:1400483705187;i:2;i:1400483705187;");

        const arrayListProperty4 : ArrayList<any> = new ArrayList<any>();
        let undefinedProperty : string;
        arrayListProperty4.Add("test");
        arrayListProperty4.Add("");
        arrayListProperty4.Add(null);
        arrayListProperty4.Add(new MockBaseObject());
        arrayListProperty4.Add(new ArrayList());
        arrayListProperty4.Add(undefinedProperty);
        arrayListProperty4.Add("test");

        assert.equal(ObjectEncoder.Serialize(arrayListProperty4),
            "m:98:a:93:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayListi:1;" +
            "s:38:Io.Oidis.Commons.Primitives.BaseObjectc:0:206:o:200:s:4:" +
            "sizei:7;s:4:keysa:56:i:0;i:0;i:1;i:1;i:2;i:2;i:3;i:3;i:4;i:4;i:5;i:5;i:6;i:6;" +
            "s:4:dataa:105:i:0;s:4:testi:1;ei:2;ni:3;c:1:4:o:0:i:4;c:0:41:o:36:s:4:" +
            "sizei:0;s:4:keysa:0:s:4:dataa:0:i:5;ui:6;s:4:test");

        const arrayListProperty5 : ArrayList<any> = new ArrayList<any>();
        arrayListProperty5.Add("test");
        arrayListProperty5.Add(new MockBaseObject());

        assert.equal(ObjectEncoder.Serialize(arrayListProperty5),
            "m:98:a:93:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayListi:1;" +
            "s:38:Io.Oidis.Commons.Primitives.BaseObjectc:0:85:o:80:s:4:" +
            "sizei:2;s:4:keysa:16:i:0;i:0;i:1;i:1;s:4:dataa:26:i:0;s:4:testi:1;c:1:4:o:0:");

        return ($done : () => void) : void => {
            setTimeout(() : void => {
                this.initSendBox();
                $done();
            }, 250);
        };
    }

    @Test()
    public Url() : void {
        assert.equal(ObjectEncoder.Url("http://test http string"), "http%3A%2F%2Ftest%20http%20string");
    }

    @Test()
    public Utf8() : void {
        assert.equal(ObjectEncoder.Utf8(""), "");
        assert.equal(ObjectEncoder.Utf8("some string"), "some string");
        assert.equal(ObjectEncoder.Utf8("déšť"), "\x64\xC3\xA9\xC5\xA1\xC5\xA5");
        assert.equal(ObjectEncoder.Utf8("按钮的工具提示文本"),
            "\xE6\x8C\x89\xE9\x92\xAE\xE7\x9A\x84\xE5\xB7\xA5\xE5\x85\xB7\xE6\x8F\x90\xE7\xA4\xBA\xE6\x96\x87\xE6\x9C\xAC");
        assert.throws(() : void => {
            ObjectEncoder.Utf8(String.fromCharCode(0xD800));
        });
        assert.throws(() : void => {
            ObjectEncoder.Utf8(String.fromCharCode(0xDC00));
        });
        assert.doesNotThrow(() : void => {
            ObjectEncoder.Utf8(String.fromCharCode(0xDB00) + String.fromCharCode(0xDC00));
        });
    }

    @Test()
    public Base64() : void {
        const execute : any = () : void => {
            assert.equal(ObjectEncoder.Base64("test string"), "dGVzdCBzdHJpbmc=");
            assert.equal(ObjectEncoder.Base64("按钮的工具提示文本", false, true), "5oyJ6ZKu55qE5bel5YW35o+Q56S65paH5pys");
            assert.equal(ObjectEncoder.Base64("test string", true), "dGVzdCBzdHJpbmc.");
            assert.equal(ObjectEncoder.Base64("按钮的工具提示文本", true), "5oyJ6ZKu55qE5bel5YW35o-Q56S65paH5pys");
            assert.equal(ObjectEncoder.Base64(
                    "very very long test string repeated more times" +
                    "very very long test string repeated more times" +
                    "very very long test string repeated more times" +
                    "very very long test string repeated more times", true),
                "dmVyeSB2ZXJ5IGxvbmcgdGVzdCBzdHJpbmcgcmVwZWF0ZWQgbW9yZSB0aW1lc3ZlcnkgdmVyeSBsb25nIHRlc3Qgc3Rya" +
                "W5nIHJlcGVhdGVkIG1vcmUgdGltZXN2ZXJ5IHZlcnkgbG9uZyB0ZXN0IHN0cmluZyByZXBlYXRlZCBtb3JlIHRpbWVzdm" +
                "VyeSB2ZXJ5IGxvbmcgdGVzdCBzdHJpbmcgcmVwZWF0ZWQgbW9yZSB0aW1lcw..");
            assert.equal(ObjectEncoder.Base64(""), "");
        };
        const btoa : any = window.btoa;
        delete window.btoa;
        execute();
        (<any>ObjectEncoder).getBase64Encoder = this.globalCache.getBase64Encoder;
        window.btoa = btoa;
        execute();
    }
}
