/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { SyntaxConstants } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/SyntaxConstants.js";
import { ArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Convert } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/StringUtils.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";

class MockBaseObject extends BaseObject {
}

export class ConvertTest extends UnitTestRunner {

    @Test()
    public ToType() : void {
        assert.equal(Convert.ToType(["value1", "value2"]), "array");
        assert.equal(Convert.ToType("test"), "string");
        assert.equal(Convert.ToType(1), "number");
        assert.equal(Convert.ToType(true), "boolean");
        assert.equal(Convert.ToType({arg1: "value1", arg2: "value2"}), "object");
        assert.equal(Convert.ToType(LogIt), "Io.Oidis.Commons.Utils.LogIt");
        assert.equal(Convert.ToType(new MockBaseObject()), "Io.Oidis.Commons.Primitives.BaseObject");
    }

    @Test()
    public ArrayToString() : void {
        assert.equal(Convert.ArrayToString("", "", true), "Data object <b>EMPTY</b>",
            "convert empty object to string");
        assert.equal(Convert.ArrayToString("", "", false), "Data object EMPTY",
            "convert empty object to string without html tag");
        this.resetCounters();
        assert.equal(Convert.ArrayToString([""], "", true),
            "<i>Array object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;<b>EMPTY</b><br/>" +
            "</span>");
        assert.equal(Convert.ArrayToString([""], "", false), "Array object\r\n[ 0 ]    EMPTY\r\n");
        const list : ArrayList<any> = new ArrayList<any>();
        list.Add("value1");
        list.Add("value2", 2);
        list.Add("value3", "key3");
        list.Add(null);
        list.Add([10, 20, 30]);
        list.Add(new ArrayList<string>("test"));
        this.resetCounters();
        assert.equal(Convert.ArrayToString(list),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_2\').style.display=" +
            "document.getElementById(\'ContentBlock_2\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_2\" style=\"border: 0 solid black; display: none;\">" +
            "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;value1<br/>" +
            "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;value2<br/>" +
            "[ \"key3\" ]&nbsp;&nbsp;&nbsp;&nbsp;value3<br/>" +
            "[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;NULL<br/>" +
            "[ 4 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Array object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 0 ]" +
            "&nbsp;&nbsp;&nbsp;&nbsp;<i>Object type:</i> number. <i>Return value:</i> 10<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 1 ]" +
            "&nbsp;&nbsp;&nbsp;&nbsp;<i>Object type:</i> number. <i>Return value:</i> 20<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 2 ]" +
            "&nbsp;&nbsp;&nbsp;&nbsp;<i>Object type:</i> number. <i>Return value:</i> 30<br/>" +
            "</span>" +
            "[ 5 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_1\').style.display=" +
            "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">" +
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;test<br/>" +
            "</span></span>",
            "convert ArrayList to string"
        );

        const list2 : ArrayList<string> = new ArrayList<string>();
        list2.Add("value1");
        list2.Add("value2", 2);
        list2.Add("value3", "key3");
        this.resetCounters();
        assert.equal(Convert.ArrayToString(list2, "___"),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "___[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;value1<br/>" +
            "___[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;value2<br/>" +
            "___[ \"key3\" ]&nbsp;&nbsp;&nbsp;&nbsp;value3<br/>" +
            "</span>",
            "convert ArrayList to string with prefix"
        );

        const list3 : ArrayList<string> = new ArrayList<string>();
        list3.Add("value1");
        list3.Add("value2", 2);
        list3.Add("value3", "key3");
        this.resetCounters();
        assert.equal(Convert.ArrayToString(list3, "", false),
            "Io.Oidis.Commons.Primitives.ArrayList object\r\n" +
            "[ 0 ]    value1\r\n" +
            "[ 2 ]    value2\r\n" +
            "[ \"key3\" ]    value3\r\n",
            "convert ArrayList to string without html tag"
        );

        const array : number[] = [10, 20, 30];
        this.resetCounters();
        assert.deepEqual(Convert.ArrayToString(array),
            "<i>Array object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Object type:</i> number. <i>Return value:</i> 10<br/>" +
            "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Object type:</i> number. <i>Return value:</i> 20<br/>" +
            "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Object type:</i> number. <i>Return value:</i> 30<br/>" +
            "</span>",
            "convert array to string");

        this.resetCounters();
        assert.deepEqual(Convert.ArrayToString({
                key1: "test1",
                key2: "test2",
                12  : "test3"
            }, "", false),
            "[ \"12\" ]    test3\r\n" +
            "[ \"key1\" ]    test1\r\n" +
            "[ \"key2\" ]    test2\r\n",
            "convert object to string");

        const nativearray : string[] = ["test0", "test1", "test2"];
        this.resetCounters();
        assert.deepEqual(Convert.ArrayToString(nativearray, "", false),
            "Array object\r\n[ 0 ]    test0\r\n[ 1 ]    test1\r\n[ 2 ]    test2\r\n");
    }

    @Test()
    public StringToInteger() : void {
        assert.equal(Convert.StringToInteger("115"), 115);
    }

    @Test()
    public StringToBoolean() : void {
        assert.equal(Convert.StringToBoolean("false"), false);
    }

    @Test()
    public BooleanToString() : void {
        assert.equal(Convert.BooleanToString(true), "true");
        assert.equal(Convert.BooleanToString(false), "false");
        assert.equal(Convert.BooleanToString(null), "");
    }

    @Test()
    public FunctionToString() : void {
        assert.equal(Convert.FunctionToString(1235), "");
        const input : () => void = () : void => {
            // mock implementation for test function
        };
        (<any>input).toSource = () : string => {
            return "(function () {var test = \"\";})();";
        };
        assert.equal(this.stripInstrumentation(Convert.FunctionToString(input)), "function () {var test = \"\";})()");

        assert.equal(this.stripInstrumentation(Convert.FunctionToString(() : void => {
            const test : string = "";
        })), "function () {const test = \"\";}");

        assert.patternEqual(this.stripInstrumentation(Convert.FunctionToString(() : void => {
                const test : string = "";
            }, true)),
            "function () {*\n" +
            "const test = \"\";*\n" +
            "}");
    }

    @Test(true)
    public ObjectToString() : void {
        let property : any;
        assert.equal(Convert.ObjectToString(property), "NOT DEFINED");
        assert.equal(Convert.ObjectToString("", "", false), "EMPTY");
        assert.equal(Convert.ObjectToString(LogIt.ClassName()), "Io.Oidis.Commons.Utils.LogIt");
        assert.equal(Convert.ObjectToString(0, "", false), "Object type: number. Return value: 0");
        assert.equal(Convert.ObjectToString(false), "<i>Object type:</i> boolean. <i>Return value:</i> false");
        assert.equal(Convert.ObjectToString({test: "value", object: "for object"}, "", false),
            "{\"test\":\"value\",\"object\":\"for object\"}");
        assert.equal(Convert.ObjectToString({test: "value", object: "for object"}, "", true),
            "{<br/>&nbsp;&nbsp;&nbsp;\"test\": \"value\",<br/>&nbsp;&nbsp;&nbsp;\"object\": \"for object\"<br/>}");
        assert.equal(Convert.ObjectToString(["one", "two", "three"], "", true),
            "<i>Object type:</i> array. <i>Return values:</i><br/>" +
            "<i>Array object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;one<br/>" +
            "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;two<br/>" +
            "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;three<br/>" +
            "</span>");
        assert.equal(Convert.ObjectToString(["test0", "test1", "test2"], "", false),
            "Object type: array. Return values: \r\nArray object\r\n[ 0 ]    test0\r\n[ 1 ]    test1\r\n[ 2 ]    test2\r\n");

        const testFunction : (arg : string) => void = (arg : string) : void => {
            const test : string = "this is function";
        };
        assert.equal(this.stripInstrumentation(Convert.ObjectToString(testFunction)),
            "<i>Object type:</i> function. <i>Source code:</i> function (arg) {const test = \"this is function\";}");
        assert.equal(this.stripInstrumentation(Convert.ObjectToString(testFunction, "", false)),
            "Object type: function. Source code: function (arg) {const test = \"this is function\";}");

        assert.equal(StringUtils.Remove(this.stripInstrumentation(Convert.ObjectToString(Convert, "", false)),
                "/* istanbul ignore next */"),
            "Object type: Io.Oidis.Commons.Utils.Convert. Source code: " +
            "function Convert() {return _super !== null && _super.apply(this, arguments) || " +
            "this;}");
        assert.equal(Convert.ObjectToString(new MockBaseObject()), "object type of 'Io.Oidis.Commons.Primitives.BaseObject'");

        const object : BaseObject = new MockBaseObject();
        object[SyntaxConstants.TO_STRING] = "haha";
        assert.equal(Convert.ObjectToString(object, "", false),
            "Object \"" + object.getClassName() + "\" does not implement method ToString.");

        assert.equal(Convert.ObjectToString({key1: "test1", key2: "test2", 12: "test3"}, "", false),
            "{\"12\":\"test3\",\"key1\":\"test1\",\"key2\":\"test2\"}");
    }

    @Test()
    public TimeToGMTformat() : void {
        const timestamp : number = 1472673592195;
        assert.equal(Convert.TimeToGMTformat(new Date(timestamp)), "Wed, 31 Aug 2016 19:59:52 GMT");
        assert.equal(Convert.TimeToGMTformat(timestamp), "Wed, 31 Aug 2016 19:59:52 GMT");
    }

    @Test()
    public TimeToSeconds() : void {
        assert.equal(Convert.TimeToSeconds(50000), 50);
        assert.equal(Convert.TimeToSeconds(-1), 0);
    }

    @Test()
    public HexadecimalToString() : void {
        assert.equal(Convert.HexadecimalToString(0xff), "000000ff");
        assert.equal(Convert.HexadecimalToString(null), "");
    }

    @Test()
    public StringToHexadecimal() : void {
        assert.equal(Convert.StringToHexadecimal("000000ff"), 3472328296227694000);
        assert.equal(Convert.StringToHexadecimal(""), null);
    }

    @Test()
    public UnicodeToString() : void {
        assert.equal(Convert.UnicodeToString(115, 115), "ss");
    }

    @Test()
    public ToFixed() : void {
        assert.equal(Convert.ToFixed(1.15, 5), 1.15000);
        assert.notEqual(Convert.ToFixed(1.15, 5), 1.15001);
        assert.equal(Convert.ToFixed(2.35, 0), 2);
        assert.equal(Convert.ToFixed(2.55, 0), 3);
        assert.equal(Convert.ToFixed(0, 0), 0);
        assert.equal(Convert.ToFixed(1585054254448, 0), 1585054254448);
        assert.equal(Convert.ToFixed(-1585054254448, 0), -1585054254448);
        assert.equal(Convert.ToFixed(1585054254448.25, 0), 1585054254448);
        assert.equal(Convert.ToFixed(1585054254448.25369, 3), 1585054254448.254);
        assert.equal(Convert.ToFixed(-1585054254448.25369, 3), -1585054254448.254);
        assert.equal(Convert.ToFixed(347232823472328296227694000, 0), 347232823472328296227694000);
    }

    @Test()
    public DegToRad() : void {
        assert.equal(Convert.DegToRad(60), 1.0471975511965976);
    }

    @Test()
    public RadToDeg() : void {
        assert.equal(Convert.RadToDeg(2), 114.59155902616465);
    }

    @Test()
    public IntegerToSize() : void {
        assert.equal(Convert.IntegerToSize(128), "128 B");
        assert.equal(Convert.IntegerToSize(-1), "0 B");
        assert.equal(Convert.IntegerToSize(1048575), "1024 kB");
        assert.equal(Convert.IntegerToSize(10737418), "10.2 MB");
        assert.equal(Convert.IntegerToSize(1073741820), "1024 MB");
        assert.equal(Convert.IntegerToSize(10995116277), "10.24 GB");
        assert.equal(Convert.IntegerToSize(1099511627776), "1 TB");
        assert.equal(Convert.IntegerToSize(1125899906842624), "1125899906842624 B");

        assert.equal(Convert.IntegerToSize(128, 0), "128 B");
        assert.equal(Convert.IntegerToSize(-1, 0), "0 B");
        assert.equal(Convert.IntegerToSize(1048575, 0), "1024 kB");
        assert.equal(Convert.IntegerToSize(10737418, 1), "10.2 MB");
        assert.equal(Convert.IntegerToSize(1073741820, 0), "1024 MB");
        assert.equal(Convert.IntegerToSize(10995116277, 2), "10.24 GB");
        assert.equal(Convert.IntegerToSize(1099511627776, 0), "1 TB");
        assert.equal(Convert.IntegerToSize(1125899906842624, 0), "1125899906842624 B");
    }

    @Test()
    public ToHtmlContentBlock() : void {
        assert.equal(Convert.ToHtmlContentBlock("header", "test"),
            "header" +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">test</span>");
        this.resetCounters();
        assert.equal(Convert.ToHtmlContentBlock("", "test", "open"),
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">open</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">test</span>");
    }
}
