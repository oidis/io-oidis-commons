/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

import { AsyncHttpResolver } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/Resolvers/AsyncHttpResolver.js";
import { RuntimeTestRunner } from "../../../../../../../source/typescript/Io/Oidis/Commons/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { IArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/IArrayList.js";
import { IBaseObject } from "../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { Interface } from "../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/Interface.js";
import { IReflection } from "../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/IReflection.js";
import { ArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/StringUtils.js";
import { Test } from "../BaseUnitTestRunner.js";
import { EventsManagerTest } from "../Events/EventsManagerTest.js";

class MockBaseObject extends BaseObject {
}

export class ReflectionTest extends UnitTestRunner {

    constructor() {
        super();
        // this.setMethodFilter("testClassHasInterface");
    }

    @Test()
    public setInstanceNamespaces() : void {
        Reflection.setInstanceNamespaces();
        Reflection.setInstanceNamespaces("Io.Oidis", "Xyz.Company", "Abc", "Declared");
        assert.deepEqual((<any>Reflection).singletonNamespaces, ["Io.Oidis", "Xyz.Company", "Abc", "Declared"]);
    }

    @Test()
    public getInstance() : void {
        assert.equal(ObjectValidator.IsEmptyOrNull(Reflection.getInstance()), false);
        assert.equal(Reflection.getInstance().getClassName(new MockBaseObject()),
            "Io.Oidis.Commons.Primitives.BaseObject");
    }

    @Test()
    public Constructor() : void {
        (<any>window).Abc = (() : any => {
            function Abc() : void {
                // namaspace with empty body
            }

            (<any>Abc).Test = () : void => {
                // namaspace with empty body
            };

            return Abc;
        })();

        (<any>window).Xyz = (() : any => {
            function Xyz() : void {
                // namaspace with empty body
            }

            return Xyz;
        })();
        (<any>window).Xyz.Company = (() : any => {
            function Company() : void {
                // class with empty body
            }

            return Company;
        })();

        const reflection : Reflection = new Reflection();
        assert.ok(!reflection.Exists("Xyz.Company"));
        assert.ok(!reflection.Exists("Abc.Test"));
        const thisClass : AsyncHttpResolver = Reflection.getInstance().getClass(AsyncHttpResolver.ClassName());
        const getClass : string = Reflection.getInstance().getClassName();

        assert.equal(Reflection.getInstance().getNamespaceName(), "Io.Oidis.Commons.Utils");
        const thisnewClass : AsyncHttpResolver = Reflection.getInstance().getClass(AsyncHttpResolver.NamespaceName());
        const getnameSpace : string = Reflection.getInstance().getNamespaceName();
        assert.equal(getnameSpace, "Io.Oidis.Commons.Utils");
        const list : ArrayList<any> = ArrayList.getInstance().getNamespaceName();
        assert.equal(list, "Io.Oidis.Commons.Primitives");

        const thisNamespace : AsyncHttpResolver = Reflection.getInstance().getClass(AsyncHttpResolver.ClassNameWithoutNamespace());
        const withoutNamespace : string = Reflection.getInstance().getClassNameWithoutNamespace();
        assert.equal(withoutNamespace, "Reflection");
        const array : ArrayList<any> = ArrayList.getInstance().getClassNameWithoutNamespace();
        assert.equal(array, "ArrayList");
    }

    @Test()
    public ClassName() : void {
        assert.equal(Reflection.ClassName(), "Io.Oidis.Commons.Utils.Reflection");
    }

    @Test()
    public ClassNameWithoutNamespace() : void {
        assert.equal(Reflection.ClassNameWithoutNamespace(), "Reflection");
    }

    @Test()
    public NamespaceName() : void {
        assert.equal(Reflection.NamespaceName(), "Io.Oidis.Commons.Utils");
    }

    @Test()
    public UID() : void {
        assert.notEqual(Reflection.UID(), Reflection.UID());
        assert.notEqual(Reflection.UID(BaseObject.ClassName()), Reflection.UID(BaseObject.ClassName()));
    }

    @Test()
    public getAllClasses() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.ok(!ObjectValidator.IsEmptyOrNull(reflect.getAllClasses()));
    }

    @Test()
    public Exists() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.equal(reflect.Exists("Io.Oidis.Commons.Primitives.BaseObject"), true);
        assert.equal(reflect.Exists(BaseObject.ClassName()), true);
        assert.equal(reflect.Exists(null), false);
        assert.equal(reflect.Exists("Io.Oidis.Commons.NotExisting"), false);
    }

    @Test()
    public getClass() : void {
        const reflect : Reflection = Reflection.getInstance();
        const className : any = reflect.getClass("Io.Oidis.Commons.Primitives.BaseObject");
        const object : BaseObject = new className();
        assert.equal(className.ClassName(), "Io.Oidis.Commons.Primitives.BaseObject");
        assert.equal(object instanceof BaseObject, true);
    }

    @Test()
    public getClassName1() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.equal(reflect.getClassName(), "Io.Oidis.Commons.Utils.Reflection");
        assert.equal(reflect.getClassName(new MockBaseObject()), "Io.Oidis.Commons.Primitives.BaseObject");
    }

    @Test()
    public getNamespaceName1() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.equal(reflect.getNamespaceName(), "Io.Oidis.Commons.Utils");
        assert.equal(reflect.getNamespaceName(new MockBaseObject()), "Io.Oidis.Commons.Primitives");
    }

    @Test()
    public getClassNameWithoutNamespace1() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.equal(reflect.getClassNameWithoutNamespace(), "Reflection");
        assert.equal(reflect.getClassNameWithoutNamespace(new MockBaseObject()), "BaseObject");
    }

    @Test()
    public getUID1() : void {
        const reflect1 : Reflection = Reflection.getInstance();
        const reflect2 : Reflection = Reflection.getInstance();
        assert.notEqual(reflect1.getUID(), reflect2.getUID());
    }

    @Test()
    public IsInstanceOf() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.equal(reflect.IsInstanceOf(new MockBaseObject(), "Io.Oidis.Commons.Primitives.BaseObject"), true);
        assert.equal(reflect.IsInstanceOf(new MockBaseObject(), BaseObject.ClassName()), true);
        assert.equal(reflect.IsInstanceOf(new MockBaseObject(), ArrayList.ClassName()), false);
        assert.equal(reflect.IsInstanceOf(new ArrayList(), BaseObject), false);
        assert.equal(reflect.IsInstanceOf(new ArrayList(), "Io.Oidis.Commons.Primitives.ArrayList"), true);
        assert.equal(reflect.IsInstanceOf(<any>{}, BaseObject), false);
        const array : ArrayList<any> = new ArrayList<any>();
        assert.equal(reflect.IsInstanceOf(array, array.getProperties().toString()), false);
        assert.equal(reflect.IsInstanceOf(MockBaseObject.getInstance(), "Io.Oidis.Commons.Primitives.BaseObject"), true);
        const thisClass : any = () : void => {
            const object : ArrayList<string> = new ArrayList<string>();
            object.Add("test0");
            object.Add("test1");
        };
        assert.equal(reflect.IsInstanceOf(new ArrayList<string>(), thisClass), false);
    }

    @Test()
    public IsMemberOf1() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.equal(reflect.IsMemberOf(<any>BaseObject), true);
        assert.equal(reflect.IsMemberOf(<any>Reflection), true);
        assert.equal(reflect.IsMemberOf(<any>ArrayList), false);
        assert.equal(reflect.IsMemberOf(new MockBaseObject(), BaseObject.ClassName()), true);
        assert.equal(reflect.IsMemberOf(new ArrayList<string>(), BaseObject.ClassName()), true);
        assert.equal(reflect.IsMemberOf(new MockBaseObject(), ArrayList.ClassName()), false);
        assert.equal(reflect.IsMemberOf(null, ""), false);
        assert.equal(reflect.IsMemberOf(new MockBaseObject(),
            "Io.Oidis.Commons.Primitives.BaseObject"), true);
        assert.equal(reflect.IsMemberOf(new ArrayList<string>(),
            "Io.Oidis.Commons.Primitives.ArrayList"), true);
        assert.equal(reflect.IsMemberOf(<any>ArrayList.ClassName()), false);
        const object : ArrayList<any> = new ArrayList<any>();
        assert.equal(reflect.IsMemberOf(object, ArrayList), true);
        assert.equal(reflect.IsMemberOf(<any>true), false);
        assert.equal(reflect.IsMemberOf(undefined, BaseObject.ClassName()), false);
    }

    @Test(true)
    public testClassHasInterface() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.equal(reflect.ClassHasInterface(BaseObject, IBaseObject), true);
        assert.equal(reflect.ClassHasInterface(BaseObject.ClassName(), IBaseObject), true);
        assert.equal(reflect.ClassHasInterface(ArrayList.ClassName(), IArrayList), true);
        assert.equal(reflect.ClassHasInterface(BaseObject, IArrayList), false);
        assert.equal(reflect.ClassHasInterface(BaseObject.ClassName(), IArrayList), false);
        assert.equal(reflect.ClassHasInterface(EventsManagerTest.ClassName(), RuntimeTestRunner.ClassName()), true);
        assert.equal(reflect.ClassHasInterface(BaseObject, ""), false);
        assert.equal(reflect.ClassHasInterface(reflect.getClassName(), IReflection), true);
        assert.equal(reflect.ClassHasInterface("Io.Oidis.Commons.Primitives.ArrayList",
            "Io.Oidis.Commons.Interfaces.IArrayList"), true);

        const mockClass : any = (() : any => {
            const property : string = "";

            function mockClass() : void {
                // namaspace with empty body
            }

            mockClass.prototype[property] = false;

            return mockClass;
        })();
        assert.equal(reflect.ClassHasInterface(BaseObject, Interface.getInstance([])), false);
        assert.equal(reflect.ClassHasInterface(mockClass, IBaseObject), false);
    }

    @Test()
    public Implements1() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.equal(reflect.Implements(IReflection), true);
        assert.equal(reflect.Implements(IBaseObject), true);
        assert.equal(reflect.Implements(IArrayList), false);
        assert.equal(reflect.Implements(new MockBaseObject(), IBaseObject), true);
        assert.equal(reflect.Implements(new ArrayList<string>(), IArrayList), true);
        assert.equal(reflect.Implements(new MockBaseObject(), IArrayList), false);
        assert.equal(reflect.Implements("Io.Oidis.Commons.Interfaces.IArrayList"), false);
        assert.equal(reflect.Implements(BaseObject.ClassName(), IBaseObject), false);
        assert.equal(reflect.Implements(ArrayList.ClassName(), IArrayList), false);
        assert.equal(reflect.Implements(new MockBaseObject(), null), false);
        assert.equal(reflect.Implements("", null), false);
        assert.equal(reflect.Implements(ArrayList.ClassName(),
            "Io.Oidis.Commons.Interfaces.IArrayList"), false);
        assert.equal(reflect.Implements("Io.Oidis.Commons.Primitives.ArrayList",
            "Io.Oidis.Commons.Interfaces.IArrayList"), false);
        assert.equal(reflect.Implements(<any>BaseObject, IBaseObject), false);
        assert.equal(reflect.Implements(<any>true), false);
    }

    @Test()
    public getInstanceOf() : void {
        assert.deepEqual(Reflection.getInstanceOf(BaseObject.ClassName(), {test: "value"}), null);
        const instance : BaseObject = Reflection.getInstanceOf("Io.Oidis.Commons.Primitives.BaseObject");
        assert.ok(instance.IsTypeOf(BaseObject));
        const object : any = "getClassName";
        assert.equal(Reflection.getInstanceOf("getClassName", object),
            null);

        const list : any = {size: 0, keys: [], data: []};
        Reflection.getInstanceOf(ArrayList.ClassName(), {size: 0, keys: [], data: []});
        Reflection.getInstanceOf("Io.Oidis.Commons.Primitives.ArrayList", {size: 0, keys: [], data: []});

        const dataObject : any = {
            getClassName() {
                return ArrayList.ClassName();
            }, size: 100
        };
        Reflection.getInstanceOf(ArrayList.ClassName(), dataObject);
        assert.equal(ArrayList.ClassName(), "Io.Oidis.Commons.Primitives.ArrayList");
    }

    @Test()
    public getHash1() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.equal(reflect.getHash(), StringUtils.getCrc(JSON.stringify(reflect.getAllClasses())));
    }

    @Test()
    public IsTypeOf1() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.equal(reflect.IsTypeOf(BaseObject), false);
        assert.equal(reflect.IsTypeOf(String), false);
        assert.equal(reflect.IsTypeOf(Reflection), true);
    }

    @Test()
    public SerializationData1() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.deepEqual(reflect.SerializationData(), {classesList: reflect.getAllClasses()});
    }

    @Test()
    public getProperties1() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.deepEqual(reflect.getProperties(), []);
    }

    @Test(true)
    public getMethods1() : void {
        const reflect : Reflection = Reflection.getInstance();
        assert.deepEqual(reflect.getMethods(), [
            "getUID",
            "getClassName",
            "getNamespaceName",
            "getClassNameWithoutNamespace",
            "getAllClasses",
            "Exists",
            "getClass",
            "IsInstanceOf",
            "IsMemberOf",
            "ClassHasInterface",
            "Implements",
            "getProperties",
            "getMethods",
            "getHash",
            "IsTypeOf",
            "SerializationData",
            "ToString",
            "toString",
            "setNamespace",
            "getInterfaceProperties"
        ]);
    }

    @Test(true)
    public ToString1() : void {
        const reflect : Reflection = Reflection.getInstance();
        this.resetCounters();
        assert.patternEqual(reflect.ToString(),
            "<i>Array object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;Io.Oidis.Commons.EnvironmentArgs<br/>" +
            "[ * ]&nbsp;&nbsp;&nbsp;&nbsp;Io.Oidis.Commons.IEnvironmentConfig<br/>" +
            "[ * ]&nbsp;&nbsp;&nbsp;&nbsp;Io.Oidis.Commons.Index<br/>" +
            "[ * ]&nbsp;&nbsp;&nbsp;&nbsp;Io.Oidis.Commons.Loader<br/>" +
            "*<br/>" +
            "</span>",
            "convert reflection to string");

        this.resetCounters();
        assert.patternEqual(reflect.ToString("___"),
            "<i>Array object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
            "___[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;Io.Oidis.Commons.EnvironmentArgs<br/>" +
            "___[ * ]&nbsp;&nbsp;&nbsp;&nbsp;Io.Oidis.Commons.IEnvironmentConfig<br/>" +
            "___[ * ]&nbsp;&nbsp;&nbsp;&nbsp;Io.Oidis.Commons.Index<br/>" +
            "___[ * ]&nbsp;&nbsp;&nbsp;&nbsp;Io.Oidis.Commons.Loader<br/>" +
            "*<br/>" +
            "</span>",
            "convert reflection to string with prefix");

        this.resetCounters();
        assert.patternEqual(reflect.ToString("", false),
            "Array object\r\n" +
            "[ 0 ]    Io.Oidis.Commons.EnvironmentArgs\r\n" +
            "[ * ]    Io.Oidis.Commons.IEnvironmentConfig\r\n" +
            "[ * ]    Io.Oidis.Commons.Index\r\n" +
            "[ * ]    Io.Oidis.Commons.Loader\r\n" +
            "*\r\n",
            "convert reflection to plain text string");
    }

    @Test()
    public toString1() : void {
        const reflect : Reflection = Reflection.getInstance();
        this.resetCounters();
        const value : string = reflect.ToString();
        this.resetCounters();
        assert.equal(reflect.toString(), value);
    }
}
