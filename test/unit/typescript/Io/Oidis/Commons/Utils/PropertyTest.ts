/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { NewLineType } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/NewLineType.js";
import { PersistenceType } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/PersistenceType.js";
import { Convert } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Convert.js";
import { ObjectValidator } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Property.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

class MockPropertyClass {
    private stringProperty : string;
    private nullStringProperty : string;
    private eolStringProperty : string;
    private intProperty : number;
    private noLimitIntProperty : number;
    private boolProperty : boolean;
    private timeProperty : number;
    private sizeProperty : number;

    public StringProperty($value? : string) : string {
        return this.stringProperty = Property.String(this.stringProperty, $value);
    }

    public NullStringProperty($value? : string) : string {
        return this.nullStringProperty = Property.NullString(this.nullStringProperty, $value);
    }

    public EOLStringProperty($value? : string | boolean) : string {
        this.eolStringProperty = Property.EOLString(this.eolStringProperty, ObjectValidator.IsSet($value) ? $value : true);
        return Property.EOLString(this.eolStringProperty, ObjectValidator.IsBoolean($value) && $value);
    }

    public IntProperty($value? : number | string) : number {
        this.intProperty = Property.PositiveInteger(this.intProperty, $value, 10, 30);
        return this.intProperty;
    }

    public NoLimitIntProperty($value? : number) : number {
        return this.noLimitIntProperty = Property.Integer(this.noLimitIntProperty, $value);
    }

    public BoolProperty($value? : string | number | boolean) : boolean {
        return this.boolProperty = Property.Boolean(this.boolProperty, $value);
    }

    public TimeProperty($value? : number | string) : number {
        return this.timeProperty = Property.Time(this.timeProperty, $value);
    }

    public SizeProperty($value? : number | string) : number {
        return this.sizeProperty = Property.Size(this.sizeProperty, $value);
    }
}

export class PropertyTest extends UnitTestRunner {

    @Test()
    public String() : void {
        const object : MockPropertyClass = new MockPropertyClass();
        assert.equal(object.StringProperty(), "");
        object.StringProperty("set this value");
        assert.equal(object.StringProperty(), "set this value");
        object.StringProperty(null);
        assert.equal(object.StringProperty(), "set this value");
    }

    @Test()
    public NullString() : void {
        const object : MockPropertyClass = new MockPropertyClass();
        assert.equal(object.NullStringProperty(), null);
        object.NullStringProperty("set this value");
        assert.equal(object.NullStringProperty(), "set this value");
        object.NullStringProperty(null);
        assert.equal(object.NullStringProperty(), null);
    }

    @Test()
    public EOLString() : void {
        const object : MockPropertyClass = new MockPropertyClass();
        assert.equal(object.EOLStringProperty(), "");
        object.EOLStringProperty("set" + NewLineType.WINDOWS + "this" + NewLineType.WINDOWS + "value");
        assert.equal(object.EOLStringProperty(), "set" + NewLineType.WINDOWS + "this" + NewLineType.WINDOWS + "value");
        object.EOLStringProperty(null);
        assert.equal(object.EOLStringProperty(), "set" + NewLineType.WINDOWS + "this" + NewLineType.WINDOWS + "value");
        assert.equal(object.EOLStringProperty(true), "set" + NewLineType.DATABASE + "this" + NewLineType.DATABASE + "value");
    }

    @Test()
    public Integer() : void {
        const object : MockPropertyClass = new MockPropertyClass();
        assert.equal(object.IntProperty(), null);
        object.IntProperty(15);
        assert.equal(object.IntProperty(), 15);
        object.IntProperty(-10);
        assert.equal(object.IntProperty(), 15);
        object.IntProperty(5);
        assert.equal(object.IntProperty(), 15);
        object.IntProperty(35);
        assert.equal(object.IntProperty(), 15);
        object.IntProperty(10);
        assert.equal(object.IntProperty(), 10);
        object.IntProperty(30);
        assert.equal(object.IntProperty(), 30);
        object.IntProperty(null);
        assert.equal(object.IntProperty(), 30);
        object.IntProperty(12.7);
        assert.equal(object.IntProperty(), 12);
        object.IntProperty("15.35");
        assert.equal(object.IntProperty(), 15);
        object.NoLimitIntProperty(-100);
        assert.equal(object.NoLimitIntProperty(), -100);
        object.NoLimitIntProperty(100);
        assert.equal(object.NoLimitIntProperty(), 100);
    }

    @Test()
    public Boolean() : void {
        const object : MockPropertyClass = new MockPropertyClass();
        assert.equal(object.BoolProperty(), null);
        object.BoolProperty(1);
        assert.equal(object.BoolProperty(), true);
        object.BoolProperty(0);
        assert.equal(object.BoolProperty(), false);
        object.BoolProperty("true");
        assert.equal(object.BoolProperty(), true);
        object.BoolProperty("false");
        assert.equal(object.BoolProperty(), false);
        object.BoolProperty("1");
        assert.equal(object.BoolProperty(), true);
        object.BoolProperty("0");
        assert.equal(object.BoolProperty(), false);
        object.BoolProperty("on");
        assert.equal(object.BoolProperty(), true);
        object.BoolProperty("off");
        assert.equal(object.BoolProperty(), false);
        object.BoolProperty(true);
        assert.equal(object.BoolProperty(), true);
        object.BoolProperty(false);
        assert.equal(object.BoolProperty(), false);
        object.BoolProperty(1.15);
        assert.equal(object.BoolProperty(), true);
    }

    @Test(true)
    public Time() : void {
        const object : MockPropertyClass = new MockPropertyClass();
        assert.equal(object.TimeProperty(), null);
        object.TimeProperty(<any>true);
        assert.equal(object.TimeProperty(), null);
        object.TimeProperty(123456);
        assert.equal(object.TimeProperty(), 123456);
        object.TimeProperty("October 13, 1975 11:13:00");
        assert.equal(object.TimeProperty(), 182427180000);
        object.TimeProperty("+60 sec");
        assert.equal(Convert.ToFixed(object.TimeProperty() / 1000, 0), Convert.ToFixed(new Date().getTime() / 1000, 0) + 60);
        object.TimeProperty("-60 s");
        assert.equal(Convert.ToFixed(object.TimeProperty() / 1000, 0), Convert.ToFixed(new Date().getTime() / 1000, 0) - 60);
        object.TimeProperty("+1 year");
        assert.equal(Convert.ToFixed(object.TimeProperty() / 1000, 0), Convert.ToFixed(new Date().getTime() / 1000, 0) + 31536000);
        object.TimeProperty("+2 days");
        assert.equal(Convert.ToFixed(object.TimeProperty() / 1000, 0), Convert.ToFixed(new Date().getTime() / 1000, 0) + 172800);
        object.TimeProperty("November 16");
        assert.equal(object.TimeProperty(), 1005865200000);
        object.TimeProperty("2004-07-24 09:45:52.189");
        assert.equal(object.TimeProperty(), 1090655152189);
        object.TimeProperty("Tuesday, February 16");
        assert.equal(object.TimeProperty(), 982278000000);
        object.TimeProperty("60 s");
        assert.equal(object.TimeProperty(), 60000);
        object.TimeProperty("60 h");
        assert.equal(object.TimeProperty(), 216000000);
        object.TimeProperty("6 w");
        assert.equal(object.TimeProperty(), 3628800000);
        object.TimeProperty("10 min");
        assert.equal(object.TimeProperty(), 600000);
        object.TimeProperty("2 d");
        assert.equal(object.TimeProperty(), 172800000);
        object.TimeProperty("2015-07-24 09:45:52.189");
        assert.equal(object.TimeProperty(), 1437723952189);
        object.TimeProperty("3 m");
        assert.equal(object.TimeProperty(), 8035200000);

        assert.throws(() : void => {
            const object : MockPropertyClass = new MockPropertyClass();
            object.TimeProperty("error");
        }, /"error" is not supported format for Time property\./);
    }

    @Test()
    public Size() : void {
        const object : MockPropertyClass = new MockPropertyClass();
        assert.equal(object.SizeProperty(), 0);
        object.SizeProperty("3 M");
        assert.equal(object.SizeProperty(), 3145728);
        object.SizeProperty("4 k");
        assert.equal(object.SizeProperty(), 4096);
        object.SizeProperty("5 M");
        assert.equal(object.SizeProperty(), 5242880);
        object.SizeProperty("2 G");
        assert.equal(object.SizeProperty(), 2147483648);
        object.SizeProperty("1 T");
        assert.equal(object.SizeProperty(), 1099511627776);
        object.SizeProperty("4 b");
        assert.equal(object.SizeProperty(), 4);
        object.SizeProperty(45);
        assert.equal(object.SizeProperty(), 45);
        object.SizeProperty("6 kB");
        assert.equal(object.SizeProperty(), 6144);
        object.SizeProperty("9");
        assert.equal(object.SizeProperty(), 9);
        object.SizeProperty(-45);
        assert.equal(object.SizeProperty(), 0);
        object.SizeProperty("-1");
        assert.equal(object.SizeProperty(), 0);
    }

    @Test()
    public EnumType() : void {
        const enum1 : PersistenceType = PersistenceType.BROWSER;
        assert.equal(Property.EnumType(enum1, PersistenceType.BROWSER), PersistenceType.BROWSER);

        const enum2 : PersistenceType = PersistenceType.BROWSER;
        assert.equal(Property.EnumType(enum2, "test"), "test");

        const enum3 : PersistenceType = PersistenceType.BROWSER;
        assert.equal(Property.EnumType(enum3, "test", PersistenceType), PersistenceType.BROWSER);

        const enum4 : PersistenceType = PersistenceType.BROWSER;
        assert.equal(Property.EnumType(enum4, "test", PersistenceType, PersistenceType.CLIENT_IP), PersistenceType.BROWSER);

        let enum5 : PersistenceType;
        assert.equal(Property.EnumType(enum5, "test", PersistenceType), undefined);

        let enum6 : PersistenceType;
        assert.equal(Property.EnumType(enum6, PersistenceType.BROWSER, PersistenceType, PersistenceType.CLIENT_IP),
            PersistenceType.BROWSER);

        let enum7 : PersistenceType;
        assert.equal(Property.EnumType(enum7, "test", PersistenceType, PersistenceType.CLIENT_IP), PersistenceType.CLIENT_IP);

        let enum8 : PersistenceType;
        let enumValue : PersistenceType;
        assert.equal(Property.EnumType(enum8, enumValue), undefined);

        let enum9 : PersistenceType;
        let enumValue2 : PersistenceType;
        assert.equal(Property.EnumType(enum9, enumValue2, PersistenceType, PersistenceType.CLIENT_IP), PersistenceType.CLIENT_IP);

        let enum10 : PersistenceType;
        let enumValue3 : PersistenceType;
        assert.equal(Property.EnumType(enum10, enumValue3, PersistenceType), undefined);
    }
}
