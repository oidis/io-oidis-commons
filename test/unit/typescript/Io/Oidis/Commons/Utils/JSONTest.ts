/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JSONFallback } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/JSON.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class JSONTest extends UnitTestRunner {

    @Test()
    public stringify() : void {
        assert.equal(JSONFallback.stringify(""), JSON.stringify(""));
        let value : string;
        assert.equal(JSONFallback.stringify(value), JSON.stringify(value));

        const object : string = "value";
        assert.equal(JSONFallback.stringify(object), JSON.stringify(object));

        const object2 : () => void = () : void => {
        };
        assert.equal(JSONFallback.stringify(object2), JSON.stringify(object2));

        let property : any;
        const object3 : any = {
            key  : "value",
            key2 : 1,
            key3 : 12.1,
            key4 : false,
            key5 : [1, 2, property, 3],
            key6 : {key1: "test"},
            key7 : "",
            key8 : null,
            key9 : true,
            key10: property,
            key11() : void {
            },
            key12: new Date()
        };
        assert.equal(JSONFallback.stringify(object3), JSON.stringify(object3));

        let property2 : any;
        const replaceHandler : ($key : string, $value : any) => any = ($key : string, $value : any) : any => {
            if ($key === "key2") {
                return 15;
            }
            if ($key === "key3") {
                return property2;
            }
            return $value;
        };
        const object4 : any = {
            key : "value",
            key2: 1,
            key3: 12.1
        };

        assert.equal(JSONFallback.stringify(object4), JSON.stringify(object4));
        assert.equal(JSONFallback.stringify(object4, replaceHandler), JSON.stringify(object4, replaceHandler));
        assert.equal(JSONFallback.stringify(object4, replaceHandler, 11), JSON.stringify(object4, replaceHandler, 11));
        assert.equal(JSONFallback.stringify(object4, null), JSON.stringify(object4, null));
        assert.equal(JSONFallback.stringify(object4, null, null), JSON.stringify(object4, null, null));
        assert.equal(JSONFallback.stringify(object4, replaceHandler, 0), JSON.stringify(object4, replaceHandler, 0));
        assert.equal(JSONFallback.stringify(object4, null, "___"), JSON.stringify(object4, null, "___"));
    }

    @Test()
    public parse() : void {
        assert.throws(() : void => {
            JSONFallback.parse("test");
        }, /Unexpected token in JSON/);

        const object : string = "{" +
            "\"key1\":\"value\"," +
            "\"key2\":1," +
            "\"key3\":12.3," +
            "\"key4\":true," +
            "\"key5\":false," +
            "\"key6\":[10,20,30]," +
            "\"key7\":{\"key\":\"test\"}," +
            "\"key8\":null," +
            "\"key9\":\"\"" +
            "}";
        assert.deepEqual(JSONFallback.parse(object), JSON.parse(object));
    }
}
