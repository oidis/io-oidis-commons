/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { ArrayList } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectDecoder } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectValidator } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IUnitTestRunnerPromise, Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";

export class ObjectDecoderTest extends UnitTestRunner {

    @Test()
    public Unserialize() : IUnitTestRunnerPromise {
        const property : any = ObjectDecoder.Unserialize("u");
        assert.equal(ObjectValidator.IsSet(property), false);
        ObjectDecoder.Unserialize("u", ($data : any) : void => {
            assert.equal(ObjectValidator.IsSet($data), false);
        });

        const nullProperty : any = ObjectDecoder.Unserialize("n");
        assert.equal(nullProperty, null);
        assert.equal(ObjectValidator.IsEmptyOrNull(nullProperty), true);
        ObjectDecoder.Unserialize("n", ($data : any) : void => {
            assert.equal($data, null);
            assert.equal(ObjectValidator.IsEmptyOrNull($data), true);
        });

        assert.throws(() : void => {
            ObjectDecoder.Unserialize(
                "m:51:a:46:i:0;s:37:Io.Oidis.Commons.Primitives.ARRAYListc:0:109:o:103:" +
                "s:4:sizei:3;s:4:keysa:27:i:0;i:0;i:1;i:2;i:2;s:3:key" +
                "s:4:dataa:38:i:0;s:4:testi:1;s:5:test2i:2;s:5:test3");
        });
        assert.throws(() : void => {
            ObjectDecoder.Unserialize("g:60:");
        });
        assert.throws(() : void => {
            ObjectDecoder.Unserialize(
                "m:51:a:46:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayListc:1:109:o:103:");
        });
        assert.throws(() : void => {
            ObjectDecoder.Unserialize("c:0:109:o:103:");
        });

        const emptyProperty : string = ObjectDecoder.Unserialize("e");
        assert.equal(emptyProperty, "");
        assert.equal(ObjectValidator.IsEmptyOrNull(emptyProperty), true);
        ObjectDecoder.Unserialize("e", ($data : any) : void => {
            assert.equal($data, "");
            assert.equal(ObjectValidator.IsEmptyOrNull($data), true);
        });

        let boolProperty : boolean = ObjectDecoder.Unserialize("b:1");
        assert.equal(boolProperty, true);
        boolProperty = ObjectDecoder.Unserialize("b:0");
        assert.equal(boolProperty, false);
        ObjectDecoder.Unserialize("b:0", ($data : any) : void => {
            assert.equal($data, false);
        });

        const intProperty : number = ObjectDecoder.Unserialize("i:1;");
        assert.equal(ObjectValidator.IsInteger(intProperty), true);
        assert.equal(intProperty, 1);
        ObjectDecoder.Unserialize("i:1;", ($data : any) : void => {
            assert.equal(ObjectValidator.IsInteger($data), true);
            assert.equal($data, 1);
        });

        const doubleProperty : number = ObjectDecoder.Unserialize("d:1.2;");
        assert.equal(ObjectValidator.IsDouble(doubleProperty), true);
        assert.equal(doubleProperty, 1.2);
        ObjectDecoder.Unserialize("d:1.2;", ($data : any) : void => {
            assert.equal(ObjectValidator.IsDouble($data), true);
            assert.equal($data, 1.2);
        });

        let stringProperty : number = ObjectDecoder.Unserialize("s:4:keys");
        assert.equal(ObjectValidator.IsString(stringProperty), true);
        assert.equal(stringProperty, "keys");
        stringProperty = ObjectDecoder.Unserialize("s:8:\nstr\ring");
        assert.equal(stringProperty, "\nstr\ring");
        stringProperty = ObjectDecoder.Unserialize("s:9:按钮的工具提示文本");
        assert.equal(stringProperty, "按钮的工具提示文本");
        ObjectDecoder.Unserialize("s:4:keys", ($data : any) : void => {
            assert.equal(ObjectValidator.IsString($data), true);
            assert.equal($data, "keys");
        });

        const arrayProperty : any[] = ObjectDecoder.Unserialize(
            "a:0:");
        assert.equal(ObjectValidator.IsEmptyOrNull(arrayProperty), true);
        assert.equal(ObjectValidator.IsNativeArray(arrayProperty), true);
        ObjectDecoder.Unserialize("a:0:", ($data : any) : void => {
            assert.equal(ObjectValidator.IsEmptyOrNull($data), true);
            assert.equal(ObjectValidator.IsNativeArray($data), true);
        });

        let arrayProperty2 : any[] = ObjectDecoder.Unserialize(
            "a:39:i:0;i:1;i:1;s:4:testi:2;d:1.012;i:3;b:0");
        assert.equal(ObjectValidator.IsNativeArray(arrayProperty2), true);
        assert.equal(arrayProperty2.length, 4);
        assert.equal(arrayProperty2[0], 1);
        assert.equal(arrayProperty2[1], "test");
        assert.equal(arrayProperty2[2], 1.012);
        assert.equal(arrayProperty2[3], false);

        arrayProperty2 = ObjectDecoder.Unserialize(
            "a:80:i:0;i:1400483705187;i:1;i:1400483705187;i:2;i:1400483705187;i:3;i:1400483705187;");
        assert.deepEqual(arrayProperty2, [1400483705187, 1400483705187, 1400483705187, 1400483705187]);
        ObjectDecoder.Unserialize("a:80:i:0;i:1400483705187;i:1;i:1400483705187;i:2;i:1400483705187;i:3;i:1400483705187;",
            ($data : any) : void => {
                assert.deepEqual($data, [1400483705187, 1400483705187, 1400483705187, 1400483705187]);
            });

        const arrayProperty3 : any[] = ObjectDecoder.Unserialize(
            "a:81:i:0;s:4:testi:1;b:1i:2;a:43:i:0;s:5:valuei:1;d:1.12;i:2;b:0i:3;s:4:testi:3;i:150;");
        assert.equal(ObjectValidator.IsNativeArray(arrayProperty3), true);
        assert.equal(arrayProperty3.length, 4);
        assert.equal(arrayProperty3[0], "test");
        assert.equal(arrayProperty3[1], true);
        assert.equal(ObjectValidator.IsNativeArray(arrayProperty3[2]), true);
        assert.equal(arrayProperty3[2].length, 4);
        assert.equal(arrayProperty3[2][0], "value");
        assert.equal(arrayProperty3[2][1], 1.12);
        assert.equal(arrayProperty3[2][2], false);
        assert.equal(arrayProperty3[2][3], "test");
        assert.equal(arrayProperty3[3], 150);

        assert.deepEqual(ObjectDecoder.Unserialize("o:0:"), {});

        assert.deepEqual(ObjectDecoder.Unserialize(
                "o:31:s:4:names:5:values:4:testi:125;"),
            {name: "value", test: 125});

        const funcProperty : ($arg : string) => string =
            ObjectDecoder.Unserialize(
                "f:96:$arg) {var property = \"test\\r\\n2\";" +
                "var property2 = \"test2=\\\"test\\\"\";" +
                "var property3 = 1;return $arg");
        assert.equal(funcProperty("test"), "test");
        assert.equal(ObjectValidator.IsFunction(funcProperty), true);
        ObjectDecoder.Unserialize(
            "f:96:$arg) {var property = \"test\\r\\n2\";" +
            "var property2 = \"test2=\\\"test\\\"\";" +
            "var property3 = 1;return $arg", ($data : any) : void => {
                assert.equal($data("test"), "test");
                assert.equal(ObjectValidator.IsFunction($data), true);
            });

        const mixedArrayProperty : any[] =
            ObjectDecoder.Unserialize(
                "a:124:i:0;s:4:testi:1;f:96:$arg) {var property = \"test\\r\\n2\";" +
                "var property2 = \"test2=\\\"test\\\"\";var property3 = 1;return $argi:2;b:1");
        assert.equal(ObjectValidator.IsNativeArray(mixedArrayProperty), true);
        assert.equal(mixedArrayProperty[0], "test");
        assert.equal(ObjectValidator.IsFunction(mixedArrayProperty[1]), true);
        assert.equal(mixedArrayProperty[1]("test call"), "test call");
        assert.equal(mixedArrayProperty[2], true);

        const objectProperty : any =
            ObjectDecoder.Unserialize(
                "o:156:s:9:objstrings:4:tests:11:objFunctionf:96:$arg) {var property = \"test\\r\\n2\";" +
                "var property2 = \"test2=\\\"test\\\"\";var property3 = 1;return $args:10:objbooleanb:1");
        assert.equal(ObjectValidator.IsObject(objectProperty), true);
        assert.equal(objectProperty.objstring, "test");
        assert.equal(ObjectValidator.IsFunction(objectProperty.objFunction), true);
        assert.equal(objectProperty.objFunction("test call"), "test call");
        assert.equal(objectProperty.objboolean, true);

        const listProperty : ArrayList<string> =
            ObjectDecoder.Unserialize(
                "m:51:a:46:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayListc:0:109:o:103:" +
                "s:4:sizei:3;s:4:keysa:27:i:0;i:0;i:1;i:2;i:2;s:3:key" +
                "s:4:dataa:38:i:0;s:4:testi:1;s:5:test2i:2;s:5:test3");
        assert.equal(listProperty.Length(), 3);
        assert.equal(listProperty.getItem(2), "test2");
        assert.equal(listProperty.getItem("key"), "test3");
        ObjectDecoder.Unserialize(
            "m:51:a:46:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayListc:0:109:o:103:" +
            "s:4:sizei:3;s:4:keysa:27:i:0;i:0;i:1;i:2;i:2;s:3:key" +
            "s:4:dataa:38:i:0;s:4:testi:1;s:5:test2i:2;s:5:test3", ($data : any) : void => {
                assert.equal($data.Length(), 3);
                assert.equal($data.getItem(2), "test2");
                assert.equal($data.getItem("key"), "test3");
            });

        const mixedArrayProperty2 : any[] =
            ObjectDecoder.Unserialize(
                "m:51:a:46:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayList" +
                "a:201:i:0;i:1;i:1;i:2;i:2;c:0:165:o:159:s:4:sizei:4;" +
                "s:4:keysa:50:i:0;i:0;i:1;i:2;i:2;s:5:arrayi:3;s:12:bool val key" +
                "s:4:dataa:71:i:0;s:4:testi:1;d:1.11;i:2;a:32:i:0;i:1;i:1;i:2;i:2;i:3;i:3;i:4;i:3;b:1i:3;i:3;");
        assert.equal(mixedArrayProperty2.length, 4);
        assert.equal(mixedArrayProperty2[0], 1);
        assert.equal(mixedArrayProperty2[1], 2);
        assert.equal(ObjectValidator.IsArray(mixedArrayProperty2[2]) && !ObjectValidator.IsNativeArray(mixedArrayProperty2[2]), true);
        assert.equal(mixedArrayProperty2[2].Length(), 4);
        assert.equal(mixedArrayProperty2[2].getItem(2), 1.11);
        assert.equal(mixedArrayProperty2[2].getItem("array")[2], 3);
        assert.equal(mixedArrayProperty2[2].getItem("bool val key"), true);
        assert.equal(mixedArrayProperty2[3], 3);

        const arrayListProperty : ArrayList<any> =
            ObjectDecoder.Unserialize(
                "m:51:a:46:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayListc:0:200:o:194:s:4:" +
                "sizei:4;s:4:keysa:50:i:0;i:0;i:1;i:2;i:2;s:12:bool val keyi:3;s:5:array" +
                "s:4:dataa:105:i:0;s:4:testi:1;i:1;i:2;b:1i:3;c:0:67:o:62:s:4:" +
                "sizei:1;s:4:keysa:11:i:0;s:3:keys:4:dataa:13:i:0;s:5:value");
        assert.equal(ObjectValidator.IsArray(arrayListProperty) && !ObjectValidator.IsNativeArray(arrayListProperty), true);
        assert.equal(arrayListProperty.Length(), 4);
        assert.equal(arrayListProperty.getItem(2), 1);
        assert.equal(arrayListProperty.getItem("bool val key"), true);
        assert.equal(ObjectValidator.IsArray(arrayListProperty.getItem("array")) &&
            !ObjectValidator.IsNativeArray(arrayListProperty.getItem("array")), true);
        assert.equal(arrayListProperty.getItem("array").Length(), 1);
        assert.equal(arrayListProperty.getItem("array").getItem("key"), "value");

        assert.equal(ObjectDecoder.Unserialize("i:1400483705187;"), 1400483705187);
        assert.equal(ObjectDecoder.Unserialize("d:1.40048370518714e+25;"), 14004837051871400483705187);
        assert.deepEqual(ObjectDecoder.Unserialize("a:20:i:0;i:1400483705187;"), [1400483705187]);
        assert.deepEqual(ObjectDecoder.Unserialize(
                "a:60:i:0;i:1400483705187;i:1;i:1400483705187;i:2;i:1400483705187;"),
            [1400483705187, 1400483705187, 1400483705187]);

        const arrayListProperty2 : ArrayList<any> = ObjectDecoder.Unserialize(
            "m:98:a:93:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayListi:1;" +
            "s:38:Io.Oidis.Commons.Primitives.BaseObjectc:0:206:o:200:s:4:" +
            "sizei:7;s:4:keysa:56:i:0;i:0;i:1;i:1;i:2;i:2;i:3;i:3;i:4;i:4;i:5;i:5;i:6;i:6;" +
            "s:4:dataa:105:i:0;s:4:testi:1;ei:2;ni:3;c:1:4:o:0:i:4;c:0:41:o:36:s:4:" +
            "sizei:0;s:4:keysa:0:s:4:dataa:0:i:5;ui:6;s:4:test");
        assert.equal(arrayListProperty2.getItem(0), "test");
        assert.equal(arrayListProperty2.getItem(1), "");
        assert.equal(arrayListProperty2.getItem(2), null);
        assert.equal(arrayListProperty2.getItem(3).IsTypeOf(BaseObject.ClassName()), true);
        assert.equal(arrayListProperty2.getItem(4).IsTypeOf(ArrayList.ClassName()), true);
        assert.equal(ObjectValidator.IsSet(arrayListProperty2.getItem(5)), false);
        assert.equal(arrayListProperty2.getItem(6), "test");

        const arrayListProperty3 : ArrayList<any> = ObjectDecoder.Unserialize(
            "m:98:a:93:i:0;s:37:Io.Oidis.Commons.Primitives.ArrayListi:1;" +
            "s:38:Io.Oidis.Commons.Primitives.BaseObjectc:0:85:o:80:s:4:" +
            "sizei:2;s:4:keysa:16:i:0;i:0;i:1;i:1;s:4:dataa:26:i:0;s:4:testi:1;c:1:4:o:0:");
        assert.equal(arrayListProperty3.getItem(0), "test");
        assert.equal(arrayListProperty3.getItem(1).IsTypeOf(BaseObject.ClassName()), true);

        return ($done : () => void) : void => {
            setTimeout(() : void => {
                this.initSendBox();
                $done();
            }, 250);
        };
    }

    @Test()
    public Url() : void {
        assert.equal(ObjectDecoder.Url("http%3A%2F%2Ftest%20http%20string"), "http://test http string");
    }

    @Test()
    public Utf8() : void {
        assert.equal(ObjectDecoder.Utf8("some string"), "some string");
        assert.equal(ObjectDecoder.Utf8(""), "");
        assert.equal(ObjectDecoder.Utf8("\xC2\xA9"), "©");
        assert.equal(ObjectDecoder.Utf8("\x73\x6F\x6D\x65\x20\x73\x74\xC4\x9B\x6E\xC5\xA1\x74\xC3\xAD"), "some stěnští");
        assert.equal(ObjectDecoder.Utf8("\xE2\x82\xAC"), "€");
        assert.equal(ObjectDecoder.Utf8("\xF0\x90\x8D\x88"), "𐍈");
    }

    @Test()
    public Base64() : void {
        const execute : any = () : void => {
            assert.equal(ObjectDecoder.Base64("test string"), "test string");
            assert.equal(ObjectDecoder.Base64(""), "");
            assert.equal(ObjectDecoder.Base64("dGVzdCBzdHJpbmc=", false), "test string");
            assert.equal(ObjectDecoder.Base64("5oyJ6ZKu55qE5bel5YW35o+Q56S65paH5pys"), "按钮的工具提示文本");
            assert.equal(ObjectDecoder.Base64("dGVzdCBzdHJpbmc."), "test string");
            assert.equal(ObjectDecoder.Base64("5oyJ6ZKu55qE5bel5YW35o-Q56S65paH5pys"), "按钮的工具提示文本");
            assert.equal(ObjectDecoder.Base64(
                    "dmVyeSB2ZXJ5IGxvbmcgdGVzdCBzdHJpbmcgcmVwZWF0ZWQgbW9yZSB0aW1lc3ZlcnkgdmVyeSBsb25nIHRlc3Qgc3Rya" +
                    "W5nIHJlcGVhdGVkIG1vcmUgdGltZXN2ZXJ5IHZlcnkgbG9uZyB0ZXN0IHN0cmluZyByZXBlYXRlZCBtb3JlIHRpbWVzdm" +
                    "VyeSB2ZXJ5IGxvbmcgdGVzdCBzdHJpbmcgcmVwZWF0ZWQgbW9yZSB0aW1lcw.."),
                "very very long test string repeated more times" +
                "very very long test string repeated more times" +
                "very very long test string repeated more times" +
                "very very long test string repeated more times");
        };
        const atob : any = window.atob;
        delete window.atob;
        execute();
        (<any>ObjectDecoder).getBase64Decoder = this.globalCache.getBase64Decoder;
        window.atob = atob;
        execute();
    }
}
