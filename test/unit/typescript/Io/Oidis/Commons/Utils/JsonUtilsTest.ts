/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { JsonUtils } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/StringUtils.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";

export class JsonUtilsTest extends UnitTestRunner {

    constructor() {
        super();
        // this.setMethodFilter("testExtend");
    }

    @Test()
    public Extend() : void {
        let input : any = {
            test: true
        };
        assert.equal(input.test, true);
        JsonUtils.Extend(input, {test: false});
        assert.equal(input.test, false);

        input = {
            testArray : [1, true, "test"],
            testBool  : true,
            testInt   : 12,
            testObject: {
                test: "test"
            }
        };
        JsonUtils.Extend(input, {
            testArray : [true, 1, "test"],
            testBool  : false,
            testObject: {
                test2: true
            }
        });
        assert.deepEqual(input, {
            testArray : [true, 1, "test"],
            testBool  : false,
            testInt   : 12,
            testObject: {
                test : "test",
                test2: true
            }
        });

        input = {
            testObjectArray: [
                {
                    test: 1
                },
                {
                    test: 2
                }
            ]
        };
        JsonUtils.Extend(input, {
            testObjectArray : [
                "string",
                {
                    test2: 3
                },
                {
                    test3: 1
                }
            ],
            testObjectArray2: []
        });
        assert.deepEqual(input, {
            testObjectArray : [
                "string",
                {
                    test2: 3
                },
                {
                    test3: 1
                }
            ],
            testObjectArray2: []
        });
    }

    @Test()
    public Clone() : void {
        const reference : any = {};
        const input : any = {
            test: reference
        };
        assert.equal(input.test, reference);
        assert.notEqual(JsonUtils.Clone(input).test, reference);
    }

    @Test()
    public DeepClone() : void {
        const reference : any = {
            value: "string"
        };
        const input : any = {
            test: reference
        };
        reference.parent = input;
        assert.equal(input.test, reference);
        const clone : any = JsonUtils.DeepClone(input);
        assert.notEqual(clone.test, reference);
        assert.equal(clone.test.value, "string");
        assert.throws(() : void => {
            JSON.stringify(clone);
        }, "Converting circular structure to JSON");

        const cloneWithSymbols : any = JsonUtils.DeepClone(input, false);
        assert.deepEqual(cloneWithSymbols, {
            test: {
                parent: {
                    test: {
                        $ref: "#/test"
                    }
                },
                value : "string"
            }
        });
        assert.doesNotThrow(() : void => {
            JSON.stringify(cloneWithSymbols);
        });

        const cloneWithoutSymbols : any = JsonUtils.DeepClone(input, false, false);
        assert.deepEqual(cloneWithoutSymbols, {
            test: {
                parent: {
                    test: {
                        parent: {
                            test: {}
                        },
                        value : "string"
                    }
                },
                value : "string"
            }
        });
        assert.doesNotThrow(() : void => {
            JSON.stringify(cloneWithoutSymbols);
        });
    }

    @Test()
    public ParseRefSymbols() : void {
        assert.deepEqual(JsonUtils.ParseRefSymbols({
            references: {
                test: {
                    value: "string"
                }
            },
            test      : {
                $ref: "#/references/test/value"
            }
        }), {
            references: {
                test: {
                    value: "string"
                }
            },
            test      : "string"
        });
    }

    @Test()
    public ToJsonp() : void {
        assert.equal(JsonUtils.ToJsonp({
            test : true,
            test2: () : void => {
                // test function
            }
        }), "" +
            "JsonpData({\n" +
            "    test: true,\n" +
            "    test2: function () {// test function}\n" +
            "});\n" +
            "");

        assert.equal(JsonUtils.ToJsonp({test: true}, JsonUtilsTest), "" +
            "Io.Oidis.Commons.Utils.JsonUtilsTest.Data({\n" +
            "    test: true\n" +
            "});\n" +
            "");

        assert.equal(JsonUtils.ToJsonp({test: true}, JsonUtilsTest, [
            ObjectValidator, StringUtils
        ]), "" +
            "var ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;\n" +
            "var StringUtils = Io.Oidis.Commons.Utils.StringUtils;\n" +
            "\n" +
            "Io.Oidis.Commons.Utils.JsonUtilsTest.Data({\n" +
            "    test: true\n" +
            "});\n" +
            "");
    }

    @Test()
    public Sort() : void {
        assert.deepEqual(JSON.stringify(JsonUtils.Sort({})), JSON.stringify({}));
        assert.deepEqual(JSON.stringify(JsonUtils.Sort(1)), JSON.stringify(1));
        assert.deepEqual(JSON.stringify(JsonUtils.Sort([1, 3, 2])), JSON.stringify([1, 3, 2]));
        assert.deepEqual(JSON.stringify(JsonUtils.Sort(
                {
                    b: 1,
                    a: 2,
                    c: 4
                })),
            JSON.stringify({
                a: 2,
                b: 1,
                c: 4
            }));

        assert.deepEqual(JSON.stringify(JsonUtils.Sort(
                {
                    b: "ha",
                    c: [
                        {
                            y: 11,
                            x: 12,
                            i: 13
                        },
                        {
                            q: "q",
                            k: "ka",
                            l: true
                        }
                    ],
                    a: 2
                })),
            JSON.stringify({
                a: 2,
                b: "ha",
                c: [
                    {
                        i: 13,
                        x: 12,
                        y: 11
                    },
                    {
                        k: "ka",
                        l: true,
                        q: "q"
                    }
                ]
            }));

        assert.deepEqual(JSON.stringify(JsonUtils.Sort(
                {
                    b: new Date(0),
                    c: [
                        {
                            y: 11,
                            x: 12,
                            i: new Date(100)
                        },
                        {
                            q: "q",
                            k: "ka",
                            l: true
                        }
                    ],
                    a: 2
                })),
            JSON.stringify({
                a: 2,
                b: new Date(0),
                c: [
                    {
                        i: new Date(100),
                        x: 12,
                        y: 11
                    },
                    {
                        k: "ka",
                        l: true,
                        q: "q"
                    }
                ]
            }));
    }
}
