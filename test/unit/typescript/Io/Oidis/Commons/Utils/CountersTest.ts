/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Counters } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/Counters.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class CountersTest extends UnitTestRunner {

    @Test()
    public getNext() : void {
        assert.equal(Counters.getNext(CountersTest.ClassName()), 0);
        assert.equal(Counters.getNext(CountersTest.ClassName()), 1);
        assert.equal(Counters.getNext(CountersTest.ClassName()), 2);
        assert.equal(Counters.getNext(BaseObject.ClassName()), 0);
        assert.equal(Counters.getNext(BaseObject.ClassName()), 1);
        assert.equal(Counters.getNext(CountersTest.ClassName()), 3);
    }

    @Test()
    public ClearOne() : void {
        assert.equal(Counters.getNext(CountersTest.ClassName()), 0);
        assert.equal(Counters.getNext(CountersTest.ClassName()), 1);
        assert.equal(Counters.getNext(CountersTest.ClassName()), 2);
        assert.equal(Counters.getNext(BaseObject.ClassName()), 0);
        assert.equal(Counters.getNext(BaseObject.ClassName()), 1);
        Counters.Clear(CountersTest.ClassName());
        assert.equal(Counters.getNext(CountersTest.ClassName()), 0);
        assert.equal(Counters.getNext(BaseObject.ClassName()), 2);
    }

    @Test()
    public ClearAll() : void {
        assert.equal(Counters.getNext(CountersTest.ClassName()), 0);
        assert.equal(Counters.getNext(CountersTest.ClassName()), 1);
        assert.equal(Counters.getNext(CountersTest.ClassName()), 2);
        assert.equal(Counters.getNext(BaseObject.ClassName()), 0);
        assert.equal(Counters.getNext(BaseObject.ClassName()), 1);
        Counters.Clear();
        assert.equal(Counters.getNext(CountersTest.ClassName()), 0);
        assert.equal(Counters.getNext(BaseObject.ClassName()), 0);
    }
}
