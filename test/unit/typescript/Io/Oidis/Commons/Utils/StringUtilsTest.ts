/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "../../../../../../../source/typescript/Io/Oidis/Commons/Utils/StringUtils.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class StringUtilsTest extends UnitTestRunner {

    @Test()
    public NewLine() : void {
        assert.equal(StringUtils.NewLine(), "<br/>", "return xhtml new line");
        assert.equal(StringUtils.NewLine(false), "\r\n", "return windows special char");
    }

    @Test()
    public Space() : void {
        assert.equal(StringUtils.Space(), "&nbsp;", "return space");
        assert.equal(StringUtils.Space(2), "&nbsp;&nbsp;", "return 2 spaces");
        assert.equal(StringUtils.Space(4, false), "    ", "return 4 space");
        assert.equal(StringUtils.Space(0, false), "", "return 0 spaces");
    }

    @Test()
    public Tab() : void {
        assert.equal(StringUtils.Tab(), "&nbsp;&nbsp;&nbsp;", "return tab");
        assert.equal(StringUtils.Tab(2), "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "return 2 tabs");
    }

    @Test()
    public Length() : void {
        assert.equal(StringUtils.Length(""), 0);
        assert.equal(StringUtils.Length(null), 0);
        assert.equal(StringUtils.Length("String"), 6);
        assert.equal(StringUtils.Length("1400483705187"), 13);
    }

    @Test()
    public IsEmpty() : void {
        assert.equal(StringUtils.IsEmpty("test string"), false);
        assert.equal(StringUtils.IsEmpty(""), true);
        assert.equal(StringUtils.IsEmpty(null), true);
    }

    @Test()
    public ToLowerCase() : void {
        assert.equal(StringUtils.ToLowerCase("StRiNg"), "string");
        assert.equal(StringUtils.ToLowerCase(""), "");
        assert.equal(StringUtils.ToLowerCase(null), "");
    }

    @Test()
    public ToUpperCase() : void {
        assert.equal(StringUtils.ToUpperCase("StRiNg"), "STRING");
        assert.equal(StringUtils.ToUpperCase(""), "");
        assert.equal(StringUtils.ToUpperCase(null), "");
    }

    @Test()
    public IndexOf() : void {
        assert.equal(StringUtils.IndexOf("string", "t"), 1);
        assert.equal(StringUtils.IndexOf("stringstring", "t", true, 3), 7);
        assert.equal(StringUtils.IndexOf("string", "n", false), 4);
        assert.equal(StringUtils.IndexOf("stringstring", "t", false, 5), 7);
        assert.equal(StringUtils.IndexOf("stringstring", ""), 0);
        assert.equal(StringUtils.IndexOf("stringstring", "", false), 12);
        assert.equal(StringUtils.IndexOf("stringstring", "u"), -1);
    }

    @Test()
    public Contains() : void {
        assert.equal(StringUtils.Contains("String", "tri"), true);
        assert.equal(StringUtils.Contains("StRiNg", "st", "Ng"), true);
        assert.equal(StringUtils.Contains("StRiNg", "", ""), false);
        assert.equal(StringUtils.Contains("StRiNg", "tri"), false);
        assert.equal(StringUtils.Contains("string", "ssttrriinngg"), false);
        assert.equal(StringUtils.Contains("test string", "s"), true);
        assert.equal(StringUtils.Contains("test string", null), false);
        assert.equal(StringUtils.Contains(null, "find me"), false);
        assert.equal(StringUtils.Contains(null, null), false);
        assert.equal(StringUtils.Contains("test", "test"), true);
        assert.equal(StringUtils.Contains("test string", ""), false);
        assert.equal(StringUtils.Contains("tester ", "tes ter"), false);
    }

    @Test()
    public ContainsIgnoreCase() : void {
        assert.equal(StringUtils.ContainsIgnoreCase("StRiNg", "string"), true);
        assert.equal(StringUtils.ContainsIgnoreCase("StRiNg", "sttr"), false);
        assert.equal(StringUtils.ContainsIgnoreCase("", "sttr"), false);
        assert.equal(StringUtils.ContainsIgnoreCase(null, "sttr"), false);
        assert.equal(StringUtils.ContainsIgnoreCase("StRiNg", ""), false);
        assert.equal(StringUtils.ContainsIgnoreCase("StRiNg", "sta", "nGr", "InG"), true);
    }

    @Test()
    public OccurrenceCount() : void {
        assert.equal(StringUtils.OccurrenceCount("StRiNg", "St"), 1);
        assert.equal(StringUtils.OccurrenceCount("StRiNg", "tr"), 0);
        assert.equal(StringUtils.OccurrenceCount("StRiNg", ""), 0);
        assert.equal(StringUtils.OccurrenceCount("StRiNg", null), 0);
        assert.equal(StringUtils.OccurrenceCount("", "a"), 0);
        assert.equal(StringUtils.OccurrenceCount("stringstringstring", "string "), 0);
    }

    @Test()
    public StartsWith() : void {
        assert.equal(StringUtils.StartsWith("", "St"), false);
        assert.equal(StringUtils.StartsWith(null, "St"), false);
        assert.equal(StringUtils.StartsWith("StRiNg", "St"), true);
        assert.equal(StringUtils.StartsWith("StRiNg", "Ri"), false);
        assert.equal(StringUtils.StartsWith("StRiNg", "st"), false);
    }

    @Test()
    public EndsWith() : void {
        assert.equal(StringUtils.EndsWith("StRiNg", "Ng"), true);
        assert.equal(StringUtils.EndsWith("StRiNg", "Ri"), false);
    }

    @Test()
    public PatternMatch() : void {
        assert.equal(StringUtils.PatternMatched("string", "string"), true);
        assert.equal(StringUtils.PatternMatched("*", "string"), true);
        assert.equal(StringUtils.PatternMatched("string*", "stringStr"), true);
        assert.equal(StringUtils.PatternMatched("*string", "Strstring"), true);
        assert.equal(StringUtils.PatternMatched("*string*", "StrstringStr"), true);
        assert.equal(StringUtils.PatternMatched("string*", "StrstringStr"), false);
        assert.equal(StringUtils.PatternMatched("*test*", "7t77est7"), false);
        assert.equal(StringUtils.PatternMatched("string*", "Strstring"), false);
        assert.equal(StringUtils.PatternMatched("*test*", null), false);
        assert.equal(StringUtils.PatternMatched("str*ing", "string2"), false);
        assert.equal(StringUtils.PatternMatched(null, "string2"), false);
        assert.equal(StringUtils.PatternMatched("str**i*ng", "string"), true);
    }

    @Test()
    public Substring() : void {
        assert.equal(StringUtils.Substring("StRiNg", 2, 4), "Ri");
        assert.equal(StringUtils.Substring("StRiNg", 2), "RiNg");
        assert.equal(StringUtils.Substring("karlejatojde", 3, 9), "lejato");
    }

    @Test()
    public Replace() : void {
        assert.equal(StringUtils.Replace("StRiNg", "Ri", "St"), "StStNg");
        assert.equal(StringUtils.Replace("StRiNgStRiNgStRiNg", "Ri", "St"), "StStNgStStNgStStNg");
    }

    @Test()
    public Remove() : void {
        assert.equal(StringUtils.Remove("StRiNg", "Ri"), "StNg");
        assert.equal(StringUtils.Remove("StRiNg"), "StRiNg");
        assert.equal(StringUtils.Remove("StRiNg", "StRiNg"), "");
        assert.equal(StringUtils.Remove("", "StRiNg"), "");
        assert.equal(StringUtils.Remove("StRiNg", "pr"), "StRiNg");
        assert.equal(StringUtils.Remove("StRiNg", "pr", "Ng"), "StRi");
    }

    @Test()
    public Split() : void {
        assert.deepEqual(StringUtils.Split("St*Ri*Ng", "*"), ["St", "Ri", "Ng"]);
        assert.deepEqual(StringUtils.Split("name1:value1;name2:value2", ";", ":"), [
            "name1", "value1", "name2", "value2"
        ]);
        assert.deepEqual(StringUtils.Split("*string*", "*"), ["", "string", ""]);
        assert.deepEqual(StringUtils.Split("string"), ["string"]);
    }

    @Test()
    public Format() : void {
        assert.equal(StringUtils.Format("String {0}-{1}", "F", "S-L"), "String F-S-L");
        assert.equal(StringUtils.Format("String {0}", null), "String NULL");
        assert.equal(StringUtils.Format("String {0}", true), "String true");
        assert.equal(StringUtils.Format("String {0}", {test: 1234}), "String {<br/>&nbsp;&nbsp;&nbsp;\"test\": 1234<br/>}");
        assert.equal(StringUtils.Format("", {test: 1234}), "");
        assert.equal(StringUtils.Format(null, {test: 1234}), null);
        assert.equal(StringUtils.Format("String {0}", true, false, 123), "String true");
    }

    @Test()
    public ToArray() : void {
        assert.deepEqual(StringUtils.ToArray("String"), ["S", "t", "r", "i", "n", "g"]);
    }

    @Test()
    public ToInteger() : void {
        assert.equal(StringUtils.ToInteger("1"), 1);
        assert.deepEqual(StringUtils.ToInteger("a"), parseInt("a", 10));
    }

    @Test()
    public ToDouble() : void {
        assert.equal(StringUtils.ToDouble("1"), 1.0);
        assert.equal(StringUtils.ToDouble("1.0908"), 1.0908);
        assert.deepEqual(StringUtils.ToDouble("a"), parseFloat("a"));
    }

    @Test()
    public ToBoolean() : void {
        assert.equal(StringUtils.ToBoolean("false"), false);
    }

    @Test()
    public StripSlashes() : void {
        assert.equal(StringUtils.StripSlashes("f\'oo"), "f'oo");
        assert.equal(StringUtils.StripSlashes("f\\\'oo"), "f\'oo");
        assert.equal(StringUtils.StripSlashes("string"), "string");
        assert.equal(StringUtils.StripSlashes("str\0ing"), "str\u0000ing");
        assert.equal(StringUtils.StripSlashes(""), "");
        assert.equal(StringUtils.StripSlashes(null), "");
    }

    @Test()
    public StripTags() : void {
        assert.equal(StringUtils.StripTags("<b>string</b>"), "string");
        assert.equal(StringUtils.StripTags("<script type='text/javascript'>var prop=1;</script>"), "");
        assert.equal(StringUtils.StripTags(
                "<script type='text/javascript'>var prop=1;</script><b>Html string</b> and plain text"),
            "Html string and plain text");
        assert.equal(StringUtils.StripTags("<b>string</b>", "<b>"), "<b>string</b>");
        assert.equal(StringUtils.StripTags("string"), "string");
        assert.equal(StringUtils.StripTags(""), "");
        assert.equal(StringUtils.StripTags(null), "");
    }

    @Test()
    public HardWrap() : void {
        assert.equal(StringUtils.HardWrap("this is very long test string this is very long test string this is " +
                "very long test string", 50, false),
            "this is very long test string this is very long te\r\n" +
            "st string this is very long test string");
        assert.equal(StringUtils.HardWrap("this is very long test string this is very long test string this is " +
                "very long test string", 50),
            "this is very long test string this is very long te<br/>" +
            "st string this is very long test string");
        assert.equal(StringUtils.HardWrap("You will be able to read and write code for a large number of platforms " +
                "-- everything from microcontrollers to the most advanced scientific systems can be written in C," +
                " and many modern operating systems are written in C. Check out this article in Digikey's techzone and " +
                "get to know which analog and power products from NXP help in designing for functional safety. " +
                "The H-Bridge driver HB2001, for example, responds to a growing need for critical functions in high" +
                " reliability systems. Find out more about our Safe Assure program and the products supported here"),
            "You will be able to read and write code for a large number of platforms -- everything from microcontrollers" +
            " to the most advanced scientific systems can be written in C, and many modern operating systems" +
            " are written in C. Check out this article in Digikey\'s techzone and get to know which analog and p<br/>" +
            "ower products from NXP help in designing for functional safety." +
            " The H-Bridge driver HB2001, for example, responds to a growing need for critical functions in" +
            " high reliability systems. Find out more about our Safe Assure program and the products supported here");
    }

    @Test()
    public getCharacterAt() : void {
        assert.equal(StringUtils.getCharacterAt("test", 2), "s");
        assert.equal(StringUtils.getCharacterAt("test", 10), "");
        assert.equal(StringUtils.getCharacterAt("", 10), "");
        assert.equal(StringUtils.getCharacterAt(null, 5), "");
    }

    @Test()
    public getCodeAt() : void {
        assert.equal(StringUtils.getCodeAt("test", 2), 115);
        assert.equal(StringUtils.getCodeAt("test", 10), null);
        assert.equal(StringUtils.getCodeAt("", 10), null);
        assert.equal(StringUtils.getCodeAt(null, 5), null);
    }

    @Test()
    public getCrc() : void {
        assert.equal(StringUtils.getCrc("test string"), 323425605);
        assert.equal(StringUtils.getCrc("tets string"), 2112057749);
        assert.equal(StringUtils.getCrc("按钮的工具提示文本"), -839019276);
    }

    @Test()
    public ToHexadecimal() : void {
        assert.equal(StringUtils.ToHexadecimal("test"), 1952805748);
        assert.equal(StringUtils.ToHexadecimal(""), null);
        assert.equal(StringUtils.ToHexadecimal(null), null);
    }

    @Test()
    public getSha1() : void {
        assert.equal(StringUtils.getSha1("test string"), "661295c9cbf9d6b2f6428414504a8deed3020641");
        assert.equal(StringUtils.getSha1("test strings"), "7510ea390b14653c2c3157453664fe33f0dc3cdc");
        assert.equal(StringUtils.getSha1("test stringsa"), "685c78b5ab454df6b7b152bf3ea481a5677c6dee");
        assert.equal(StringUtils.getSha1("test stringsak"), "879f29896cfeff0d8637d5ed9cb4c43b34006f2a");
    }

    @Test()
    public StripComments() : void {
        assert.equal(StringUtils.StripComments("/*string*/abc"), "abc");
        assert.equal(StringUtils.StripComments("/*strg*/abc"), "abc");
        assert.equal(StringUtils.StripComments("/*string*/"), "");
        assert.equal(StringUtils.StripComments(null), "");
    }

    @Test()
    public VersionIsLower() : void {
        assert.equal(StringUtils.VersionIsLower("", "1.1.0"), false);
        assert.equal(StringUtils.VersionIsLower("1.0.0", ""), false);
        assert.equal(StringUtils.VersionIsLower("", ""), false);
        assert.equal(StringUtils.VersionIsLower("1.0.0", "1.1.0"), true);
        assert.equal(StringUtils.VersionIsLower("1.0.0-beta", "1.1.0"), true);
        assert.equal(StringUtils.VersionIsLower("1.0.0asd", "1.1.0"), true);
        assert.equal(StringUtils.VersionIsLower("1.0.0.0", "1.1.0"), true);
        assert.equal(StringUtils.VersionIsLower("1.0", "1.1.0"), true);
        assert.equal(StringUtils.VersionIsLower("1.1", "1.x.x"), false);
        assert.equal(StringUtils.VersionIsLower("2.0.0.0", "1.1.x"), false);
        assert.equal(StringUtils.VersionIsLower("3.3.3", "3.3.3"), false);
        assert.equal(StringUtils.VersionIsLower("1.0.2", "1.0.2b12"), false);
        assert.equal(StringUtils.VersionIsLower("10.10", "11.0"), true);
        assert.equal(StringUtils.VersionIsLower("1.2-rc3", "1.2-a1"), false);
        assert.equal(StringUtils.VersionIsLower("1.x.2.*", "1.x.x.*"), false);
        assert.equal(StringUtils.VersionIsLower("2.0.0.0", "1.0.0.0"), false);
        assert.equal(StringUtils.VersionIsLower("2.1.*.", "2.*."), false);
        assert.equal(StringUtils.VersionIsLower("2.*.*.", "2.*."), false);
        assert.equal(StringUtils.VersionIsLower("2.11.0.windows.1", "2.10.2"), false);
    }

    @Test()
    public ToCamelCase() : void {
        assert.equal(StringUtils.ToCamelCase("test string"), "testString");
        assert.equal(StringUtils.ToCamelCase("test-string"), "testString");
        assert.equal(StringUtils.ToCamelCase("test_string"), "testString");
        assert.equal(StringUtils.ToCamelCase("TestString"), "testString");
        assert.equal(StringUtils.ToCamelCase("testString"), "testString");
        assert.equal(StringUtils.ToCamelCase(""), "");
        assert.equal(StringUtils.ToCamelCase(null), "");
    }
}
