/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "./UnitTestEnvironment.js";
import { UnitTestLoader, UnitTestRunner } from "./UnitTestRunner.js";

import { Index } from "../../../../../../source/typescript/Io/Oidis/Commons/Index.js";
import { Test } from "./BaseUnitTestRunner.js";

export class IndexTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        assert.doesNotThrow(() : void => {
            const instance : Index = new Index();
        });
    }

    @Test(true)
    public Process1() : void {
        UnitTestLoader.Load(<any>{
            build: {time: new Date().toTimeString(), type: "prod"}, name: "com-wui-framework-commons", version: "1.0.0"
        });
        assert.resolveEqual(Index, "" +
            "<head></head>" +
            "<body><div id=\"Content\"><span guitype=\"HtmlAppender\">" +
            "<h3>Runtime tests</h3></span><span guitype=\"HtmlAppender\">" +
            "<a href=\"#/com-wui-framework-commons/web/HttpRequestParserTest\">HttpRequestParserTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/HttpManagerTest\">HttpManagerTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/PersistenceApiTest\">PersistenceApiTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/EventsManagerTest\">EventsManagerTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/ExceptionsManagerTest\">ExceptionsManagerTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/TimeoutTest\">TimeoutTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/ChromiumConnectorTest\">ChromiumConnectorTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/CoverageTest\">CoverageTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/AsyncRuntimeTest\">AsyncRuntimeTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/JsonpFileReaderTest\">JsonpFileReaderTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a *</a></span><span guitype=\"HtmlAppender\">" +
            "<h3>About resolvers</h3></span><span guitype=\"HtmlAppender\">" +
            "<a href=\"#/com-wui-framework-commons/about/Cache\">/about/Cache</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/about/Env\">/about/Env</a></span>" +
            "</div>" +
            "</body>");
        this.initSendBox();
    }

    @Test(true)
    public Process2() : void {
        UnitTestLoader.Load(<any>{
            build: {time: new Date().toTimeString(), type: "prod"}, name: "com-wui-framework-commons", version: "1.0.0"
        });
        assert.resolveEqual(Index, "" +
            "<head></head>" +
            "<body><div id=\"Content\"><span guitype=\"HtmlAppender\">" +
            "<h3>Runtime tests</h3></span><span guitype=\"HtmlAppender\">" +
            "<a href=\"#/com-wui-framework-commons/web/HttpRequestParserTest\">HttpRequestParserTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/HttpManagerTest\">HttpManagerTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/PersistenceApiTest\">PersistenceApiTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/EventsManagerTest\">EventsManagerTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/ExceptionsManagerTest\">ExceptionsManagerTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/TimeoutTest\">TimeoutTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/ChromiumConnectorTest\">ChromiumConnectorTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/CoverageTest\">CoverageTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/AsyncRuntimeTest\">AsyncRuntimeTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/web/JsonpFileReaderTest\">JsonpFileReaderTest</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a *</a></span><span guitype=\"HtmlAppender\">" +
            "<h3>About resolvers</h3></span><span guitype=\"HtmlAppender\">" +
            "<a href=\"#/com-wui-framework-commons/about/Cache\">/about/Cache</a></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<a href=\"#/com-wui-framework-commons/about/Env\">/about/Env</a></span>" +
            "</div>" +
            "</body>"
        );
        this.initSendBox();
    }
}
