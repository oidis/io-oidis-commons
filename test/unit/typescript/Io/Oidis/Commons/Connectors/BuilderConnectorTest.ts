/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { BuilderConnector } from "../../../../../../../source/typescript/Io/Oidis/Commons/Connectors/BuilderConnector.js";
import { EventsManager } from "../../../../../../../source/typescript/Io/Oidis/Commons/Events/EventsManager.js";
import { IBuilderEvents } from "../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/Events/IBuilderEvents.js";
import { IUnitTestRunnerPromise, Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";

export class BuilderConnectorTest extends UnitTestRunner {

    @Test()
    public Connect() : void {
        const singleton : BuilderConnector = BuilderConnector.Connect();
        assert.equal(BuilderConnector.Connect(), singleton);
        EventsManager.getInstanceSingleton().Clear(singleton.getId().toString());
    }

    @Test()
    public Constructor() : void {
        assert.doesNotThrow(() : void => {
            const instance : BuilderConnector = new BuilderConnector();
            EventsManager.getInstanceSingleton().Clear(instance.getId().toString());
        });
        assert.doesNotThrow(() : void => {
            const instance2 : BuilderConnector = new BuilderConnector(false);
            EventsManager.getInstanceSingleton().Clear(instance2.getId().toString());
        });
    }

    @Test()
    public getEvents() : void {
        const connect : BuilderConnector = new BuilderConnector();
        const events : IBuilderEvents = connect.getEvents();
        const types : string[] = [];
        let property : string;
        for (property in events) {
            if (events.hasOwnProperty(property)) {
                types.push(property);
            }
        }
        assert.deepEqual(types, [
            "OnClose",
            "OnError",
            "OnStart",
            "OnTimeout",
            "OnBuildStart",
            "OnBuildComplete",
            "OnWarning",
            "OnFail",
            "OnMessage"
        ]);
        EventsManager.getInstanceSingleton().Clear(connect.getId().toString());
    }

    @Test()
    public getId() : void {
        const connect : BuilderConnector = new BuilderConnector(true);
        assert.ok(connect.getId(), "1924497448");
        EventsManager.getInstanceSingleton().Clear(connect.getId().toString());
    }

    @Test(true)
    public testSend() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const connect : BuilderConnector = new BuilderConnector(true);
            connect.Send("Cmd", {
                args: ["--version"],
                cmd : "wui"
            }, ($data : any) : void => {
                assert.equal($data.data, "WzBd");
                assert.equal($data.status, 200);
                assert.equal($data.type, "Cmd");
                (<any>connect).client.StopCommunication();
                EventsManager.getInstanceSingleton().Clear(connect.getId().toString());
                $done();
            });
        };
    }

    @Test()
    public AddEventListener() : void {
        const connect : BuilderConnector = new BuilderConnector(true);
        const handler : any = () : void => {
        };
        connect.AddEventListener("AddEventListener", handler);
        connect.AddEventListener("AddEventListener", handler);
        EventsManager.getInstanceSingleton().Clear(connect.getId().toString());
    }

    @Test()
    public RemoveEventListener() : void {
        const connect : BuilderConnector = new BuilderConnector(true);
        const handler : any = () : void => {
            let event : IBuilderEvents;
        };
        connect.AddEventListener("Listener", handler);
        connect.AddEventListener("Event", handler);
        connect.RemoveEventListener("Event");
        connect.RemoveEventListener("Try");
        EventsManager.getInstanceSingleton().Clear(connect.getId().toString());
    }
}
