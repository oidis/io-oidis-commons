/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IOHandlerType } from "../../../../../../../source/typescript/Io/Oidis/Commons/Enums/IOHandlerType.js";
import { IOHandler } from "../../../../../../../source/typescript/Io/Oidis/Commons/Interfaces/IOHandler.js";
import { ConsoleHandler } from "../../../../../../../source/typescript/Io/Oidis/Commons/IOApi/Handlers/ConsoleHandler.js";
import { HTMLElementHandler } from "../../../../../../../source/typescript/Io/Oidis/Commons/IOApi/Handlers/HTMLElementHandler.js";
import { IOHandlerFactory } from "../../../../../../../source/typescript/Io/Oidis/Commons/IOApi/IOHandlerFactory.js";
import { BaseObject } from "../../../../../../../source/typescript/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Test } from "../BaseUnitTestRunner.js";
import { assert } from "../UnitTestEnvironment.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

class MockBaseObject extends BaseObject {
}

export class IOHandlerFactoryTest extends UnitTestRunner {

    @Test()
    public getHendler() : void {
        const handler1 : IOHandler = IOHandlerFactory.getHandler(IOHandlerType.CONSOLE, "config");
        const handler2 : IOHandler = IOHandlerFactory.getHandler(IOHandlerType.INPUT_FILE, "input.txt");
        const handler3 : IOHandler = IOHandlerFactory.getHandler(IOHandlerType.OUTPUT_FILE, "output.txt");
        const handler4 : IOHandler = IOHandlerFactory.getHandler(IOHandlerType.HTML_ELEMENT);
        const handler5 : IOHandler = IOHandlerFactory.getHandler("test", "owner");

        assert.equal(IOHandlerFactory.getHandler(IOHandlerType.CONSOLE, "config"), handler1);
        assert.equal(IOHandlerFactory.getHandler(IOHandlerType.INPUT_FILE), handler2);
        assert.equal(IOHandlerFactory.getHandler(IOHandlerType.OUTPUT_FILE), handler3);
        assert.equal(IOHandlerFactory.getHandler(IOHandlerType.HTML_ELEMENT), handler4);
        assert.deepEqual(handler5, null);
    }

    @Test()
    public getHandlerType() : void {
        const console : ConsoleHandler = new ConsoleHandler("test1");
        const htmlElement : HTMLElementHandler = new HTMLElementHandler("test3");
        assert.equal(IOHandlerFactory.getHandlerType(console), IOHandlerType.CONSOLE);
        assert.equal(IOHandlerFactory.getHandlerType(htmlElement), IOHandlerType.HTML_ELEMENT);
        assert.equal(IOHandlerFactory.getHandlerType(<any>(new MockBaseObject())), null);
        assert.equal(IOHandlerFactory.getHandlerType(undefined), null);
    }

    @Test()
    public DestroyAll() : void {
        this.registerElement("HtmlElement");
        this.registerElement("testHtmlTarget");
        const htmlElement : IOHandler = IOHandlerFactory.getHandler(IOHandlerType.HTML_ELEMENT, "testHtmlTarget");
        htmlElement.Init();
        htmlElement.setOnPrint(($message : string) : void => {
            assert.equal($message, "testing");
        });
        htmlElement.Print("testing");
        assert.equal(document.getElementById("testHtmlTarget").innerHTML, "<span guitype=\"HtmlAppender\">testing</span>");
        IOHandlerFactory.DestroyAll();
        assert.equal(document.getElementById("testHtmlTarget").innerHTML, "");

        const member : string = "handlersList";
        delete IOHandlerFactory[member];
        IOHandlerFactory.DestroyAll();
        this.initSendBox();
    }
}
