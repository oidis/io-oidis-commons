/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseOutputHandler } from "../../../../../../../../source/typescript/Io/Oidis/Commons/IOApi/Handlers/BaseOutputHandler.js";
import { HTMLElementHandler } from "../../../../../../../../source/typescript/Io/Oidis/Commons/IOApi/Handlers/HTMLElementHandler.js";
import { Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class HTMLElementHandlerTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const htmlElement : HTMLElementHandler = new HTMLElementHandler("Content");
        assert.equal(htmlElement.Name(), "Content");
    }

    @Test()
    public Init() : void {
        (<any>BaseOutputHandler).handlerIterator = 1;
        const htmlElement : HTMLElementHandler = new HTMLElementHandler();
        assert.throws(() : void => {
            htmlElement.Init();
        }, /Element "OutputHandler_2" has not been found./);
    }

    @Test()
    public Print() : void {
        const htmlElement : HTMLElementHandler = new HTMLElementHandler("Content");
        htmlElement.Init();
        htmlElement.Print("printing of element");
        assert.equal(htmlElement.Encoding(), "UTF-8");
        assert.equal(htmlElement.ToString("", false),
            "object type of \'Io.Oidis.Commons.IOApi.Handlers.HTMLElementHandler\'");
    }

    @Test()
    public Clear() : void {
        const htmlElement : HTMLElementHandler = new HTMLElementHandler("Content");
        htmlElement.Clear();
        assert.equal(htmlElement.ToString("", true),
            "object type of \'Io.Oidis.Commons.IOApi.Handlers.HTMLElementHandler\'");
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
