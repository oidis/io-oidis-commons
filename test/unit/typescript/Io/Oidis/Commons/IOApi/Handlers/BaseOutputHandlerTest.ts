/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { NewLineType } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Enums/NewLineType.js";
import { BaseOutputHandler } from "../../../../../../../../source/typescript/Io/Oidis/Commons/IOApi/Handlers/BaseOutputHandler.js";
import { StringUtils } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Utils/StringUtils.js";
import { Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

class MockBaseOutputHandler extends BaseOutputHandler {
    public testNewLineType() : void {
        this.setNewLineType(NewLineType.WINDOWS);
    }
}

class MockBaseHandler extends BaseOutputHandler {
    public handlerSet() : void {
        return this.setHandler((message : string) : void => {
            const mess : string = "Testing of Handler";
        });
    }

    public handlerGet() : any {
        this.setHandler((message : string) : void => {
            const mess : string = "Testing of Handler";
        });
        this.Init();
        this.getHandler();
    }

    public newLinetype() : void {
        return this.setNewLineType(NewLineType.WINDOWS);
    }
}

class MockEmptyHandler extends BaseOutputHandler {
    public handlGet() : void {
        this.getHandler();
    }
}

export class BaseOutputHandlerTest extends UnitTestRunner {
    private baseHandler : BaseOutputHandler;

    @Test()
    public Constructor() : void {
        assert.equal(this.baseHandler.Name(), "baseoutput");
    }

    @Test()
    public Init() : void {
        assert.doesNotThrow(() : void => {
            this.baseHandler.Init();
        });
    }

    @Test()
    public Encoding() : void {
        assert.equal(this.baseHandler.Encoding(), undefined);
        this.baseHandler.Init();
        assert.equal(this.baseHandler.Encoding(), "UTF-8");
    }

    @Test()
    public NewLineType() : void {
        assert.equal(this.baseHandler.NewLineType(), undefined);
    }

    @Test()
    public Clear() : void {
        assert.doesNotThrow(() : void => {
            this.baseHandler.Clear();
        });
        this.baseHandler.Init();
        assert.equal(this.baseHandler.Encoding(), "UTF-8");
        this.baseHandler.setOnPrint((message : string) : void => {
            const mess : string = "message";
        });
        this.baseHandler.Clear();
    }

    @Test()
    public setOnPrint() : void {
        assert.doesNotThrow(() : void => {
            this.baseHandler.setOnPrint(($message : string) : void => {
                assert.equal($message, "test");
            });
        });
    }

    @Test()
    public setOnPrintNullHandler() : void {
        this.baseHandler.setOnPrint(null);
    }

    @Test()
    public Print() : void {
        const exception : RegExp = new RegExp(
            "'Io.Oidis.Commons.IOApi.Handlers.BaseOutputHandler' " +
            "is abstract class and does not provides fully implemented Print method.");
        assert.throws(() : void => {
            this.baseHandler.Print("test");
        }, exception);
    }

    @Test()
    public toString1() : void {
        assert.equal(this.baseHandler.toString(),
            "object type of \'Io.Oidis.Commons.IOApi.Handlers.BaseOutputHandler\'");
    }

    @Test(true)
    public SetHandler() : void {
        const handler : MockBaseHandler = new MockBaseHandler();
        assert.patternEqual(StringUtils.Remove(this.stripInstrumentation(MockBaseHandler.toString()),
            "/* istanbul ignore next */ "), "" +
            StringUtils.Remove("function MockBaseHandler() {*\n*" +
                "return _super !== null && _super.apply(this, arguments) || this;*\n*" +
                "}", "/* istanbul ignore next */ "));
    }

    @Test()
    public setHandler() : void {
        const handler : MockBaseHandler = new MockBaseHandler();
        const testhandler : any = (message : string) : void => {
            const text : string = "Testing BaseHandler";
        };
        assert.equal(handler.handlerSet(), undefined);
    }

    @Test()
    public getHandler() : void {
        const handler : MockBaseHandler = new MockBaseHandler();
        assert.deepEqual(handler.handlerGet(), undefined);
    }

    @Test()
    public newLineType() : void {
        const handler : MockBaseHandler = new MockBaseHandler();
        handler.newLinetype();
    }

    @Test()
    public getHandlerEmpty() : void {
        const handler : MockEmptyHandler = new MockEmptyHandler();
        assert.equal(handler.handlGet(), undefined);
    }

    protected setUp() : void {
        this.baseHandler = new MockBaseOutputHandler("baseoutput");
    }
}
