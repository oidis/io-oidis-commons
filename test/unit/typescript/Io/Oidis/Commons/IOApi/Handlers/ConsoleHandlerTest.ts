/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ConsoleHandler } from "../../../../../../../../source/typescript/Io/Oidis/Commons/IOApi/Handlers/ConsoleHandler.js";
import { Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class ConsoleHandlerTest extends UnitTestRunner {

    @Test()
    public Constructor() : void {
        const console : ConsoleHandler = new ConsoleHandler("handler");
        assert.equal(console.Name(), "handler");
    }

    @Test()
    public Init() : void {
        const consoleHandler : ConsoleHandler = new ConsoleHandler("haha");
        consoleHandler.Init();
    }

    @Test()
    public Print() : void {
        const consoleHandler : ConsoleHandler = new ConsoleHandler("haha");
        consoleHandler.Print("Handling Console");
        assert.deepEqual(consoleHandler.Encoding(), "UTF-8");
    }

    @Test()
    public Clear() : void {
        const console : ConsoleHandler = new ConsoleHandler();
        assert.doesNotThrow(() : void => {
            console.Clear();
        });
    }
}
