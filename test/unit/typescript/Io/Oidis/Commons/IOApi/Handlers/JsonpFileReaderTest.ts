/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestLoader, UnitTestRunner } from "../../UnitTestRunner.js";

import { HttpRequestEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Events/Args/HttpRequestEventArgs.js";
import { ExceptionsManager } from "../../../../../../../../source/typescript/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { JsonpFileReader } from "../../../../../../../../source/typescript/Io/Oidis/Commons/IOApi/Handlers/JsonpFileReader.js";
import { IUnitTestRunnerPromise, Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";

export class JsonpFileReaderTest extends UnitTestRunner {

    @Test(true)
    public Load() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const path : string = "test/resource/data/Io/Oidis/Commons/ValidData.jsonp";
            JsonpFileReader.Load(
                path,
                ($data : any, $filePath? : string) : void => {
                    assert.deepEqual($data,
                        {
                            testKey1: "testValue1",
                            testKey2: true
                        });
                    assert.equal($filePath, path);
                    ExceptionsManager.Clear();
                    $done();
                },
                ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                    assert.equal($filePath, path);
                    assert.ok(false, "ValidData has not been loaded correctly.");
                    ExceptionsManager.Clear();
                    $done();
                });
        };
    }

    @Test(true)
    public LoadSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.setUrl("file:///" + this.getAbsoluteRoot() + "/build/target/index.html");
            UnitTestLoader.Load(<any>{
                build: {time: new Date().toTimeString(), type: "prod"}, name: "com-wui-framework-commons", version: "1.0.0"
            });
            const path : string = "file://test/\"?dummy\"/test.txt";
            JsonpFileReader.Load(
                path, ($data : any, $filePath? : string) : void => {
                    assert.deepEqual($filePath, "file://test/\"?dummy\"/test.txt");
                    ExceptionsManager.Clear();
                    $done();
                },
                ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                    assert.ok($filePath, "file://test/\"?dummy\"/test.txt");
                    $done();
                    ExceptionsManager.Clear();
                });
        };
    }

    @Test(true)
    public LoadThirdHttps() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.setUrl("https://localhost:80/required/path/location");
            const path : string = "https://localhost:80/required/path/location";

            JsonpFileReader.Load(
                path, ($data : any, $filePath? : string) : void => {
                    assert.deepEqual($filePath, "https://localhost:80/required/path/location");
                    ExceptionsManager.Clear();
                    $done();
                },
                ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                    assert.ok($filePath, "https://localhost:8888/required/path/location");
                    ExceptionsManager.Clear();
                    $done();
                });
        };
    }

    @Test(true)
    public LoadWithoutErrorHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            assert.onRedirect(
                () : void => {
                    const path : string = "test/resource/data/Io/Oidis/Commons/ValidData.jsonp";
                    JsonpFileReader.Load(
                        path, ($data : any, $filePath? : string) : void => {
                            assert.deepEqual($filePath, "test/resource/data/Io/Oidis/Commons/ValidData.jsonp");
                        });
                },
                ($eventArgs : HttpRequestEventArgs) : void => {
                    assert.equal($eventArgs.Url(), "//ServerError/Http/NotFound");
                },
                () : void => {
                    this.initSendBox();
                    $done();
                });
        };
    }

    @Test(true)
    public LoadFifthHttp() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.setUrl("http://localhost:8888/required/path/location");
            const path : string = "http://localhost:8888/required/path/location";

            JsonpFileReader.Load(
                path, ($data : any, $filePath? : string) : void => {
                    assert.deepEqual($filePath, "http://localhost:8888/required/path/location");
                    ExceptionsManager.Clear();
                    $done();
                },
                ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                    assert.ok($filePath, "http://localhost:8888/required/path/location");
                    ExceptionsManager.Clear();
                    $done();
                });
        };
    }

    @Test(true)
    public LoadException() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            assert.throws(() : void => {
                JsonpFileReader.Load("", null);
                throw new Error("Not Found File Path.");
            }, /Not Found File Path./);
            $done();
        };
    }

    protected setUp() : void {
        (<any>this.getRequest()).isIdeaHost = true;
    }
}
