/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ScriptHandler } from "../../../../../../../../source/typescript/Io/Oidis/Commons/IOApi/Handlers/ScriptHandler.js";
import { IUnitTestRunnerPromise, Test } from "../../BaseUnitTestRunner.js";
import { assert } from "../../UnitTestEnvironment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class ScriptHandlerTest extends UnitTestRunner {

    @Test(true)
    public testLoad() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const handler : ScriptHandler = new ScriptHandler();
            handler.Path("test/resource/data/Io/Oidis/Commons/ValidData3.jsonp");
            handler.ErrorHandler(() : void => {
                assert.ok(false, "ValidData has not been loaded correctly.");
                $done();
            });
            handler.SuccessHandler(() : void => {
                assert.equal(handler.Data(), "");
                $done();
            });
            handler.Load();
        };
    }
}
