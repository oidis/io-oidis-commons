/* ********************************************************************************************************* *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference path="../../../../reference.d.ts" />
// eslint-disable-next-line @typescript-eslint/no-namespace
namespace Io.Oidis.Commons.RuntimeTests {
    "use strict";

    export class ExecuteRuntimeTest extends SeleniumTestRunner {

        public __IgnoretestDOM() : void {
            this.driver.executeScript(() : string => {
                return Io.Oidis.Commons.Primitives.BaseObject.ClassName();
            }).then(($value : string) : void => {
                assert.equal($value, "Io.Oidis.Commons.Primitives.BaseObject");
            });
        }

        public testHttpRequestParser() : void {
            this.driver.findElement(this.by.linkText("HttpRequestParserTest")).click().then(() : void => {
                this.validate();
            });
        }

        public testHttpManager() : void {
            this.driver.findElement(this.by.linkText("HttpManagerTest")).click().then(() : void => {
                this.validate();
            });
        }

        public testPersistenceApi() : void {
            this.driver.findElement(this.by.linkText("PersistenceApiTest")).click().then(() : void => {
                this.validate();
            });
        }

        protected setUp() : void {
            this.driver.get("file:///" + this.getAbsoluteRoot() + "/build/target/index.html");
        }

        protected after() : void {
            this.driver.quit();
        }

        private validate() : void {
            this.driver.findElement(this.by.className("Result")).getText().then(($value : string) : void => {
                assert.equal($value, "SUCCESS");
            });
        }
    }
}
