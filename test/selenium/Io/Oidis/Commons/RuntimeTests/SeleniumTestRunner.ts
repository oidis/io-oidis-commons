/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference path="../../../../reference.d.ts" />
// eslint-disable-next-line @typescript-eslint/no-namespace
namespace Io.Oidis.Commons {
    "use strict";

    export class SeleniumTestEnvironmentArgs extends EnvironmentArgs {
        public Load($appConfig : any, $handler : () => void) : void {
            super.Load($appConfig, $handler);
        }

        protected getConfigPaths() : string[] {
            return [];
        }
    }

    export class SeleniumTestLoader extends Loader {
        protected initEnvironment() : SeleniumTestEnvironmentArgs {
            return new SeleniumTestEnvironmentArgs();
        }
    }

    export class SeleniumTestRunner extends Io.Oidis.UnitTestRunner {
        protected initLoader() : void {
            super.initLoader(SeleniumTestLoader);
        }
    }
}
