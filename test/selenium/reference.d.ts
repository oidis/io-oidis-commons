/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* tslint:disable:no-reference */
///<reference path='../../bin/resource/scripts/UnitTestEnvironment.d.ts'/>
///<reference path='../../source/typescript/reference.d.ts'/>
///<reference path='../../bin/resource/scripts/UnitTestRunner.ts'/>
///<reference path='Io/Oidis/Commons/RuntimeTests/SeleniumTestRunner.ts'/>
